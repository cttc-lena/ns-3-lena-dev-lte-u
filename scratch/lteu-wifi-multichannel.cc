/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Nicola Baldo <nbaldo@cttc.es>
 */

//
//  This example program can be used to experiment with wireless coexistence
//  in a simple scenario.  The scenario consists of two cells whose radio
//  coverage overlaps but representing, notionally, two operators in the
//  same region whose transmissions may impact mechanisms such as clear
//  channel assessment and adaptive modulation and coding.
//  The technologies used are LTE Licensed Assisted Access (LAA)
//  operating on EARFCN 255444 (5.180 GHz), and Wi-Fi 802.11n
//  operating on channel 36 (5.180 GHz).
//
//  The notional scenario consists of two operators A and B, each
//  operating a single cell, and each cell consisting of a base
//  station (BS) and a user equipment (UE), as represented in the following
//  diagram:
//
//
//    BS A  . .  d2 . .  UE B1 . .  d2 . .  BS B2
//     |                   |
//  d1 |                d1 |
//     |                   |
//    UE A  . .  d2 . .  BS B2 . .  d2 . .  UE B2
//
//
//  cell A              cell B
//
//  where 'd1' and 'd2' distances (in meters) separate the nodes.
//
//  When using LTE, the BS is modeled as an eNB and the UE as a UE.
//  When using Wi-Fi, the BS is modeled as an AP and the UE as a STA.
//
//  In addition, both BS are connected to a "backhaul" client node that
//  originates data transfer in the downlink direction from client to UE(s).
//  The links from client to BS are modeled as well provisioned; low latency
//  links.
//
//  The figure may be redrawn as follows:
//
//     +---------------------------- client
//     |                               |
//    BS A  . .  d2 . .  UE B          |
//     |                   |           |
//  d1 |                d1 |           |
//     |                   |           |
//    UE A  . .  d2 . .  BS B----------+
//
//
//  In general, the program can be configured at run-time by passing
//  command-line arguments.  The command
//  ./waf --run "laa-wifi-coexistence-simple --help"
//  will display all of the available run-time help options, and in
//  particular, the command
//  ./waf --run "laa-wifi-coexistence-simple --PrintGlobals" should
//  display the following:
//
// Global values:
//     --ChecksumEnabled=[false]
//         A global switch to enable all checksums for all protocols
//     --RngRun=[1]
//         The run number used to modify the global seed
//     --RngSeed=[1]
//         The global seed of all rng streams
//     --SchedulerType=[ns3::MapScheduler]
//         The object class to use as the scheduler implementation
//     --SimulatorImplementationType=[ns3::DefaultSimulatorImpl]
//         The object class to use as the simulator implementation
//     --cellConfigA=[Lte]
//         Lte or Wifi
//     --cellConfigB=[Wifi]
//         Lte or Wifi
//     --d1=[10]
//         intra-cell separation (e.g. AP to STA)
//     --d2=[50]
//         inter-cell separation
//     --duration=[1]
//         Data transfer duration (seconds)
//     --pcapEnabled=[false]
//         Whether to enable pcap traces for Wi-Fi
//     --transport=[Udp]
//         Whether to use Tcp or Udp
//
//  The bottom seven are specific to this example.  In particular,
//  passing the argument '--cellConfigA=Wifi' will cause both
//  cells to use Wifi but on a different SSID, while the (default)
//  will cell A to use LTE and cellB to use Wi-Fi.  Passing an argument
//  of '--cellConfigB=Lte" will cause both cells to use LTE.
//
//  In addition, some other variables may be modified at compile-time
//  such as simulation run-time.  For simplification, an IEEE 802.11n
//  configuration is used with an 'Ideal' rate control.
//
//  We refer to the left-most cell as 'cell A' and the right-most
//  cell as 'cell B'.  Both cells are configured to run UDP transfers from
//  client to BS.  The application data rate is 20 Mb/s, which saturates
//  the Wi-Fi link but can be handled by the LTE link.
//
// In addition, the program outputs various statistics files from the
// available LTE and Wi-Fi traces, including, for Wi-Fi, some statistics
// patterned after the 'athstats' tool, and for LTE, the stats
// described in the LTE Module documentation, which can be found at
// https://www.nsnam.org/docs/models/html/lte-user.html#simulation-output
//
// These files are named:
//  athstats-ap_002_000
//  athstats-sta_003_000
//  DlMacStats.txt
//  DlPdcpStats.txt
//  DlRlcStats.txt
//  DlRsrpSinrStats.txt
//  DlRxPhyStats.txt
//  DlTxPhyStats.txt
//  UlInterferenceStats.txt
//  UlRxPhyStats.txt
//  UlSinrStats.txt
//  UlTxPhyStats.txt
//  etc.

#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/internet-module.h>
#include <ns3/point-to-point-module.h>
#include <ns3/lte-module.h>
#include <ns3/wifi-module.h>
#include <ns3/config-store-module.h>
#include <ns3/spectrum-module.h>
#include <ns3/applications-module.h>
#include <ns3/flow-monitor-module.h>
#include <ns3/propagation-module.h>
#include <ns3/scenario-helper.h>
#include <ns3/lte-u-wifi-coexistence-helper.h>

#ifndef UINT32_MAX
#define UINT32_MAX 4294967295U
#endif

using namespace ns3;

// =============== Globals in ScenarioHelpers ===============

static ns3::GlobalValue g_wifiStandard ("wifiStandard",
                                        "802.11a, basic 802.11n w/o aggregation and block acks, full 802.11n",
                                        ns3::EnumValue (St80211nFull),
                                        ns3::MakeEnumChecker (St80211n, "80211n",
                                                              St80211a, "80211a",
                                                              St80211nFull, "80211nFull"));


static ns3::GlobalValue g_serverStartTimeSeconds ("serverStartTimeSeconds",
                                                  "Server start time (seconds)",
                                                  ns3::DoubleValue (2),
                                                  ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_clientStartTimeSeconds ("clientStartTimeSeconds",
                                                  "Client start time (seconds)",
                                                  ns3::DoubleValue (2),
                                                  ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_serverLingerTimeSeconds ("serverLingerTimeSeconds",
                                                   "Server linger time (seconds)",
                                                   ns3::DoubleValue (1),
                                                   ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_simulationLingerTimeSeconds ("simulationLingerTimeSeconds",
                                                       "Simulation linger time (seconds)",
                                                       ns3::DoubleValue (1),
                                                       ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_remDir ("remDir",
                                  "directory where to save REM-related output files",
                                  ns3::StringValue ("./"),
                                  ns3::MakeStringChecker ());

static ns3::GlobalValue g_laaEdThreshold ("lteUEdThreshold",
                                   "CCA-ED threshold for channel access manager (dBm)",
                                   ns3::DoubleValue (-62.0),
                                   ns3::MakeDoubleChecker<double> (-100.0, -50.0));

static ns3::GlobalValue g_wifiEdThreshold ("wifiEdThreshold",
                                   "CCA-ED threshold for wifi devices (dBm)",
                                   ns3::DoubleValue (-62.0),
                                   ns3::MakeDoubleChecker<double> (-100.0, -50.0));

static ns3::GlobalValue g_monitorMode ("lteUMonitorMode",
                                       "monitor mode that will be used by lte-u nodes",
                                       ns3::EnumValue (LteUWifiCoexistenceHelper::REGULAR_WIFI_MONITOR),
                                       ns3::MakeEnumChecker(LteUWifiCoexistenceHelper::ENERGY_DETECTION_ONLY, "energy_detection_only",
                                                            LteUWifiCoexistenceHelper::REGULAR_WIFI_MONITOR, "regular_wifi"));

static ns3::GlobalValue g_lteUChannelAccessManagerInstallTime ("lteUChannelAccessManagerInstallTime",
                                                              "LTE channel access manager install time (seconds)",
                                                              ns3::DoubleValue (2),
                                                              ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_logWifiRetries ("logWifiRetries",
                                              "Log when a data packet requires a retry",
                                              ns3::BooleanValue (false),
                                              ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_logWifiFailRetries ("logWifiFailRetries",
                                              "Log when a data packet fails to be retransmitted successfully and is discarded",
                                              ns3::BooleanValue (false),
                                              ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_logPhyNodeId ("logPhyNodeId",
                                        "Node index of specific node to filter logPhyArrivals (default will log all nodes)",
                                        ns3::UintegerValue (UINT32_MAX),
                                        ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_logTxopNodeId ("logTxopNodeId",
                                        "Node index of specific node to filter logTxops (default will log all nodes)",
                                        ns3::UintegerValue (UINT32_MAX),
                                        ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_logCsatDutyCycleNodeId ("logCsatDutyCycleNodeId",
                                        "Node index of specific node to filter logs for duty cycle CSAT (default will log all nodes)",
                                        ns3::UintegerValue (UINT32_MAX),
                                        ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_logPhyArrivals ("logPhyArrivals",
                                          "Whether to write logfile of signal arrivals to SpectrumWifiPhy",
                                          ns3::BooleanValue (false),
                                          ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_logTxops ("logTxops",
                                          "Whether to write logfile of txop opportunities of eNb",
                                          ns3::BooleanValue (false),
                                          ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_logCsatDutyCycle ("logCsatDutyCycle",
                                            "Whether to write logfile of csat duty cycle traces of eNb",
                                             ns3::BooleanValue (false),
                                             ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_logDataTx ("logDataTx",
                                          "Whether to write logfile when data is transmitted at eNb specified by logCsatDutyCycleNodeId parameter",
                                          ns3::BooleanValue (false),
                                          ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_logCwChanges ("logCwChanges",
                                        "Whether to write logfile of contention window changes",
                                        ns3::BooleanValue (false),
                                        ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_logCwNodeId ("logCwNodeId",
                                        "Node index of specific node to filter logCwChanges (default will log all nodes)",
                                        ns3::UintegerValue (UINT32_MAX),
                                        ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_logBackoffChanges ("logBackoffChanges",
                                             "Whether to write logfile of backoff values drawn",
                                             ns3::BooleanValue (false),
                                             ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_logBackoffNodeId ("logBackoffNodeId",
                                            "Node index of specific node to filter logBackoffChanges (default will log all nodes)",
                                            ns3::UintegerValue (UINT32_MAX),
                                            ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_logBeaconNodeId ("logBeaconNodeId",
                                        "Node index of specific node to filter logBeaconArrivals (default will log all nodes)",
                                        ns3::UintegerValue (UINT32_MAX),
                                        ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_logBeaconArrivals ("logBeaconArrivals",
                                          "Whether to write logfile of beacon arrivals to STA devices",
                                          ns3::BooleanValue (false),
                                          ns3::MakeBooleanChecker ());



// 75 Mb/s will saturate LAA and WiFi SISO 20 MHz
static const uint64_t UDP_SATURATION_RATE = 75000000;

static ns3::GlobalValue g_udpRate ("udpRate",
                                   "Data rate of UDP application",
                                   ns3::DataRateValue (DataRate (UDP_SATURATION_RATE)),
                                   ns3::MakeDataRateChecker ());

static ns3::GlobalValue g_udpSize ("udpPacketSize",
                                   "Packet size of UDP application",
                                   ns3::UintegerValue (1000),
                                   ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_ftpFileSize ("ftpFileSize",
                                       "File size of the ftp file transfer application (bytes)",
                                       ns3::UintegerValue (512000),
                                       ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_spreadUdpLoad ("spreadUdpLoad",
                                         "optimization to try to spread a saturating UDP load across multiple UE and cells",
                                         ns3::BooleanValue (false),
                                         ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_wifiMacQueueMaxDelay ("wifiQueueMaxDelay",
                                         "change from default 500 ms to change the maximum queue dwell time for Wifi packets",
                                         ns3::UintegerValue (500),
                                         ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_wifiMacQueueMaxSize ("wifiQueueMaxSize",
                                         "change from default 400 packets to change the queue size for Wifi packets",
                                         ns3::UintegerValue (400),
                                         ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_tcpRlcMode ("tcpRlcMode",
                                        "RLC_AM, RLC_UM",
                                        ns3::EnumValue (LteEnbRrc::RLC_AM_ALWAYS),
                                        ns3::MakeEnumChecker (LteEnbRrc::RLC_AM_ALWAYS, "RlcAm",
                                                              LteEnbRrc::RLC_UM_ALWAYS, "RlcUm"));

static ns3::GlobalValue g_csatCycleDuration ("csatCycleDuration",
                                        "Duration of LTE-U CSAT cycle in number of subframes.",
                                        ns3::UintegerValue (160),
                                        ns3::MakeUintegerChecker<uint32_t> (40, MAX_CSAT_DURATION));
                                        
static ns3::GlobalValue g_csatDutyCycle ("csatDutyCycle",
                                        "Portion of ON time in CsatCycle. If csat is enabled this value is only used as inital value",
                                        ns3::DoubleValue (0.5),
                                        ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_puncturing ("puncturing",
                                        "CSAT parameter that defines the maximum number of consecutive ON subframes during the ON period.",
                                        ns3::UintegerValue (20),
                                        ns3::MakeUintegerChecker<uint32_t> (1, 20));

static ns3::GlobalValue g_puncturingDuration ("puncturingDuration",
                                        "CSAT parameter that defines the duration of puncturing in the number of subframes.",
                                        ns3::UintegerValue (1),
                                        ns3::MakeUintegerChecker<uint32_t> (1, 5));

static ns3::GlobalValue g_maxInterMibTime ("maxInterMibTime",
                                        "CSAT parameter that defines the maximum amount of time that can pass between two consecutive MIBs.",
                                        ns3::UintegerValue (160),
                                        ns3::MakeUintegerChecker<uint32_t> (10, 160));

static ns3::GlobalValue g_csatEnabled ("csatEnabled",
                                       "If true enable CSAT algorithm (duty cycle is update dynamically during simulation), "
                                       "If false use constant duty cycle during simulation.",
                                       ns3::BooleanValue (true),
                                       ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_tOnMinInMilliSec ("tOnMinInMilliSec",
                                        "CSAT parameter that defines the minimum total ON time during one CSAT cycle in number of subframes.",
                                        ns3::UintegerValue (120),
                                        ns3::MakeUintegerChecker<uint32_t> (4, MAX_CSAT_DURATION ));

static ns3::GlobalValue g_minOffTime ("minOffTime",
                                        "CSAT parameter that defines the minimum total off time during one CSAT cycle in number of subframes.",
                                        ns3::UintegerValue (20),
                                        ns3::MakeUintegerChecker<uint32_t> (0, 40));

static ns3::GlobalValue g_monitoringTime ("monitoringTime",
                                        "CSAT parameter that defines the The monitoring time in number of subframes.",
                                        ns3::UintegerValue (20),
                                        ns3::MakeUintegerChecker<uint32_t> (10, 40));

static ns3::GlobalValue g_alphaMU ("alphaMU",
                                   "A value used in CSAT algorithm for MU calculation.",
                                   ns3::DoubleValue (0.8),
                                   ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_thresholdMuLow ("thresholdMuLow",
                                          "The lower MU threshold value used in CSAT algorithm for duty cycle update as lower MU threshold.",
                                          ns3::DoubleValue (0.4),
                                          ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_thresholdMuHigh ("thresholdMuHigh",
                                           "The upper MU threshold value used in CSAT algorithm for the duty cycle update.",
                                           ns3::DoubleValue (0.6),
                                           ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_deltaTUp ("deltaTUp",
                                    "The delta value used in CSAT algorithm when duty cycle is being increased",
                                    ns3::DoubleValue (0.2),
                                     ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_deltaTDown ("deltaTDown",
                                      "The delta value used in CSAT algorithm when the duty cycle is being reduced.",
                                      ns3::DoubleValue (0.3),
                                      ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_rlcReportBufferStatusTimer ("rlcAmRbsTimer",
                                                      "Set value of ReportBufferStatusTimer attribute of RlcAm.",
                                                      ns3::UintegerValue (20),
                                                      ns3::MakeUintegerChecker<uint32_t> ());

static ns3::GlobalValue g_pdcchDuration ("pdcchDuration",
                                                      "Set value PDCCH duration in number of symbols.",
                                                      ns3::UintegerValue (3),
                                                      ns3::MakeUintegerChecker<uint32_t> (1,3));

static ns3::GlobalValue g_ldsPeriod ("ldsPeriod",
                                      "LDS interval, time between two consecutive LDS.",
                                      ns3::UintegerValue (80),
                                      ns3::MakeUintegerChecker<uint32_t> (40, 160));

static ns3::GlobalValue g_assignDutyCycle ("assignDutyCycle",
                                           "Manually assign duty cycle when configuring LTE-U. If CSAT is enabled. The setting become initial duty cycle",
                                           ns3::BooleanValue (false),
                                           ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_assignTOnMinInMilliSec ("assignTOnMinInMilliSec",
                                                  "A flag to manually assign tOnMinInMilliSec (max value tOnMin can take) when configuring LTE-U ",
                                                  ns3::BooleanValue (false),
                                                  ns3::MakeBooleanChecker ());

// =============== End of Globals in ScenarioHelpers ===============

// 
// Global Values are used in place of command line arguments so that these
// values may be managed in the ns-3 ConfigStore system.
//
      
static ns3::GlobalValue g_d1 ("d1",
                              "intra-cell separation (e.g. AP to STA)",
                              ns3::DoubleValue (10),
                              ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_d2 ("d2",
                              "inter-cell separation",
                              ns3::DoubleValue (50),
                              ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_duration ("duration",
                                    "Data transfer duration (seconds)",
                                    ns3::DoubleValue (1),
                                    ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_cellConfigA ("cellConfigA",
                                       "Lte or Wifi",
                                        ns3::EnumValue (LTE_U),
                                        ns3::MakeEnumChecker (WIFI, "Wifi",
                                                              LTE, "Lte",
                                                              LTE_U,"Lte_U"));
static ns3::GlobalValue g_cellConfigB ("cellConfigB",
                                       "Lte or Wifi",
                                        ns3::EnumValue (LTE_U),
                                        ns3::MakeEnumChecker (WIFI, "Wifi",
                                                              LTE, "Lte",
                                                              LTE_U,"Lte_U"));

static ns3::GlobalValue g_pcap ("pcapEnabled",
                                "Whether to enable pcap trace files for Wi-Fi",
                                ns3::BooleanValue (false),
                                ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_transport ("transport",
                                     "whether to use Udp or Tcp",
                                     ns3::EnumValue (UDP),
                                     ns3::MakeEnumChecker (UDP, "Udp",
                                                           TCP, "Tcp",
                                                           FTP, "Ftp"));
/*
static ns3::GlobalValue g_csatDutyCycle ("csatDutyCycle",
                                    "Duty cycle value to be used for LTE",
                                    ns3::DoubleValue (1),
                                    ns3::MakeDoubleChecker<double> (0.0, 1.0));
*/
static ns3::GlobalValue g_generateRem ("generateRem",
                                       "if true, will generate a REM and then abort the simulation;"
                                       "if false, will run the simulation normally (without generating any REM)",
                                       ns3::BooleanValue (false),
                                       ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_simTag ("simTag",
                                  "tag to be appended to output filenames to distinguish simulation campaigns",
                                  ns3::StringValue ("default"),
                                  ns3::MakeStringChecker ());

static ns3::GlobalValue g_outputDir ("outputDir",
                                     "directory where to store simulation results",
                                     ns3::StringValue ("./"),
                                     ns3::MakeStringChecker ());

// Higher lambda means faster arrival rate; values [0.5, 1, 1.5, 2, 2.5]
// recommended
static ns3::GlobalValue g_ftpLambda ("ftpLambda",
                                    "Lambda value for FTP model 1 application",
                                    ns3::DoubleValue (2.5),
                                    ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_voiceEnabled ("voiceEnabled",
                                       "Whether to enable voice",
                                       ns3::BooleanValue (false),
                                       ns3::MakeBooleanChecker ());

// ======== New globals specific to channel selection example
static ns3::GlobalValue g_chsType ("chsType",
                                   "Type of channel selection algorithm",
                                   ns3::StringValue ("Centralized"),
                                   ns3::MakeStringChecker ());

static ns3::GlobalValue g_numCellPerRow ("numCellPerRow",
                                         "Number of cell per row in the topology",
                                         ns3::UintegerValue (5),
                                         ns3::MakeUintegerChecker<uint32_t> (1, 10));

static ns3::GlobalValue g_numUePerCell ("numUePerCell",
                                        "Number of UE/STA per cell",
                                        ns3::UintegerValue (1),
                                        ns3::MakeUintegerChecker<uint32_t> (1, 20));

static ns3::GlobalValue g_fadingSigma("fadingSigma",
                                        "The vairance of the indoor wireless fading model",
                                        ns3::DoubleValue(1),
                                        ns3::MakeDoubleChecker<double> (0, 20));



// Global variables for use in callbacks.
double g_signalDbmAvg[2];
double g_noiseDbmAvg[2];
uint32_t g_samples[2];
uint16_t g_channelNumber[2];
uint32_t g_rate[2];

/*static const struct Wifi5GHzChannel
{
  uint32_t m_wifiChannelNumber;
  uint32_t m_dlEarfcn;
  uint32_t m_dlBandwidthInNoOfRbs;
} g_wifi5GHzChannels[] = {
  { 32, 255244, 100},
  { 36, 255444, 100},
  { 40, 255644, 100},
  { 44, 255844, 100},
  { 48, 256044, 100},
  { 149, 261094, 100},
  { 153, 261294, 100},
  { 157, 261494, 100},
  { 161, 261694, 100},
  { 165, 261894, 100}
};*/



#define NUM_5GHZ_WIFI_CHANNELS (sizeof (g_wifi5GHzChannels) / sizeof (Wifi5GHzChannel))


std::map< uint32_t, Ptr<RadioEnvironmentMapHelper> > g_remHelperList;

std::map< uint32_t, Wifi5GHzChannel > g_cellIdToWifiChannel;

uint32_t g_remHelperNotificationsCounter = 0;


Wifi5GHzChannel GetChannelInfo (uint32_t channel)
{

  Wifi5GHzChannel chInfo;

  NS_ASSERT_MSG( channel==32 || channel==36 || channel==40 || channel==44 || 
                 channel==48 || channel==149 || channel==153 || channel==157 || 
                 channel==161 || channel==165, 
                 "Channel :"<<channel<<" not supported");

  for (uint32_t i = 0; i < NUM_5GHZ_WIFI_CHANNELS; i++)
    {
      if (g_wifi5GHzChannels[i].m_wifiChannelNumber == channel)
        {
          chInfo = g_wifi5GHzChannels[i];
          return chInfo;
        }
     }
  return chInfo;
}


NS_LOG_COMPONENT_DEFINE ("LteUMultiChannel");



int
main (int argc, char *argv[])
{
  
  LogComponentEnable ("LteUMultiChannel", LOG_LEVEL_DEBUG);
  LogComponentEnable ("ScenarioHelper", LOG_LEVEL_DEBUG);
  //LogComponentEnable ("UdpClient", LOG_LEVEL_INFO);
  //LogComponentEnable ("UdpServer", LOG_LEVEL_INFO);
  
  CommandLine cmd;
  cmd.Parse (argc, argv);
  ConfigStore inputConfig;
  inputConfig.ConfigureDefaults ();
  // parse again so you can override input file default values via command line
  cmd.Parse (argc, argv);

  DoubleValue doubleValue;
  EnumValue enumValue;
  BooleanValue booleanValue;
  StringValue stringValue;
  UintegerValue uintegerValue;

  GlobalValue::GetValueByName ("d1", doubleValue);
  double d1 = doubleValue.Get ();
  GlobalValue::GetValueByName ("d2", doubleValue);
  double d2 = doubleValue.Get ();
  GlobalValue::GetValueByName ("cellConfigA", enumValue);
  enum Config_e cellConfigA = (Config_e) enumValue.Get ();
  GlobalValue::GetValueByName ("cellConfigB", enumValue);
  enum Config_e cellConfigB = (Config_e) enumValue.Get ();
  GlobalValue::GetValueByName ("duration", doubleValue);
  double duration = doubleValue.Get ();
  GlobalValue::GetValueByName ("transport", enumValue);
  enum Transport_e transport = (Transport_e) enumValue.Get ();
  GlobalValue::GetValueByName ("csatDutyCycle", doubleValue);
  double csatDutyCycle = doubleValue.Get ();
  //GlobalValue::GetValueByName ("generateRem", booleanValue);
  //bool generateRem = booleanValue.Get ();
  GlobalValue::GetValueByName ("simTag", stringValue);
  std::string simTag = stringValue.Get ();
  GlobalValue::GetValueByName ("outputDir", stringValue);
  std::string outputDir = stringValue.Get ();
  
  std::string outFileName = outputDir + "/lteu_multichannel_" + simTag;

  // Create nodes and containers
  NodeContainer bsNodesA, bsNodesB;  // for APs and eNBs
  NodeContainer ueNodesA, ueNodesB;  // for STAs and UEs
  NodeContainer allWirelessNodes;  // container to hold all wireless nodes
  GlobalValue::GetValueByName ("numCellPerRow", uintegerValue);
  uint32_t numCellPerRow = uintegerValue.Get ();
  GlobalValue::GetValueByName ("numUePerCell", uintegerValue);
  uint32_t numUePerCell = uintegerValue.Get ();
  
  // Each network A and B gets one type of node each
  bsNodesA.Create (numCellPerRow*numCellPerRow);
  bsNodesB.Create (1);
  ueNodesA.Create (numCellPerRow*numCellPerRow*numUePerCell);
  ueNodesB.Create (1);
  
  //
  // Allocate the locations of the deployment
  //
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  double initX = 50;
  double initY = 50;
  double initZ = 2;
  double interCellDist = 30; // Similar to 36.889 indoor scenario's intercell distance
  double interRowDist = interCellDist*std::sqrt(3);
  Vector initPos(initX, initY, initZ);
  // Placing all eNBs in a hexagonical topology
  for (uint32_t i = 0; i < numCellPerRow; i ++)
    {
      for (uint32_t j = 0; j < numCellPerRow; j++)
        {
          positionAlloc->Add 
            (
              Vector 
                (
                  initX + j*interCellDist + (j%2)*interCellDist/2, 
                  initY + i*interRowDist, 
                  2 // cells are two meters high
                )
            );
        }
    }
  
  //bsNodeB at same location as the first bsNodeA
  positionAlloc->Add (Vector (initX, initY, initZ));
  
  // Now randomly allocate ueNodesA. Currently doing a deterministic drop
  // Also setup UE node ID to eNB node ID map
  for (uint32_t i = 0; i < numCellPerRow; i ++)
    {
      for (uint32_t j = 0; j < numCellPerRow; j++)
        {
          for (uint32_t k = 0; k < numUePerCell; k++)
            {
              positionAlloc->Add 
                (
                  Vector 
                    (
                      initX + j*interCellDist 
                      + (k%2)*interCellDist/2 // one cell center, one cell edge
                      + 1 - 2*(k%2) , //  Cell edge shifted 1m toward cell loc
                      initY + i*interRowDist, 
                      1.5 // UEs are 1.5 meters high
                    )
                );
            }
        }
    }
  
  //ueNodeB is right under bsNodeB
  positionAlloc->Add (Vector (initX, initY, initZ - 1));
  
  
  
  allWirelessNodes = NodeContainer (bsNodesA, bsNodesB, ueNodesA, ueNodesB);
  //allWirelessNodes = NodeContainer (bsNodesA, ueNodesA);
  
  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetPositionAllocator (positionAlloc);
  mobility.Install (allWirelessNodes);

  Time durationTime = Seconds (duration);
  
  //
  // Assigning Channel for operator A and operator B
  //
  
  GlobalValue::GetValueByName ("chsType", stringValue);
  std::string chsType = stringValue.Get ();
  std::map <uint32_t, Wifi5GHzChannel > wifiNodeIdToBandInfo;
  Wifi5GHzChannel Ch0 = GetChannelInfo (32);
  Wifi5GHzChannel Ch1 = GetChannelInfo (36);
  Wifi5GHzChannel Ch2 = GetChannelInfo (40);
  Wifi5GHzChannel Ch3 = GetChannelInfo (44);
  std::vector <uint32_t> chlAllocation;
  uint32_t bestAssignment[25] = {3,2,1,3,2,1,3,2,1,3,2,2,1,3,2,1,3,2,1,3,3,2,1,3,2};
  uint32_t worstAssignment[25] = {1,1,3,3,2,2,2,1,1,2,1,3,3,2,3,1,2,1,3,1,3,3,1,2,2};
  std::map <uint32_t, Wifi5GHzChannel > lteUNodeIdToBandInfo;
  
  if (chsType == "Centralized")
    {
      NS_LOG_DEBUG ("Using centralized channel selection");
      for (uint32_t i = 0; i < 25; i++)
        {
          chlAllocation.push_back (bestAssignment[i]);
        }
    }
  else 
    {
      NS_LOG_DEBUG ("Using distributed channel selection");
      for (uint32_t i = 0; i < 25; i++)
        {
          chlAllocation.push_back (worstAssignment[i]);
        }
       
    }
  for (uint32_t i = 0; i < numCellPerRow*numCellPerRow; i ++)
    {
      if (chlAllocation[i] == 1)
        {
          lteUNodeIdToBandInfo[i] = Ch1;
        }
      else if (chlAllocation[i] == 2)
        {
          lteUNodeIdToBandInfo[i] = Ch2;
        }
      else if (chlAllocation[i] == 3)
        {
          lteUNodeIdToBandInfo[i] = Ch3;
        }
      else 
        {
          lteUNodeIdToBandInfo[i] = Ch0;
        }
    }
  uint32_t bsNodesBChl = 32;

  //std::map < uint32_t, InitialDensitiesPerChannel > initialNodeDensitiesMap;
  std::vector <Wifi5GHzChannel> channelsInUse;

  
  std::cout<<"Total number of nodes in operator A is:"<<bsNodesA.GetN()
      <<" , and operator B is:"<<bsNodesB.GetN()<<std::endl;


  // TODO: output the UE and BS location csv files just as lena dual stripe script
  // REM settings tuned to get a nice figure for this specific scenario
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::OutputFile", 
                      StringValue ("lte-u-multichannel.rem"));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::XMin", DoubleValue (0));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::XMax", DoubleValue (300));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::YMin", DoubleValue (0));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::YMax", DoubleValue (300));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::XRes", UintegerValue (600));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::YRes", UintegerValue (600));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::Z", DoubleValue (1.5));

  // we want deterministic behavior in this simple scenario, so we disable shadowing
  // TYC: set a global parameter to adjust the variance
  GlobalValue::GetValueByName("fadingSigma", doubleValue);
  NS_LOG_DEBUG ("Setting the fading sigma to be " << doubleValue.Get ());
  Config::SetDefault ("ns3::Ieee80211axIndoorPropagationLossModel::Sigma", 
                      doubleValue);
  

  
  // Specify some physical layer parameters that will be used below and
  // in the scenario helper.

  PhyParams phyParams;
  phyParams.m_bsTxGain = 0; // dB antenna gain
  phyParams.m_bsRxGain = 0; // dB antenna gain
  phyParams.m_bsTxPower = 18; // dBm
  phyParams.m_bsNoiseFigure = 5; // dB
  phyParams.m_ueTxGain = 0; // dB antenna gain
  phyParams.m_ueRxGain = 0; // dB antenna gain
  phyParams.m_ueTxPower = 18; // dBm
  phyParams.m_ueNoiseFigure = 9; // dB
  phyParams.m_channelNumber = 36;
  phyParams.m_receivers = 2;
  phyParams.m_transmitters = 2;


  // calculate rx power corresponding to d2 for logging purposes
  // note: a separate propagation loss model instance is used, make
  // sure the settings are consistent with the ones used for the
  // simulation
  const uint32_t earfcn = 255444;
  double dlFreq = LteSpectrumValueHelper::GetCarrierFrequency (earfcn);
  Ptr<PropagationLossModel> plm = CreateObject<Ieee80211axIndoorPropagationLossModel> ();
  plm->SetAttribute ("Frequency", DoubleValue (dlFreq));

  double txPowerFactors = phyParams.m_bsTxGain + phyParams.m_ueRxGain +
                          phyParams.m_bsTxPower;
  double rxPowerDbmD1 = plm->CalcRxPower (txPowerFactors,
                                          bsNodesA.Get (0)->GetObject<MobilityModel> (),
                                          ueNodesA.Get (0)->GetObject<MobilityModel> ());
  double rxPowerDbmD2 = plm->CalcRxPower (txPowerFactors,
                                          bsNodesB.Get (0)->GetObject<MobilityModel> (),
                                          ueNodesB.Get (0)->GetObject<MobilityModel> ());


  std::ostringstream simulationParams;
  simulationParams << d1 << " " << d2 << " "
                   << rxPowerDbmD1 << " "
                   << rxPowerDbmD2 << " "
                   << csatDutyCycle << " ";
  // Enable Coexistence manager logging
  
  NS_LOG_DEBUG ("Running simulation with simulationParams: " 
                << simulationParams.str () << std::endl);
  
  
  
  // ====================== Code from ConfigureAndRunScenario ==================
  //DoubleValue doubleValue;
  //BooleanValue booleanValue;
  //StringValue stringValue;
  //UintegerValue uintegerValue;
  // Configure times that applications will run, based on 'duration' above
  // First, let the scenario initialize for some seconds
  GlobalValue::GetValueByName ("serverStartTimeSeconds", doubleValue);
  Time serverStartTime = Seconds (doubleValue.Get ());
  GlobalValue::GetValueByName ("clientStartTimeSeconds", doubleValue);
  Time clientStartTime = Seconds (doubleValue.Get ());
  // run servers a bit longer than clients
  GlobalValue::GetValueByName ("serverLingerTimeSeconds", doubleValue);
  Time serverLingerTime = Seconds (doubleValue.Get ());
  // run simulation a bit longer than data transfers
  GlobalValue::GetValueByName ("simulationLingerTimeSeconds", doubleValue);
  Time simulationLingerTime = Seconds (doubleValue.Get ());

  // In order to be sure that we have received HARQ feedback information from all users acks or nacsk, as a temporal solution,
  // we are disabling error model for ctrl signals in order to ensure that UE is aware of transmissions from eNB
  // so it will always send to eNB harq feedback. This enables us to more precisely update CW in LbtAccessManager (update is based on harq feedback).
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (true));

  GlobalValue::GetValueByName ("ldsPeriod", uintegerValue);
  Config::SetDefault ("ns3::LteEnbPhy::LdsPeriod", uintegerValue);

  GlobalValue::GetValueByName("rlcAmRbsTimer", uintegerValue);
  Config::SetDefault ("ns3::LteRlcAm::ReportBufferStatusTimer", TimeValue (MilliSeconds(uintegerValue.Get ())));

  GlobalValue::GetValueByName("pdcchDuration", uintegerValue);
  Config::SetDefault ("ns3::LteSpectrumPhy::DlCtrlDuration", uintegerValue);
  // We need to do more tests with different value for LteRlcAm buffer status timer.
  // We have noticed that in general can imrove performance.
  //Config::SetDefault ("ns3::LteRlcAm::ReportBufferStatusTimer", TimeValue (MilliSeconds(1)));

  // VoiceApplication defaults
  Config::SetDefault ("ns3::VoiceApplication::Protocol", TypeIdValue (UdpSocketFactory::GetTypeId ()));
  Config::SetDefault ("ns3::VoiceApplication::Interval", TimeValue (MilliSeconds (10)));
  Config::SetDefault ("ns3::Queue::MaxPackets", UintegerValue (10000));
  
  
  if (cellConfigA==LTE_U || cellConfigB==LTE_U)
    {
      GlobalValue::GetValueByName ("csatCycleDuration", uintegerValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::CsatCycleDuration", uintegerValue);
      // set csatDutyCycle
      GlobalValue::GetValueByName ("csatDutyCycle", doubleValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::CsatDutyCycle", doubleValue);
      // set puncturing
      GlobalValue::GetValueByName ("puncturing", uintegerValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::Puncturing", uintegerValue);
      // set puncturingDuration
      GlobalValue::GetValueByName ("puncturingDuration", uintegerValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::PuncturingDuration", uintegerValue);
      // set maxInterMibTime
      GlobalValue::GetValueByName ("maxInterMibTime", uintegerValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::MaxInterMibTime", uintegerValue);
      //set csatEnabled
      GlobalValue::GetValueByName ("csatEnabled", booleanValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::CsatEnabled", booleanValue);
      // set tOnMinInMilliSec
      GlobalValue::GetValueByName ("tOnMinInMilliSec", uintegerValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::TOnMinInMilliSec", uintegerValue);
      // set minOffTime
      GlobalValue::GetValueByName ("minOffTime", uintegerValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::MinOffTime", uintegerValue);
      // set monitoringTime
      GlobalValue::GetValueByName ("monitoringTime", uintegerValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::MonitoringTime", uintegerValue);
      // set alphaMU
      GlobalValue::GetValueByName ("alphaMU", doubleValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::AlphaMU", doubleValue);
      // set thresholdMuLow
      GlobalValue::GetValueByName ("thresholdMuLow", doubleValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::ThresholdMuLow", doubleValue);
      // set thresholdMuHigh
      GlobalValue::GetValueByName ("thresholdMuHigh", doubleValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::ThresholdMuHigh", doubleValue);
      // set deltaUp
      GlobalValue::GetValueByName ("deltaTUp", doubleValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::DeltaTUp", doubleValue);
      // set deltaTDown
      GlobalValue::GetValueByName ("deltaTDown", doubleValue);
      Config::SetDefault ("ns3::LteUCoexistenceManager::DeltaTDown", doubleValue);
    }

  // Now set start and stop times derived from the above
  Time serverStopTime = serverStartTime + durationTime + serverLingerTime;
  Time clientStopTime = clientStartTime + durationTime;
  // Overall simulation stop time is below
  Time stopTime = serverStopTime + simulationLingerTime;
  
  DataRateValue dataRateValue;
  GlobalValue::GetValueByName ("udpRate", dataRateValue);
  
  GlobalValue::GetValueByName ("udpPacketSize", uintegerValue);
  uint64_t bitRate = dataRateValue.Get().GetBitRate ();
  uint32_t packetSize = uintegerValue.Get (); // bytes
  double interval = static_cast<double> (packetSize * 8) / bitRate;
  Time udpInterval;
  // if bitRate < UDP_SATURATION_RATE, use the calculated interval 
  // if bitRate >= UDP_SATURATION_RATE, and the spreadUdpLoad optimization is
  // enabled,  spread the offered load across BS/UE in the cell
  if (transport == UDP)
    {
      GlobalValue::GetValueByName ("spreadUdpLoad", booleanValue);
      if (bitRate < UDP_SATURATION_RATE || booleanValue.Get () == false)
        {
          udpInterval = Seconds (interval);
        }
      else
        {
          NS_LOG_DEBUG ("Applying spreadUdpLoad optimization for factor " 
              << ueNodesA.GetN () / static_cast<double> (bsNodesA.GetN ()));
          udpInterval = Seconds ((interval * ueNodesA.GetN ()) 
                        / bsNodesA.GetN ());
        }
      NS_LOG_DEBUG ("UDP will use application interval " 
                    << udpInterval.GetSeconds () << " sec");
   }
  

  std::cout << "Running simulation for " << durationTime.GetSeconds () 
            << " sec of data transfer; " << stopTime.GetSeconds () 
            << " sec overall" << std::endl;

  std::cout << "Operator A: " << CellConfigToString (cellConfigA)
            << "; number of cells " << bsNodesA.GetN () << "; number of UEs "
            << ueNodesA.GetN () << std::endl;
  std::cout << "Operator B: " << CellConfigToString (cellConfigB)
            << "; number of cells " << bsNodesB.GetN () << "; number of UEs "
            << ueNodesB.GetN () << std::endl;
  GlobalValue::GetValueByName ("csatEnabled", booleanValue);
  bool csatEnabled = booleanValue.Get ();
  std::cout << "CSAT enable flag is set as " << csatEnabled << std::endl;
  GlobalValue::GetValueByName ("assignDutyCycle", booleanValue);
  bool assignDutyCycle= booleanValue.Get ();
  if (!csatEnabled && assignDutyCycle)
    {
      std::cout << "Manually assigning fixed CSAT duty cycle for each node" 
                << std::endl;
      GlobalValue::GetValueByName ("csatDutyCycleMid", doubleValue);
      double csatDutyCycleMid = doubleValue.Get ();
      std::cout << "Even node IDs will have duty cycle " << csatDutyCycleMid 
                << std::endl;
      GlobalValue::GetValueByName ("csatDutyCycleRight", doubleValue);
      double csatDutyCycleRight= doubleValue.Get ();
      std::cout << "Odd node IDs will have duty cycle " << csatDutyCycleRight 
                << std::endl;
    }
  // All application data will be sourced from the client node so that
  // it shows up on the downlink
  NodeContainer clientNodesA;  // for the backhaul application client
  clientNodesA.Create (1); // create one remote host for sourcing traffic
  NodeContainer clientNodesB;  // for the backhaul application client
  clientNodesB.Create (1); // create one remote host for sourcing traffic

  // For Wi-Fi, the client node needs to be connected to the bsNodes via a single
  // CSMA link
  //
  //
  //                 client
  //                   |    
  //               =========
  //              /   |    |
  //           bs0   bs1... bsN
  //
  //  First we create a Csma helper that will instantiate the
  //  client <-> bsN link for each network

  //CsmaHelper csmaHelper;

  //
  // Wireless setup phase
  //

  // Device containers hold the WiFi or LTE NetDevice objects created
  //NetDeviceContainer bsDevicesA;
  //NetDeviceContainer ueDevicesA;
  //NetDeviceContainer bsDevicesB;
  //NetDeviceContainer ueDevicesB;
  
  // Configure Devices
  Wifi5GHzChannel lteChInfo;
  NetDeviceContainer bsDevicesA;
  NetDeviceContainer bsDevicesB;
  NetDeviceContainer ueDevicesA;
  NetDeviceContainer ueDevicesB;
  
  NetDeviceContainer enbDevice;
  Ptr<Node> enbNode;
  NetDeviceContainer ueDevice;
  Ptr<Node> ueNode;
  
  uint32_t dlEarfcn = 0;
  uint32_t dlBandwidth = 0;
  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<EpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();

  // Start to create the wireless devices by first creating the shared channel
  // Note:  the design of LTE requires that we use an LteHelper to
  // create the channel, if we are potentially using LTE in one of the networks
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::Ieee80211axIndoorPropagationLossModel"));
  // since LAA is using CA, RRC messages will be excanged in the
  // licensed bands, hence we model it using the ideal RRC
  lteHelper->SetAttribute ("UseIdealRrc", BooleanValue (true));
  lteHelper->SetAttribute ("UsePdschForCqiGeneration", BooleanValue (true));
  lteHelper->SetEpcHelper (epcHelper);
  lteHelper->Initialize ();
  std::vector<LteSpectrumValueCatcher> lteDlSinrCatcherVectorA;
  std::vector<LteSpectrumValueCatcher> lteDlSinrCatcherVectorB;

  // set channel MaxLossDb to discard all transmissions below -15dB SNR.
  // The calculations assume -174 dBm/Hz noise PSD and 20 MHz bandwidth (73 dB)
  Ptr<SpectrumChannel> dlSpectrumChannel = lteHelper->GetDownlinkSpectrumChannel ();
  //           loss  = txpower -noisepower            -snr    ;
  double dlMaxLossDb = (phyParams.m_bsTxPower + phyParams.m_bsTxGain) -
    (-174.0 + 73.0 + phyParams.m_ueNoiseFigure) -
    (-15.0);
  dlSpectrumChannel->SetAttribute ("MaxLossDb", DoubleValue (dlMaxLossDb));
  Ptr<SpectrumChannel> ulSpectrumChannel = lteHelper->GetUplinkSpectrumChannel ();
  //           loss  = txpower -noisepower            -snr    ;
  double ulMaxLossDb = (phyParams.m_ueTxPower + phyParams.m_ueTxGain)  -
    (-174.0 + 73.0 + phyParams.m_bsNoiseFigure) -
    (-15.0);
  ulSpectrumChannel->SetAttribute ("MaxLossDb", DoubleValue (ulMaxLossDb));



  // LTE requires some Internet stack configuration prior to device installation
  // Wifi configures it after device installation
  InternetStackHelper internetStackHelper;
  // Install an internet stack to all the nodes with IP
  internetStackHelper.Install (clientNodesA);
  internetStackHelper.Install (clientNodesB);
  internetStackHelper.Install (ueNodesA);
  internetStackHelper.Install (ueNodesB);

  Ptr<LteUWifiCoexistenceHelper> lteUWifiCoexistenceHelper = CreateObject<LteUWifiCoexistenceHelper> ();

  Ipv4Address ipBackhaulA;
  lteHelper->SetEnbDeviceAttribute ("CsgIndication", BooleanValue (true));
  lteHelper->SetEnbDeviceAttribute ("CsgId", UintegerValue (1));
  lteHelper->SetUeDeviceAttribute ("CsgId", UintegerValue (1));
  Ipv4AddressHelper internetIpv4HelperA;
  internetIpv4HelperA.SetBase ("1.0.0.0", "255.0.0.0");
  /*
  ConfigureLte (cellConfigA, lteHelper, epcHelper, internetIpv4Helper, bsNodesA, 
                ueNodesA, clientNodesA, bsDevicesA, ueDevicesA, phyParams, 
                lteDlSinrCatcherVectorA, absPattern, transport);
  */
  // Configure operator A
  
  //====== ConfigureLte Code Starts
  // TODO: make LteRlcUmMaxTxBufferSize a global parameter 
  Config::SetDefault ("ns3::LteRlcUm::MaxTxBufferSize", UintegerValue (4000000000));
  // For LTE, the client node needs to be connected only to the PGW/SGW node
  // The EpcHelper will then take care of connecting the PGW/SGW node to the eNBs
  Ptr<Node> clientNodeA = clientNodesA.Get (0);
  Ptr<Node> pgwA = epcHelper->GetPgwNode ();
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.0)));
  NetDeviceContainer internetDevicesA = p2ph.Install (pgwA, clientNodeA);

  Ipv4InterfaceContainer internetIpIfacesA = internetIpv4HelperA.Assign (internetDevicesA);
  // interface 0 is localhost, 1 is the p2p device
  //Ipv4Address clientNodeAddr = internetIpIfaces.GetAddress (1);
  
  // make LTE and network reachable from the client node
  Ipv4StaticRoutingHelper ipv4RoutingHelperA;
  Ptr<Ipv4StaticRouting> clientNodeStaticRouting = ipv4RoutingHelperA.GetStaticRouting (clientNodeA->GetObject<Ipv4> ());
  clientNodeStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);



  // LTE configuration parametes
  lteHelper->SetSchedulerType ("ns3::PfFfMacScheduler");
  lteHelper->SetSchedulerAttribute ("UlCqiFilter", EnumValue (FfMacScheduler::PUSCH_UL_CQI));
  // LTE-U DL transmission @5180 MHz
  //lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (255444));
  //lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (100));
  // needed for initial cell search
  //lteHelper->SetUeDeviceAttribute ("DlEarfcn", UintegerValue (255444));
  // LTE calibration
  lteHelper->SetEnbAntennaModelType ("ns3::IsotropicAntennaModel");
  lteHelper->SetEnbAntennaModelAttribute ("Gain",   DoubleValue (phyParams.m_bsTxGain));
  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (phyParams.m_bsTxPower));
  Config::SetDefault ("ns3::LteUePhy::TxPower", DoubleValue (phyParams.m_ueTxPower));
  
  //EnumValue enumValue;
  enum LteEnbRrc::LteEpsBearerToRlcMapping_t rlcMode = LteEnbRrc::RLC_AM_ALWAYS;
  if (GlobalValue::GetValueByNameFailSafe ("tcpRlcMode", enumValue))
    {
      rlcMode = (LteEnbRrc::LteEpsBearerToRlcMapping_t) enumValue.Get ();
    }

  switch (transport)
    {
      case TCP:
        Config::SetDefault ("ns3::LteEnbRrc::EpsBearerToRlcMapping", EnumValue (rlcMode));
        break;

      case UDP:
      case FTP:
      default:
        Config::SetDefault ("ns3::LteEnbRrc::EpsBearerToRlcMapping", EnumValue (LteEnbRrc::RLC_UM_ALWAYS));
        break;
    }

  // Create Devices and install them in the Nodes (eNBs and UEs)
  
  NodeContainer::Iterator it_ue = ueNodesA.Begin ();
  for (NodeContainer::Iterator it = bsNodesA.Begin (); it!=bsNodesA.End (); it++)
    {
      enbNode = (*it);
      lteChInfo = lteUNodeIdToBandInfo.find (enbNode->GetId ())->second;
      
      dlEarfcn = lteChInfo.m_dlEarfcn;
      dlBandwidth = lteChInfo.m_dlBandwidthInNoOfRbs;
      lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (dlEarfcn));
      lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (dlBandwidth));
      enbDevice = lteHelper->InstallEnbDevice (enbNode);
      bsDevicesA.Add (enbDevice);
      
      NS_LOG_INFO ("\n Installed eNb for operator A with node Id:"
          <<enbNode->GetId ()<<", cell id:"
          <<enbDevice.Get (0)->GetObject<LteEnbNetDevice> ()->GetCellId ()
          <<" channel:"<<lteChInfo.m_wifiChannelNumber <<" ,dl earfcn:"
          <<enbDevice.Get (0)->GetObject<LteEnbNetDevice> ()->GetDlEarfcn ()
      );
      
      for (uint32_t u = 0; u < numUePerCell; u++)
        {
          ueNode = (*it_ue );
          lteHelper->SetUeDeviceAttribute ("DlEarfcn", UintegerValue (dlEarfcn));
          //lteHelper->SetUeDeviceAttribute ("DlBandwidth", UintegerValue (dlBandwidth));
          ueDevice = lteHelper->InstallUeDevice (ueNode);          
          ueDevicesA.Add (ueDevice);
          
          NS_LOG_INFO ("\n Installed UE for operator A under eNB node Id:"
              <<enbNode->GetId ()<<", UE IMSI:"
              <<ueDevice.Get (0)->GetObject<LteUeNetDevice> ()->GetImsi ()
              <<" channel:"<<lteChInfo.m_wifiChannelNumber <<" ,dl earfcn:"
              <<ueDevice.Get (0)->GetObject<LteUeNetDevice> ()->GetDlEarfcn ()
          );
          it_ue++;
        }
       
    }
  
  NetDeviceContainer ueLteDevsA (ueDevicesA);

  // additional UE-specific configuration
  Ipv4InterfaceContainer clientIpIfacesA;
  NS_ASSERT_MSG (lteDlSinrCatcherVectorA.empty (), 
                 "Must provide an empty lteDlSinCatcherVector");
  // side effect: will create LteSpectrumValueCatchers
  // note that nobody else should resize this vector otherwise callbacks will be using invalid pointers
  lteDlSinrCatcherVectorA.resize (ueNodesA.GetN ());
  Ptr<Node> ue;
  Ptr<NetDevice> ueDeviceA;
  for (uint32_t u = 0; u < ueNodesA.GetN (); ++u)
    {
      ue = ueNodesA.Get (u);
      ueDeviceA = ueDevicesA.Get (u);
      Ptr<LteUeNetDevice> ueLteDevice = ueDeviceA->GetObject<LteUeNetDevice> ();

      // assign IP address to UEs
      Ipv4InterfaceContainer ueIpIface 
          = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueDeviceA));
      clientIpIfacesA.Add (ueIpIface);
      // set the default gateway for the UE
      //Ipv4StaticRoutingHelper ipv4RoutingHelper;
      Ptr<Ipv4StaticRouting> ueStaticRoutingA 
          = ipv4RoutingHelperA.GetStaticRouting (ue->GetObject<Ipv4> ());
      ueStaticRoutingA->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);

      // set up SINR monitoring
      Ptr<LtePhy> uePhy = ueLteDevice->GetPhy ()->GetObject<LtePhy> ();
      Ptr<LteAverageChunkProcessor> monitorLteChunkProcessor  
        = Create<LteAverageChunkProcessor> ();
      monitorLteChunkProcessor->AddCallback (
        MakeCallback (&LteSpectrumValueCatcher::ReportValue, 
                      &lteDlSinrCatcherVectorA.at (u))
      );
      uePhy->GetDownlinkSpectrumPhy ()
        ->AddDataSinrChunkProcessor (monitorLteChunkProcessor);
      
    }

  // instruct all devices to attach using the LTE initial cell selection procedure
  lteHelper->Attach (ueDevicesA);
  //====== ConfigureLte Code for Operator B ends

  ConfigureLteU (lteUWifiCoexistenceHelper, bsDevicesA, phyParams);
  // Add X2 interface
  lteHelper->AddX2Interface (bsNodesA);

 
  // The IP address for the backhaul traffic source will be 1.0.0.2
  ipBackhaulA = Ipv4Address ("1.0.0.2");
  

  
  // ========================================
  
  // Configure operator B
  // For bsNodesB it is a simple assignment on channel 32, where wifi doesn't use
  Ipv4Address ipBackhaulB;
  
  lteHelper->SetEnbDeviceAttribute ("CsgIndication", BooleanValue (true));
  lteHelper->SetEnbDeviceAttribute ("CsgId", UintegerValue (2));
  lteHelper->SetUeDeviceAttribute ("CsgId", UintegerValue (2));
  Ipv4AddressHelper internetIpv4HelperB;
  internetIpv4HelperB.SetBase ("2.0.0.0", "255.0.0.0");
  /*
  ConfigureLte (cellConfigB, lteHelper, epcHelper, 
                internetIpv4Helper, bsNodesB, ueNodesB, 
                clientNodesB, bsDevicesB, ueDevicesB, phyParams, 
                lteDlSinrCatcherVectorB, absPattern, transport);
   */
  
  //====== ConfigureLte Code for operator B Starts
  // TODO: make LteRlcUmMaxTxBufferSize a global parameter 
  Config::SetDefault ("ns3::LteRlcUm::MaxTxBufferSize", UintegerValue (4000000000));
  // For LTE, the client node needs to be connected only to the PGW/SGW node
  // The EpcHelper will then take care of connecting the PGW/SGW node to the eNBs
  Ptr<Node> clientNodeB = clientNodesB.Get (0);
  Ptr<Node> pgwB = epcHelper->GetPgwNode ();
  PointToPointHelper p2phB;
  p2phB.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2phB.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2phB.SetChannelAttribute ("Delay", TimeValue (Seconds (0.0)));
  NetDeviceContainer internetDevicesB = p2phB.Install (pgwB, clientNodeB);

  Ipv4InterfaceContainer internetIpIfacesB = internetIpv4HelperB.Assign (internetDevicesB);
  // interface 0 is localhost, 1 is the p2p device
  //Ipv4Address clientNodeAddr = internetIpIfaces.GetAddress (1);
  
  // make LTE and network reachable from the client node
  Ipv4StaticRoutingHelper ipv4RoutingHelperB;
  Ptr<Ipv4StaticRouting> clientNodeStaticRoutingB 
      = ipv4RoutingHelperB.GetStaticRouting (clientNodeB->GetObject<Ipv4> ());
  clientNodeStaticRoutingB->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);



  // LTE configuration parametes
  lteHelper->SetSchedulerType ("ns3::PfFfMacScheduler");
  lteHelper->SetSchedulerAttribute ("UlCqiFilter", EnumValue (FfMacScheduler::PUSCH_UL_CQI));
  // LTE-U DL transmission @5180 MHz
  //lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (255444));
  //lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (100));
  // needed for initial cell search
  //lteHelper->SetUeDeviceAttribute ("DlEarfcn", UintegerValue (255444));
  // LTE calibration
  lteHelper->SetEnbAntennaModelType ("ns3::IsotropicAntennaModel");
  lteHelper->SetEnbAntennaModelAttribute ("Gain",   DoubleValue (phyParams.m_bsTxGain));
  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (phyParams.m_bsTxPower));
  Config::SetDefault ("ns3::LteUePhy::TxPower", DoubleValue (phyParams.m_ueTxPower));
  
  // Create Devices and install them in the Nodes (eNBs and UEs)
  enbNode = bsNodesB.Get (0);
  lteChInfo = GetChannelInfo (bsNodesBChl);
  dlEarfcn = lteChInfo.m_dlEarfcn;
  dlBandwidth = lteChInfo.m_dlBandwidthInNoOfRbs;
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (dlEarfcn));
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (dlBandwidth));
  
  ueNode = ueNodesB.Get (0);
  //lteChInfo = GetChannelInfo (36);
  
  dlEarfcn = lteChInfo.m_dlEarfcn;
  dlBandwidth = lteChInfo.m_dlBandwidthInNoOfRbs;
  lteHelper->SetUeDeviceAttribute ("DlEarfcn", UintegerValue (dlEarfcn));
  //lteHelper->SetUeDeviceAttribute ("DlBandwidth", UintegerValue (dlBandwidth));
  
  NetDeviceContainer enbDeviceContainer;
  enbDeviceContainer = lteHelper->InstallEnbDevice (enbNode);
  bsDevicesB.Add (enbDeviceContainer);
  //ueDevice = lteHelper->InstallEnbDevice (ueNode);
  ueDevicesB.Add (lteHelper->InstallUeDevice (ueNode));
  
  NS_LOG_INFO ("\n Installed eNb for operator B with node Id:"
    <<enbNode->GetId ()<<", cell id:"
    <<enbDeviceContainer.Get (0)->GetObject<LteEnbNetDevice> ()->GetCellId ()
    <<" channel:"<<lteChInfo.m_wifiChannelNumber <<" ,dl earfcn:"
    <<enbDeviceContainer.Get (0)->GetObject<LteEnbNetDevice> ()->GetDlEarfcn ()
  );
  
  NS_LOG_INFO ("\n Installed UE for operator B under eNB node Id:"
    <<enbNode->GetId ()<<", UE IMSI:"
    <<ueDevicesB.Get (0)->GetObject<LteUeNetDevice> ()->GetImsi ()
    <<" channel:"<<lteChInfo.m_wifiChannelNumber <<" ,dl earfcn:"
    <<ueDevicesB.Get (0)->GetObject<LteUeNetDevice> ()->GetDlEarfcn ()
  );
  
  NetDeviceContainer ueLteDevsB (ueDevicesB);

  // additional UE-specific configuration
  Ipv4InterfaceContainer clientIpIfacesB;
  NS_ASSERT_MSG (lteDlSinrCatcherVectorB.empty (), "Must provide an empty lteDlSinCatcherVector");
  // side effect: will create LteSpectrumValueCatchers
  // note that nobody else should resize this vector otherwise callbacks will be using invalid pointers
  lteDlSinrCatcherVectorB.resize (ueNodesB.GetN ());

  for (uint32_t u = 0; u < ueNodesB.GetN (); ++u)
    {
      Ptr<Node> ue = ueNodesB.Get (u);
      Ptr<NetDevice> ueDeviceB = ueDevicesB.Get (u);
      Ptr<LteUeNetDevice> ueLteDevice = ueDeviceB->GetObject<LteUeNetDevice> ();

      // assign IP address to UEs
      Ipv4InterfaceContainer ueIpIface 
          = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueDeviceB));
      clientIpIfacesB.Add (ueIpIface);
      // set the default gateway for the UE
      //Ipv4StaticRoutingHelper ipv4RoutingHelper;
      Ptr<Ipv4StaticRouting> ueStaticRoutingB
          = ipv4RoutingHelperB.GetStaticRouting (ue->GetObject<Ipv4> ());
      ueStaticRoutingB->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);

      // set up SINR monitoring
      Ptr<LtePhy> uePhy = ueLteDevice->GetPhy ()->GetObject<LtePhy> ();
      Ptr<LteAverageChunkProcessor> monitorLteChunkProcessor  = Create<LteAverageChunkProcessor> ();
      monitorLteChunkProcessor->AddCallback (
        MakeCallback (&LteSpectrumValueCatcher::ReportValue, 
                      &lteDlSinrCatcherVectorB.at (u))
      );
      uePhy->GetDownlinkSpectrumPhy ()->AddDataSinrChunkProcessor (monitorLteChunkProcessor);
    }

  // instruct all devices to attach using the LTE initial cell selection procedure
  lteHelper->Attach (ueDevicesB);
  //====== ConfigureLte Code for Operator B ends
  
  ipBackhaulB = Ipv4Address ("2.0.0.2");
  
  ConfigureLteU (lteUWifiCoexistenceHelper, bsDevicesB, phyParams);
  // Add X2 interface
  lteHelper->AddX2Interface (bsNodesB);

  
  FlowMonitorHelper flowmonHelperA;
  NodeContainer endpointNodesA;
  endpointNodesA.Add (clientNodesA);
  endpointNodesA.Add (ueNodesA);
  FlowMonitorHelper flowmonHelperB;
  NodeContainer endpointNodesB;
  endpointNodesB.Add (clientNodesB);
  endpointNodesB.Add (ueNodesB);


  Ptr<FlowMonitor> monitorA = flowmonHelperA.Install (endpointNodesA);
  monitorA->SetAttribute ("DelayBinWidth", DoubleValue (0.001));
  monitorA->SetAttribute ("JitterBinWidth", DoubleValue (0.001));
  monitorA->SetAttribute ("PacketSizeBinWidth", DoubleValue (20));

  Ptr<FlowMonitor> monitorB = flowmonHelperB.Install (endpointNodesB);
  monitorB->SetAttribute ("DelayBinWidth", DoubleValue (0.001));
  monitorB->SetAttribute ("JitterBinWidth", DoubleValue (0.001));
  monitorB->SetAttribute ("PacketSizeBinWidth", DoubleValue (20));
  
  //======================== Setup internet inferface
  
  Ipv4InterfaceContainer ipBsA;
  Ipv4InterfaceContainer ipUeA;
  Ipv4InterfaceContainer ipBsB;
  Ipv4InterfaceContainer ipUeB;

  Ipv4AddressHelper ueAddress;
  ueAddress.SetBase ("17.0.0.0", "255.255.0.0");
  ipBsA = ueAddress.Assign (bsDevicesA);
  ipUeA = ueAddress.Assign (ueDevicesA);

  ueAddress.SetBase ("18.0.0.0", "255.255.0.0");
  ipBsB = ueAddress.Assign (bsDevicesB);
  ipUeB = ueAddress.Assign (ueDevicesB);
  
  Ptr<ExponentialRandomVariable> ftpArrivals = CreateObject<ExponentialRandomVariable> ();
  double ftpLambda;
  bool found = GlobalValue::GetValueByNameFailSafe ("ftpLambda", doubleValue);
  if (found)
    {
      ftpLambda = doubleValue.Get ();
      ftpArrivals->SetAttribute ("Mean", DoubleValue (1 / ftpLambda));
    }
  //uint32_t nextClient = 0;
  
  if (transport == UDP)
    {
      ApplicationContainer serverApps, clientApps;
      NS_LOG_DEBUG("configuring UDP servers with start: "
          <<serverStartTime<< ", stop time: "<<serverStopTime );
      NS_LOG_DEBUG("configuring UDP client with start: "
          <<clientStartTime<< ", stop time: "<<clientStopTime );
      serverApps.Add (ConfigureUdpServers (ueNodesA, serverStartTime, serverStopTime));
      clientApps.Add (ConfigureUdpClients (clientNodesA, ipUeA, clientStartTime, clientStopTime, udpInterval));
      serverApps.Add (ConfigureUdpServers (ueNodesB, serverStartTime, serverStopTime));
      clientApps.Add (ConfigureUdpClients (clientNodesB, ipUeB, clientStartTime, clientStopTime, udpInterval));
    }
  else if (transport == FTP)
    {
      ApplicationContainer serverApps, clientApps;
      NS_LOG_DEBUG("configuring FTP servers with start: "
          <<serverStartTime<< ", stop time: "<<serverStopTime
      );
      NS_LOG_DEBUG("configuring FTP client with start: "
          <<clientStartTime<< ", stop time: "<<clientStopTime
      );
      ApplicationContainer ftpServerApps, ftpClientApps;
      ftpServerApps.Add (ConfigureFtpServers (ueNodesA, serverStartTime));
      ftpClientApps.Add (ConfigureFtpClients (clientNodesA, ipUeA, clientStartTime));
      ftpServerApps.Add (ConfigureFtpServers (ueNodesB, serverStartTime));
      ftpClientApps.Add (ConfigureFtpClients (clientNodesB, ipUeB, clientStartTime));
      // Start file transfer arrival process
      // == Original implementation, need fix to call file transfer for each cell:
      // double firstArrival = ftpArrivals->GetValue ();
      // NS_LOG_DEBUG ("First FTP arrival at time " << clientStartTime.GetSeconds () + firstArrival);
      // Simulator::Schedule (clientStartTime + Seconds (firstArrival), &StartFileTransfer, ftpArrivals, ftpClientApps, nextClient, clientStopTime);
      // ============
      // New implementation for 3GPP FTP model 1
      double firstArrival; 
      // Per user FTP transfer arrivals. TODO: make a separate function for per user FTP 

      for (uint32_t nextClient = 0; nextClient < ftpClientApps.GetN (); nextClient ++)
        {
          Time nextTime = Seconds (0);
          firstArrival = ftpArrivals->GetValue ();
          Time accumulatedTime = Seconds (clientStartTime.GetSeconds () + firstArrival);
          NS_LOG_DEBUG ("First FTP arrival for client " 
                        << nextClient << " at time " 
                        << clientStartTime.GetSeconds () + firstArrival); 
          nextTime = Seconds (firstArrival);
          while (accumulatedTime + nextTime < clientStopTime)
            {
              Simulator::Schedule (accumulatedTime, 
                                   &SingleFileTransfer,
                                   ftpClientApps, 
                                   nextClient,
                                   nextTime);
              nextTime = Seconds (ftpArrivals->GetValue ());
              accumulatedTime += nextTime;
            }
        }

    }
  
  //
  // Kicking off simulator
  //
  
  Ptr<Node> node = bsNodesA.Get(0);
  Vector pos = node -> GetObject<MobilityModel> ()-> GetPosition ();
  NS_LOG_INFO("first bsA location is " << pos.x << ", " << pos.y << std::endl);
  node = bsNodesB.Get(0);
  pos = node -> GetObject<MobilityModel> ()-> GetPosition ();
  NS_LOG_INFO("first bsB location is " << pos.x << ", " << pos.y << std::endl);
  node = ueNodesA.Get(0);
  pos = node -> GetObject<MobilityModel> ()-> GetPosition ();
  NS_LOG_INFO("ueA location is " << pos.x << ", " << pos.y << std::endl);
  node = ueNodesB.Get(0);
  pos = node -> GetObject<MobilityModel> ()-> GetPosition ();
  NS_LOG_INFO("ueB location is " << pos.x << ", " << pos.y << std::endl);
  //bool disableApps = false;
  
  Simulator::Stop (stopTime);

  //
  // Schedule the trace log
  //
  
  SetCsatDutyCycleLog (clientStartTime, clientStopTime,
                       bsDevicesA);
  SetCsatDutyCycleLog (clientStartTime, clientStopTime,
                       bsDevicesB);
  
  Simulator::Run ();

  
  //
  // Post-processing phase
  //

  std::cout << "--------monitorA----------" << std::endl;
  PrintFlowMonitorStats (monitorA, flowmonHelperA, durationTime.GetSeconds ());
  std::cout << "--------monitorB----------" << std::endl;
  PrintFlowMonitorStats (monitorB, flowmonHelperB, durationTime.GetSeconds ());
  bool saveResults = true;
  if (saveResults)
    {

      if (transport == TCP)
        {
          SaveTcpFlowMonitorStats (outFileName + "_operatorA", 
                                   simulationParams.str(), monitorA, 
                                   flowmonHelperA, durationTime.GetSeconds ());
          SaveTcpFlowMonitorStats (outFileName + "_operatorB", 
                                   simulationParams.str(), monitorB, 
                                   flowmonHelperB, durationTime.GetSeconds ());
          //NS_LOG_DEBUG("Saving TCP flow monitor stats");
        }
      else if (transport == UDP || transport == FTP)
        {
          SaveUdpFlowMonitorStats (outFileName + "_operatorA", 
                                   simulationParams.str(), monitorA, 
                                   flowmonHelperA, durationTime.GetSeconds ());
          SaveUdpFlowMonitorStats (outFileName + "_operatorB", 
                                   simulationParams.str(), monitorB, 
                                   flowmonHelperB, durationTime.GetSeconds ());
          //NS_LOG_DEBUG("Saving UDP/FTP flow monitor stats");
        }
      else
        {
          NS_FATAL_ERROR ("transport parameter invalid: " << transport);
        }

     }
  
  SetCsatDutyCycleStats (outFileName);
 
  Simulator::Destroy ();
  

  
  
  return 0;
}
