/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <biljana.bojovic@cttc.es>
 *
 */

#ifndef TEST_RR_SDL_CCM_H
#define TEST_RR_SDL_CCM_H

#include "ns3/simulator.h"
#include "ns3/test.h"
#include <ns3/nstime.h>
#include <ns3/scenario-helper.h>


using namespace ns3;


/**
  *  Test for testing different use cases of usage of RrSdlComponentCarrierManager.
 */

// =====  rr-sdl-ccm test ==================================== //

/*
 * \brief This system test program creates different test cases with a single eNB and
 * several UEs, all having the same Radio Bearer specification. In each test 
 * case, the UEs see the same SINR from the eNB; different test cases are 
 * implemented obtained by using different SINR values and different numbers of 
 * UEs. The test consists of checking that the throughput
 * obtained over different carriers are equal in the downlink, while the uplink
 * traffic goes over primary carrier.
 */
class RrSdlComponentCarrierManagerTestCase : public TestCase
{
public:

  RrSdlComponentCarrierManagerTestCase (uint16_t nUser, uint16_t dist, uint32_t dlbandwidth, uint32_t ulBandwidth, uint8_t numberOfComponentCarriers);
  virtual ~RrSdlComponentCarrierManagerTestCase ();
  void DlScheduling (DlSchedulingCallbackInfo dlInfo);
  void UlScheduling (uint32_t frameNo, uint32_t subframeNo, uint16_t rnti, uint8_t mcs, uint16_t sizeTb, uint8_t componentCarrierId);

private:

  virtual void DoRun (void);
  static std::string BuildNameString  (uint16_t nUser, uint16_t dist, uint32_t dlBandwidth, uint32_t ulBandwidth, uint32_t numberOfComponentCarriers);

  uint16_t m_nUser;
  uint16_t m_dist;
  uint32_t m_dlBandwidth;
  uint32_t m_ulBandwidth;
  uint32_t m_numberOfComponentCarriers;

  std::map <uint8_t, uint32_t> m_ccDownlinkTraffic;
  std::map <uint8_t, uint32_t> m_ccUplinkTraffic;
};



class TestRrSdlComponentCarrierManagerSuite : public TestSuite
{
public:
  TestRrSdlComponentCarrierManagerSuite ();
};


/**
 * \brief Test suite for generating calls to UE measurements test case
 *        ns3::RrSdlCcmUseScellOpportunisticallyTestCase.
 *        RrSdlCcm with Lte-U, test that T_ON is set to zero when all UEs have deactivated secondary carrier. In this case only mandatory MIB and LDS ctrl can be sent.
 */

class RrSdlCcmUseScellOpportunisticallyTestSuite : public TestSuite
{
public:
  RrSdlCcmUseScellOpportunisticallyTestSuite ();
};


// ===== rr-sdl-use-scell-opp test ==================================== //

/**
 * \brief Testing secondary carrier activation/deactivation in LTE with simulation of 1 eNodeB and 1 UE in
 *        piecewise configuration and 120 ms report interval.
 */
class RrSdlCcmUseScellOpportunisticallyTestCase : public TestCase
{
public:
  RrSdlCcmUseScellOpportunisticallyTestCase (std::string name, uint8_t numberOfComponentCarriers, double prbOccupancyThreshold,
                                             uint64_t rlcQueuesSizeThreshold, bool gbrFlow, int nUes, int maxNUesPerTti,
                                             Config_e scellType, uint32_t maxInterMibTime, uint32_t ldsInterval);

  virtual ~RrSdlCcmUseScellOpportunisticallyTestCase ();

  void DlScheduling (DlSchedulingCallbackInfo dlInfo);
  void UlScheduling (uint32_t frameNo, uint32_t subframeNo, uint16_t rnti, uint8_t mcs, uint16_t sizeTb, uint8_t componentCarrierId);
  void CtrlMessageCheck (std::list<Ptr<LteControlMessage> > ctrlMsg);
  void ActiveOnTimeCheck (bool activeOnTime, Time csatEndTime);

private:
  /**
   * \brief Setup the simulation with the intended UE measurement reporting
   *        configuration, run it, and connect the
   *        `RecvMeasurementReportCallback` function to the
   *        `LteUeRrc::RecvMeasurementReport` trace source.
   */
  virtual void DoRun ();
  void TeleportVeryNear ();
  void TeleportVeryFar ();

  std::vector< Ptr<MobilityModel> > m_ueMobilityVector;
  bool m_secondaryExpectedActive;
  Time m_lastTransportTime;
  Time m_l1MeasReportIntervalDelay; // delay due to layer 1 measurement report interval which is normally 200ms

  uint32_t m_secondaryActivityCounterDl;
  uint32_t m_secondaryActivityCounterUl;

  uint8_t m_numberOfComponentCarriers;

  double m_prbOccupancyThreshold;
  uint64_t m_rlcQueuesSizeThreshold;

  bool m_gbrFlow;
  int m_nUes;

  uint64_t m_frameNo;
  uint64_t m_subframeNo;
  std::map <uint8_t, uint8_t> m_nUesDlCounter; // one counter per each carrier in the downlink
  uint8_t m_nUesUlCounter; // there should be only one carrier active in the uplink in this test

  int m_maxNUesPerTti;

  Config_e m_scellType;

  // attributes to check if MIB and LDS were sent properly
  Time m_lastMibSent;
  Time m_lastLdsSent;
  uint32_t m_maxInterMibTime;
  uint32_t m_ldsInterval;
  bool m_activeOnTime;
  Time m_currentCsatStarts;
  Time m_currentCsatEnds;


  uint32_t m_lteUCarrierMibCounter;
  uint32_t m_lteUCarrierLdsCounter;

}; // end of class RrSdlCcmUseScellOpportunisticallyTestCase


#endif /* TEST_RR_SDL_CCM_H */
