/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es>
 *
 */

#include <ns3/object.h>
#include <ns3/spectrum-interference.h>
#include <ns3/spectrum-error-model.h>
#include <ns3/log.h>
#include <ns3/test.h>
#include <ns3/simulator.h>
#include <ns3/packet.h>
#include <ns3/ptr.h>
#include <iostream>
#include "ns3/radio-bearer-stats-calculator.h"
#include <ns3/constant-position-mobility-model.h>
#include <ns3/eps-bearer.h>
#include <ns3/node-container.h>
#include <ns3/mobility-helper.h>
#include <ns3/net-device-container.h>
#include <ns3/lte-ue-net-device.h>
#include <ns3/lte-enb-net-device.h>
#include <ns3/lte-ue-rrc.h>
#include <ns3/lte-helper.h>
#include "ns3/string.h"
#include "ns3/double.h"
#include <ns3/lte-enb-phy.h>
#include <ns3/lte-ue-phy.h>
#include <ns3/boolean.h>
#include <ns3/enum.h>
#include <ns3/config-store-module.h>
#include <ns3/callback.h>
#include <ns3/config.h>
#include <ns3/string.h>
#include <ns3/double.h>

#include <ns3/ff-mac-scheduler.h>
#include <ns3/lte-enb-rrc.h>
#include <ns3/lte-rrc-sap.h>

#include <ns3/point-to-point-epc-helper.h>
#include <ns3/internet-stack-helper.h>
#include <ns3/point-to-point-helper.h>
#include <ns3/ipv4-address-helper.h>
#include <ns3/ipv4-static-routing-helper.h>

#include <ns3/node-container.h>
#include <ns3/net-device-container.h>
#include <ns3/ipv4-interface-container.h>

#include <list>
#include <set>
#include <vector>

#include <ns3/lte-common.h>
// to use overloaded operators functions
#include "lte-test-ue-measurements.h"
#include "lte-test-rr-sdl-component-carrier-manager.h"
#include <ns3/lte-u-wifi-coexistence-helper.h>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TestRrSdlComponentCarrierManager");


void
LteTestDlSchedulingCallback1 (RrSdlComponentCarrierManagerTestCase *testcase, std::string path, DlSchedulingCallbackInfo dlInfo)
{
  testcase->DlScheduling (dlInfo);
}

void
LteTestUlSchedulingCallback1 (RrSdlComponentCarrierManagerTestCase *testcase, std::string path,
                             uint32_t frameNo, uint32_t subframeNo, uint16_t rnti,
                             uint8_t mcs, uint16_t sizeTb, uint8_t ccId)
{
  testcase->UlScheduling (frameNo, subframeNo, rnti, mcs, sizeTb, ccId);
}

void
LteTestDlSchedulingCallback2 (RrSdlCcmUseScellOpportunisticallyTestCase *testcase, std::string path, DlSchedulingCallbackInfo dlInfo)
{
  testcase->DlScheduling (dlInfo);
}

void
LteTestUlSchedulingCallback2 (RrSdlCcmUseScellOpportunisticallyTestCase *testcase, std::string path,
                             uint32_t frameNo, uint32_t subframeNo, uint16_t rnti,
                             uint8_t mcs, uint16_t sizeTb, uint8_t ccId)
{
  testcase->UlScheduling (frameNo, subframeNo, rnti, mcs, sizeTb, ccId);
}

void
LteTestCtrlMessageCheckCallback (RrSdlCcmUseScellOpportunisticallyTestCase *testcase, std::string path, std::list<Ptr<LteControlMessage> > list )
{
  testcase->CtrlMessageCheck (list);
}

void
LteTestActiveOnTimeCallback (RrSdlCcmUseScellOpportunisticallyTestCase *testcase, std::string path, bool activeOnTimeOld, Time csatEnds)
{
  testcase->ActiveOnTimeCheck (activeOnTimeOld, csatEnds);
}

TestRrSdlComponentCarrierManagerSuite::TestRrSdlComponentCarrierManagerSuite ()
  : TestSuite ("rr-sdl-ccm", SYSTEM)
{
  NS_LOG_INFO ("creating RrSdlComponentCarrierManagerTestCase");

  // bandwidth is 25 and there are 2 carriers

  AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,0, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,0, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,0, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,0, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,0, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,0, 25, 25, 2), TestCase::EXTENSIVE);

  AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,4800, 25, 25, 2), TestCase::QUICK);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,4800, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,4800, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,4800, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,4800, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,4800, 25, 25, 2), TestCase::EXTENSIVE);

  AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,6000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,6000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,6000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,6000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,6000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,6000, 25, 25, 2), TestCase::EXTENSIVE);

  AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,20000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,20000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,20000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,20000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,20000, 25, 25, 2), TestCase::EXTENSIVE);
  AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,20000, 25, 25, 2), TestCase::QUICK);


   // bandwidth is 25 and there are 3 carriers
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,0, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,0, 25, 25, 3), TestCase::QUICK);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,0, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,0, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,0, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,0, 25, 25, 3), TestCase::EXTENSIVE);

   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,4800, 25, 25, 3), TestCase::QUICK);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,4800, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,4800, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,4800, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,4800, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,4800, 25, 25, 3), TestCase::EXTENSIVE);

   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,6000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,6000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,6000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,6000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,6000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,6000, 25, 25, 3), TestCase::EXTENSIVE);

   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,20000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,20000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,20000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,20000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,20000, 25, 25, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,20000, 25, 25, 3), TestCase::EXTENSIVE);


   // bandwidth = 6 RB and there are 3 carriers
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,0, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,0, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,0, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,0, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,0, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,0, 6, 6, 3), TestCase::EXTENSIVE);

   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,4800, 6, 6, 3), TestCase::QUICK);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,4800, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,4800, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,4800, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,4800, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,4800, 6, 6, 3), TestCase::EXTENSIVE);

   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,6000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,6000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,6000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,6000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,6000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,6000, 6, 6, 3), TestCase::EXTENSIVE);

   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,20000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,20000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,20000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,20000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,20000, 6, 6, 3), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,20000, 6, 6, 3), TestCase::EXTENSIVE);

   // bandwidth = 6 RB and there are 2 carriers
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,0, 6, 6, 2), TestCase::QUICK);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,0, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,0, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,0, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,0, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,0, 6, 6, 2), TestCase::EXTENSIVE);

   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,4800, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,4800, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,4800, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,4800, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,4800, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,4800, 6, 6, 2), TestCase::EXTENSIVE);

   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,6000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,6000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,6000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,6000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,6000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,6000, 6, 6, 2), TestCase::QUICK);

   AddTestCase (new RrSdlComponentCarrierManagerTestCase (1,20000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (3,20000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (6,20000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (9,20000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (12,20000, 6, 6, 2), TestCase::EXTENSIVE);
   AddTestCase (new RrSdlComponentCarrierManagerTestCase (15,20000, 6, 6, 2), TestCase::QUICK);

}

static TestRrSdlComponentCarrierManagerSuite rrSdlComponentCarrierManagerSuite;

std::string 
RrSdlComponentCarrierManagerTestCase::BuildNameString (uint16_t nUser, uint16_t dist, uint32_t dlBandwidth, uint32_t ulBandwidth, uint32_t numberOfComponentCarriers)
{
  std::ostringstream oss;
  oss << nUser << " UEs, distance " << dist << " m"<< " dlBandwidth "<< dlBandwidth <<" ulBandwidth "<< ulBandwidth <<" number of carriers "<<numberOfComponentCarriers;
  return oss.str ();
}

RrSdlComponentCarrierManagerTestCase::RrSdlComponentCarrierManagerTestCase (uint16_t nUser, uint16_t dist, uint32_t dlbandwidth, uint32_t ulBandwidth, uint8_t numberOfComponentCarriers)
  : TestCase (BuildNameString (nUser, dist, dlbandwidth, ulBandwidth, numberOfComponentCarriers)),
    m_nUser (nUser),
    m_dist (dist),
    m_dlBandwidth (dlbandwidth),
    m_ulBandwidth (ulBandwidth),
    m_numberOfComponentCarriers(numberOfComponentCarriers)
{
}

RrSdlComponentCarrierManagerTestCase::~RrSdlComponentCarrierManagerTestCase ()
{
}

void
RrSdlComponentCarrierManagerTestCase::DoRun (void)
{
  NS_LOG_FUNCTION (this << m_nUser << m_dist << m_dlBandwidth << m_ulBandwidth << m_numberOfComponentCarriers);

  Config::SetDefault ("ns3::LteEnbNetDevice::DlBandwidth", UintegerValue (m_dlBandwidth));
  Config::SetDefault ("ns3::LteEnbNetDevice::UlBandwidth", UintegerValue (m_ulBandwidth));
  Config::SetDefault ("ns3::LteHelper::UseCa", BooleanValue (true));
  Config::SetDefault ("ns3::LteHelper::NumberOfComponentCarriers", UintegerValue (m_numberOfComponentCarriers));
  // set round robin suplemental downlink component carrier manager
  Config::SetDefault ("ns3::LteHelper::EnbComponentCarrierManager", StringValue ("ns3::RrSdlComponentCarrierManager"));
  // disable SSR in the uplink of the secondary carriers
  Config::SetDefault ("ns3::LteUePhy::DisableSrsOnSecondaryCarriers", BooleanValue (true));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));

  Config::SetDefault ("ns3::RrSdlComponentCarrierManager::UseSCellOpportunistically", BooleanValue (false));
  Config::SetDefault ("ns3::LteHelper::UseIdealRrc", BooleanValue (true));
  Config::SetDefault ("ns3::RrFfMacScheduler::CqiTimerThreshold", UintegerValue (100));

  // This is needed as the RR scheduler does not allocate resources properly for retransmission
 // Config::SetDefault ("ns3::LteRlcAm::TxOpportunityForRetxAlwaysBigEnough", BooleanValue (true));

  //Disable Uplink Power Control
 // Config::SetDefault ("ns3::LteUePhy::EnableUplinkPowerControl", BooleanValue (false));

  /**
   * Initialize Simulation Scenario: 1 eNB and m_nUser UEs
   */

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));

  // Create Nodes: eNodeB and UE
  NodeContainer enbNodes;
  NodeContainer ueNodes;
  enbNodes.Create (1);
  ueNodes.Create (m_nUser);

  // Install Mobility Model
  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (enbNodes);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (ueNodes);

  // Create Devices and install them in the Nodes (eNB and UE)
  NetDeviceContainer enbDevs;
  NetDeviceContainer ueDevs;
  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");
  enbDevs = lteHelper->InstallEnbDevice (enbNodes);
  ueDevs = lteHelper->InstallUeDevice (ueNodes);

  // Attach a UE to a eNB
  lteHelper->Attach (ueDevs, enbDevs.Get (0));

  // Activate an EPS bearer
  enum EpsBearer::Qci q = EpsBearer::GBR_CONV_VOICE;
  EpsBearer bearer (q);
  lteHelper->ActivateDataRadioBearer (ueDevs, bearer);
  
 
  Ptr<LteEnbNetDevice> lteEnbDev = enbDevs.Get (0)->GetObject<LteEnbNetDevice> ();
  Ptr<LteEnbPhy> enbPhy = lteEnbDev->GetPhy ();
  enbPhy->SetAttribute ("TxPower", DoubleValue (30.0));
  enbPhy->SetAttribute ("NoiseFigure", DoubleValue (5.0));

  // Set UEs' position and power
  for (int i = 0; i < m_nUser; i++)
    {
      Ptr<ConstantPositionMobilityModel> mm = ueNodes.Get (i)->GetObject<ConstantPositionMobilityModel> ();
      mm->SetPosition (Vector (m_dist, 0.0, 0.0));
      Ptr<LteUeNetDevice> lteUeDev = ueDevs.Get (i)->GetObject<LteUeNetDevice> ();
      Ptr<LteUePhy> uePhy = lteUeDev->GetPhy ();
      uePhy->SetAttribute ("TxPower", DoubleValue (23.0));
      uePhy->SetAttribute ("NoiseFigure", DoubleValue (9.0));
    }


  double statsStartTime = 0.300; // need to allow for RRC connection establishment + SRS
  double statsDuration = 0.4;
  Simulator::Stop (Seconds (statsStartTime + statsDuration - 0.0001));

  Config::Connect ("/NodeList/*/DeviceList/*/ComponentCarrierMap/*/LteEnbMac/DlScheduling",
                    MakeBoundCallback (&LteTestDlSchedulingCallback1, this));

  Config::Connect ("/NodeList/*/DeviceList/*/ComponentCarrierMap/*/LteEnbMac/UlScheduling",
                    MakeBoundCallback (&LteTestUlSchedulingCallback1, this));

  lteHelper->EnableTraces();

  Simulator::Run ();

  /**
   * Check that the assignation is done in a RR fashion
   */
  NS_LOG_INFO ("DL - Test with " << m_nUser << " user(s) at distance " << m_dist);
  std::vector <uint64_t> dlDataRxed;

  // tolerance increases with the number of users because the lc 0 and lc 1 will go always over primary carrier, so as the number of users increases the difference between primary and secondary
  //carrier will increase

  bool testDownlinkShare = true;
  int downlinkCarrierCounter = 0;

  for (std::map <uint8_t, uint32_t>::iterator itDownlink = m_ccDownlinkTraffic.begin(); itDownlink!=m_ccDownlinkTraffic.end(); itDownlink++)
   {
      downlinkCarrierCounter++;

      if (itDownlink == m_ccDownlinkTraffic.begin())
        {
           NS_LOG_INFO ("Downlink traffic per carrier:"<<itDownlink->second);
        }
      else
        {
          if (itDownlink->second != m_ccDownlinkTraffic.begin()->second)
           {
             testDownlinkShare = false;
             break;
           }
        }
   }

  NS_TEST_ASSERT_MSG_EQ (m_ccUplinkTraffic.size(), 1, "Uplink traffic should go only over primary carrier!");
  NS_TEST_ASSERT_MSG_EQ (m_ccDownlinkTraffic.size(), m_numberOfComponentCarriers, "Number of carriers in downlink does not correspond to number of carriers being configured in test.");

  NS_TEST_ASSERT_MSG_EQ (testDownlinkShare, true, " Downlink traffic not split equally between carriers!");

  Simulator::Destroy ();
}

void
RrSdlComponentCarrierManagerTestCase::DlScheduling (DlSchedulingCallbackInfo dlInfo)
{
  //NS_LOG_FUNCTION (dlInfo.frameNo << dlInfo.subframeNo << dlInfo.rnti << (uint32_t) dlInfo.mcsTb1 << dlInfo.sizeTb1 << (uint32_t) dlInfo.mcsTb2 << dlInfo.sizeTb2<<(uint16_t)dlInfo.componentCarrierId);
  // need to allow for RRC connection establishment + CQI feedback reception + persistent data transmission
  if (Simulator::Now () > MilliSeconds (300))
    {
      if (m_ccDownlinkTraffic.find(dlInfo.componentCarrierId) == m_ccDownlinkTraffic.end())
        {
          m_ccDownlinkTraffic.insert (std::pair<uint8_t, uint32_t> (dlInfo.componentCarrierId, dlInfo.sizeTb1 + dlInfo.sizeTb2));
        }
      else
        {
          m_ccDownlinkTraffic[dlInfo.componentCarrierId]+= (dlInfo.sizeTb1 + dlInfo.sizeTb2);
        }
    }
}

void
RrSdlComponentCarrierManagerTestCase::UlScheduling (uint32_t frameNo, uint32_t subframeNo, uint16_t rnti,
                                       uint8_t mcs, uint16_t sizeTb, uint8_t componentCarrierId)
{
  //NS_LOG_FUNCTION (frameNo << subframeNo << rnti << (uint32_t) mcs << sizeTb);
  // need to allow for RRC connection establishment + SRS transmission
  if (Simulator::Now () > MilliSeconds (300))
    {
      if (m_ccUplinkTraffic.find(componentCarrierId) == m_ccUplinkTraffic.end())
        {
          m_ccUplinkTraffic.insert (std::pair<uint8_t, uint32_t> (componentCarrierId, sizeTb));
        }
      else
        {
          m_ccUplinkTraffic[componentCarrierId] += sizeTb;
        }
    }
}


// ===== rr-sdl-use-scell-opp test ==================================== //


RrSdlCcmUseScellOpportunisticallyTestSuite::RrSdlCcmUseScellOpportunisticallyTestSuite ()
  : TestSuite ("rr-sdl-use-scell-opp", SYSTEM)
{

  // when PRB occupancy and RLC queues size thresholds are set to 0, the data load condition will always be satisfied
  AddTestCase (new RrSdlCcmUseScellOpportunisticallyTestCase ("Test activation and de-activation of scell", 2, 0, 0, false, 1, 0, LTE, 80, 80),TestCase::QUICK);
  // test that GBR goes only over primary
  AddTestCase (new RrSdlCcmUseScellOpportunisticallyTestCase ("Test activation and de-activation of scell", 2, 0, 0, true, 1, 0 , LTE, 80, 80),TestCase::QUICK);
  // test that there are maximum 2 UEs scheduled per TTI
  AddTestCase (new RrSdlCcmUseScellOpportunisticallyTestCase ("Test activation and de-activation of scell", 2, 0, 0, false, 6, 2, LTE, 80, 80),TestCase::QUICK);
  // test that T_ON=0 when all UEs get deactivated
  AddTestCase (new RrSdlCcmUseScellOpportunisticallyTestCase ("Test activation and de-activation of scell", 2, 0, 0, false, 6, 2, LTE_U, 80, 80),TestCase::QUICK);

} // end of RrSdlCcmUseScellOpportunisticallyTestSuite::RrSdlCcmUseScellOpportunisticallyTestSuite

static RrSdlCcmUseScellOpportunisticallyTestSuite rrSdlCcmUseScellOpportunisticallyTestSuite;


/*
 * Test Case
 */

RrSdlCcmUseScellOpportunisticallyTestCase::RrSdlCcmUseScellOpportunisticallyTestCase (
  std::string name, uint8_t numberOfComponentCarriers, double prbOccupancyThreshold, uint64_t rlcQueuesSizeThreshold, bool gbrFlow, int nUes, int maxNUesPerTti,
  Config_e scellType, uint32_t maxInterMibTime, uint32_t ldsInterval )
  : TestCase (name),
    m_numberOfComponentCarriers (numberOfComponentCarriers),
    m_prbOccupancyThreshold (prbOccupancyThreshold),
    m_rlcQueuesSizeThreshold (rlcQueuesSizeThreshold),
    m_gbrFlow (gbrFlow),
    m_nUes (nUes),
    m_maxNUesPerTti (maxNUesPerTti),
    m_scellType (scellType),
    m_maxInterMibTime (maxInterMibTime),
    m_ldsInterval (ldsInterval)
{
  NS_LOG_INFO (this << " name=" << name);

  m_lastTransportTime = Simulator::Now();
  // account for l1 mesurement report delay which is by default 200ms + 6ms delay of measurement message transmission/reception and 20ms delay of BSR at eNB.
  m_l1MeasReportIntervalDelay = MilliSeconds (200) + MilliSeconds (6) + MilliSeconds (20);
  m_secondaryActivityCounterDl = 0;
  m_secondaryActivityCounterUl = 0;
  m_secondaryExpectedActive = true;
  m_activeOnTime = false;
  m_currentCsatStarts = Seconds (0);
  m_currentCsatEnds = Seconds (0);
  m_lteUCarrierMibCounter = 0;
  m_lteUCarrierLdsCounter = 0;
}

RrSdlCcmUseScellOpportunisticallyTestCase::~RrSdlCcmUseScellOpportunisticallyTestCase ()
{
  NS_LOG_FUNCTION (this);
}

void
RrSdlCcmUseScellOpportunisticallyTestCase::DoRun ()
{
  NS_LOG_INFO (this << " " << GetName ());

  Config::SetDefault ("ns3::LteHelper::UseCa", BooleanValue (true));
  Config::SetDefault ("ns3::LteHelper::NumberOfComponentCarriers", UintegerValue (m_numberOfComponentCarriers));
  // set round robin suplemental downlink component carrier manager
  Config::SetDefault ("ns3::LteHelper::EnbComponentCarrierManager", StringValue ("ns3::RrSdlComponentCarrierManager"));
  // disable SRS in the uplink of the secondary carriers
  Config::SetDefault ("ns3::LteUePhy::DisableSrsOnSecondaryCarriers", BooleanValue (true));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteHelper::UseIdealRrc", BooleanValue (true));
  Config::SetDefault ("ns3::LteEnbRrc::DefaultTransmissionMode", UintegerValue (2));
  Config::SetDefault ("ns3::LteHelper::PathlossModel",StringValue ("ns3::FriisSpectrumPropagationLossModel"));
  //Disable Uplink Power Control
  Config::SetDefault ("ns3::LteUePhy::EnableUplinkPowerControl", BooleanValue (false));

  Config::SetDefault ("ns3::RrSdlComponentCarrierManager::UseSCellOpportunistically", BooleanValue (true));
  Config::SetDefault ("ns3::RrSdlComponentCarrierManager::PositiveOffset", DoubleValue (0));
  Config::SetDefault ("ns3::RrSdlComponentCarrierManager::RsrpActivateThreshold", DoubleValue (50));
  Config::SetDefault ("ns3::RrSdlComponentCarrierManager::RsrpDeactivateThreshold", DoubleValue (40));
  // data load parameters
  Config::SetDefault ("ns3::RrSdlComponentCarrierManager::PrpOccupancyThreshold", DoubleValue (m_prbOccupancyThreshold));
  Config::SetDefault ("ns3::RrSdlComponentCarrierManager::RlcQueuesThreshold", UintegerValue (m_rlcQueuesSizeThreshold));
  // enable PRB occupancy reporting at scheduler
  Config::SetDefault ("ns3::RrFfMacScheduler::PrbOccupancyReportInterval", UintegerValue (200));
  Config::SetDefault ("ns3::RrFfMacScheduler::MaxUesPerTti", UintegerValue (m_maxNUesPerTti));

  // configure MIB and LDS interval times
  Config::SetDefault ("ns3::LteUCoexistenceManager::MaxInterMibTime", UintegerValue (m_maxInterMibTime));
  Config::SetDefault ("ns3::LteEnbPhy::LdsPeriod", UintegerValue (m_ldsInterval));

  Config::SetDefault ("ns3::LteEnbPhy::ChannelAccessManagerStartTime", TimeValue (Seconds (0)));

  // Create Nodes: eNodeB and UE
  NodeContainer enbNodes;
  NodeContainer ueNodes;
  enbNodes.Create (1);
  ueNodes.Create (m_nUes);

  /*
   * The topology is the following:
   *
   * eNodeB     UE
   *    |       |
   *    x ----- x ------------------------- --- ------------------- x
   *         VeryNear                                             VeryFar
   */

  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0)); // eNodeB
  for (int i=0; i< m_nUes; i++)
    {
      positionAlloc->Add (Vector (100.0, i, 0.0)); // UE
    }
  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetPositionAllocator (positionAlloc);
  mobility.Install (enbNodes);
  mobility.Install (ueNodes);
  for (int i=0; i< m_nUes; i++)
    {
       m_ueMobilityVector.push_back(ueNodes.Get (i)->GetObject<MobilityModel> ());
    }

  // Disable layer-3 filtering
  Config::SetDefault ("ns3::LteEnbRrc::RsrpFilterCoefficient", UintegerValue (0));

  // Create Devices and install them in the Nodes (eNB and UE)
  NetDeviceContainer enbDevs;
  NetDeviceContainer ueDevs;

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (255444));
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (100));
  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");
  lteHelper->SetSchedulerAttribute ("UlCqiFilter", EnumValue (FfMacScheduler::PUSCH_UL_CQI));
  enbDevs = lteHelper->InstallEnbDevice (enbNodes);

  // if secondary cells are of type LTE_U configure them accordingly
  if (m_scellType == LTE_U)
    {
      // Specify some physical layer parameters that will be used in scenario helper
      PhyParams phyParams;
      phyParams.m_bsTxGain = 5; // dB antenna gain
      phyParams.m_bsRxGain = 5; // dB antenna gain
      phyParams.m_bsTxPower = 18; // dBm
      phyParams.m_bsNoiseFigure = 5; // dB
      phyParams.m_ueTxGain = 0; // dB antenna gain
      phyParams.m_ueRxGain = 0; // dB antenna gain
      phyParams.m_ueTxPower = 18; // dBm
      phyParams.m_ueNoiseFigure = 9; // dB
      phyParams.m_channelNumber = 36;
      phyParams.m_receivers = 2;
      phyParams.m_transmitters = 2;
      Ptr<LteUWifiCoexistenceHelper> lteUWifiCoexistenceHelper = CreateObject<LteUWifiCoexistenceHelper> ();
      ConfigureLteU (lteUWifiCoexistenceHelper, enbDevs, phyParams);
    }

  ueDevs = lteHelper->InstallUeDevice (ueNodes);

  // Attach UE to eNodeB
  for (int i=0; i< m_nUes; i++)
    {
       lteHelper->Attach (ueDevs.Get (i), enbDevs.Get (0));
    }

  // Activate an EPS bearer
  enum EpsBearer::Qci q;
  if (m_gbrFlow)
    {
      q = EpsBearer::GBR_CONV_VOICE;
    }
  else
    {
      q = EpsBearer::NGBR_VOICE_VIDEO_GAMING;
    }
  EpsBearer bearer (q);
  lteHelper->ActivateDataRadioBearer (ueDevs, bearer);

  // Connect to trace sources
  Config::Connect ("/NodeList/*/DeviceList/*/ComponentCarrierMap/*/LteEnbMac/DlScheduling",
                    MakeBoundCallback (&LteTestDlSchedulingCallback2, this));

  Config::Connect ("/NodeList/*/DeviceList/*/ComponentCarrierMap/*/LteEnbMac/UlScheduling",
                    MakeBoundCallback (&LteTestUlSchedulingCallback2, this));

  // specific test cases when the secondary carrier is LTE_U
  if (m_scellType == LTE_U)
    {
        Config::Connect ("/NodeList/0/DeviceList/*/ComponentCarrierMap/1/LteEnbPhy/CtrlMsgTransmission",
                            MakeBoundCallback (&LteTestCtrlMessageCheckCallback, this));

        Config::Connect ("/NodeList/0/DeviceList/*/ComponentCarrierMap/1/LteEnbPhy/ChannelAccessManager/ActiveOnTimeExtendedTrace",
                            MakeBoundCallback (&LteTestActiveOnTimeCallback, this));
    }
  /*
   * Schedule "teleports"
   *          +-------------------+-------------------+---------> time
   *           0-500     500-1000  1000-1500  1500-2000  2000-2500
   * VeryNear |_________          ___________           ___________
   *  VeryFar |         __________           ___________
   */
  Simulator::Schedule (MilliSeconds (500),
                       &RrSdlCcmUseScellOpportunisticallyTestCase::TeleportVeryFar, this);
  Simulator::Schedule (MilliSeconds (1000),
                       &RrSdlCcmUseScellOpportunisticallyTestCase::TeleportVeryNear, this);
  Simulator::Schedule (MilliSeconds (1500),
                       &RrSdlCcmUseScellOpportunisticallyTestCase::TeleportVeryFar, this);
  Simulator::Schedule (MilliSeconds (2000),
                       &RrSdlCcmUseScellOpportunisticallyTestCase::TeleportVeryNear, this);
  lteHelper->EnableDlMacTraces();
  lteHelper->EnableDlPhyTraces();
  // Run simulation
  Simulator::Stop (Seconds (2.500));
  Simulator::Run ();


  // If the simulation duration is 2.5 seconds and the secondary carrier starts to be active around 300ms, we expect to have at least (2.5 - 0.3)/mib_interval MIBs and (2.5 - 0.3)/lds_interval LDSs.
  if (m_scellType == LTE_U)
    {
        NS_TEST_EXPECT_MSG_GT (m_lteUCarrierLdsCounter, (2500 - 300)/m_ldsInterval, "Lds was not being sent with the minimum mandatory interval.");
        NS_TEST_EXPECT_MSG_GT (m_lteUCarrierMibCounter, (2500 - 300)/m_maxInterMibTime, "Mib was not being sent with the minimum mandatory interval.");
    }

  Simulator::Destroy ();

} // end of void RrSdlCcmUseScellOpportunisticallyTestCase::DoRun ()


void
RrSdlCcmUseScellOpportunisticallyTestCase::TeleportVeryNear ()
{
  NS_LOG_FUNCTION (this);
  for (uint32_t i = 0; i< m_ueMobilityVector.size(); i++)
    {
      m_ueMobilityVector.at(i)->SetPosition (Vector (100.0, i, 0.0));
    }
  m_secondaryExpectedActive = true;
  m_lastTransportTime = Simulator::Now();
}


void
RrSdlCcmUseScellOpportunisticallyTestCase::TeleportVeryFar ()
{
  NS_LOG_FUNCTION (this);
  for (uint32_t i = 0; i< m_ueMobilityVector.size(); i++)
      {
         m_ueMobilityVector.at(i)->SetPosition (Vector (4000.0, i, 0.0));
      }

  if (m_secondaryExpectedActive)
    {
       m_secondaryExpectedActive = false;

       if (m_gbrFlow)
         {
           NS_TEST_ASSERT_MSG_EQ (m_secondaryActivityCounterDl, 0 , "Secondary carrier was active in downlink when the flow is GBR!");
         }
       else
         {
          // check if it was really active
           if (m_scellType!=LTE_U)
             {
               NS_TEST_ASSERT_MSG_GT (m_secondaryActivityCounterDl, 0 , "Secondary carrier was not active in downlink!");
             }
         }
       NS_TEST_ASSERT_MSG_EQ (m_secondaryActivityCounterUl, 0 , "Secondary carrier was active in uplink!");
       // reset counters of activity of secondary carrier
       m_secondaryActivityCounterDl = 0;
       m_secondaryActivityCounterUl = 0;
    }

  m_lastTransportTime = Simulator::Now();
}


void
RrSdlCcmUseScellOpportunisticallyTestCase::DlScheduling (DlSchedulingCallbackInfo dlInfo)
{
  NS_LOG_FUNCTION (this);
  NS_TEST_ASSERT_MSG_EQ ((uint16_t)dlInfo.componentCarrierId!=0 && m_gbrFlow, false , "Flow is GBR and secondary carrier found active! Time:"<<Simulator::Now().GetMilliSeconds());

  if ( Simulator::Now() > m_lastTransportTime + m_l1MeasReportIntervalDelay )
    {
      if (m_secondaryExpectedActive == false)
        {
          bool lteUCsatInProgress = false;
          if (m_scellType == LTE_U && Simulator::Now() < m_currentCsatEnds && m_activeOnTime)
            {
              lteUCsatInProgress = true;
            }

          // if the secondary is not LTE-U that is the middle of csat cycle, proceed to the regular test case check
          if (!lteUCsatInProgress)
            NS_TEST_ASSERT_MSG_EQ ((uint16_t)dlInfo.componentCarrierId, 0 , "Secondary carrier in DL should be shut down already! Time:"<<Simulator::Now().GetMilliSeconds());
        }
      else
        {
          if (dlInfo.componentCarrierId!=0)
            {
               m_secondaryActivityCounterDl ++;
            }
        }
    }

  if (((m_subframeNo == dlInfo.subframeNo) && (m_frameNo == dlInfo.frameNo)))
    {

      if (m_nUesDlCounter.find(dlInfo.componentCarrierId)!= m_nUesDlCounter.end())
        {
          m_nUesDlCounter [dlInfo.componentCarrierId] = m_nUesDlCounter [dlInfo.componentCarrierId] + 1;

          NS_TEST_ASSERT_MSG_LT_OR_EQ ((uint16_t) m_nUesDlCounter[dlInfo.componentCarrierId], m_maxNUesPerTti, "There is more Ues per Tti than allowed in the downlink.");
        }
      else
        {
          m_nUesDlCounter.insert(std::pair <uint8_t, uint8_t>(dlInfo.componentCarrierId,1));
        }
    }
  else
    {
      m_nUesDlCounter.clear();
      m_nUesUlCounter = 0;
      m_subframeNo = dlInfo.subframeNo;
      m_frameNo = dlInfo.frameNo;
    }

}

void
RrSdlCcmUseScellOpportunisticallyTestCase::UlScheduling (uint32_t frameNo, uint32_t subframeNo, uint16_t rnti,
                                       uint8_t mcs, uint16_t sizeTb, uint8_t componentCarrierId)
{
   NS_LOG_FUNCTION (this);
   NS_TEST_ASSERT_MSG_EQ((uint16_t) componentCarrierId, 0 , "Secondary carrier in UL should be down always."<<Simulator::Now().GetMilliSeconds());

   if ( (m_subframeNo == subframeNo) && (m_frameNo == frameNo))
     {
         m_nUesUlCounter ++;
         NS_TEST_ASSERT_MSG_LT_OR_EQ((uint16_t)m_nUesUlCounter, m_maxNUesPerTti, "There is more Ues per Tti than allowed in the downlink.");
     }
   else
     {
       m_nUesDlCounter.clear();
       m_nUesUlCounter = 0;
       m_subframeNo = subframeNo;
       m_frameNo = frameNo;
     }
}


void
RrSdlCcmUseScellOpportunisticallyTestCase::CtrlMessageCheck (std::list<Ptr<LteControlMessage> > ctrlMsg)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_INFO(this<<"Ctrl message at:"<<Now().GetMicroSeconds()<<", count:"<<ctrlMsg.size());

  if (Simulator::Now() >= MilliSeconds(300))
    {
      // check MIB and LDS intervals
      if (ctrlMsg.size () > 0)
        {
          int counterOtherCtrl = 0;
          bool mib = false;
          bool lds = false;

          std::list<Ptr<LteControlMessage> >::iterator it;
          it = ctrlMsg.begin ();
          while (it != ctrlMsg.end ())
            {
              Ptr<LteControlMessage> msg = (*it);
              if (msg->GetMessageType() == LteControlMessage::MIB)
                {
                  m_lteUCarrierMibCounter++;

                  mib = true;
                  if (m_lastMibSent!=Seconds(0))
                    {
                      NS_TEST_ASSERT_MSG_LT_OR_EQ(Simulator::Now().GetMilliSeconds()- m_lastMibSent.GetMilliSeconds(), m_maxInterMibTime, "Mib was not sent for :"<<Simulator::Now()- m_lastMibSent);
                    }
                  m_lastMibSent = Simulator::Now();
                }
              else if (msg->GetMessageType() == LteControlMessage::LDS)
                {
                  m_lteUCarrierLdsCounter++;
                  lds = true;
                  if (m_lastLdsSent!=Seconds(0))
                    {
                      NS_TEST_ASSERT_MSG_LT_OR_EQ(Simulator::Now().GetMilliSeconds()- m_lastLdsSent.GetMilliSeconds(), m_ldsInterval, "Lds was not sent for :"<<Simulator::Now()- m_lastLdsSent);
                    }
                  m_lastLdsSent = Simulator::Now();
                }
              else
                {
                  counterOtherCtrl++;
                }
              it++;
            }

          if (Simulator::Now()> m_currentCsatStarts && Simulator::Now()< m_currentCsatEnds && !mib && !lds)
             {
               if (m_activeOnTime == false && counterOtherCtrl > 0)
                 {                  
                   NS_TEST_ASSERT_MSG_EQ (m_activeOnTime, true, "When secondary cell is T_ON is set to 0, only mandatory MIB and LDS can be sent.");
                 }
              }
        }
    }
}

void RrSdlCcmUseScellOpportunisticallyTestCase::ActiveOnTimeCheck (bool activeOnTime, Time csatEndTime)
{
   NS_LOG_FUNCTION (this);
  // do not change the variables if we are still inside of the csat in which they will hold
   m_activeOnTime = activeOnTime;
   m_currentCsatStarts = Simulator::Now();
   m_currentCsatEnds = csatEndTime;
}
