function generate_sinr_to_mi_tables;

format long;


% Creating tables for 64QAM
firstValue=0.25;
incrementValue=0.10;
a=zeros(1,3800);

for i=1:3800
    a(1,i)=firstValue+(i-1)*incrementValue;
end

b=mi_mapping(a,6);

dlmwrite('x_axis.txt',a);
dlmwrite('y_axis.txt',b);
