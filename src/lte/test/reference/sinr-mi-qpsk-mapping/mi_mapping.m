% Implemenation of MI function as defined in paper: "A Lightweight and Accurate Link Abstraction Model for 
% the Simulation of LTE Networks in ns-3" authored by Marco Mezzavilla, 
% Marco Miozzo, Michele Rossi, % Nicola Baldo, Michele Zorzi.

function f=mi_maping(sinr,modOrder)
digits(6)
s=sqrt(sinr)

if (modOrder==2)
f=j_function(2*s)
elseif (modOrder==4)
f=1/2*j_function(0.8*s)+1/4*j_function(2.17*s)+1/4*j_function(0.965*s)
elseif (modOrder==6)
f=1/3*(j_function(1.47*s)+j_function(0.529*s)+j_function(0.366*s))
end

%round to 6 decimal
f=round(f*1000000)/1000000

