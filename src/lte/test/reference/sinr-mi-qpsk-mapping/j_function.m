% Implemenation of j function used for calculation of MI values as 
% defined in paper: "A Lightweight and Accurate Link Abstraction Model for 
% the Simulation of LTE Networks in ns-3" authored by Marco Mezzavilla, 
% Marco Miozzo, Michele Rossi, % Nicola Baldo, Michele Zorzi.

function y=j_function(t)

a1=-0.04210661;
b1=0.209252;
c1=-0.00640081;

a2=0.00181492;
b2=-0.142675;
c2=-0.0822054;
d2=0.0549608;

y=t;

for r=1:size(t,1)
    for c=1:size(t,2)          
        x=t(r,c);
        if (x < 0.001) 
            y(r,c)=0;
        elseif (x >= 0.001) && (x < 1.6363)
            y(r,c)=a1*x.^3+b1*x.^2+c1*x;            
        elseif (x>=1.6363) && (x<=50)
            y(r,c)=1-exp(a2*x^3 + b2*x^2+c2*x+d2);    
        else
            y(r,c)=1;    
        end
    end
end








