/*
 * lte-test-effective-sinr-CQI-mapping.cc
 *
 *  Created on: Nov 12, 2015
 *      Author: biljkus
 */


/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es>
 */

#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/string.h"
#include "ns3/double.h"
#include "ns3/boolean.h"
#include <ns3/enum.h>

#include "ns3/mobility-helper.h"
#include "ns3/lte-helper.h"

#include "ns3/lte-ue-phy.h"
#include "ns3/lte-ue-net-device.h"
#include "ns3/lte-enb-net-device.h"

#include <ns3/lte-chunk-processor.h>
#include <ns3/lte-mi-error-model.h>
#include <ns3/math.h>

#include <ns3/lte-spectrum-value-helper.h>

#include "lte-test-effective-sinr-to-cqi-mapping.h"


using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("LteEffectiveSinrToCqiMappingTest");

/**
 * Test 1.3 Link Adaptation
 */


/**
  * Table of CQI index and its spectral efficiency. Taken from 3GPP TSG-RAN WG1
  * [R1-081483 Conveying MCS and TB size via PDCCH]
  * (http://www.3gpp.org/ftp/tsg_ran/WG1_RL1/TSGR1_52b/Docs/R1-081483.zip)
  * file `TBS_support.xls` tab "MCS Table" (rounded to 2 decimal digits).
  * The index of the vector (range 0-15) identifies the CQI value.
  */
  static const double SpectralEfficiencyForCqi[16] = {
   0.0, // out of range
   0.15, 0.23, 0.38, 0.6, 0.88, 1.18,
   1.48, 1.91, 2.41,
   2.73, 3.32, 3.9, 4.52, 5.12, 5.55
 };

  /**
   * Table of MCS index and its spectral efficiency. Taken from 3GPP TSG-RAN WG1
   * [R1-081483 Conveying MCS and TB size via PDCCH]
   * (http://www.3gpp.org/ftp/tsg_ran/WG1_RL1/TSGR1_52b/Docs/R1-081483.zip)
   * file `TBS_support.xls` tab "MCS Table" (rounded to 2 decimal digits).
   * The index of the vector (range 0-31) corresponds to the MCS index according
   * to the convention in TS 36.213 (i.e., the MCS index reported in R1-081483
   * minus one)
   */

  static const double SpectralEfficiencyForMcs[32] = {
    0.15, 0.19, 0.23, 0.31, 0.38, 0.49, 0.6, 0.74, 0.88, 1.03, 1.18,
    1.33, 1.48, 1.7, 1.91, 2.16, 2.41, 2.57,
    2.73, 3.03, 3.32, 3.61, 3.9, 4.21, 4.52, 4.82, 5.12, 5.33, 5.55,
    0, 0, 0
  };

 /**
   * Test vectors: SNRDB, Spectral Efficiency, MCS index
   * From XXX
   */

struct SnrEfficiencyMcs
{
  double  snrDb;
  double  efficiency;
  int  mcsIndex;
};


 SnrEfficiencyMcs snrEfficiencyMcs[] = {
   { -5.00000,  0.08024,        -1},
   { -4.00000,  0.10030,        -1},
   { -3.00000,  0.12518,        -1},
   { -2.00000,  0.15589,        0},
   { -1.00000,  0.19365,        0},
   { 0.00000,   0.23983,        2},
   { 1.00000,   0.29593,        2},
   { 2.00000,   0.36360,        2},
   { 3.00000,   0.44451,        4},
   { 4.00000,   0.54031,        4},
   { 5.00000,   0.65251,        6},
   { 6.00000,   0.78240,        6},
   { 7.00000,   0.93086,        8},
   { 8.00000,   1.09835,        8},
   { 9.00000,   1.28485,        10},
   { 10.00000,  1.48981,        12},
   { 11.00000,  1.71229,        12},
   { 12.00000,  1.95096,        14},
   { 13.00000,  2.20429,        14},
   { 14.00000,  2.47062,        16},
   { 15.00000,  2.74826,        18},
   { 16.00000,  3.03560,        18},
   { 17.00000,  3.33115,        20},
   { 18.00000,  3.63355,        20},
   { 19.00000,  3.94163,        22},
   { 20.00000,  4.25439,        22},
   { 21.00000,  4.57095,        24},
   { 22.00000,  4.89060,        24},
   { 23.00000,  5.21276,        26},
   { 24.00000,  5.53693,        26},
   { 25.00000,  5.86271,        28},
   { 26.00000,  6.18980,        28},
   { 27.00000,  6.51792,        28},
   { 28.00000,  6.84687,        28},
   { 29.00000,  7.17649,        28},
   { 30.00000,  7.50663,        28},
 };

void
LteTestDlSchedulingCallback (LteEffectiveSinrToCqiMappingTestCase *testcase, std::string path,
                             uint32_t frameNo, uint32_t subframeNo, uint16_t rnti,
                             uint8_t mcsTb1, uint16_t sizeTb1, uint8_t mcsTb2, uint16_t sizeTb2)
{
  testcase->DlScheduling (frameNo, subframeNo, rnti, mcsTb1, sizeTb1, mcsTb2, sizeTb2);
}

/**
 * TestSuite
 */

LteEffectiveSinrToCqiMappingTestSuite::LteEffectiveSinrToCqiMappingTestSuite ()
  : TestSuite ("lte-effective-sinr", SYSTEM)
{
  NS_LOG_INFO ("Creating LteLinkAdaptionTestSuite");


  int numOfTests = sizeof (snrEfficiencyMcs) / sizeof (SnrEfficiencyMcs);


  double txPowerDbm = 30; // default eNB TX power over whole bandwidth
  double ktDbm = -174;    // reference LTE noise PSD
  double noisePowerDbm = ktDbm + 10 * std::log10 (25 * 180000); // corresponds to kT*bandwidth in linear units
  double receiverNoiseFigureDb = 9.0; // default UE noise figure


  for ( int i = 1 ; i < numOfTests; i++ )
    {
	double lossDb = txPowerDbm - snrEfficiencyMcs[i].snrDb - noisePowerDbm - receiverNoiseFigureDb;
	std::ostringstream name;
	name << " snr= " << snrEfficiencyMcs[i].snrDb << " dB, "
	<< " mcs= " << snrEfficiencyMcs[i].mcsIndex;
	AddTestCase (new LteEffectiveSinrToCqiMappingTestCase (name.str (),  snrEfficiencyMcs[i].snrDb, lossDb, snrEfficiencyMcs[i].mcsIndex), TestCase::QUICK);
    }
}

static LteEffectiveSinrToCqiMappingTestSuite lteEffectiveSinrToCqiMappingTestSuite;


/**
 * TestCase
 */

LteEffectiveSinrToCqiMappingTestCase::LteEffectiveSinrToCqiMappingTestCase (std::string name, double snrDb, double loss, int mcsIndex)
  : TestCase (name),
    m_snrDb (snrDb),
    m_loss (loss),
    m_mcsIndex (mcsIndex)
{
  std::ostringstream sstream1, sstream2;
  sstream1 << " snr=" << snrDb
           << " mcs=" << mcsIndex;

  m_mcsIndex = mcsIndex;
  NS_LOG_INFO ("Creating LteEffectiveSinrToCqiMappingTestCase: " + sstream1.str ());
}

LteEffectiveSinrToCqiMappingTestCase::~LteEffectiveSinrToCqiMappingTestCase ()
{
}

void
LteEffectiveSinrToCqiMappingTestCase::DoRun (void)
{
  Config::Reset ();

  std::cout<<"\n "<<GetName();

  // Enable effective sinr calculation for CQI
  Config::SetDefault ("ns3::LteHelper::EffectiveSinrCalculation", BooleanValue (true));
  Config::SetDefault ("ns3::LteHelper::UseIdealRrc", BooleanValue (true));
  Config::SetDefault ("ns3::LteAmc::AmcModel", EnumValue (LteAmc::PiroEW2010));
  Config::SetDefault ("ns3::LteAmc::Ber", DoubleValue (0.00005));
  Config::SetDefault ("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue (2));
  //Disable Uplink Power Control
  Config::SetDefault ("ns3::LteUePhy::EnableUplinkPowerControl", BooleanValue (false));

  /**
    * Simulation Topology
    */

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
//   lteHelper->EnableLogComponents ();
  lteHelper->EnableMacTraces ();
  lteHelper->EnableRlcTraces ();
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::ConstantSpectrumPropagationLossModel"));
  NS_LOG_INFO ("SNR = " << m_snrDb << "  LOSS = " << m_loss);
  lteHelper->SetPathlossModelAttribute ("Loss", DoubleValue (m_loss));

  // Create Nodes: eNodeB and UE
  NodeContainer enbNodes;
  NodeContainer ueNodes;
  enbNodes.Create (1);
  ueNodes.Create (1);
  NodeContainer allNodes = NodeContainer ( enbNodes, ueNodes );

  // Install Mobility Model
  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (allNodes);

  // Create Devices and install them in the Nodes (eNB and UE)
  NetDeviceContainer ueDevs;
  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");
  NetDeviceContainer enbDevs = lteHelper->InstallEnbDevice (enbNodes);
  ueDevs = lteHelper->InstallUeDevice (ueNodes);

  // Attach a UE to a eNB
  lteHelper->Attach (ueDevs, enbDevs.Get (0));

  // Activate the default EPS bearer
  enum EpsBearer::Qci q = EpsBearer::NGBR_VIDEO_TCP_DEFAULT;
  EpsBearer bearer (q);
  lteHelper->ActivateDataRadioBearer (ueDevs, bearer);

  // Use testing chunk processor in the PHY layer
  // It will be used to test that the SNR is as intended
  Ptr<LtePhy> uePhy = ueDevs.Get (0)->GetObject<LteUeNetDevice> ()->GetPhy ()->GetObject<LtePhy> ();
  Ptr<LteAverageChunkProcessor> testSinr = Create<LteAverageChunkProcessor> ();
  LteSpectrumValueCatcher sinrCatcher;
  testSinr->AddCallback (MakeCallback (&LteSpectrumValueCatcher::ReportValue, &sinrCatcher));
  uePhy->GetDownlinkSpectrumPhy ()->AddCtrlSinrChunkProcessor (testSinr);

  Config::Connect ("/NodeList/0/DeviceList/0/LteEnbMac/DlScheduling",
                   MakeBoundCallback (&LteTestDlSchedulingCallback, this));

  /*
   * Calculate expected values for MCS according to PIRO model.
   * Calculate using the original LTE model calculation
   */
  Ptr<LteEnbNetDevice> lteEnb = enbDevs.Get(0)->GetObject<LteEnbNetDevice>();
  Ptr<SpectrumModel> sm = LteSpectrumValueHelper::GetSpectrumModel (lteEnb->GetDlEarfcn(), lteEnb->GetDlBandwidth());
  Ptr<SpectrumValue> test = Create<SpectrumValue>(sm);
  *test = exp10(m_snrDb/10);
  std::list<LteListChunkProcessor::Chunk> sinrListChunk;
  LteListChunkProcessor::Chunk sinrChunk = LteListChunkProcessor::Chunk (test, Seconds(1));
  sinrListChunk.push_back(sinrChunk);

  Ptr<SpectrumValue> effectiveSinrValue = LteMiErrorModel::CalculateEffectiveSinr(sinrListChunk, LteMiErrorModel::MOD6);
  double effectiveSinr = *(effectiveSinrValue->ValuesBegin());
  double ber = 0.00005;
  double sEffectiveCqiCalc = log2 ( 1 + ( effectiveSinr / ( (-std::log (5.0 * ber )) / 1.5) ));

  // perform calculation of MCS based on PIRO model

  int effectiveCqiCalc = 0;
  while ((effectiveCqiCalc < 15) && (SpectralEfficiencyForCqi[effectiveCqiCalc + 1] < sEffectiveCqiCalc))
    {
      ++effectiveCqiCalc;
    }

  double sForEffectiveCQI = SpectralEfficiencyForCqi[effectiveCqiCalc];
  m_effectiveMcsCalc = 0;
  while ((m_effectiveMcsCalc < 28) && (SpectralEfficiencyForMcs[m_effectiveMcsCalc + 1] <= sForEffectiveCQI))
    {
      ++m_effectiveMcsCalc;
    }

  double sForOriginalCqiCalc = log2 ( 1 + ( exp10(m_snrDb/10) / ( (-std::log (5.0 * ber )) / 1.5) ));
  int originalCqi = 0;
  while ((originalCqi < 15) && (SpectralEfficiencyForCqi[originalCqi + 1] < sForOriginalCqiCalc))
    {
      ++originalCqi;
    }

  double sOriginalCqi = SpectralEfficiencyForCqi[originalCqi];
  m_originalMcsCalc = 0;
  while ((m_originalMcsCalc < 28) && (SpectralEfficiencyForMcs[m_originalMcsCalc + 1] <= sOriginalCqi))
    {
      ++m_originalMcsCalc;
    }

  //std::cout<<"\nsinr="<<exp10(m_snrDb/10)<<"  MCS="<<orMcs<<", eSINR="<<effectiveSinr<<" eMCS="<<m_effectiveMcsCalc;

  Simulator::Stop (Seconds (0.040));
  Simulator::Run ();


  double calculatedSinrDb = 10.0 * std::log10 (sinrCatcher.GetValue ()->operator[] (0));
  NS_TEST_ASSERT_MSG_EQ_TOL (calculatedSinrDb, m_snrDb, 0.0000001, "Wrong SINR !");
  Simulator::Destroy ();
}


void
LteEffectiveSinrToCqiMappingTestCase::DlScheduling (uint32_t frameNo, uint32_t subframeNo, uint16_t rnti,
                                         uint8_t mcsTb1, uint16_t sizeTb1, uint8_t mcsTb2, uint16_t sizeTb2)
{

  static bool firstTime = true;

  if ( firstTime )
    {
      firstTime = false;
      NS_LOG_INFO ("SNR\tRef_MCS\tCalc_MCS");
    }

//    std::cout<<"\n"<<Simulator::Now().GetSeconds()<<" expected MCS="<<m_effectiveMcsCalc<<"  "<<" found mcs:"<<(uint32_t)mcsTb1;

  /**
   * Note:
   * the MCS can only be properly evaluated after:
   * RRC connection has been completed and
   * CQI feedback is available at the eNB.
   */
  if (Simulator::Now ().GetSeconds () > 0.030)
    {
      NS_LOG_INFO ("\n DL MCS:"<<(uint16_t)mcsTb1 << "\t calculated MCS based on effective SINR CQI calculation:" <<m_effectiveMcsCalc << m_effectiveMcsCalc<<"\t calculated MCS using original CQI calculation:"<<m_originalMcsCalc);
      NS_TEST_ASSERT_MSG_EQ ((uint16_t)mcsTb1, m_effectiveMcsCalc , "Wrong MCS index");
      NS_TEST_ASSERT_MSG_EQ_TOL( (uint16_t)mcsTb1, m_originalMcsCalc, 2 , "Effective MCS is too different from original MCS.");
    }
}




