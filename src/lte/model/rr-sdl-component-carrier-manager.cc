/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.cat>
 */

#include "rr-sdl-component-carrier-manager.h"
#include <ns3/log.h>
#include <ns3/random-variable-stream.h>
#include <ns3/boolean.h>
#include <ns3/double.h>
#include "ns3/trace-helper.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("RrSdlComponentCarrierManager");
NS_OBJECT_ENSURE_REGISTERED (RrSdlComponentCarrierManager);

void
RrSdlComponentCarrierManager::DoInitialize()
{
  NS_LOG_FUNCTION (this);

  RrComponentCarrierManager::DoInitialize ();

  if (m_useSCellOpportunistically)
    {
      NS_LOG_LOGIC (this << " requesting Event A1 measurements"
                << " (threshold = 0" << ")");
      LteRrcSap::ReportConfigEutra reportConfig;
      reportConfig.eventId = LteRrcSap::ReportConfigEutra::EVENT_A1;
      reportConfig.threshold1.choice = LteRrcSap::ThresholdEutra::THRESHOLD_RSRP;
      //reportConfig.threshold1.range = 0;
      reportConfig.threshold1.range = m_rsrpActivateThreshold + m_positiveOffset;
      reportConfig.triggerQuantity = LteRrcSap::ReportConfigEutra::RSRP;
      reportConfig.reportInterval = LteRrcSap::ReportConfigEutra::MS120;
      reportConfig.timeToTrigger = 0;
      m_measIdA1 = m_ccmRrcSapUser->AddUeMeasReportConfigForComponentCarrier (reportConfig);

      NS_LOG_LOGIC (this << " requesting Event A2 measurements"
                   << " (threshold = 0" << ")");
      reportConfig.eventId = LteRrcSap::ReportConfigEutra::EVENT_A2;
      reportConfig.threshold1.choice = LteRrcSap::ThresholdEutra::THRESHOLD_RSRP;
      //reportConfig.threshold1.range = 0;
      reportConfig.threshold1.range = m_rsrpDeactivateThreshold + m_positiveOffset;
      reportConfig.triggerQuantity = LteRrcSap::ReportConfigEutra::RSRP;
      reportConfig.reportInterval = LteRrcSap::ReportConfigEutra::MS120;
      reportConfig.timeToTrigger = 0;
      m_measIdA2 = m_ccmRrcSapUser->AddUeMeasReportConfigForComponentCarrier (reportConfig);
    }

  DoCheckIfThereAreActiveUes();

}

RrSdlComponentCarrierManager::RrSdlComponentCarrierManager ():
        m_measIdA1(0),
        m_measIdA2 (0),
        m_useSCellOpportunistically (0),
        m_positiveOffset (0),
        m_rsrpActivateThreshold (0),
        m_rsrpDeactivateThreshold (0),
        m_prbOccupancyThreshold (0),
        m_rlcQueuesThreshold (0),
        m_rlcQueuesValueExpireTimer (Seconds(0)),
        m_checkIfActiveUesInterval (Seconds(0))
{
  NS_LOG_FUNCTION (this);
}

RrSdlComponentCarrierManager::~RrSdlComponentCarrierManager ()
{
  NS_LOG_FUNCTION (this);
}

TypeId
RrSdlComponentCarrierManager::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::RrSdlComponentCarrierManager")
     .SetParent<NoOpComponentCarrierManager> ()
     .SetGroupName("Lte")
     .AddConstructor<RrSdlComponentCarrierManager> ()
     .AddAttribute("UseSCellOpportunistically",
                   "If true it will be enabled automatic activation and de-activation of secondary "
                   "carriers based on UE measurements and PRB usage.",
                   BooleanValue (true),
                   MakeBooleanAccessor(&RrSdlComponentCarrierManager::m_useSCellOpportunistically),
                   MakeBooleanChecker())
     .AddAttribute("PositiveOffset",
                   "The offset that is added to the RSRP thresholds",
                   DoubleValue (10.0),
                   MakeDoubleAccessor(&RrSdlComponentCarrierManager::m_positiveOffset),
                   MakeDoubleChecker<double>())
     .AddAttribute("RsrpActivateThreshold",
                   "The RSRP activate threshold used decide when to activate the secondary carriers.",
                   DoubleValue (45.0),
                   MakeDoubleAccessor(&RrSdlComponentCarrierManager::m_rsrpActivateThreshold),
                   MakeDoubleChecker<double>())
     .AddAttribute("RsrpDeactivateThreshold",
                   "The RSRP deactivate threshold used to decide when to deactivate the secondary "
                   "carriers.",
                   DoubleValue (35.0),
                   MakeDoubleAccessor(&RrSdlComponentCarrierManager::m_rsrpDeactivateThreshold),
                   MakeDoubleChecker<double>())
     .AddAttribute("PrpOccupancyThreshold",
                   "The prb occupancy threshold used to detect when is necessary to activate "
                   "secondary carriers. It is expressed as percentage.",
                   DoubleValue (90),
                   MakeDoubleAccessor(&RrSdlComponentCarrierManager::m_prbOccupancyThreshold),
                   MakeDoubleChecker<double>(0,100))
     .AddAttribute("RlcQueuesThreshold",
                   "The rlc queues threshold, used to detect when is necessary to activate "
                   "secondary carriers. "
                   "Expressed in number of bytes.",
                   UintegerValue (0),
                   MakeUintegerAccessor(&RrSdlComponentCarrierManager::m_rlcQueuesThreshold),
                   MakeUintegerChecker<uint64_t>())
     .AddAttribute("RlcQueuesValueExpireTimer",
                   "After how much time to remove the rlc queue size information from a certain "
                   "flow if it was not updated.",
                   TimeValue (MilliSeconds(100)),
                   MakeTimeAccessor(&RrSdlComponentCarrierManager::m_rlcQueuesValueExpireTimer),
                   MakeTimeChecker())
     .AddAttribute("CheckIfActiveUesInterval",
                  "How often to check if there are active users on secondary carriers.",
                   TimeValue (MilliSeconds(100)),
                   MakeTimeAccessor(&RrSdlComponentCarrierManager::m_checkIfActiveUesInterval),
                   MakeTimeChecker())
     .AddAttribute("RetxOverPrimaryImpl",
                  "Which implementation option to use for retransmission only over primary.",
                   EnumValue (OptionC),
                   MakeEnumAccessor(&RrSdlComponentCarrierManager::m_retxOverPrimaryImplChoice),
                   MakeEnumChecker(OptionA, "OptionA",
                                   OptionB, "OptionB",
                                   OptionC, "OptionC"));
      return tid;
}

void
RrSdlComponentCarrierManager::DoUlReceiveMacCe (MacCeListElement_s bsr, uint8_t componentCarrierId)
{
  NS_LOG_FUNCTION (this);
  NS_ASSERT_MSG (componentCarrierId == 0, "Received BSR from a ComponentCarrier not allowed, ComponentCarrierId = " << componentCarrierId);
  NS_ASSERT_MSG (bsr.m_macCeType == MacCeListElement_s::BSR, "Received a Control Message not allowed " << bsr.m_macCeType);
  // notify only primary, uplink goes over primary in this implementation
  NS_ASSERT_MSG (m_ccmMacSapProviderMap.find(0)!=m_ccmMacSapProviderMap.end(), "Mac sap provider does not exist.");
  m_ccmMacSapProviderMap[0]->ReportMacCeToScheduler(bsr);
}

void
RrSdlComponentCarrierManager::DoReportUeMeas (uint16_t rnti, LteRrcSap::MeasResults measResults)
{
  NS_LOG_FUNCTION (this << rnti << (uint16_t) measResults.measId);

  if (m_useSCellOpportunistically)
    {
      // event based triggering is leveraging only on primary cell measurements, however the measurement report will
      // provide measurements of all carriers, in root of measResults structure are placed primary cell measurements,
      // while in ints substructure measResults.measScellResultsList are provided measurements per carrier
      if (measResults.measId == m_measIdA1)
        {
          NS_ASSERT_MSG (measResults.rsrpResult >= m_rsrpActivateThreshold + m_positiveOffset, "Invalid UE measurement report");
          NS_LOG_INFO (" Reported measurement A1 at: "<<Simulator::Now().GetMilliSeconds()<<" rsrp:"<< (uint32_t) measResults.rsrpResult<<" activate.");
          m_sCellActivationPerUe [rnti] = true;
        }
      else if (measResults.measId == m_measIdA2)
        {
          NS_ASSERT_MSG (measResults.rsrpResult <= m_rsrpDeactivateThreshold + m_positiveOffset, "Invalid UE measurement report");
          NS_LOG_INFO (" Reported measurement A2 at : "<<Simulator::Now().GetMilliSeconds()<<" rsrp:"<<(uint32_t)measResults.rsrpResult<< " deactivate.");
          m_sCellActivationPerUe [rnti] = false;
        }
      else
        {
          NS_LOG_WARN ("Ignoring measId " << (uint16_t) measResults.measId);
        }
    }
}

void RrSdlComponentCarrierManager::DoAddUe (uint16_t rnti, uint8_t state)
{
  RrComponentCarrierManager::DoAddUe (rnti, state);
  // at the beginning secondary carrier is disabled, it will be activated only if signal quality
  // fulfills the requirements defined by activation threshold, and if there is data available to
  // schedule
  m_sCellActivationPerUe.insert (std::pair <uint16_t, bool > (rnti, false));
}


bool RrSdlComponentCarrierManager::IsGbr (LteMacSapProvider::ReportBufferStatusParameters params)
{
  NS_ASSERT_MSG (m_rlcLcInstantiated.find(params.rnti) != m_rlcLcInstantiated.end(), "Trying to check the QoS of unknown UE");
  NS_ASSERT_MSG (m_rlcLcInstantiated.find(params.rnti)->second.find(params.lcid) != m_rlcLcInstantiated.find(params.rnti)->second.end(), "Trying to check the QoS of unknown logical channel");
  return m_rlcLcInstantiated.find(params.rnti)->second.find(params.lcid)->second.isGbr;
}

void
RrSdlComponentCarrierManager::RemoveQueueInfo (LteFlowId_t flowId)
{
   NS_ASSERT_MSG (m_flowIdQueue.find (flowId)!=m_flowIdQueue.end(), "Tried to remove a non-existing flow.");
   m_flowIdQueue.erase ( m_flowIdQueue.find (flowId));
}

uint64_t
RrSdlComponentCarrierManager::GetRlcQueuesTotalSize ()
{
  uint64_t retVal = 0;

  for (std::map<LteFlowId_t, uint64_t>::iterator it = m_flowIdQueue.begin(); it != m_flowIdQueue.end(); it++ )
    {
      retVal += it->second;
    }
  return retVal;
}

double
RrSdlComponentCarrierManager::GetPrbOccupancy ()
{
  double retVal = 0;

  if (m_ccPrbOccupancy.find(0)!= m_ccPrbOccupancy.end())
    {
      retVal = m_ccPrbOccupancy.find(0)->second;
    }

  return retVal;
}

void
RrSdlComponentCarrierManager::RlcRetxBufferSizeChanged (uint16_t rnti, uint8_t lcid, uint32_t oldValue, uint32_t newValue)
{
  if (m_rntiToRlcRetxQueueSizeList.find(rnti) == m_rntiToRlcRetxQueueSizeList.end())
    {
      std::map <uint8_t, uint32_t> newMap;
      newMap.insert(std::pair<uint8_t, uint32_t> (lcid,newValue));
      m_rntiToRlcRetxQueueSizeList[rnti] = newMap;
    }
  else
    {
      m_rntiToRlcRetxQueueSizeList[rnti][lcid] = newValue;
    }
}

void
RrSdlComponentCarrierManager::UpdateQueueSizeMeasurements (LteMacSapProvider::ReportBufferStatusParameters params)
{
  NS_LOG_FUNCTION (this);
  LteFlowId_t flowId = LteFlowId_t (params.rnti, params.lcid);
  EventId eventId = Simulator::Schedule (m_rlcQueuesValueExpireTimer, &RrSdlComponentCarrierManager::RemoveQueueInfo, this, flowId);
  // cancel the expiration timer
  if ( m_flowIdExpireTimer.find(flowId)!= m_flowIdExpireTimer.end())
    {
      Simulator::Cancel(eventId);
      m_flowIdExpireTimer.erase(m_flowIdExpireTimer.find(flowId));
    }
  // update buffer status information for the flow
  m_flowIdQueue.insert(std::pair <LteFlowId_t, uint64_t> (flowId, params.txQueueSize + params.retxQueueSize));
  // create an expiration timer
  m_flowIdExpireTimer.insert(std::pair<LteFlowId_t, EventId> (flowId,eventId));
}

std::vector<LteCcmRrcSapProvider::LcsConfig>
RrSdlComponentCarrierManager::DoSetupDataRadioBearer (EpsBearer bearer, uint8_t bearerId, uint16_t rnti, uint8_t lcid, uint8_t lcGroup, LteMacSapUser* msu)
{
  NS_LOG_FUNCTION(this);
  std::vector<LteCcmRrcSapProvider::LcsConfig> lcsConfig = RrComponentCarrierManager::DoSetupDataRadioBearer (bearer, bearerId, rnti, lcid, lcGroup, msu);

  EnumValue epsBearerToRlcMapping;
  m_rrc->GetAttribute ("EpsBearerToRlcMapping", epsBearerToRlcMapping);
  // if in acknowledged mode then we want to connect our function to retransmission buffer trace
  if (epsBearerToRlcMapping.Get() == LteEnbRrc::RLC_AM_ALWAYS)
    {
      Ptr<LteRlc> rlc = m_rrc->GetRlc (rnti,lcid);
      if (rlc)
        {
          ns3::Callback <void, uint16_t, uint8_t, uint32_t, uint32_t > callback =
              MakeCallback(&RrSdlComponentCarrierManager::RlcRetxBufferSizeChanged, this);
          rlc->TraceConnectWithoutContext("RetxBufferSize",callback.TwoBind (rnti, lcid));
        }
      else
        {
          NS_LOG_ERROR("Could not find RLC");
        }

      // configure if RLC needs to chech through which carrier to forward the traffic
      if (m_retxOverPrimaryImplChoice == OptionC)
        {
          rlc->SetAttribute ("RetxOverPrimaryCc", BooleanValue (true));
        }
      else
        {
          rlc->SetAttribute ("RetxOverPrimaryCc", BooleanValue (false));
        }
    }
  return lcsConfig;
}

void
RrSdlComponentCarrierManager::DoCheckIfThereAreActiveUes()
{
  NS_LOG_FUNCTION (this);
  bool activeUes = false;
  for ( std::map <uint16_t, bool>::iterator it = m_sCellActivationPerUe.begin(); it != m_sCellActivationPerUe.end(); it++)
    {
      // UE is considered active only if rsrp and data conditions are fulfilled
      if (it->second && (GetRlcQueuesTotalSize () > m_rlcQueuesThreshold || GetPrbOccupancy() > m_prbOccupancyThreshold))
        {
          activeUes = true;
          break;
        }
    }
  // notify RRC who notifies PHY and CAM if transmission should be enabled on secondary carriers
  if (activeUes)
    {
      m_ccmRrcSapUser->EnableScellTx();
    }
  else
    {
      m_ccmRrcSapUser->DisableScellTx();
    }
  Simulator::Schedule (m_checkIfActiveUesInterval, &RrSdlComponentCarrierManager::DoCheckIfThereAreActiveUes, this);
}

uint32_t
RrSdlComponentCarrierManager::GetRlcRetxBufferSize (uint16_t rnti, uint8_t lcid)
{
  uint32_t retval = 0;
  if (m_rntiToRlcRetxQueueSizeList.find(rnti) != m_rntiToRlcRetxQueueSizeList.end())
    {
      if (m_rntiToRlcRetxQueueSizeList[rnti].find(lcid) != m_rntiToRlcRetxQueueSizeList[rnti].end())
       {
          retval = m_rntiToRlcRetxQueueSizeList[rnti][lcid];
       }
    }
  return retval;
}

void
RrSdlComponentCarrierManager::DoReportBufferStatus (LteMacSapProvider::ReportBufferStatusParameters params)
{
  NS_LOG_FUNCTION (this);
  NS_ASSERT_MSG (m_sCellActivationPerUe.find(params.rnti) != m_sCellActivationPerUe.end(),"SCell activation state not found for rnti:"<<params.rnti);

  if (m_useSCellOpportunistically)
    {
      // update queues
      UpdateQueueSizeMeasurements (params);
      bool isSCellActivatedForThisUser = m_sCellActivationPerUe.find(params.rnti)->second && (GetRlcQueuesTotalSize() >= m_rlcQueuesThreshold || GetPrbOccupancy() >= m_prbOccupancyThreshold);
      NS_LOG_DEBUG ("Is secondary cell activated for UE?" << (m_sCellActivationPerUe.find(params.rnti)->second) );
      NS_LOG_DEBUG ("Is PRB occupancy higher than the threshold?" << (GetPrbOccupancy() > m_prbOccupancyThreshold) << " PRB occupancy:"<< GetPrbOccupancy() );
      NS_LOG_DEBUG ("Is total RLC occupancy higher than the threshold?" << (GetRlcQueuesTotalSize() > m_rlcQueuesThreshold)<< "RLC Queue size:"<< GetRlcQueuesTotalSize() );

      // transmission only over primary if a) RLC retransmission, b) is GBR traffic
      // the secondary carriers may be used only when a) RSRP requirements are fulfilled
      // and b) data is available (this is considered to be the case when PRB or RLC
      // queue size condition is satisfied)

      if (!isSCellActivatedForThisUser)
        {
          NS_LOG_DEBUG ("Forward BSR only to primary. Secondary cell is not activated for this UE.");
          m_macSapProvidersMap.find(0)->second->ReportBufferStatus(params);
        }
      else if (params.lcid == 0 || params.lcid==1)
        {
          NS_LOG_INFO("Forward BSR only to primary. Signaling bearer SRB0, SRB1.");
          m_macSapProvidersMap.find(0)->second->ReportBufferStatus(params);
        }
      else if (IsGbr(params))
        {
          NS_LOG_DEBUG ("Forward BSR only to primary. The flow is GBR.");
          m_macSapProvidersMap.find(0)->second->ReportBufferStatus(params);
        }
      else
        {
          // if there is something to be retransmitted, and the implementation option is A then forward BSR only to primary
          if ((m_retxOverPrimaryImplChoice == OptionA) && (GetRlcRetxBufferSize (params.rnti, params.lcid) > 0))
            {
              //Implementation option A for retransmission only over primary: forward BSR only to primary
              NS_LOG_DEBUG ("Forward BSR only to primary. RLC retx queue size:"<<params.retxQueueSize);
              m_macSapProvidersMap.find(0)->second->ReportBufferStatus(params);
            }
          else // forward BSR to primary and secondary
            {
              // Implementation option B/C for retransmission only over primary: create different buffer status reports for each carrier,
              // and intercept and block TxOpportunity coming from secondary carrier (option B), or disable retx over secondary directly in RLC (option C).
              NS_LOG_DEBUG ("Forward BSR to primary and secondary.");
              LteMacSapProvider::ReportBufferStatusParameters paramsForPrimary = params;
              LteMacSapProvider::ReportBufferStatusParameters paramsForSecondary = params;
              uint32_t numberOfCarriersForUe = m_enabledComponentCarrier.find(params.rnti)->second;

              // prepare BSR for primary carrier, retransmission buffer size will be forwarded only to primary carrier
              paramsForPrimary.txQueueSize /= numberOfCarriersForUe;
              m_macSapProvidersMap.find(0)->second->ReportBufferStatus(paramsForPrimary);

              // prepare BSR for secondary carriers, retransmission buffer size will be set to zero, since retransmission goes only to primary
              paramsForSecondary.retxQueueSize = 0;
              paramsForSecondary.txQueueSize /= numberOfCarriersForUe;

              // check if there is something in transmission buffer and status before forwarding
              if (paramsForSecondary.txQueueSize > 0 || paramsForSecondary.statusPduSize > 0 )
                {
                  for ( uint16_t i = 1;  i < numberOfCarriersForUe ; i++)
                    {
                      NS_ASSERT_MSG (m_macSapProvidersMap.find(i)!=m_macSapProvidersMap.end(), "Mac sap provider does not exist.");
                      m_macSapProvidersMap.find(i)->second->ReportBufferStatus(paramsForSecondary);
                    }
                }
            }
        }
    }
  else
    {
      NS_LOG_DEBUG ("Forward BSR to primary and secondary. Not using sCell opportunistic mode.");
      RrComponentCarrierManager::DoReportBufferStatus (params);
    }
}

void
RrSdlComponentCarrierManager::DoNotifyTxOpportunity (uint32_t bytes, uint8_t layer, uint8_t harqId, uint8_t componentCarrierId, uint16_t rnti, uint8_t lcid)
{
  NS_LOG_FUNCTION (this);
  std::map <uint16_t, std::map<uint8_t, LteMacSapUser*> >::iterator rntiIt = m_ueAttached.find (rnti);
  NS_ASSERT_MSG (rntiIt != m_ueAttached.end (), "could not find RNTI" << rnti);

  std::map<uint8_t, LteMacSapUser*>::iterator lcidIt = rntiIt->second.find (lcid);
  NS_ASSERT_MSG (lcidIt != rntiIt->second.end (), "could not find LCID " << (uint16_t) lcid);

  // part of implementation option B for retransmission only over primary, if the BSR is forwarded to all carriers, then it has to be checked when TxOpportunity arrives if it should be forwarded to RLC
  // check if the user needs to retransmit, and transmission opportunity comes from some of secondary carriers, do not forward this transmission opportunity to RLC
  if (m_retxOverPrimaryImplChoice == OptionB)
    {
      if ((GetRlcRetxBufferSize(rnti, lcid) > 0) && (componentCarrierId != 0))
      {
        NS_LOG_DEBUG ("Do not forward transmission opportunity from secondary carrier since there is data in the retransmission queue and Option B is used.");
        return;
      }
    }

  NS_LOG_DEBUG (this << " rnti= " << rnti << " lcid= " << (uint32_t) lcid << " layer= " << (uint32_t)layer<<" ccId="<< (uint32_t)componentCarrierId);
  (*lcidIt).second->NotifyTxOpportunity (bytes, layer, harqId, componentCarrierId, rnti, lcid);

}

} // end of namespace ns3
