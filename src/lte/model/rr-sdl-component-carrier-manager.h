/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.cat>
 *
 */

#ifndef RR_SDL_COMPONENT_CARRIER_MANAGER_H
#define RR_SDL_COMPONENT_CARRIER_MANAGER_H

#include <ns3/no-op-component-carrier-manager.h>
#include <ns3/lte-ccm-rrc-sap.h>
#include <ns3/lte-rrc-sap.h>
#include <ns3/lte-rlc.h>
#include <map>

namespace ns3 {
  class UeManager;
  class LteCcmRrcSapProvider;

/**
 * \brief Component Carrier Manager that splits traffic equally among component
 * carriers in the downlink when the secondary carriers of UE are activated,
 * while the uplink traffic goes always over the primary component carrier. If
 * the downlink traffic is GBR it will be transmitted only over the primary
 * carrier. Also RLC retransmissions (in RLC acknowledged mode) will by
 * transmitted over the primary carrier. Secondary carrier will be enabled only
 * if the RSRP measurements fulfill the threshold constraints for activation,
 * and if the data requirement is fulfilled, i.e. physical resource block
 * occupancy.
 */

enum RetxOverPrimaryImpl_e {
  OptionA, // forward BSR only to primary if the RLC retransmission queue is not
           // empty
  OptionB, // forward BSR to primary and secondary (retransmission only to primary,
           // and the transmission queue divide among primary and secondary),
           // intercept transmission opportunities, i.e. if coming from secondary
           // forward only if the RLC retransmission queue is empty
  OptionC  // forward BSR to primary and secondary as in Option B, but do not
           // intercept transmission opportunities, let RLC decide how to use
           // transmission opportunity coming from the carrier
};

class RrSdlComponentCarrierManager : public RrComponentCarrierManager
{
  public:

    RrSdlComponentCarrierManager ();
    virtual ~RrSdlComponentCarrierManager ();
    static TypeId GetTypeId ();

    /*
     * \brief Implements a callback function that can be hooked to the RLC
     * retransmission buffer size trace source.
     */
    void RlcRetxBufferSizeChanged (uint16_t rnti, uint8_t lcid, uint32_t oldValue, uint32_t newValue);

  protected:

    // Inherited methods
    virtual void DoInitialize (void);
    virtual void DoAddUe (uint16_t rnti, uint8_t state);

    /*
     * \brief Redefined to forward BSR only to the primary carrier.
     */
    virtual void DoUlReceiveMacCe (MacCeListElement_s bsr, uint8_t componentCarrierId);

    /*
     * \brief Based on the measurements report, activates or deactivates the secondary carrier of the user.
     */
    virtual void DoReportUeMeas (uint16_t rnti, LteRrcSap::MeasResults measResults);

    /*
     * \brief This function contains most of the RrSdlComponentCarrierManager logic.
     */
    void DoReportBufferStatus (LteMacSapProvider::ReportBufferStatusParameters params);

    /*
     * \brief Intercepts function calls from MAC of component carriers when it notifies RLC
     * of transmission opportunities. This function decides id the transmission opportunity
     * will be forwarded to the RLC.
     */
    virtual void DoNotifyTxOpportunity (uint32_t bytes, uint8_t layer, uint8_t harqId, uint8_t componentCarrierId, uint16_t rnti, uint8_t lcid);

    /*
     * \brief Returns the total size of RLC queues.
     */
    uint64_t GetRlcQueuesTotalSize ();

    /*
     * \brief Returns PRB occupancy metric of the primary carrier.
     */
    double GetPrbOccupancy ();

    /*
     * \brief Performs periodical update of queue size metric.
     */
    void UpdateQueueSizeMeasurements (LteMacSapProvider::ReportBufferStatusParameters params);

    /*
     * \brief Overload DoSetupBadaRadioBearer to connect directly to Rlc retransmission buffer size.
     */
    virtual std::vector<LteCcmRrcSapProvider::LcsConfig> DoSetupDataRadioBearer (EpsBearer bearer, uint8_t bearerId, uint16_t rnti, uint8_t lcid, uint8_t lcGroup, LteMacSapUser* msu);

    /*
     * \brief Returns the size of retransmission queue of the flow.
     */
    uint32_t GetRlcRetxBufferSize (uint16_t rnti, uint8_t lcid);

  private:
    /*
     * \brief Removes queue information for the flow.
     * \param flowId - id of the flow for which will be remove queueu inforamation
     */
    void RemoveQueueInfo (LteFlowId_t flowId);
    /*
     * \brief Checks if the flow is is GBR.
     */
    bool IsGbr (LteMacSapProvider::ReportBufferStatusParameters params);
    /*
     * \brief Performs a periodical checks if there are active users.
     */
    void DoCheckIfThereAreActiveUes ();


    uint8_t m_measIdA1;  //!< Id of the measurement which corresponds to the event when serving cell becomes better than the threshold.
    uint8_t m_measIdA2; //!< Id of the measurement which corresponds to the event when serving cell becomes worse than the threshold.
    bool m_useSCellOpportunistically; //!< Whether to enable automatic activation and de-activation of the secondary carriers based on RSRP measurements and PRB usage.
    double m_positiveOffset; //!< // The offset that is added to the RSRP activation and de-activation thresholds.
    double m_rsrpActivateThreshold; //!<  // The RSRP activation threshold.
    double m_rsrpDeactivateThreshold; //!< // The RRSP de-activation threshold.
    std::map <uint16_t, bool> m_sCellActivationPerUe; //!< If to enable the secondary carrier for UE.
    uint16_t m_prbOccupancyThreshold;//!< The physical resource block occupancy threshold used to decide when is necessary to activate secondary carriers of UE.
    uint64_t m_rlcQueuesThreshold; //!< The RLC queues threshold used to decide when is necessary to activate the secondary carriers of UE.
    Time m_rlcQueuesValueExpireTimer; //!< The RLC queues size timer.
    Time m_checkIfActiveUesInterval; //!< The timer of the check if the secondary carriers are active.
    std::map<LteFlowId_t, uint64_t> m_flowIdQueue; //!< The map that holds the information about the queue size per flows, the queue size represents the sum of the transmission and the retransmission queue.
    std::map<LteFlowId_t, EventId > m_flowIdExpireTimer; //!< The map that holds the expiration timer event id for each flow, when queue information is updated the timer is being canceled.
    std::map <uint16_t, std::map<uint8_t, uint32_t> > m_rntiToRlcRetxQueueSizeList; //!< The map that holds the size of rlc retransmission queue per flow (rnti, lcid).
    enum RetxOverPrimaryImpl_e m_retxOverPrimaryImplChoice; //!< The implementation choice for retransmissions over primary, for more information see description of RetxOverPrimaryImpl enum values.

};
} // end of namespace ns3

#endif /* RR_SDL_COMPONENT_CARRIER_MANAGER_H */
