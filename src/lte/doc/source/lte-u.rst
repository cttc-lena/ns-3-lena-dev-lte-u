LAA Wi-Fi Coexistence Module Documentation
-----------------------------------------

.. include:: replace.txt
.. highlight:: cpp


.. heading hierarchy:
   ------------- Document Title
   ************* Chapter (#)
   ============= Section (#.#)
   ############# Subsection (#.#.#)
   ~~~~~~~~~~~~~ Paragraph (no number)
   
.. toctree:: 4

Introduction
************

This document presents the |ns3| support for LTE Unlicensed (LTE-U) systems 
developed in the context of the SCALAA project.

The LTE in unlicensed (LTE-U) system, provides support to simulate with |ns3| 
the coexistence behavior between the LTE-U variant in the 5GHz band and WiFi 
systems, following the description of [LTEUForum]_ and [TR36889]_, for what it 
applies. The support for simulating the LTE and Wi-Fi technology is provided by 
the separate |ns3| LTE and Wi-Fi modules. As a result of a previous 
collaboration with the Wi-Fi Alliance (WFA), |ns3| already provides a 
``laa-wifi-coexistence`` module which focuses on the 
additional features that are needed to create a simulation scenario that 
accounts for the coexistence of the Wi-Fi and LTE technologies in the 5 GHz 
band. The source code for the LAA Wi-Fi Coexistence module lives in the 
directory ``src/laa-wifi-coexistence``. This code is the starting point for 
the LTE-U implementation. The LTE and Wi-Fi modules of |ns3|, as stand-alone 
modules, are separately documented in other documents. This document primarily 
concerns itself with extensions and tests to support LTE-U implementations. 
Over time, extensions found in this module may migrate to the existing |ns3| 
main development tree.

The rest of this document is organized into five major chapters:

2. **Design:** Described how the LTE and LAA WiFi Coexistence modules have 
been extended to support LTE-U study and implementation.

3. **Usage:** Documents how users may run and extend LTE-U functionalities 
themselves.

4. **Validation:** Documents how the models and scenarios have been verified 
and validated by test programs.

5. **Results:** Documents preliminary results from the LTE-U implementation and 
how to reproduce them.

6. **Open Issues and Future Work:** Describes topics for which future work on 
model enhancements is recommended, or for which questions on interpretations of 
standards documents may be listed.

Design
******
In the following, we document the design decisions and modeling assumptions on 
which the LTE-U implementation is based.

Packet-level simulation
=======================

Simulation tools for network performance evaluation differ on the scope, or 
level of abstraction, for simulations. Tradeoffs exist between increasing 
fidelity in particular aspects of the system, and reducing resource 
requirements of the simulation, in terms of memory usage, computational time, 
and wall-clock runtime necessary to conduct the simulation.  
On the one hand, this may mean that particular tools are unsuitable to answer 
specific questions. A tool that abstracts the physical layer properties of the 
system too much will not be useful to study subtleties of the design or 
performance of the physical layer. However, a tool that provides high fidelity 
of the physical layer, down to the symbol level with ray tracing propagation 
models, will not be useful to study large-scale wireless questions such as 
the interference properties of thousands of nodes coexisting in a stadium 
or arena.  

Network simulators are sometimes categorized as being physical-layer simulators, 
packet-level simulators, and session-level simulators.  One way to think of 
this is to think of the granularity of modeling fidelity as being at the symbol, 
(sub)carrier, or propagation ray level for physical-layer simulators, at the 
frame or packet level for packet-level simulators, and at the flow or session 
level (i.e. individual packets are not modeled) for session-level simulators. 
In the following, we clarify how these concept apply to the |ns3| Wi-Fi and 
LTE models.

Wi-Fi model granularity
#######################

The |ns3| Wi-Fi model is a packet-level simulation model.
This means that ns-3 provides modeling granularity down to the PDU level at the 
boundary of the MAC and PHY, but the PDUs are not further decomposed into bits 
or symbols such as in an actual implementation.  

In |ns3|, physical layer effects are abstracted and accounted for by
the modeling assumptions in the channel and the receiver.  |ns3| Wi-Fi Packets
(logically, a Wi-Fi frame but encapsulated in an ns3::Packet data structure)
are sent through |ns3| Channel objects that schedule the Packet for arrival on 
a receiver attached to the channel; the receive time is scheduled based on the 
transmission delay (data rate) and propagation delay between the two stations. 
These Packets contain information about the assumed modulation and coding rate 
used on the channel, as well as a value to track the signal power, but the 
channel and receiver model the physical layer effects by adjusting the signal
and noise levels according to statistical loss models and abstractions of
antennas.  When it comes time to decide whether a frame is received, the
signal-to-noise/interference levels are used to lookup error rates 
corresponding to the modulation and coding being modeled (and possibly
the antenna configuration and other effects) to make decisions such as
"Was the preamble decoded and sync obtained?" or "Was the payload received
error-free?".

Above the physical layer, the Wi-Fi MAC is modeled with relatively high
accuracy, since the unit of granularity in |ns3| (the frame, in this case)
is the same unit of granularity dealt with in actual implementations and
in the standard.

Physical layer simulators can be used to provide a number of error-rate
tables used in simulators such as |ns3|, through a technique known
as ``link-to-system-mapping``, but the key thing to understand is that,
at the physical layer, everything in the modeled system is oriented towards
obtaining an estimate of a received frame's SINR, or accounting for 
signal energy in the system for CCA modeling.  The frame's SINR may 
then be used to decide on the probability of successful reception, and 
above this MAC/PHY boundary, the |ns3| modeling granularity permits 
a fuller modeling.

One research group developed a higher fidelity Wi-Fi physical layer simulator
for |ns3|, called ``PhySimWifi`` [PhySimWifi]_.  However, this has not been
separately maintained or adopted by the main |ns3| project, partly because
it takes quite a long time to run simulations of even the simplest
configurations.


LTE model granularity
#####################

The |ns3| LTE model is a  simulation model that has a granularity
corresponding to a Resource Block (RB) on the frequency domain and to
the LTE subframe structure in the time domain. In LTE, a RB is the
unit used for the allocation of radio resources, corresponding to a
set of 12 subcarriers (180 kHz total). According to the standard
[TR36814]_, the downlink control frame starts at the beginning of each
subframe and lasts up to three symbols across the whole system
bandwidth, where the actual duration is provided by the Physical
Control Format Indicator Channel (PCFICH). The information on the
allocation are then mapped in the remaining resource up to the
duration defined by the PCFICH, in the so called Physical Downlink
Control Channel (PDCCH). A PDCCH transports a single message called
Downlink Control Information (DCI) coming from the MAC layer, where
the scheduler indicates the resource allocation for a specific user. 
The PCFICH and PDCCH are modeled with the transmission of the control
frame of a fixed duration of 3/14 of milliseconds spanning in the
whole available bandwidth, since the scheduler does not estimate the
size of the control region. This implies that a single transmission
event models the entire control frame with a fixed power (i.e., the
one used for the PDSCH) across all the available RBs. The same
simulated event is also used to model the DL Reference Signal (RS). 
The Sounding Reference Signal (SRS) transmitted in UL are modeled
similar to the downlink control frame. The SRS is periodically placed
in the last symbol of the subframe in the whole system bandwidth.  

The |ns3| LTE model implementation uses the Spectrum module to
represent the power spectral densities of transmission events with the
RB granularity and to associate to these events other data structures
specific of the LTE model that are used to simulate other PHY and MAC
aspects, such as lists of |ns3| Packets modeling the MAC PDUs being
transmitted, and other structures for conveying DCI and other LTE
control messages.


High-level design
=================
The implementation of the LTE-U functionality is implemented in the |ns3| eNB 
node. The eNB hosts a LTE device and also imports a WiFi device in order to take 
advantage of the WiFi-like functionalities already available in the WiFi device. 
The core of the LTE-U implementations is based on the definition of the LTE-U 
coexistence manager object. This object interacts with both the LTE device and 
the WiFi device. The LTE device has data available for transmission and requests 
a transmission opportunity (TxOP). The LTE-U coexistence manager takes 
information on the activity of the channel from the WiFi device, by means of 
WiFi like functionalities, such as the Energy Detection (ED), the Preamble 
Detection (PD) and the Beacon detection (BD) and after processing this 
information, defines the TxOP, i.e. the Duty cycle, which can be granted to the 
LTE device. The high level design is shown in Figure:ref:`fig-u-architecture`. 
The main mechanism by which LTE-U interacts with co-channel Wi-Fi devices 
(received by the LTE-U eNB at a power above -62 dBm) is to duty cycle the LTE-U 
cell. The LTE-U eNB completely shuts off all cell transmissions for X 
milliseconds (ms), followed by a burst of continuous LTE transmissions for Y ms, 
and so forth. X and Y refer to the LTE-U OFF and ON time, respectively, 
while the quantity X+Y is the duty cycle period. The duty cycle refers to the 
percentage of time that LTE-U is on, and thus is Y/(X+Y).

.. _fig-u-architecture:

.. figure:: figures-lteu/LTE-U_LAA_Functional_Blocks2.*
   :align: center

   LTE-U node architecture
   

Lte-U node instantiation
========================
The instantiation of LTE-U nodes can be done by using the helper class 
``LteUWifiCoexistenceHelper``. The function ``ConfigureLteUSingleWifiChannel`` 
is used to instantiate the LTE-U nodes starting from the container of LTE 
devices passed as argument. For each LTE device in the container it generates a 
corresponding LTE-U node. 

``ConfigureLteUSingleWifiChannel`` installs the Wi-Fi device in each LTE device 
by attaching it to its downlink ``SpectrumChannel`` object. As the purpose of 
the Wi-Fi device is to monitor the channel, it is configured in 
``ns::AdhocWifiMac``, which implements a Wi-Fi MAC that does not perform any 
kind of beacon generation, probing, or association. Additionally, the energy 
detection threshold to be used for all LTE-U nodes can be configured thanks to 
the ``ConfigureLteUSingleWifiChannel``. Finally, this function instantiates the 
``LteUCoexistenceManager`` for each LTE-U node and creates the correspondent 
hooks between LTE-U node and its ``LteUCoexistenceManager``. 

The first hook is implemented between the LTE device and its 
``LteUCoexistenceManage``, which takes care of the channel access mechanisms. 
This hook is bi-directional and it will be further enhanced to enable the LTE 
device to request channel access from its ``LteUCoexistenceManager``, and also 
to enable the ``LteUCoexistenceManager`` to inform the LTE device when the 
access to the channel is granted. 

The second hook is currently uni-directional and is used to enable the Wi-Fi 
device to inform the ``LteUCoexistenceManager`` of different events at Wi-Fi 
protocol stack, e.g. signal detected, preamble successfully decoded, beacon 
arrival, etc.

LTE Channel access enhancements 
===============================
To implement alternate LTE-U ON and OFF periods, the LTE eNodeB needs an 
interface to be able to ask for permissions to access the channel, and also to 
receive the grant to transmit. For this reason, a new class called 
``ChannelAccessManager`` is added to the LTE module. The implementation is 
designed in this way, in order to assure compatibility with eventual 
inclusion of Listen Before Talk (LBT) functionalities to be developed in line 
with 3GPP Release 13. The ``ChannelAccessManager`` is a generic class that 
defines and implements the basic interface for this bidirectional communication. 
``LteEnbPhy`` class is extended with an attribute that holds a pointer to its 
``ChannelAccessManager`` instance. If no channel access manager is set, the LTE 
eNodeb behaves as a legacy LTE eNB. This design choice has been taken in order 
to avoid generating unnecessary dependencies between the LTE and 
``laa-wifi-coexistence`` modules. This also allows to define different channel 
access managers by inheriting the basic ``ChannelAccessManager`` class and by 
setting the instance of channel access to the ``LteEnbPhy`` by using its 
function ``SetChannelAccessManager``. For example in the case of LTE-U the 
channel access will be always granted at the beginning of the defined ON period, 
while in a potential implementation of Licensed Assisted Access (LAA), the 
channel access will be granted subject to LBT.

LteUCoexistenceManager
======================
``LteUCoexistenceManager`` is the main class that implements LTE-U channel 
access mechanisms. This class inherits the basic ``ChannelAccessManager`` class 
and implements its functions. Currently, this class is capable of receiving the 
request for channel access from the LTE device. It is also capable of receiving 
the information from the Wi-Fi device. Hooks between the 
``LteUCoexistenceManager`` and the Wi-Fi device are implemented through two 
listeners: ``LteUPhyListener`` and ``LteUMacLowListener``. 

* The ``LteUPhyListener`` inherits the ``WifiPhyListener`` and is set to listen 
  for events on the ``SpectrumWifiPhy`` object of the Wi-Fi device of the LTE-U 
  node. The ``LteUPhyListener`` redefines the functions inherited from the 
  ``WifiPhyListener`` in order to inform the ``LteUCoexistenceManager`` of all 
  the events coming from the ``SpectrumWifiPhy`` instance. 

* The ``LteUMacLowListener`` inherits the ``MacLowDcfListener`` and its function 
  is to informs its ``LteUCoexistenceManager`` of all the events coming from the 
  ``MacLow``. Similarly to the ``LteUPhyListener``, the ``LteUMacLowListener`` 
  also redefines functions of the ``MacLowDcfListener`` in order to notify the 
  ``LteUCoexistenceManager`` of all the events from the ``MacLow``.  

``LteUCoexistenceManager`` is also implementing the CSAT algorithm and notifies 
the eNodeB PHY about the transmission opportunities. Additionally, it performs 
the monitoring of the channel ("AP scan").

Finer time resolution LTE PHY model
===================================
The pre-existing interference model provided by the |ns3| LTE module was relying 
on a simplifying assumption that all interfering signals were LTE signals and 
were synchronized at LTE subframe level. This does not allow to simulate 
interference from non-LTE signals, as is required by the simulation in a 
coexistence scenario, as it is the 5 GHz band, and so has to be a primary target 
of the LTE-U implementation. In order to overcome this limitation, the LTE model 
has to be enhanced to handle interference by signals of any type (LTE or not) 
modeled with the Spectrum framework and having arbitrary timings. The 
``LAA-WIFI-coexistence`` module provides a basic implementation to solve this 
problem, but it needs further extensions. The proposed modeling approach 
consists of evaluating the reception of LTE signals by chunks, where each chunk 
is identified by a constant power spectral density of interference.

* For error modeling purposes, SINR (Signal to Interference and Noise Ratio) 
  chunks are aggregated first in the frequency domain using the Mutual Information 
  Based Effective SINR (MIESM) model, thus obtaining a Mutual Information (MI) 
  value per chunk. Then, different chunks are aggregated in the time domain,
  weighting the MI of each chunk by the duration of the chunk, to determine 
  the overall MI of the Transport Block (TB) being transmitted. This overall 
  MI is then used to look at the error rate according to the link-to-system 
  mapping model. The existing |ns3| LTE MIESM-based error model is used for 
  this purpose.

* For modeling the reference signals, aggregation in the time domain is not 
  needed, as only RX power of the intended signal is evaluated (not interference).

* For interference power calculations, aimed at CQI feedback and interference 
  reporting, time domain linear averaging of interference value is considered. 
  In this respect, we consider that a model similar to the one implemented for 
  the data frame has to be implemented. In fact, the signal used to evaluate 
  the CQIs spans over several symbols and therefore a simple linear average cannot 
  capture the real effect of the interference of a Wi-Fi device. Contrarily, 
  the MI of the different chunks can better model this behavior. In detail, 
  similarly to what done for PCFICH + PDCCH Error Model, the SINR samples of all 
  the chunks are included in the evaluation of the MI associated and, according 
  to this values, the effective SINR (eSINR) can be obtained by inverting the MI 
  evaluation process.

In detail, the eSINR is evaluated according to this formula:

.. math::

  eSINR_{i-RB} = \theta^{-1}\left(\sum_{k=1}^{N_C} w_K MI_{i-RB}^{K}\right)

where :math:`MI_{i-RB}^{K}=\theta(SINR_{i-RB}^{K})` and :math:`\theta` is 
defined in [PaduaPEM]_. The CQIs are generated per RB basis according to Table 
7.2.3-1 of TS 36.213:

.. math::

  CQI_{i-RB} = f(eSINR_{i-RB})
  
For what concern the in-band CQIs, for a subband :math:`S` spanning RBs 
:math:`\{m, m+1, ..., m+S-1\}`, the CQI is computed by averaging, in detail:

.. math::

  CQI_{S} = \frac{\sum_{i=m}^{m+S-1}CQI_{i-RB}}{S}
  
Finally, the wideband CQIs are calculated averaging through the whole bandwidth 
:math:`N_{RB}`, in detail:

.. math::

  CQI_{W} = \frac{\sum_{i=1}^{N_{RB}}CQI_{i-RB}}{N_{RB}}


According to the legacy implementation of the LTE module, there are no 
analytical functions implemented in |ns3| for MI calculation based on SINR and 
vice versa. Rather, these functions are implemented in the LTE module by using 
vectors of MI to SINR and vice versa. These vectors are generated by using 
Matlab scripts that implement a function for MI calculation based on the SINR 
defined in [Mezzavilla]_. Hence, by generating these vectors it is possible to 
map the SINR to the MI and vice-versa, but due to having a limited number of 
values in the vectors, there will always be some residual quantization error. 

Due to this, in order to preserve regular LTE module behavior, the new finer 
time resolution model implemented is not replacing the old LTE physical model, 
but is instead possible to configure which model to use and is left to the user 
to decide if to use the old implementation of CQI calculation or the one based 
on MI and effective SINR. This can be configured through the |ns3| configuration 
system in the following way:: 

    Config::SetDefault ("ns3::LteHelper::EffectiveSinrCalculation", 
    BooleanValue(true)); 

For the common usage of the LTE module, when all devices at the same channel are 
LTE devices, the signals are synchronous, so there is no need to use this new 
finer time resolution LTE PHY model. While, in the case of asynchronous signals, 
as it is the case when LTE and Wi-Fi use the same channel, it is recommended to 
use a finer time resolution model for CQI calculations.


Wi-Fi Beacon detection for LTE
==============================

Wi-Fi-like functionalities such as beacon detection should be added in such a 
way that they do not affect the performance of |ns3| LTE module. For this reason 
Wi-Fi-like functionalities should be implemented in a separate module which will 
be using inheritance mechanisms to encapsulate full legacy LTE eNb 
functionalities, along with some physical and MAC layer Wi-Fi features. In this 
way, it will be possible to configure an eNB to be either a legacy eNB or an 
eNB that contains some Wi-Fi-like features. If the eNB has Wi-Fi-like 
functionalities, during the installation procedure of the eNB, the Wi-Fi-like 
features will be installed by adding physical and MAC layer functionalities of 
the legacy Wi-Fi device.  The beacon detection will be performed by the Wi-Fi 
MAC layer of the LTE-U eNB, and will mimic a typical behavior of Wi-Fi stations 
in listen mode. By using the LTE-U eNB interface it will be possible to attach 
listener objects that will use data provided by the beacon detection event. The 
Wi-Fi module will need to be extended to support beacon detection event 
notification to other modules.


Energy and Wi-Fi Preamble Detection for LTE
============================================
Similarly to the above, energy detection and preamble detection are part of 
Wi-Fi-like functionalities of the LTE-U node. Energy detection is performed by 
the Wi-Fi physical layer at the LTE-U eNB. In order to receive events, the 
``LteUCoexistenceManager`` registers a Wi-Fi listener. The ``LteUPhyListener`` 
forwards notification of all events happening at the ``SpectrumWifiPhy`` to the 
``LteUCoexistenceManager``. It is possible to configure the energy level 
threshold of the Wi-Fi device at the LTE-U node by setting the corresponding 
attribute of the ``ChannelAccessManager``, either by using its function 
``SetEdThreshold``, or through the |ns3| config system, in the following way::

    Config::SetDefault ("ns3::ChannelAccessManager::EnergyDetectionThreshold", 
    DoubleValue (-62.0));
  
Preamble detection will rely on the Wi-Fi MAC low functionality. Similar to the 
energy detection events forwarding, the ``LteUCoexistenceManager`` registers a 
``LteUMacLowListener``, which will forward all notifications coming from the 
``MacLow`` object of Wi-Fi device.


Reference LTE-U Implementation
==============================
The reference LTE-U implementation that we plan to implement will be based on 
the Carrier-Sensing Adaptive Transmission (CSAT) algorithm proposed by Qualcomm 
[CSATQC]_.
Based on the documentation available online on this algorithm, it is generally 
organized onto 3 phases: 

* Channel selection, during which the LTE-U cells  scan the channel and identify 
the cleanest one for transmission. The switching of carrier based on the 
occupancy is supported by Rel. 10/11 functionalities that are currently not 
available in |ns3|. The scan proposed by Qualcomm algorithm is based on energy 
detection, but also on technology-specific measurements to improve interference 
detection sensitivity and additional information collection (e.g. preamble 
detection). Similarly, a LTE network listening module is used to detect 
neighboring LTE-Us PSS, SSS and PBCH channel. In addition, device-assisted 
enhancements, such as 802.11k and UE measurements, can be used to address the 
hidden node effect and thus help to select a better channel. The way to support 
the initial channel selection has been intensively discussed during the project, 
as due to the lack of many functionalities and on the general focus of the |ns3| 
project on the connected mode, this phase was not initially included in the 
design. In the end a two simulation-phases approach has been designed and 
delivered.

* CSAT (Carrier-Sensing Adaptive Transmission): In CSAT the LTE-U cells sense 
the spectrum during the OFF period for a time that is longer than in LBT or 
CSMA, e.g. 10s ms-200ms. According to the observed medium activities, the 
algorithm gates off LTE transmission proportionally. The Wi-Fi Monitoring 
Utilization (MU) is realized when all LTE-U cells are required to turn OFF their 
5 GHz RF transmission. The MU is also averaged over multiple observations. 
CSAT algorithm increases/decreases the LTE ON period if MU is above/below a 
certain threshold (60%). there is a minimum duration of the ON period to 
guarantee LTE grants a fair share of the spectrum, and a maximum duration of the 
ON period to guarantee the OFF period is long enough to achieve a proper sensing 
of WiFi activity.  The CSAT cycles (LTE ON+OFF) will be parametric, common 
values that are considered in industry are 80 ms, 160 ms, but also higher values 
can be considered.

* Opportunistic Secondary DL.: The use of the secondary DL is activated only in 
case the DL traffic exceeds a certain threshold, otherwise the traffic is only 
managed by the primary carrier. Since carrier aggregation is not currently 
supported by |ns3|, this phase will not be implemented, and this is left for 
future work. 


Channel Selection
==============================

Channel selection functionality is a fundamental functionality to be supported 
for achieving fair coexistence in unlicensed band. This functionality was not 
included in previous work on coexistence.

The central component of the channel selection functionality is a simulation 
script that implements a scenario in which LTE-U and Wi-Fi modules coexist over 
different available channels. The main requirement of the simulation script for 
channel selection is to allow LTE-U eNBs to change the channel. To allow for 
this, the channel selection procedure works under different steps. The first 
step of the simulation allows the  LTE-U nodes to monitor the available 
channels. The second step, based on the information collected in the monitoring 
phase, performs the channel selection procedure for each LTE-U node leveraging 
data gathered during the monitoring phase. The third and final step runs the 
simulation by using the selected channel. 

The channel selection functionality is implemented in 
``channel-selection-dual-stripe.cc`` and the implementation is based on 
``lte/lena-dual-stripe.cc`` which implements 3GPP dual stripe scenario. The 
original simulation script for dual stripe scenario has been extended to allow 
to use LTE-U, besides the legacy LTE implementation, to allow their operation 
on different channels and to allow installation of Wi-Fi nodes in the scenario. 

The channels available for selection are those specified by LTE forum, mainly 
UNII-1 and UNII-3. The latest Wi-Fi module included implementation of only 
four 5GHz channels: 36. 40, 44, 48. We extended the module to support the rest 
of UNII-1 and UNII-3 channels, namely, 32, 149, 153, 157, 161, 165. Channel 
selection implementation is mainly based on two different modes of operation:
 
* `Mode 1` is the monitoring phase in which the LTE-U dual stripe is configured, 
the measurements are collected and saved into the output file. 

* `Mode 2` is the run phase in which the measurements gathered by `Mode 1` are 
used to perform channel selection for each LTE-U node and after channel 
configuration the simulation is run.

The Mode of operation is configured by using the ``configMode`` parameter in 
the channel dual stripe script. To run simulation in `Mode 1`, the parameter 
should be set to ``scanMode``, and to run it in ``Mode 2`` it should be set to 
``runMode``.


`Mode 1` or "scan mode"
#######################
 
In `Mode 1` phase the channel selection simulation script is run by using the 
following parameters:

* Channels to be used in simulation.

* Densities of LTE-U nodes per channel.

* Densities of Wi-Fi nodes per channel.

These parameters should be provided to the channel selection dual stripe script 
through a configuration file and the path to the file can be provided to the 
simulation script by using the parameter ``channelConfigFile`` which defaults 
to ``channel-config.txt``. The channel selection configuration file has the 
following structure:

    :math:`channel_1` :math:`lteDensity_1` :math:`wifiDensity_1`
    :math:`channel_2` :math:`lteDensity_2` :math:`wifiDensity_2`
    ...
    :math:`channel_n` :math:`lteDensity_n` :math:`wifiDensity_n`

The channel selection simulation can be run also without specifying the 
configuration file, and in that case the densitites of LTE-U and Wi-Fi nodes 
will be generated randomly. In order to run the simulation without the 
configuration file the parameter, ``useChannelConfigFile`` should be set to 
``false``. 

To implement the monitoring in `Mode 1` we have extended the 
``RadioEnvironmentalMapHelper`` and ``RemSpectrumPhy`` classes to support 
measuring the power received from each node in specific sets of points. Current 
implementation supports grid distribution of REM points. The implementation of 
``RemSpectrumPhy`` only supports the measurement of LTE signals. In order to 
obtain measurements also from Wi-Fi APs during the monitoring phase, the Wi-Fi 
APs are replaced by LTE-U nodes, which are configured to have equivalent 
physical parameters as the corresponding Wi-Fi APs. During the monitoring phase 
the results are stored in a complex structure in memory that contains all the 
positions of the REM, and the corresponding values of measured power and 
associated cellId. These monitoring results can be obtained by using the 
``GetPowerPerCellIdMap`` function of the ``RadioEnvironmentalMapHelper``. 
One ``RadioEnvironmentalMapHelper`` is installed per channel. Data from all 
``RadioEnvironmentaMapHelper`` instances are collected into a single output file 
with the following structure:

    :math:`position_1(x,y,z)`   :math:`channel_1`   :math:`cellId_1`  :math:`power_1`  :math:`cellId_2`  :math:`power_2` :math:`cellId_3` :math:`power_3`  ... :math:`cellId_n` :math:`power_n`
...
The location of this output file can be configured by using the 
``channelStatsFile`` parameter in the channel selection dual stripe 
simulation script.


`Mode 2` or "run mode"
######################

In `Mode 2` phase the channel simulation has to be run with the same 
configuration of channel as in `Mode 1`. This is mandatory in order to be able 
to reproduce exactly the same topology, i.e. generate exactly the same number 
of LTE-U and Wi-Fi nodes as in `Mode 1`, configure their positions in the same 
way as in `Mode 1`, and configure specific channels to each Wi-Fi access point. 
Hence, at the beginning of `Mode 2` the input configuration file is loaded, 
based on that both LTE-U cells and Wi-Fi APs are placed in the scenario and 
the topology is created. In this phase Wi-Fi APs are not replaced by LTE-U 
nodes. Wi-Fi APs are configured according to densities per channel that are 
specified in the configuration file and they have identical spatial and channel 
distribution as in `Mode 1`. On the other hand, in `Mode 2`, LTE-U cells are 
not configured to use channels specified in the ``channelConfigFile``, but the 
channel for each LTE-U cell is selected by the function ``SelectChannel``. 
This function has as parameters the REM, defined at the beginning of `Mode 2`, 
loaded from the output file of the `Mode 1`.


Sample channel selection algorithm
###################################

We have implemented a sample channel selection algorithm that selects the 
channel characterized by the lowest number of Wi-Fi and LTE-U cells. This 
algorithm is run in `Mode 2` for each LTE-U, and the information of which 
channel is selected is output parameter of the ``SelectChannel`` function. 


Summary of configuration parameters channel selection dual stripe parameters
############################################################################

*   **configMode** (used in both modes). In `Mode 1` should be set 
    to ``scanMode``, in `Mode 2` to ``runMode``.

*   **channelConfigFile** (used in both modes) defines the file 
    name or the path to the ``channelConfigFile``.

*   **config file** (used in both modes) defines which channels 
    will be used and which are initial densities of LTE-U nodes and Wi-Fi nodes 
    per channel. 

*   **channelStatsFile** (used in both modes) defines the interface 
    between the two modes, i.e., the file name or the path, where the 
    statistics per channel and cell Id are written, during `Mode 1`, 
    and from where they be loaded statistics in `Mode 2`.

*   **channel stats file** is used only in `Mode 2` as input. 


CSAT (Carrier-Sensing Adaptive Transmission)
=============================================

CSAT is the main algorithm which has been proposed for coexistence in LTE-U 
implementations. The algorithm is patented by Qualcomm. In Qualcomm public 
implementation, the algorithm mainly allows for dynamic configuration of the 
duty cycle, i.e. the percentage of time during which the LTE-U cell can be ON 
and has to be OFF. The main reference that we have used for implementing the 
algorithm is [CSATALG]_. The algorithm proposes a Time Division Multiplexed 
(TDM) transmission pattern of the LTE-U cells, based on a :math:`T_{CSAT}` 
cycle. The length of this cycle and the specific ON-OFF pattern to follow is 
selected based on transmissions measured from Wi-Fi APs. During the LTE-U OFF 
period, the channel is sensed and measurements from Wi-Fi APs and STAs are 
collected and analyzed. Differently from what presented in [CSATALG]_, in |ns3| 
implementation it is not guaranteed that the LTE-U cells switch OFF 
simultaneously. Since our implementation allows LTE-U cells to preamble detect 
Wi-Fi signals, the activity of other LTE-U cells will not prevent from detecting 
the presence and activity of Wi-Fi nodes.

Our implementation considers that at the end of every CSAT period, in 
particular, 2 ms before that each CSAT ends, the configuration parameters of 
the following CSAT period are selected based on the activity that the LTE-U 
nodes have monitored during the OFF period of the previous CSAT period. The 
LTE-U nodes carry out an estimation of the so called `Medium Utilization (MU)`, 
which is averaged across multiple observations.
MU, for each monitoring time, whose duration defaults to 20 ms, is computed as 
follows:

.. math::

  MU(n) = \frac{1}{MonitoringTime} \cdot \sum_{i=1}^{K} W_i \cdot D_i

where :math:`W_i` and :math:`D_i` denote the weight and duration of the `i-th` 
packet detected during the `n-th` monitoring time, assuming that ``K`` packets 
are detected during that monitoring time. The MU is also filtered as follows, 

.. math::

  \overline{MU(n)} = \alpha_{MU} MU (n) + (1-\alpha_{MU}) \overline{MU(n-1)}

where :math:`\alpha_{MU} = 0,8` to place more weight on current MU. 

In our implementation :math:`W_i` defaults to 1.

CSAT algorithm increases/decreases the LTE-U :math:`T_{ON}` according to the 
following rule:

.. math::
   T_{ON}(n+1) &= \min(T_{ON}(n) + \Delta_{UP}, T_{ON, max}) \qquad   if \overline{MU(n)} < MU_{Thr1}

.. math::
   T_{ON}(n+1) &= T_{ON}(n) \qquad if MU_{Thr1} \leq \overline{MU(n)} \leq MU_{Thr2}
  
.. math::
   T_{ON}(n+1) &= \max(T_{ON}(n) - \Delta_{DOWN}, T_{ON, min}) \qquad   if \overline{MU(n)} > MU_{Thr2}

where:

* :math:`\Delta_{UP}` and :math:`\Delta_{DOWN}` are configured to be equal to 
:math:`0,05 \cdot T_{CSAT}`. 
* :math:`MU_{Thr1}` and :math:`MU_{Thr2}` are equal to :math:`0,4` and 
:math:`0,6` respectively.
* :math:`T_{ON, max}` is a limit imposed on the maximum time that 
:math:`T_{ON}` can take, in order to allow for a minimum :math:`T_{OFF}` time 
during :math:`T_{CSAT}` of :math:`20 ms`.
* :math:`T_{ON, min}` is defined in the following based on the number of Wi-Fi 
APs that are operating in the environment. This value cannot be lower than 
:math:`4 ms`, according to LTE-U Forum specifications [LTEUFCSAT]_.

Every :math:`APSCAN_{period}` which is a configurable parameters that defaults 
to :math:`16 \cdot T_{CSAT}`, the transmission is interrupted during 
:math:`APSCAN_{duration}`, which is another configurable parameter that defaults 
to :math:`160 ms`. During :math:`APSCAN_{period}` the LTE-U node scans the 
medium to evaluate with how many APs it is sharing the channel. During this 
period only mandatory control signals are sent. The first AP scan will be 
performed before the first CSAT cycle, in order to obtain `NumWiFiNodes` value 
for CSAT, and it will use the obtained value until next AP scan is performed 
and hence the new value is obtained. During CSAT cycles all OFF subframes are 
divided into blocks of `Monitoring time` subframes during which information 
regarding medium utilization is obtained. In general, one CSAT cycle may contain 
several `Monitoring` blocks.

When generating the transmission pattern for the next cycle, CSAT algorithm will 
use the latest 'MU' value to update 'Duty cycle' parameter.

:math:`T_{ON, min}` is computed based on this scan as follows:

.. math::

   T_{ON, min} = \min \left\{TONMininmillisec, \frac{(N+1) \cdot T_{CSAT}}{N+1+M+NumWiFiNodes}\right\}

where:

* :math:`TONMininmillisec` is a configurable parameter aimed at guaranteeing a 
minimum duty cycle when some Wi-Fi APs may happen to be below threshold. For 
:math:`T_{CSAT}= 160 ms` a reasonable value could be :math:`100 ms`.

* In our implementation we do not support multiple operators, so :math:`M = 0` 
and :math:`N` is the number of LTE-U small cells in the scenario. The :math:`N` 
is obtained by using the Automatic Neighbor Relation (ANR) functionality of LTE 
module in ns-3.

* :math:`NumWiFiNodes` is the number of detected APs. Our implementation 
supports preamble detection, and it is able to detect Wi-Fi APs even below 
-82 dBm.

According to LTE-U Forum specifications [LTEUFCSAT]_, the maximum allowed 
consecutive ON time is :math:`20 ms`. After that, it is necessary to allow 
subframe puncturing in order to let Wi-Fi low latency traffic go through, as 
well as management traffic that may be queued at the AP. Puncturing duration is 
configurable and defaults to :math:`2 ms`, and its period is configurable too and 
defaults to :math:`20 ms`

The implementation of LTE-U CSAT considers that :math:`2 ms` before the end of a 
CSAT period, the LTE-U small cell defines a mask which establishes what are the 
subframe during which it will be ON for the next :math:`T_{CSAT}`.
Based on:

* the MU value updated during the last monitoring period of the previous CSAT 
period * the constraints imposed by the need to send signaling information like 
MIB, SIB1 and LDS, and those imposed by the puncturing 

the transmission pattern for the following :math:`T_{ON}` is computed.


Modelling of control signals
############################
|ns-3| implementation models three control signals in LTE-U transmission 
patterns: MIB, SIB1 and LDS. The modeling is based on latest LTE-U requirements 
defined in [LTEUFCSAT]_.

* According to section 5.2 in [LTEUFCSAT]_, MIB/SIB1 shall be transmitted on 
LTE-U SCell when the MIB/SIB1 transmission period coincides with a SCell 
transmission (SCell ON-state). UE may assume that the MIB is transmitted at 
least once every :math:`160 ms`. This assumes regular :math:`10 ms` transmission 
for MIB and :math:`20 ms` transmission for SIB1. As a result, in our 
implementation: whenever the SCell ON period covers subframe (SF) 0, the MIB is 
transmitted; whenever the SCell ON period covers SF 5, SIB1 is transmitted, 
if it is scheduled. If MIB transmission is not scheduled during :math:`150 ms`, 
its transmission is scheduled during the following frame, in SF 0, regardless of 
the SCell to be in ON period. This assures that MIB is transmitted at least 
every :math:`160 ms`.

* According to section 5.3 in [LTEUFCSAT]_, The LDS is defined as an instance 
of SF5 with CRS/PSS/SSS and PDCCH/PDSCH (for SIB1 transmission). UE may assume 
that the LDS is transmitted at a fixed time periodicity with a fixed offset 
signaled in the configured DMTC as per 3GPP Rel-12 DRS RRC configuration. 
LDS uses the RRC signaling defined for 3GPP Rel-12 DRS. Rel-12 DRS RRC 
configuration allows periodicities of :math:`T = 40ms, 80ms, 160ms`.
This means in practice that LDS is transmitted every :math:`T`, and the 
duration is :math:`1 ms`. The transmission time is counted toward the overall 
SCell ON time.

* Further control is modeled in the PDCCH, e.g. control associated to 
scheduling information like DCI, which is modeled in the first symbols of 
the subframe. The number of symbols occupied by the PDCCH defaults to 3, and 
is configurable.


Summary of configurable parameters in LTE-U implementation
##########################################################
The LTE-U implementation is characterized by many parameters which can be 
configured offline and that can be adjusted on-line if so is required by the 
implemented algorithms. The list of these parameters is summarized below:

*   **Length of the CSAT period** :math:`T_{CSAT}` . This value defaults to 
    :math:`160 ms`. Normal values considered in industry are :math:`80 ms` or 
    :math:`160 ms`. If LTE duty cycle is very low, 
    also higher values can be considered, like :math:`320 ms`, :math:`640 ms`, 
    or :math:`1280 ms`. This parameter can be configured through 
    ``CsatCycleDuration`` attribute of ``LteUCoexistenceManager`` class. 
    ``CsatCycleDuration`` is an integer value, and it represents the number of 
    subframes of duration of the CSAT cycle. This value can be configured in the 
    following way::
    
    Config::SetDefault ("ns3::LteUCoexistenceManager::CsatCycleDuration", UintegerValue (160));  

*   **Duty cycle**. This can be set to a default value, e.g. 50%, and if the 
    online CSAT algorithm is not activated, it remains constant during the 
    simulation. Otherwise it is updated based on the CSAT algorithm. Duty cycle 
    can be configured by setting ``CsatDutyCycle`` attribute of 
    ``LteUCoexistenceManager`` class. It defines the ratio of ON time in TCSAT 
    cycle and can take any value between 0 and 1. this can be set as follows:: 
    
    Config::SetDefault ("ns3::LteUCoexistenceManager::CsatDutyCycle", DoubleValue (0.8)); 

*   **Maximum ON time**, :math:`T_{ON, max}`, defines the maximum allowable ON 
    time, i.e. the minimum OFF time, to let the LTE-U SCell monitor the channel. 
    It defaults to :math:`T_{CSAT} - 20 ms`. The maximum allowable ON time can 
    be configured by using the attribute of ``LteUCoexistenceManager`` class that 
    defines the minimum OFF time ``MinOffTime``.

*   **Minimum ON time**, :math:`TONMinPerTxOP`, defines the minimum time that 
    the LTE-U SCell can be ON, as long as it has data in buffer. This value defaults 
    to :math:`4 ms`, as per [LTEUFCSAT]_. This parameter can be configured by using 
    ``MinOnTime`` of ``LteUCoexistenceManager`` class. 

In practice, the actual duty cycle is constrained by :math:`TONMinPerTxOP` and 
:math:`T_{ON, max}`, the first one defines the minimum possible duty cycle and 
the second one the maximim one. 

*   **Minimum OFF time**, :math:`T_{OFF, min}`, is the minimum time during 
    :math:`T_{CSAT}` when the LTE-U cell has to be OFF to allow for monitoring. 
    It defaults to :math:`20 ms`. 

*   **Offset**. It defines the subframe in the CSAT period when the ON period starts. 
    It defaults to :math:`0`, meaning that the CSAT period starts with the ON 
    period. The offset attribute can be configured by setting attribute ``Offset`` 
    in the ``LteUCoexistenceManager`` class. 

*   **Puncturing period**. It defines the interval of the puncturing and defaults 
    to :math:`20 ms`. Puncturing period can be configured by setting ``Puncturing`` 
    attribute in the ``LteUCoexistenceManager`` class.

*   **Puncturing length**. It defines the length of the puncturing. It defaults 
    to :math:`2 ms` and it can be configured by setting the ``PuncturingDuration`` 
    attribute in the ``LteUCoexistenceManager`` class.

*   **LDS perdiodicity**. This is a configurable parameter, which can take the 
    following values: :math:`T = 40 ms, 80 ms, 160 ms`. It defaults to 
    :math:`T = 80ms`. LDS periodicity can be set by using ``LdsPeriod`` attribute 
    in the ``LteEnbPhy`` class, with the following command::

    Config::SetDefault ("ns3::LteEnbPhy::LdsPeriod", UintegerValue (160));

*   **Minimum MIB periodicity**. It defines the minimum periodicity of MIB, which 
    is :math:`160 ms`. This value can be configured by using ``MaxInterMibTime`` 
    attribute of ``LteUCoexistenceManager``. This attribute is used while creating 
    CSAT bitmask for the following CSAT cycle and it defines what is the maximum 
    time separation between two MIBs. However, during the ON period the MIBs will 
    be scheduled at every MIB subframe. The interval of MIB can be configured by 
    using ``MibPeriod`` attribute of ``LteEnbPhy`` class. 
    
To enable/disable CSAT the ``CsatEnabled`` attribute of 
``LteUCoexistenceManager`` can be used. When CSAT is disabled the duty cycle is 
defined solely by the `Duty cycle` parameter which is not subject to CSAT. 

Any of the previously listed parameters can be changed online, and they can be 
redefined at the end of each CSAT cycle, and the new value is used for the 
generation of the new transmission pattern since the beginning of the next 
cycle. The transmission pattern for the following transmission cycle is 
generated 2 ms before the beginning of the subsequent cycle, to account for the 
MAC to PHY delay, which makes that the scheduler works 2 ms ahead with respect 
to the PHY. Once that the transmission pattern is generated, the notifications 
of the corresponding transmission opportunities are being scheduled, and  are 
always notified to the ``LteEnbPhy`` accounting for the MAC to PHY delay. When 
creating notifications, the ``LteUCoexistenceManager`` specifies the duration 
of transmission of opportunity.


CSAT parameters
~~~~~~~~~~~~~~~

If CSAT algorithm is enabled then also the following parameters will be used:

*   **CSAT AP scan interval and AP scan duration** can be configured by 
    using, respectively, ``ApScanInterval`` and ``ApScanDuration`` attributes of 
    ``LteUCoexistenceManager`` class. ``ApScanInterval`` is defined as number of 
    CSAT cycles between two :math:`APSCAN_{period}`, while `ApScanDuration` is 
    defined as the number of subframes, characterizing the duration.

*   **CSAT monitoring time** defines the amount of time during which the LTE-U 
    SCell monitors the channel during the CSAT cycle. Consists of blocks of 
    monitoring times do not have to be contiguous. This value defaults 
    to :math:`20 ms` and can be configured by using the ``MonitoringTime`` 
    attribute of the ``LteUCoexistenceManager`` class.

*   **CSAT delta values**, :math:`\Delta_{UP}` and :math:`\Delta_{DOWN}`, are 
    configured to be equal to :math:`0,05 \cdot :math:T_{CSAT}`. These parameters 
    can be configured by using, respectively, ``DeltaTUp`` and ``DeltaTDown`` 
    attributes of ``LteUCoexistenceManager`` class.

*   **CSAT MU threshold values**, :math:`MU_{Thr1}` and :math:`MU_{Thr2}`, are 
    equal to :math:`0,4` and :math:`0,6` respectively.  These threshold values can 
    be configured by using, respectively, ``ThresholdMuLow`` and ``ThresholdMuHigh`` 
    attributes of ``LteUCoexistenceManager`` class. These parameters are constrained 
    respectively by :math:`TONMinPerTxOP` and :math:`T_{ON, max}`, i.e. 
    :math:`MU_{Thr1}` cannot be lower than :math:`TONMinPerTxOP`, and 
    :math:`MU_{Thr1}` cannot be greater than :math:`T_{ON, max}`.

*   **CSAT minimum duty cycle**, :math:`TONMininmillisec`, controls the minimum 
    duty cycle below threshold. For :math:`T_{CSAT}=160 ms`, reasonable values are 
    :math:`100 ms` or :math:`120 ms`.
  
Duration of PDCCH and PDSCH
~~~~~~~~~~~~~~~~~~~~~~~~~~~
Finally, in LTE module the duration of PDCCH and PDSCH was not originally 
implemented as a configurable parameter, but as a constant value that was 
defined to be 3 symbols for PDCCH, and 11 symbols for PDSCH.  We extended the 
``LteSpectrumPhy`` to support the configuration of PDCCH and PDSCH duration. 
The number of symbols controlling the PDCCH can be configured in 
``LteSpectrumPhy`` class by using attribute ``DlCtrlDuration``. This attribute 
defaults to 2 symbols, while allowed values are 1, 2 and 3 symbols. This 
parameter can be configured in the following way::

    Config::SetDefault ("ns3::LteSpectrumPhy::DlCtrlDuration", UintegerValue(2));


LTE-U carrier aggregation
==============================
In order to enable LTE-U in the secondary carrier, it is necessary to have the possibility 
to aggregate different carriers. This feature has been recently developed 
as part of GSOC 2015 project and is explained in more details in the design section of 
the LTE documentation. When enabling the carrier aggregation feature, the LTE-U 
device can be defined to have a primary and several secondary carriers. In LTE-U, the secondary 
carriers are allocated in an unlicensed spectrum. The decision of how 
the traffic is split among different component carriers is decided by the component carrier manager 
(CCM) entity, which is an entity operating between RRC and MAC layers. The 
overall architecture of the LTE-U CA implementation is designed and implemented to satisfy 
the following set of requirements:

High-level design requirements:

- Support supplemental downlink only.
- Unlicensed cell cannot be a primary carrier.
- Support up to 2 secondary carriers.
- Link adaptation on both, primary and secondary carriers.
- Mobility based on primary carrier RSRP measurements.
- No cross carrier scheduling.
- Configure UE secondary carriers during RRC connection setup procedure (for initial connection) or RRC connection reconfiguration procedure (for handover) or RRC connection reestablishment. 

LTE-U component carrier manager requirements:

- GBR traffic only over primary carrier.
- Use secondary carriers opportunistically, i.e. when primary cell is fully occupied.
- Activate the secondary carriers for UE only after the signal quality is better than an activation threshold and after data is available to schedule on the secondary carrier.
- Deactivate the secondary carriers for the UE when the signal quality is bellow a deactivation threshold.
- If all UEs are deactivated then set T_ON = 0 to secondary LTE-U carriers.
- RLC retransmissions should always be transmitted over the primary carrier. 


While in the current implementation of carrier aggregation in the LTE module it is possible to 
have up to four secondary carriers, in the LTE-U implementation the maximum number of 
secondary component carriers is two according to the requirements. Even if the current 
implementation of CA in the LTE module allows for the definition of the CCM at the UE side, 
in the LTE-U implementation, the carrier aggregation decisions are placed at the eNB side, so that only the 
eNB CCM is used. The eNB CCM is in charge of controlling the traffic flow in the downlink and as well 
in the uplink, fulfilling the requirements mentioned above. The eNB CCM leverages the 
measurements provided by the UE to decide if and how to use the available component carriers. 
In the current implementation of the CA in the LTE module the only available CCM implementation is 
``NoOpComponentCarrierManager``, which does not perform the carrier aggregation and forwards 
all the traffic to the primary carrier. We also support a ``RrComponentCarrierManager``, 
which splits the traffic among the different carriers. To fulfill the requirements, we have implemented for 
LTE-U a specific CCM, the ``RrSdlComponentCarrierManager``, which is a more 
complex CCM that uses several metrics per UE or per eNB to decide how to 
distribute the traffic over the carriers. The next section explains more in 
details the implementation of 
``RrSdlComponentCarrierManager``.

Supplemental downlink carrier aggregation and SDL LTE-U CCM
########################################
The CCM is the entity which implements the main logic of 
the carrier aggregation. A supplemental downlink (SDL) carrier aggregation 
for LTE-U is implemented in the ``RrSdlComponentCarrierManager`` class. This CCM uses the 
RSRP measurements provided by the UEs, to decide if we have to activate or not the secondary 
carrier of each UE. It also uses metrics that determine the load of the Primary Cell (PCell), and 
based on this information decides when and how to offload the traffic over the Secondary Cells (SCell). 

In the current CA implementation, the UE has, by default, the same number of component carriers as 
the eNB to which it is attached. In general, it is assumed that all the UEs that are attached to the 
specific LTE eNB are capable of the same carrier aggregation configuration as the eNB to which they 
are attached to, in terms e.g. of the number of secondary carriers, bandwidth, frequencies. When 
the simulation starts and the UEs attach to the eNB, the secondary component carriers are disabled 
by default, until the first measurement report arrives from the UE, and this fulfills activation requirement.
For this purpose, the ``RrSdlComponentCarrierManager`` has two attributes for the 
configuration of the activation and the deactivation of the secondary carriers, 
the `RSRP activation threshold` and the `RSRP deactivation threshold`. During the initialization, 
requests for UE measurements are generated and these are configured by using these threshold values. 
This is implemented by leveraging the existing LTE UE measurements framework. A simplified sequence 
diagram of measurement configuration and reporting is shown in 
Figure :ref:`fig-rr-sdl-meas-reporting`.

.. _fig-rr-sdl-meas-reporting:

.. figure:: figures-lteu/rr-sdl-meas-reporting.*
   :align: center
   
   SCell activation and deactivation measurement configuration and reporting 

If the secondary carriers of all the UEs are deactivated, then the CCM sets the CSAT 
``T_ON`` time to zero, which means that only obligatory MIB and LDS are to be transmitted. Note 
that the ``T_ON`` is set to zero from the beginning of the next CSAT cycle. This is shown 
in Figure :ref:`fig-rr-sdl-T_ON-requirement`.

.. _fig-rr-sdl-T_ON_requirement:

.. figure:: figures-lteu/rr-sdl-T_ON-requirement.*
   :align: center
   
   Setting T_ON CSAT time to zero when all UEs' secondary carriers are deactivated
  

Even if the RSRP measurement reports from the UE satisfy the activation requirement and 
so the secondary carriers of the UE are activated, they will be utilized 
for transmission only if there is data available for the secondary carriers, 
i.e. if it is evaluated that the primary cell is overloaded with traffic. This ensures an opportunistic 
usage of the secondary carriers. Note that the decision about
whether to use the secondary carrier is not taken on a secondary carrier basis, but is a decision 
that applies to all the secondary carriers available to a certain UE. 

To measure the load of the primary cell, the LTE SAP interfaces are extended to allow the necessary 
message exchange among the MAC scheduler, the MAC and the CCM to allow for periodic 
notifications of the physical resource block (PRB) occupancy. The ``RrSdlComponentCarrierManager`` 
class has the PRB occupancy threshold attribute, and when this threshold is reached it is considered 
that the load at the primary carrier is high enough to start to use the secondary carrier. 

Since the Round Robin scheduler divides the available bandwidth among the flows without 
considering the real value of the resources that are needed by a certain flow, it may 
happen that this metric does not give the precise information of the real PRB occupancy, 
but rather the average 
amount of resources being assigned to the specific UE. For this reason, an additional 
metric is implemented to provide the information about the load of the primary cell 
and is obtained from the RLC 
layer. This metric represents the total size of the RLC queues of some eNB, and is 
updated every time that the BSR 
is being reported by the RLC to eNB CCM. Once the RSRP and load requirements are 
fulfilled, the ``RrSdlComponentCarrierManager`` can use the secondary carriers for 
transmission. Even if all the previous requirements for secondary carriers 
activation and transmission are being fulfiled  
there are different cases in which the ``RrSdlComponentCarrierManager`` will not 
use the secondary, and these are: 

* the bearer is the signaling bearers (SRB0, SRB1), or 
* a guaranteed bit rate (GBR) bearer, or 
* contains the RLC retransmissions (in the case of LteRlcAm only). 

This kind of traffic always has to be channeled through the PCell.
Figure :ref:`fig-ca-rr-sdl-tx` shows the sequence diagram of the 
downlink BSR reporting and transmission over several carriers when the secondary carriers 
are enabled, and when the traffic is channeled through secondary carrier. 

.. _fig-ca-rr-sdl-tx:

.. figure:: figures-lteu/ca-rr-sdl-tx.*  
   :align: center
   
   RR-SDL CCM scheduling in the downlink (when all conditions are met to use the secondary carriers) 


The implementation disposes of different options for the implementation of the RLC 
retransmissions over primary (Option A, Option B, and Option C), which 
are explained in more details in the section "RLC retransmissions over primary". 
Once that the RSRP measurements of all UEs of the eNB become lower than the deactivation 
threshold, then:

* The secondary carrier of the given UE is not considered for the next BSR. 
* T_ON time of CSAT in the corresponding carriers is set to zero. This means that the LTE-U node is 
  allowed to transmit only the mandatory MIB and LDS, but not any data. 

This is implemented by extending the SAP interfaces between CCM, RRC, PHY and CAM to 
allow activation and deactivation notifications. For example, the CCM notifies the RRC that the 
transmission on the specific carrier should be shut down. Afterwards, the RRC 
notifies the PHY, which then notifies the CAM, and specifically the ``LteUCoexistenceManager``.

Uplink over primary
####################
The implementation of LTE-U that we support only considers a Supplemental Downlink. This 
means that the uplink is only present in the primary, licensed cell and consequently there is not 
uplink traffic in the secondary cell. In the following we explain in more details the implementation 
of uplink 
(PUSCH, PUCCH, SRS) over primary. The transmission of PUSCH over 
the primary carrier is enforced by the ``RrSdlComponentCarrierManager`` CCM, which 
is responsible for forwarding the uplink buffer status report (BSR) to the MAC of the 
primary CC. To support this, the eNodeB MAC instance is extended to forward 
the uplink BSR to CCM. On the other hand, all uplink control messages are 
currently modeled as ideal messages that do not consume any radio resources. 
Because of this, no changes were necessary in implementation of PUCCH or 
handling downlink HARQ feedbacks. 
Hence, according to the current implementation at each CC the UE PHY instance 
directly passes the PUCCH to its eNB PHY instance, which then further forwards some control
information to its eNb MAC instance. Similarly, the HARQ feedback is forwarded to 
correct MAC instance. This is important because it allows the correct 
functionality of the downlink HARQ feedback algorithms that are running 
independently of MAC layer of each CC. Thanks to this ideal control messages 
the changes at eNb MAC layer and CCM are significantly reduced. Contrarily, if the 
PUCCH would be transmitted by using the radio resources of 
the primary uplink, the eNB MAC would need a mechanism that would forward the 
downlink HARQ feedback to the MAC instance of the corresponding CC. Finally, 
the transmission of the SRS over the primary CC is enforced by adding the parameter 
at the PHY layer, so that it is possible to disable the SRS on secondary carriers.

The uplink HARQ feedback is only handled by the eNB MAC instance of the 
primary carrier, and this is because PUSCH goes over the primary.

.. _ref-retx-over-primary: 
RLC retransmissions over primary 
################################
We have implemented three different options to allow the forward of RLC retransmissions 
only through the primary, licensed channel. These options are different based on where the 
intelligence is posed, if more focused on the CCM or on the CCM.

According to Option A, if the buffer status report (BSR) for the certain flow 
contains some data in the retransmission queue, the BSR will be forwarded as it is 
to the primary cell. Since the retransmission queue can be relatively small, and 
the time between two consecutive BSRs can be around 20ms, forwarding the whole 
BSR to the primary carrier can be inefficient. The sequence diagram of 
Option A is shown in figure :ref:`fig-ca-rr-sdl-rx-op1`.

.. _fig-ca-rr-sdl-rx-op1:

.. figure:: figures-lteu/ca-rr-sdl-rx-op1.*
   :scale: 90% 
   :align: center
   
   Retransmission over primary - Option A  

Option B improves option A by forwarding the BSR to both the primary and 
the secondary carriers. The main idea Behind option B is depicted in figure 
:ref:`fig-ca-rr-sdl-rx-op2`. In particular, the BSR of the primary 
carrier contains the size of the retransmission queue equal to the real value, 
while the size of the transmission queue is divided by the number of available 
component carriers (sum of primary and secondary carriers). On the other hand, 
the BSR for the secondary carriers will have the size of retransmission queue 
equal to zero, while the size of the transmission queue will be equal to the 
size of transmission queue of BSR for the primary. The disadvantage with this 
option is that according to the current implementation, the RLC is not aware 
of the carrier that is generating the TxOpportunity, so in order to prevent that 
the transmission opportunity coming from the secondary carrier is used 
for a retransmission, the CCM needs to discard these opportunities. The issue of 
this implementation is that there is a lost transmission opportunities 
coming from the secondary carriers while there is something in the 
retransmission queue, and these transmission opportunities could be used 
instead to transmit data for the retransmission queue.

.. _fig-ca-rr-sdl-rx-op2:

.. figure:: figures-lteu/ca-rr-sdl-rx-op2.*
   :scale: 90%
   :align: center

   Retransmission over primary - Option B  

Option C solves the issues of loosing transmission opportunities, by 
removing the interception of the transmission opportunities performed by CCM, 
and is instead improving the RLC with the capability of distinguishing the transmission 
opportunities. In this implementation, only transmission opportunities coming from the 
MAC of the primary carrier are used for retransmissions. Option C is described
in figure :ref:`fig-ca-rr-sdl-rx-op3`. For this purpose, the ``LteRlcAm`` is extended 
with the configuration option to define if the retransmissions 
are allowed over secondary carriers. However, the user does not need to configure 
this attribute, as this is done automatically when  RLC-AM is selected.
The default implementation proposes Option C as the preferred configuration.

.. _fig-ca-rr-sdl-rx-op3:

.. figure:: figures-lteu/ca-rr-sdl-rx-op3.*
   :scale: 90%
   :align: center
   
   Retransmission over primary - Option C


Round Robin MAC Scheduler enhancements
#######################################
The round robin scheduler described in the LTE documentation, is extended with the 
CCM PRB periodic reporting functionality and with the possibility to configure the 
maximum number of UEs per TTI. 

The PRB occupancy is calculated according to the formula for `Total PRB usage` 
defined [TR36314]_. The PRB periodic reporting is calculated as the average of 
the PRBs used over the time interval T, which is a configurable parameter of 
the ``RrFfMacScheduler``. The PRB occupancy reporting is currently only supported for 
the downlink, and is reported per carrier. In detail, the PRB occupancy 
is evaluated according to the following formula:

.. math::

   M(T) = \Bigl[\frac{M_{dl}(T)}{P(T)}\Bigr] * 100
  
  
:math:`M (T)` is the total PRB usage, the percentage of PRBs used, averaged over 
the time period :math:`T`. :math:`M_{dl}` is the number of all the PRBs used for 
the transmission in the downlink over the time period 
:math:`T`. :math:`P(T)` is the total number of PRBs available during time 
period :math:`T`. The :math:`T` is the time period during which the 
measurement is performed. The period :math:`T` can be configured in the 
following way::  

    Config::SetDefault ("ns3::RrFfMacScheduler::PrbOccupancyReportInterval", UintegerValue (50));

In order to allow the PRB occupancy reporting the SAP interfaces of MAC 
scheduler, MAC and CCM are extended. Figure 
:ref:`fig-prb-occupancy-reporting` shows the sequence diagram of the PRB 
reporting.

.. _fig-prb-occupancy-reporting:

.. figure:: figures-lteu/prb-occupancy-reporting.*
   :align: center
   
   SAP interface extensions to support PRB occupancy reporting
   

In the implementation of the constraint of the maximum number of UEs scheduled 
per TTI, the number of UEs per TTI also accounts for HARQ retransmissions, so 
if e.g. there are two UEs that have HARQ retransmission at certain TTI, no new 
data will be transmitted in that TTI by any other UE. 


Lte-U Coexistence Helper enhancements
#####################################
``LteUCoexistenceHelper`` is extended to allow the configuration of multiple 
instances of the ``LteUCoxistenceManager`` at the same LTE-U node. Each coexistence 
manager will be attached to a single Wi-Fi monitoring device, which will monitor 
the component carrier in which it is installed. The ``LteUCoexistenceManager`` of the
different carriers are independent, each of them is performing the measurement 
of its own carrier, and is adapting the duty cycle according to CSAT algorithm.

Other enhancements
###################################
The ``LteUCoexistenceManager`` duty cycle and ``LteEnbPhy`` traces are extended to 
provide information of the component carrier Id.

Supported use cases and enhancements of scenario-helper
#######################################################
The implementation of CA in LTE, along with the LTE-U functionality, allows for 
the definition of complex LTE and Wi-Fi coexistence use cases. The ``scenario-helper`` 
is extended to allow an easy and quick configuration of the a-wifi-simple`` and 
``laa-wifi-indoor`` scenarios to be used in LTE-U CA use cases. E.g. the eNB can 
be initialized to have up to 2 secondary carriers, and they can be of type 
LTE or LTE-U. The carrier frequency and bandwidth of the component carriers can 
be defined through the configuration parameters. The configuration of 
the LTE-U node also means the selection of the component carriers configuration, 
CCM type, and the CSAT parameters. The traces are extended to support the 
logging per carrier, e.g. logging of transmission opportunities and CSAT 
duty cycle parameters.


Usage
*****

This section provides the overview of some example programs, and describes how 
parameters can be controlled and changed, and what output is produced. Some 
examples are provided to verify the operation.

CSAT usage
==========

Enhancements to support LTE-U in laa-wifi-simple and laa-wifi-indoor scenario
================================================================================

The ``ScenarioHelper`` is extended in order to support a new type of LTE-U node, 
so that if the node is of type LTE-U, after a regular LTE initialization the 
``LteUWifiCoexistenceHelper`` will be called to instantiate the Wi-Fi device in 
the LTE eNBs, and to configure the ``LteUCoexistenceManager`` for each node. In 
order to define if either the operator A or the operator B are of type LTE-U, 
it is sufficient to set a value for the global variable  ``cellConfigA`` or 
``cellConfigB`` to be `LTE_U`. Thus both scenarios ``laa-wifi-simple`` and 
``laa-wifi-indoor`` (which will be better defined in the results section) can 
be run with the LTE-U nodes. The general behavior of the LTE-U node differs 
from the regular LTE node in the channel access which is controlled by the 
``LteUCoexistenceManager``.The ``LteUCoexistenceManager`` has two main channel 
access modes: 

* **CSAT algorithm is disabled**: When the CSAT algorithm is disabled the duty 
  cycle parameters are manually configured and cannot be changed online.
* **CSAT algorithm is enabled**: When the CSAT algorithm is enabled the duty cycle 
  parameters are  updated online by the CSAT algorithm.

Most of the CSAT parameters are exposed through global variable system in the 
scenarios ``laa-wifi-simlpe.cc`` and ``laa-wifi-indoor.cc``. To disable or 
enable the CSAT algorithm in any of these examples, there is an input variable 
called ``csatEnabled`` which is set to 0 for disabling CSAT and to 1 for 
enabling it. 

In the following we show how to configure the scenario ``laa-wifi-simple`` 
with disabled CSAT::

    ./waf --run laa-wifi-simple --csatEnabled=0

When running with disabled CSAT, the duty cycle defaults to 0.5. In the 
following is shown the simulation output obtained through the ``FlowMonitor`` 
tool::

  Running simulations for 1 sec of data transfer and 5 sec overall.
  Operator A: LTE-U; number of cells 1; number of UEs 1
  Operator B: Wi-Fi; number of cells 1; number of UEs 1
  Total txop duration: 0 seconds.
  Total phy arrivals duration: 0 seconds.
  --------monitorA----------
  Flow 1 (1.0.0.2:49153 -> 7.0.0.2:9) proto UDP
  Tx Packets: 7303
  Tx Bytes:   7507484
  TxOffered:  60.0599 Mbps
  Rx Bytes:   7507484
  Throughput: 37.4684 Mbps
  Mean delay:  426.163 ms
  Mean jitter:  0.302262 ms
  Rx Packets: 7303
  --------monitorB----------
  Flow 1 (14.0.0.1:49153 -> 18.0.0.2:9) proto UDP
  Tx Packets: 5197
  Tx Bytes:   5342516
  TxOffered:  42.7401 Mbps
  Rx Bytes:   5342516
  Throughput: 77.0265 Mbps
  Mean delay:  0.509075 ms
  Mean jitter:  0.0645998 ms
  Rx Packets: 5197


In previous example Wi-Fi achieves almost maximum achievable throughput when MIMO 
is not activated, which means that it does not back off and that Wi-Fi does not 
detect LTE-U transmissions. The distance is 50m. In the following example the 
distance between the LTE-U and the Wi-Fi node is 20m::

 ``./waf --run laa-wifi-simple --csatEnabled=0 --d2=20``

This simulation has the following output::
 
    Running simulation for 1 sec of data transfer; 5 sec overall
    Operator A: LTE-U; number of cells 1; number of UEs 1
    Operator B: Wi-Fi; number of cells 1; number of UEs 1
    Total txop duration: 0 seconds.
    Total phy arrivals duration: 0 seconds.
    --------monitorA----------
    Flow 1 (1.0.0.2:49153 -> 7.0.0.2:9) proto UDP
    Tx Packets: 7303
    Tx Bytes:   7507484
    TxOffered:  60.0599 Mbps
    Rx Bytes:   7507484
    Throughput: 34.6377 Mbps
    Mean delay:  499.817 ms
    Mean jitter:  0.321018 ms
    Rx Packets: 7303
    --------monitorB---------- 
    Flow 1 (14.0.0.1:49153 -> 18.0.0.2:9) proto UDP 
    Tx Packets: 5197 
    Tx Bytes:   5342516 
    TxOffered:  42.7401 Mbps 
    Rx Bytes:   4146952 
    Throughput: 52.8293 Mbps 
    Mean delay:  41.4943 ms
    Mean jitter:  0.161509 ms
    Rx Packets: 4034 

Wi-Fi performance drops since it back-offs more. A similar effect can be achieved 
by lowering the energy detection threshold of Wi-Fi. This can be done by setting 
the parameter ``wifiEdThreshold``. Similarly, it is possible to set the ED 
threshold parameter for the LTE-U nodes, i.e., ``lteUEdThreshold``. 
 
Both, ``laa-wifi-simple`` and ``laa-wifi-indoor`` take as input parameters a 
large set of CSAT parameters. E.g. ``csatDutyCycle``, ``csatCycleDuration``, 
``puncturing``, ``puncturingDuration``,etc. The CSAT parameters can be applied 
to all LTE-U nodes in the simulation. The full list of parameters of these 
examples can be obtained in the following way:: 

 ./waf --run laa-wifi-simple --PrintGlobals:

Global values::

    --ChecksumEnabled=[false]
        A global switch to enable all checksums for all protocols
    --RngRun=[1]
        The run number used to modify the global seed
    --RngSeed=[1]
        The global seed of all rng streams
    --SchedulerType=[ns3::MapScheduler]
        The object class to use as the scheduler implementation
    --SimulatorImplementationType=[ns3::DefaultSimulatorImpl]
        The object class to use as the simulator implementation
    --alphaMU=[0.8]
        A value used in CSAT algorithm for MU calculation.
    --assignDutyCycle=[false]
        Manually assign duty cycle when configuring LTE-U. If CSAT is enabled. 
        The setting is the initial duty cycle
    --cellConfigA=[Lte_U]
        LTE or Wi-fi
    --cellConfigB=[Wifi]
        LTE or Wi-fi
    --clientStartTimeSeconds=[2]
        Client start time (seconds)
    --csatCycleDuration=[160]
        Duration of LTE-U CSAT cycle in number of subframes.
    --csatDutyCycle=[0.5]
        Portion of ON time in CSAT Cycle. If CSAT is enabled, this value is only 
        used as inital value
    --csatEnabled=[true]
        If true the CSAT algorithm is enabled (duty cycle is dynamically updated 
        during the simulation). If false a constant duty cycle during simulation 
        is used.
    --d1=[10]
        intra-cell separation (e.g. AP to STA)
    --d2=[50]
        inter-cell separation
    --deltaTDown=[0.05]
        The delta value used in CSAT algorithm when the duty cycle is being reduced.
    --deltaTUp=[0.05]
        The delta value used in CSAT algorithm when duty cycle is being increased
    --duration=[1]
        Data transfer duration (seconds)
    --ftpLambda=[2.5]
        Lambda value for FTP model 1 application
    --generateRem=[false]
        if true, will generate a REM and then abort the simulation; if false, 
        will run the simulation normally (without generating any REM)
    --ldsPeriod=[80]
        LDS interval, time between two consecutive LDS.
    --logBackoffChanges=[false]
        Whether to write logfile of backoff values drawn
    --logBackoffNodeId=[4294967295]
        Node index of specific node to filter logBackoffChanges (default will log all nodes)
    --logBeaconArrivals=[false]
        Whether to write logfile of beacon arrivals to STA devices
    --logBeaconNodeId=[4294967295]
        Node index of specific node to filter logBeaconArrivals (default will log all nodes)
    --logCsatDutyCycle=[false]
        Whether to write logfile of CSAT duty cycle traces of eNB
    --logCsatDutyCycleNodeId=[4294967295]
        Node index of specific node to filter logs for duty cycle CSAT (default will log all nodes)
    --logCwChanges=[false]
        Whether to write logfile of contention window changes
    --logCwNodeId=[4294967295]
        Node index of specific node to filter logCwChanges (default will log all nodes)
    --logDataTx=[false]
        Whether to write logfile when data is transmitted at eNb specified by 
        logCsatDutyCycleNodeId parameter
    --logPhyArrivals=[false]
        Whether to write logfile of signal arrivals to SpectrumWifiPhy
    --logPhyNodeId=[4294967295]
        Node index of specific node to filter logPhyArrivals (default will log all nodes)
    --logTxopNodeId=[4294967295]
        Node index of specific node to filter logTxops (default will log all nodes)
    --logTxops=[false]
        Whether to write logfile of txop opportunities of eNb
    --logWifiFailRetries=[false]
        Log when a data packet fails to be retransmitted successfully and is discarded
    --logWifiRetries=[false]
        Log when a data packet requires a retry
    --lteDutyCycle=[1]
        Duty cycle value to be used for LTE
    --lteUChannelAccessManagerInstallTime=[2]
        LTE channel access manager install time (seconds)
    --lteUEdThreshold=[-62]
        CCA-ED threshold for channel access manager (dBm)
    --lteUMonitorMode=[regular_wifi]
        monitor mode that will be used by lte-u nodes
    --maxInterMibTime=[160]
        CSAT parameter that defines the maximum amount of time that can pass 
        between two consecutive MIBs.
    --minOffTime=[20]
        CSAT parameter that defines the minimum total off time during one CSAT 
        cycle in number of subframes.
    --monitoringTime=[20]
        CSAT parameter that defines the monitoring time in number of subframes.
    --outputDir=[./]
        directory where to store simulation results
    --pcapEnabled=[false]
        Whether to enable pcap trace files for Wi-Fi
    --pdcchDuration=[3]
        Set value PDCCH duration in number of symbols.
    --puncturing=[20]
        CSAT parameter that defines the maximum number of consecutive ON subframes during the ON period.
    --puncturingDuration=[1]
        CSAT parameter that defines the duration of puncturing in the number of subframes.
    --remDir=[./]
        directory where to save REM-related output files
    --rlcAmRbsTimer=[20]
        Set value of ReportBufferStatusTimer attribute of RlcAm.
    --serverLingerTimeSeconds=[1]
        Server linger time (seconds)
    --serverStartTimeSeconds=[2]
        Server start time (seconds)
    --simTag=[default]
        tag to be appended to output filenames to distinguish simulation campaigns
    --simulationLingerTimeSeconds=[1]
        Simulation linger time (seconds)
    --spreadUdpLoad=[false]
        optimization to try to spread a saturating UDP load across multiple UE and cells
    --tOnMinInMilliSec=[120]
        CSAT parameter that defines the minimum total ON time during one CSAT cycle in number of subframes.
    --tcpRlcMode=[RlcAm]
        RLC_AM, RLC_UM
    --thresholdMuHigh=[0.6]
        The upper MU threshold value used in CSAT algorithm for the duty cycle update.
    --thresholdMuLow=[0.4]
        The lower MU threshold value used in CSAT algorithm for duty cycle update as lower MU threshold.
    --transport=[Udp]
        whether to use Udp or Tcp
    --udpPacketSize=[1000]
        Packet size of UDP application
    --udpRate=[75000000bps]
        Data rate of UDP application
    --voiceEnabled=[false]
        Whether to enable voice
    --wifiEdThreshold=[-62]
        CCA-ED threshold for wifi devices (dBm)
    --wifiQueueMaxDelay=[500]
        change from default 500 ms to change the maximum queue dwell time for Wifi packets
    --wifiQueueMaxSize=[400]
        change from default 400 packets to change the queue size for Wifi packets
    --wifiStandard=[80211nFull]
        802.11a, basic 802.11n w/o aggregation and block acks, full 802.11n


When CSAT is enabled the duty cycle will be updated dynamically during the 
simulation, thus it may be necessary to track the values of main CSAT parameters 
over time. This can be achieved by using ns-3 ``LteUCoexistenceManager`` logs or 
by enabling simulation program traces. Since the ``LteUCoexistenceManager`` ns-3 
logs cover many different events from physical layer events till the CSAT mask, 
step by step generation and CSAT adaptation, it might be useful to configure 
logs in the debug mode in order to extract the most relevant information. E.g.:: 

    NS_LOG=LteUCoexistenceManager=level_debug ./waf --run "laa-wifi-simple --csatEnabled=1

In the following we show a small fraction of this log::

    * Duty cycle updated. Old value:0.65, new value:0.7
    GetMinOnTime, when lteUNodesNum:0 wifiNodesNum:1 minOn:80 and T_ON,min=80
    Medium utilization: 0.1602, after filtering the last medium utilization is:0.12816

    * Duty cycle updated. Old value:0.7, new value:0.75
    GetMinOnTime, when lteUNodesNum:0 wifiNodesNum:1 minOn:80 and T_ON,min=80
    Medium utilization: 0.4464, after filtering the last medium utilization is:0.382752

    * Duty cycle updated. Old value:0.75, new value:0.8
    GetMinOnTime, when lteUNodesNum:0 wifiNodesNum:1 minOn:80 and T_ON,min=80
    Medium utilization: 0.42, after filtering the last medium utilization is:0.41255

    * T_ON is not updated since MU is inside the threshold boundaries.
    Medium utilization: 0.4128, after filtering the last medium utilization is:0.41275

    * T_ON is not updated since MU is inside the threshold boundaries.
    Medium utilization: 0.4032, after filtering the last medium utilization is:0.40511


Both examples support several different trace outputs which can be configured by 
setting the corresponding simulation parameters. For example, it is possible to 
enable the Wi-Fi retry logs by setting the parameter --logWifiRetries=1. Other 
logs currently supported are: 

* Wi-Fi failed retries logs, 

* Wi-Fi logging of signal arrivals, 

* LTE-U transmission opportunity logs, 

* LTE-U duty cycle logs, 

* Wi-Fi contention window logs and 

* Wi-Fi backoff logs. 

Some of these logs are followed by another parameter that can be specified. That 
is the node id of node at which the logging is to be performed. In case that 
this parameter is not specified, by default the logging will be performed at all 
nodes that support that kind of logs. Wi-Fi logs will mainly be logged by Wi-Fi 
nodes, except for Wi-Fi logging of signal arrivals which is performed by LTE-U 
node, since the Wi-Fi device inside the LTE-U node is capable of generating this 
log. Since the Wi-Fi device at the LTE-U node is not transmitting, typical Wi-Fi 
logs are not generated for this node, such as Wi-Fi retries and failed retries, 
contention window logs, backoff logs. On the other hand, duty cycle logs and 
LTE-U transmission opportunity logs can be performed only at LTE-U nodes. An 
example of the content of the duty cycle logs is shown below::

    #time(s) nodeId dutyCycle tOnTime tOnMin mediumUtilization  
    2.330000000 0 0.700000000 112 80 0.000000000 
    2.454000000 0 0.750000000 120 80 0.040480000 
    2.474000000 0 0.800000000 128 80 0.096096000 
    2.574000000 0 0.850000000 136 80 0.149139200 
    2.642000000 0 0.875000000 140 80 0.126147840 
    2.802000000 0 0.875000000 140 80 0.113229568 

Another parameter that can be specified is the ``lteUMonitorMode``. This 
parameter can be used to specify the type of monitoring mode at LTE-U node. 
It can be configured to perform energy detection only, which means that Wi-Fi 
signals will be detected in the same way as non Wi-Fi signals. Other option is 
to use a regular monitor mode, in which Wi-Fi preambles will be decoded, thus 
the LTE-U node is able to see the Wi-Fi devices at lower energy levels than 
non-Wi-Fi signals. By default it is configured as a regular monitoring mode. 
This mode needs to be used in CSAT simulation, because the medium utilization 
is obtained by decoding the preambles.

To configure the time instant in the simulation, at which the 
``LteUCoexistenceManager`` starts operating channel access control, there is 
the argument ``lteUChannelAccessManagerInstallTime``. This argument specifies 
the time since the beggining of the simulation during which the default LTE 
channel access is used. This means that there is not any restriction over the 
LTE transmission, i.e. LTE transmits in all subframes. After this time, the 
``LteUCoexistenceManager`` controls at which subframes the LTE-U node is able 
to transmit.

Enhancements to support channel selection dual stripe scenario
==============================================================
Channel selection dual stripe is based on ``lte/exmaple/lena-dual-stripe.cc``. 
This example is extended to support operation of nodes in ten different 
channels. Supported channels are:32, 36, 40, 44, 48, 153, 157, 161 and 165. 
Also the original example ``lena-dual-stripe.cc`` contains only LTE nodes, 
while the ``channel-selection-dual-stripe.cc`` is extended to support mixed 
LTE-U and Wi-Fi toplogy. The example, can be run in two different modes: 
``scanMode`` and ``runMode``. During the ``scanMode`` the LTE-U nodes gather 
information of all channels and save this information in a file. During the 
"runMode" the file that contains the channel statistics is loaded and the 
channel selection algorithm is run for each LTE-U node. This two modes are 
implemented in such way that they mimic single simulation execution and this 
is achieved by running the same channel configuration file ``channelConfigFile``. 
This is the file that specifies an initial distribution of LTE-U and Wi-Fi nodes 
over the channels. In the following we discuss the main parameters of 
``channel-selection-dual-stripe``::

    ./waf --run channel-selection-dual-stripe --command="%s --generateRem=${generateRem} --configMode=${configMode} --channelStatsFile=${channelStatsFile} --channelConfigFile=${channelConfigFile} --useChannelConfigFile=${useChannelConfigFile}

The parameter ``configMode`` specifies the mode of the simulation. In case of 
``scanMode``, a series of different output files is generated as a result. 
The same will happen if ``runMode`` is conifgured and ``generateRem`` is enabled. 
The ``generateRem`` option is only used for testing purposes, e.g. to plot REM 
figures of simulation in ``runMode``. In case of ``scanMode`` or ``generateRem`` 
mode, once that the REM output files are being generated, the simulation stops. 
As a result, ``generateRem`` should not be used when running in ``runMode`` with 
the goal of obtaining simulation results.

The output of the ``scanMode`` channels monitoring is saved in a single file 
specified by the ``channelStatsFile`` argument, which contains power received by 
each cell for a specific channel and position. This file corresponds to a grid 
of positions which is defined by resolution and boundary parameters of the 
``RadioEnvironmentMapHelper``.

Densities per channel loaded from the configuration file can be::

    32     0.1     0.1

    36     0.1     0.2

    165    0.1     0.4

This parameters will results in the following configuration:: 

    Channel:32 LTE-U nodes:2, Wi-Fi nodes:2
    Channel:36 LTE-U nodes:2, Wi-Fi nodes:4
    Channel:165 LTE-U nodes:2, Wi-Fi nodes:8

Total number of created LTE-U nodes is:6 , and Wi-Fi is:14

Densities that need to be specify in the configuration file correspond to 
``homeEnbDeploymentRatio`` parameter of ``lena-dual-stripe.cc``, which 
represents the Home eNB (HeNB) deployment ratio as per 3GPP R4-092042. Thus 
the number of HeNbs that will be created is calculated in the following::

uint32_t nHomeEnbs = round (4 * nApartmentsX * nBlocks * nFloors * lteUDeploymentRatio * homeEnbActivationRatio);

Thus the final number of HeNbs depends on the number of apartments, blocks, 
floors, and activation ratio. Similarly, the number of Wi-Fi APs is also 
calculated. Macro eNbs are implemented in the same way as in 
``lena-dual-stripe.cc``, which means that all macro stations are operating in 
the same channel which is different from any of HeNbs channels.

Another option is to run ``channel-selection-dual-stripe`` without specifying 
the channel configuration file. In this case the densities per channel will 
be generated randomly and all available 5GHz channels will be used in the 
simulation. This option can be achieved by using the simulation parameter 
``useChannelConfigFile``, and setting its value to 0. In order to achieve the 
same topology in scan and run mode, it is necessary to fix ``RngRun`` value. 
To achieve different densities, ``RngRun`` needs to be changed. In the 
following,  we provide an example to run the simulation without the 
configuration file being specified::

    ./waf --run "channel-selection-dual-stripe --useChannelConfigFile=0"

The following channels will be configured::

    Channel:32 LTE-U nodes:16, Wi-Fi nodes:17
    Channel:36 LTE-U nodes:12, Wi-Fi nodes:11
    Channel:40 LTE-U nodes:10, Wi-Fi nodes:8
    Channel:44 LTE-U nodes:8, Wi-Fi nodes:0
    Channel:48 LTE-U nodes:7, Wi-Fi nodes:9
    Channel:149 LTE-U nodes:16, Wi-Fi nodes:18
    Channel:153 LTE-U nodes:2, Wi-Fi nodes:13
    Channel:157 LTE-U nodes:12, Wi-Fi nodes:12
    Channel:161 LTE-U nodes:19, Wi-Fi nodes:5
    Channel:165 LTE-U nodes:13, Wi-Fi nodes:2

Total number of created LTE-U nodes is:115 , and Wi-Fi is:95

The previous output is for the default value of ``RngRun`` number which is equal 
to 1. In order to create a different distribution one can specify different 
``RngRun``, as in the following::

    ./waf --run "channel-selection-dual-stripe --useChannelConfigFile=0 --rngRun=5"

This will result in the following configuration channels::
 
    Channel:32 LTE-U nodes:0, Wi-Fi nodes:20
    Channel:36 LTE-U nodes:19, Wi-Fi nodes:3
    Channel:40 LTE-U nodes:3, Wi-Fi nodes:17
    Channel:44 LTE-U nodes:11, Wi-Fi nodes:13
    Channel:48 LTE-U nodes:12, Wi-Fi nodes:12
    Channel:149 LTE-U nodes:11, Wi-Fi nodes:17
    Channel:153 LTE-U nodes:16, Wi-Fi nodes:1
    Channel:157 LTE-U nodes:8, Wi-Fi nodes:8
    Channel:161 LTE-U nodes:7, Wi-Fi nodes:0
    Channel:165 LTE-U nodes:0, Wi-Fi nodes:8

Total number of created LTE-U nodes is:87 , and Wi-Fi is:99

When running without the channel configuration file a large number of nodes is 
created by the simulation, and it requires a large amount of memory, thus it 
might be necessary to reduce the resolution of the REM grid or to reduce the 
number of REM points generated per iteration, which can be set through the 
parameter: ``ns3::RadioEnvironmentMapHelper::MaxPointsPerIteration``.

The output of the ``scanMode`` is saved in the file specified by the 
``channelStatsFile`` argument, and the same file is used during the ``runMode`` 
to load the radio environmental information information in the form of grid, 
for each channel. Besides this file, the output of simulation are REM maps per 
channel and REM maps per channel and cell id. 

To generate REM figures, there are scripts in 
``campaigns/lte-u-channel-selection-dual-stripe``. First 
``run-channel-selection-dual-stripe.sh`` can be used to run the 
``channel-selection-dual-stripe`` simulation. The configuration parameters 
are specified in ``config`` placed in the same directory. Once that the 
simulation finishes the REM maps can be generated by running: 
``plot-all-rem-maps.sh`` and ``plot-rem-per-channel-cell.sh``. The first will 
generate one REM figure per channel and these maps will represent signal to 
interference to noise ratio at each point of the grid. The second script 
generates one REM figure per channel and cell id, 
and each point at these figures represents the value of power received by 
the cell of that cell id, in each point of the grid. In the following, we show 
a graphical representation of these REM maps for the example in which the 
channel configuration file is considered as input. 

Figures :ref:`lena-dual-stripe-32-scan-mode`, 
:ref:`lena-dual-stripe-36-scan-mode` and :ref:`lena-dual-stripe-165-scan-mode` 
show the REM when the simulation is run in ``scanMode``. Since the
``RadioEnvironmentalHelper`` is currently compliant only with LTE nodes, 
in order to generate REM maps and monitor signal received also by Wi-Fi nodes, 
in "scanMode" Wi-Fi APs are replaced with LTE-U nodes. 


.. _lena-dual-stripe-32-scan-mode:

.. figure:: figures-lteu/lena-dual-stripe-32-scan-mode.png
   :scale: 70 %
   :align: center

   Radio environmental map of channel 32 of channel-selection-dual stripe in ``scanMode``

   
.. _lena-dual-stripe-36-scan-mode:

.. figure:: figures-lteu/lena-dual-stripe-36-scan-mode.png
   :scale: 70 %
   :align: center

   Radio environmental map of channel 36 of channel-selection-dual stripe in ``scanMode``

   
.. _lena-dual-stripe-165-scan-mode:

.. figure:: figures-lteu/lena-dual-stripe-165-scan-mode.png
   :scale: 70 %
   :align: center

   Radio environmental map of channel 165 of channel-selection-dual stripe in ``scanMode``

When the same simulation is run in ``runMode`` the channel selection algorithm 
is performed for each LTE-U node. We provide a very simple channel selection 
algorithm that selects the least crowded channel. 

In the following we show the output of the simulation in ``runMode``. 
In particular, first the initial distributions for LTE-U and Wi-Fi APs are shown, 
and then the logs of the channel selection algorithm, which describe which 
channel is selected by each LTE-U node.

Densities per channel loaded from configuration file::

    32     0.1     0.1

    36     0.1     0.2

    165    0.1     0.4

Which results in the following configuration::

    Channel:32 LTE-U nodes:2, Wi-Fi nodes:2
    Channel:36 LTE-U nodes:2, Wi-Fi nodes:4
    Channel:165 LTE-U nodes:2, Wi-Fi nodes:8

The total number of created LTE-U nodes is:6, and Wi-Fi is:14, and the 
configuration per channel is the following::
 
    Node:1 selected channel:32
    Installed HeNb with node Id:1, cell id:10 channel:32 ,dl earfcn:255244

    Node:2 selected channel:32
    Installed HeNb with node Id:2, cell id:11 channel:32 ,dl earfcn:255244

    Node:5 selected channel:32
    Installed HeNb with node Id:5, cell id:12 channel:32 ,dl earfcn:255244

    Node:6 selected channel:36
    Installed HeNb with node Id:6, cell id:13 channel:36 ,dl earfcn:255444

    Node:11 selected channel:32
    Installed HeNb with node Id:11, cell id:14 channel:32 ,dl earfcn:255244

    Node:12 selected channel:36
    Installed HeNb with node Id:12, cell id:15 channel:36 ,dl earfcn:255444

Figures :ref:`lena-dual-stripe-32-run-mode` and 
:ref:`lena-dual-stripe-36-run-mode` show the REM when the simulation is run in 
``runMode``. Differently from ``scanMode``, in ``runMode`` actual Wi-Fi nodes 
are used, so in the REM map only LTE-U nodes are shown. 

.. _lena-dual-stripe-32-run-mode:

.. figure:: figures-lteu/lena-dual-stripe-32-run-mode.png
   :scale: 70 %
   :align: center

   Radio environmental map of channel 32 of channel-selection-dual stripe in 
   ``runMode``
   
.. _lena-dual-stripe-36-run-mode:

.. figure:: figures-lteu/lena-dual-stripe-36-run-mode.png
   :scale: 70 %
   :align: center

   Radio environmental map of channel 36 of channel-selection-dual stripe in 
   ``runMode``
   
 
Figures :ref:`channel-stats-per-cell-32-10`, :ref:`channel-stats-per-cell-32-11`, 
:ref:`channel-stats-per-cell-32-16` and :ref:`channel-stats-per-cell-32-17`, 
show the REM maps per cell id for 2 LTE-U nodes and 2 Wi-Fi nodes, which 
are generated during the ``scanMode``.

.. _channel-stats-per-cell-32-10:

.. figure:: figures-lteu/channel-stats-per-cell-32-10.png
   :scale: 70 %
   :align: center

   Radio environmental map of channel 32 for LTE-U node (cell id=10) of 
   channel-selection-dual stripe in ``runMode``

.. _channel-stats-per-cell-32-11:

.. figure:: figures-lteu/channel-stats-per-cell-32-11.png
   :scale: 70 %
   :align: center

   Radio environmental map of channel 32 for LTE-U node (cell id=11) of 
   channel-selection-dual stripe in ``runMode``


.. _channel-stats-per-cell-32-16:

.. figure:: figures-lteu/channel-stats-per-cell-32-16.png
   :scale: 70 %
   :align: center

   Radio environmental map of channel 32 for LTE-U node (cell id=16) of 
   channel-selection-dual stripe in ``runMode``


.. _channel-stats-per-cell-32-17:

.. figure:: figures-lteu/channel-stats-per-cell-32-17.png
   :scale: 70 %
   :align: center

   Radio environmental map of channel 32 for LTE-U node (cell id=16) of 
   channel-selection-dual stripe in ``runMode``


Enhancements to support LTE-U SDL usage in examples
====================================================


.. _ref-using-lte-u-sdl-in-existing-examples

Using LTE-U SDL in existing examples (laa-wifi-simple and laa-wifi-indoor)
##########################################################################

The Scenario-helper is extended to support the definition of LTE nodes that support LTE-U 
and carrier aggregation. 

*   **numberOfSecondaryCarriers** is the main configuration parameter that 
    enables the carrier aggregation in scenario-helper. The default value is 0 and 
    the maximum value is 2. If the number of secondary carriers is set to 0, the 
    carrier aggregation is not initialized, so there is a single primary 
    carrier if the cell type is ``Lte`` and only single secondary unlicensed carrier 
    if the cell type is ``Lte_U``. On the other hand, if the 
    ``numberOfSecondaryCarriers`` is 1 or 2, and the cell type is ``Lte_U`` the 
    LTE-U SDL feature will be activated, so the eNB has the primary carrier 
    with a regular LTE cell, and the secondary carrier or carriers 
    (depending if is specified 1 or 2) with unlicensed LTE-U cells. 

*   **coexistenceCarrierIndex** is the index of the LTE secondary carrier in which 
    LTE and Wi-Fi operators coexist, in laa-wifi-simple or laa-wifi-indoor 
    scenario (in both scenarios are defined 2 operators). This index can be 1 or 2 
    since the maximum number of secondary carriers is 2. By default in the 
    scenario-helper the secondary carrier 1 corresponds to Wi-Fi channel 36, and 
    secondary carrier 2 to Wi-Fi channel 40. 

*   **primaryDlEarfcn**  is the Earfcn value of the primary. The default Earfcn 
    value of the primary carrier in the downlink, is 100.

*   **primaryDlBandwidth**  is the bandwidth of the primary carrier in the downlink, 
    the default value is 100 resource blocks.

*   **startingUlEarfcn** is the Earfcn at which will be installed the first primary 
    carrier in the uplink, the default value is 18100. Uplink carriers are equally 
    spaced, and are installed in continuation.

*   **ulBandwidth**  is the bandwidth of the uplink carriers. The default value is 
    25 resource blocks.

*   **ccmImpl** is the parameter that allows to select the CCM implementation, 
    e.g. ``RrComponentCarrierManager`` (RrCcm). The default CCM 
    is set to be ``RrSdlComponentCarrierMaanger`` (RrSdlCcm).

*   **retxOverPrimaryImpl** allows the user to select among different 
    implementations of the `retransmission over primary` requirement. There are 
    3 options available, which have been described in the design section (OptionA, 
    OptionB, OptionC).

To run a laa-wifi-simple scenario with activation of the LTE-U SDL feature it is 
enough to provide the following parameters:

``./waf --run laa-wifi-simple --numberOfSecondaryCarriers=1 --cellConfigA=Lte_U``


Using LTE-U SDL in a custom simulation script
#############################################

In this section we describe the mandatory configuration of users script in order 
to correctly activate the LTE-U SDL feature. Firstly, to configure the round robin 
SDL component carrier manager the 
following line should be introduced::

    Config::SetDefault ("ns3::LteHelper::EnbComponentCarrierManager", StringValue ("ns3::RrSdlComponentCarrierManager"));
    
Secondly, the SRS will be active by default in the each secondary component 
carrier in the uplink. In order to disable the following should be added to 
the script::

    Config::SetDefault ("ns3::LteUePhy::DisableSrsOnSecondaryCarriers", BooleanValue (true));
        
Since, by the default the MAC scheduler in use in the LTE module is 
the ``PfFfMacScheduler``, and the requirements such as "max UEs/TTI" and "PRB 
occupancy reporting" are only implemented in the ``RrFfMacScheduler`` 
the following lines should be added to the script::

    Config::SetDefault ("ns3::LteHelper::Scheduler", StringValue ("ns3::RrFfMacScheduler"));
    
In the following, we explain other optional parameters. The configuration of the implementation 
to be used for the requirement "retransmission over primary" can be configured 
by using the following command::
    
    Config::SetDefault ("ns3::RrSdlComponentCarrierManager::RetxOverPrimaryImpl", EnumValue (OptionC));

Currently, three options are available ("OptionA", "OptionB", "OptionC"). 
More about this parameter can be found in Section :ref:`ref-retx-over-primary`.

The configuration of the PRB occupancy reporting interval at the scheduler is::
    
    Config::SetDefault ("ns3::RrFfMacScheduler::PrbOccupancyReportInterval", UintegerValue (50));
          
The maximum number of UEs per TTI, the default value is 2. Another value can be 
configured in the following way::

    Config::SetDefault ("ns3::RrFfMacScheduler::MaxUesPerTti", UintegerValue (5));  


Configuration of the licensed and unlicensed channel 
#####################################################

In the existing examples, laa-wifi-simple and laa-wifi-indoor, the user can easily 
configure the physical layer parameters of the licensed and unlicensed 
channel by using the configuration parameters of scenario-helper 
:ref:`ref-using-lte-u-sdl-in-existing-examples`. However, in case that 
the user creates are a custom example, this configuration can be done in 
the following ways: 

* by using the default component carrier configuration
* by defining a custom component carrier channel configuration 

According to the first option, the user just needs to specify the number of 
component carriers that will be used in the simulation and the automatic 
channel configuration is performed by the ``LteHelper`` and the ``CCHelper``. 
In the current implementation, if the channel parameters are not provided, 
the ``LteHelper`` will use functions of the ``CcHelper`` that will created 
component carriers that are equally spaced. For more information, see the 
Carrier Aggregation documentation of the LTE module. The 
configuration of the number of component carriers can be done in the 
following way::

    GlobalValue::GetValueByName ("numberOfSecondaryCarriers", UintegerValue(2));

If the user opts for the custom component carrier channel configuration, it is 
necessary to create manually the component carrier map and to provide it to 
the ``LteHelper``. In the following, we provide a simple example of a map of 1 primary 
and 1 secondary carrier, and the secondary channel is configured to be in the Wi-Fi 
5GHz channel 36::

  std::map<unsigned char,ns3::Ptr<ns3::ComponentCarrier> > componentCarrierMap;
  
  for (uint32_t ccIndex = 0; ccIndex < 2; ccIndex++)
    {
      Ptr <ComponentCarrier> componentCarrier =  CreateObject<ComponentCarrierEnb> ();
      if (ccIndex == 0)
        {
          componentCarrier->SetAsPrimary (true);
          componentCarrier->SetDlEarfcn(100);
          componentCarrier->SetDlBandwidth(100);
        }
      else
        {
          componentCarrier->SetAsPrimary (false);
          componentCarrier->SetDlEarfcn (255444); 
          componentCarrier->SetDlBandwidth(10); 
        }       
      componentCarrier->SetUlEarfcn (18100 + ccIndex * ulBandwidth);
      componentCarrier->SetUlBandwidth (25);
      componentCarrierMap.insert(std::make_pair<uint8_t, Ptr<ComponentCarrier> >(ccIndex,componentCarrier));
    }

Once the component carrier map is configured, it can be provided to the ``LteHelper``::
      
      lteHelper->SetCcPhyParams(ccMapParamList);
      
      
Configuration of LTE-U SDL CCM (RrSdlComponentCarrierManager)
#############################################################
The LTE-U SDL functionality can be customized by 
configuring parameters of LTE-U SDL CCM. The following parameters can be 
configured:

*   **RSRP activation threshold** is the threshold used to decide when to 
    activate the secondary carriers of UEs. The default value is 45 which 
    corresponds to -96dBm. The reporting range of RSRP is defined for measured 
    values from -140 dBm to -44 dBm with 1 dB resolution. 
    For more information on RSRP report mapping see Section 9.1.4 RSRP 
    Measurement Report Mapping in [TR36133]_. This parameter can be
    configured by specifing 
    ``ns3::RrSdlComponentCarrierManager::RsrpActivateThreshold`` attribute.

*   **RSRP deactivation threshold** is the threshold used to 
    decide when to deactivate the secondary carriers of the UEs. The default 
    value is 35, which corresponds to -106dBm.

*   **RSRP offset** is the offset that is added to the activation and deactivation 
    thresholds. The default value is 10. This parameter can be configured by setting 
    ``ns3::RrSdlComponentCarrierManager::PositiveOffset`` attribute.

*   **PRB occupancy threshold** is the PRB occupancy threshold used to 
    detect when to use the secondary carriers. It is expressed as a percentage. 
    It can be configured by setting the following attribute:
    ``ns3::RrSdlComponentCarrierManager::PrpOccupancyThreshold``. The default 
    value is 90.

*   **RLC queue size threshold** is the threshold used for the 
    ``Total RLC queue size`` metric, which is also used to detect the load. 
    It is expressed in number of bytes. The default value of this threshold is 
    0, which means that this condition will be satisfied. Can be configured 
    through the ``ns3::RrSdlComponentCarrierManager::RlcQueuesThreshold``.

*   **RLC queue value expire timer** is used to define how often to refresh 
    the information of RLC queue size metric for a certain flow, if it was not 
    being updated by received a BSR from the RLC. The default value is 100 ms. 
    It can be configured through the 
    ``ns3::RrSdlComponentCarrierManager::RlcQueuesValueExpireTimer``.

*   **Check if there are active UEs interval** defines how often the simulation checks 
    if there are active users on the secondary carriers. This is important for 
    the functionality of deactivation of all secondary carriers of all UEs and for
    setting ::math:`T_ON=0`. The default value is 100 ms and can be configured 
    through the ``ns3::RrSdlComponentCarrierManager::CheckIfActiveUesInterval``.

*   **RetxOverPrimaryImpl**, is used to determine which implementation 
    option to use for retransmission over primary requirement. The default 
    option is C, which means that RLC is in charge of how to use the 
    transmission opportunity coming from a carrier. It can be configured through 
    the ``ns3::RrSdlComponentCarrierManager::RetxOverPrimaryImpl``.
 

Test
****

In this section we describe the test suites that are provided, in order to 
validate the proper functionality and correct simulation output, in some key 
configurations for which the intended behavior and output can be determined a 
priori.

Finer time resolution LTE PHY model tests
=========================================

The finer time resolution LTE PHY functionality is currently tested by using 
the ``LteEffectiveSinrToCqiMappingTestSuite``. The main purpose of this test 
suite is to check if the new CQI calculation, based on MI and effective SINR 
results, is associated to the same modulation and coding scheme, as the one 
selected in the legacy LTE implementation, in the scenario where all LTE signals 
are synchronous.

Energy detection test
=====================

The ``LteUWifiSimpleEdTestSuite`` test is used to test the energy detection 
functionality of the LTE-U node.  This test consists of a simple LTE-U Wi-Fi 
coexistence scenario, where one LTE-U node and one Wi-Fi access point (AP) 
share the same channel. The LTE-U node is not transmitting data. In particular, 
the transmission is completely disabled by setting the grant duration of the 
channel access manager to 0. The Wi-fi AP is periodically sending beacons, 
and is not transmitting data either. 

The test cases are configured by using two parameters: 1) the distance between 
the LTE-U node and the Wi-Fi AP, and 2) the energy detection threshold of the 
LTE-U node. We first choose the distance, and then for each distance we identify 
two test cases. In the first case, the value of the energy detection threshold 
is set below the expected value of the received power at the given distance. 
In the second case, the energy detection threshold is set above this value. 

The expected received power at a given distance is calculated according to 
the propagation model used in ``ns3::Ieee80211axIndoorPropagationLossModel``. 

The first test case verifies, for each value of distance that the signal is 
properly detected when the energy detection threshold is at a lower value than 
the expected received power. On the other hand, the second test case verifies 
that for a given distance no signal is detected, when the energy detection 
threshold is set to a higher value than the one expected for the received power.

AP scan test
=============
 
The AP scan test tests that the APs are being properly detected. The test 
scenario topology consists of one LTE-U node and various number of Wi-Fi APs. 
There are several test cases defined for different number of Wi-Fi APs. The 
tests show that for the larger topologies (more than 50 nodes) and standard 
beacon interval it might not be enough to perform AP scan during only 160 ms.


Beacon detection test
=====================
The ``LteUWifiSimpleBdTestSuite`` test is used to test the beacon detection 
functionality of the LTE-U node. In this test  a single LTE-U node is 
instantiated together with multiple Wi-Fi APs. While in the 
``LteUWifiSimpleEdTestSuite`` the LTE-U node is configured in energy detection 
mode, in ``LteUWifiSimpleBdTestSuite`` the LTE-U node is configured to perform 
a regular Wi-Fi functionality which allows to detect beacons and to extract 
information from the MAC header. The test counts different BSSID addresses 
present in the network and compares this number with the total number of APs 
in the topology.

CSAT test
=====================
The CSAT test verifies that the LTE-U node properly transmits according to 
CSAT mask. In particular, it tests the proper puncturing periodicity and 
duration, and that MIB is sent with the minimum configured period, and that 
LDS are sent with the fixed period. This test also checks that during the AP 
scan OFF period, MIB and LDS are sent with the minimum required periodicity. 
Multiple ``LteUCoexistenceManager`` parameters are being varied over series of 
test cases: ``puncturing``, ``puncturingDuration``, ``csatCycleDuration``, 
``offset``, ``dutyCycle``, ``mibInterval``, ``maxInterMibTime``, ``ldsInterval`` 
and ``startWithOnPeriod``.


LTE-U SDL CCM test
=====================

``RrSdlComponentCarrierManagerTestSuite`` is a system test program that 
creates different test cases with a single eNB and several UEs, all having 
the same radio bearer specification. The test consists of checking that the 
throughput obtained over different carriers are equal in the downlink, while 
the uplink traffic goes over primary carrier. This test does not configure 
``RrSdlComponentCarrierManager`` in the secondary cell activation and deactivation 
mode. It is always using the secondary cell.

``RrSdlCcmUseScellOpportunisticallyTestSuite`` is a system test that is testing 
the secondary carrier activation/deactivation in LTE with simulation of 1 
eNodeB and 1 UE in a piecewise configuration and 120 ms report interval. 
Specifically, this test is checking the following aspects of the behavior of
the ``RrSdlComponentCarrierManager``:

* when the primary load condition is satisfied (PRB occupancy and RLC queues size 
  thresholds are reached), the activation of the secondary cell has to be 
  performed according to RSRP conditions

* when all UEs get deactivated, the T_ON is set to 0 and the 
  obligatory MIB and LDS are sent, except no other data is 
  transmitted during the corresponding CSAT cycle
  
* there are maximum 2 UEs scheduled per TTI

* GBR traffic goes only over primary

The results of this test can be plotted by creating a script with the following 
content::

    set terminal png
    rsrp_results="DlRsrpSinrStats.txt"
    mac_results="DlMacStats.txt"
    set key top right
    set grid

    set output "rr-sdl-scell-activation-deactvation-rsrp.png"
    set xrange [0:5]
    set xlabel "Time [seconds]"
    set ylabel "RSRP [dBm]"
    plot rsrp_results using 1:($7==1 ? 10*log10($5)+30 : 1/0) w lp t 'RSRP'

    set output "rr-sdl-scell-activation-deactvation-throughput.png"
    set xlabel "Time [seconds]"
    set ylabel "MAC throughput [Mbps]"
    set xrange [0:5]
    set yrange [0:180]
    plot mac_results using 1:($11==0 ? ($8+$10)*8*1000/1000000 : 1/0) w lp t 'Primary carrier', \
        mac_results using 1:($11==1 ? ($8+$10)*8*1000/1000000 : 1/0) w lp t 'Secondary carrier'


An example of running a single test case for activation and deactivation of 
``RrSdlCcmUseScellOpportunisticallyTestSuite`` is shown in figures 
:ref:`fig-ca-rr-sdl-test-example-rsrp` and 
:ref:`fig-ca-rr-sdl-test-example-thr`.

.. _fig-ca-rr-sdl-test-example-rsrp:

.. figure:: figures-lteu/ca-rr-sdl-test-example-rsrp.png
   :scale: 80 %
   :align: center

   RSRP measurements reported by the primary carrier `
   

.. _fig-ca-rr-sdl-test-example-thr:

.. figure:: figures-lteu/ca-rr-sdl-test-example-thr.png
   :scale: 80 %
   :align: center

   RrSdl activation and deactivation of the secondary cell based on receive RSRP


Results
********
In this section we describe the simulation campaigns that we have run and some 
results that we have obtained. 

Simulation campaign scenarios
=============================
We introduce two scenarios for validation and evaluation, a simple two-nodes 
scenario and the indoor scenario proposed by 3GPP in [TR36889]_.

Simple scenario
###############

Figure :ref:`fig-simple-scenario-layout` provides an overview of the 
simple scenario designed as a simplified version of the indoor scenario
to be described next.

.. _fig-simple-scenario-layout:

.. figure:: figures-lteu/simple-scenario-layout.png
   :align: center

   Layout of nodes for the simple scenario

The basic idea is to provide:

* the ability to toggle between WiFi and LTE in the two networks

* the ability to vary the distance 'd1' between the BS and UE, to experiment
  with different signal strength effects

* the ability to vary the distance 'd2' between the two networks, to 
  experiment with different levels of interference isolation between
  the two networks.

In addition to the depicted nodes, there is a 'backhaul' node that serves
as traffic client and that is connected by point-to-point links to both
base stations.  This node originates all traffic on the downlinks towards
the UEs.

Several other arguments can be provided to the scenario program to control
its behavior, including whether to use UDP or TCP, and others.

Indoor scenario
###############

The indoor scenario follows Appendix A.1.1 of [TR36889]_ as described herein.
We describe the two operators as "operator A" and "operator B".

Figure :ref:`fig-indoor-scenario-layout` provides an overview of the 
node layout corresponding to TR36.889.

.. _fig-indoor-scenario-layout:

.. figure:: figures-lteu/indoor-scenario-layout.*
   :align: center

   Layout of nodes for the indoor scenario

Two operators deploy four small cells in a building with no walls and
dimensions as shown.  The four base stations for each operator are equally
spaced, but the offset on the X axis between base stations for operators
A and B is controlled by a global value of the program called  "bsDistance".
The remaining UEs are randomly distributed in the rectangular region.

The indoor scenario also has these design aspects:

* **System bandwidth per carrier:** Is 20Mhz single channel centered at 5.180 GHz'

* **Number of carriers:** There is some ambiguity as to how to interpret
  the value of 4 carriers specified in [TR36889]_.  For now, the scenario
  supports 1 carrier (co-channel only).

* **Total BS TX power:**  18 dBm 

* **Total UE TX power:**  18 dBm

* **Distance-dependent path loss:** ITU InH

* **Penetration:** Do not know how to interpret the 0 dB requirement.

* **Shadowing:** ITU InH

* **Antenna pattern:** 2D omni

* **Antenna height:** Not modeled yet although should be possible (6m)

* **UE antenna height:** Not modeled yet although should be possible (1.5m)

* **antenna gain and loss:**  5dBi 

* **fast fading:** The fast fading model is not incorporated in the
  ns-3 propagation loss models in use in the indoor and outdoor
  scenarios. The Wi-Fi error rate model has been
  generated considering channel model D. The LTE error model has been
  generated considering an AWGN model [MezMio12]_. 

* **Number of UEs:** 20 per operator; 40 total.

* **UE dropping per network:** [TR36889]_ specifies that all UEs
  should be randomly dropped and be within coverage of the small cell
  in the unlicensed band. After comparing the path loss model
  characteristics with the topology specification of this scenario, we
  concluded that all UEs dropped within the indoor topology will
  satisfy the coverage requirement without the need for any further
  operation.

* **Minimum distance:** Not sure how to interpret the 3m requirement

* **Traffic model:** FTP Model 1 as in TR36.814; overall offered load is
  the same in both networks

* **UE receiver:** Not sure how to interpret the MMSE-IRC requirement

* **UE noise figure:**  9 dB 

* **UE speed**  Interpreted this to mean a fading parameter

* **Cell selection criteria:**  For Wi-Fi, strongest RSS of WiFi
  APs. For LTE, the initial cell selection procedure is evaluated
  using strongest RSRP, and cell reselection is not evaluated

* **UE bandwidth:**  For Wi-Fi, 20 MHz.  For LTE, 20MHz in the
  unlicensed band are used, with RRC signaling being transmitted
  out-of-band, in order to simulate a Carrier Aggregation setup with
  data plane transmissions occurring entirely in the unlicensed band,
  in order to have a fair comparison with Wi-Fi.
 
* **Network synchronization:** all LTE base stations are synchronized
  among themselves. There is no synchronization between LTE base
  stations and Wi-Fi base stations.

* **DL/UL traffic ratio:** 100% on DL 

Simple scenario
================
We first evaluate the LTE-U performance in the simple scenario. For this purpose 
we facilitate a simulation campaign script that allows 
to easily configure the scenario and modify some test parameters, like the 
distances between nodes in the scenario, the traffic model, 
the possibility to enable or not the CSAT algorithm, etc.We first define 
baseline performance by simulating Wi-Fi over Wi-Fi, and 
then we evaluate the performance of the system when we substitute operator 
A node with LTE-U.


Baseline
#########
In this subsection we first show results of FTP application over UDP, and then 
of a Constant Bit Rate (CBR) traffic over UDP transport.

FTP over UDP
~~~~~~~~~~~~~ 
Figure :ref:`scenario-simple-baseline-throughput` and Figure 
:ref:`scenario-simple-baseline-latency` provide the performance, in the simple 
scenario, in terms of throughout and latency, respectively, of Wi-Fi APs when 
Wi-Fi coexists with WiFi. We consider FTP traffic. We observe that when the 
APs are at 1000 mt distance, they are not interfering each other. At 50 mt 
distance, they can preamble detect each other, and at 10 mt distance they 
can also energy detect each other. At 50 mt distance, there are some flows 
which are affected. Going down into the traces, we observed that those flows 
are starting very closely in time, and this makes that they are interfering 
each other. The backoff procedure makes increase the latency and so decrease 
the throughput of these flows. When the distance is reduced at 10 mt, this 
effect is stronger.


.. _scenario-simple-baseline-throughput:

.. figure:: figures-lteu/scenario-simple-baseline-throughput.*
   :align: center

   Throughput performance, as a function of the distance between two Wi-Fi APs in the simple scenario. Wi-Fi coexists with Wi-Fi. FTP over UDP traffic


.. _scenario-simple-baseline-latency:

.. figure:: figures-lteu/scenario-simple-baseline-latency.*
   :align: center

   Latency performance, as a function of the distance between two Wi-Fi APs in the simple scenario. Wi-Fi coexists with Wi-Fi. FTP over UDP traffic

UDP CBR
~~~~~~~~
Figure :ref:`simple-baseline-UDP75-throughput` and Figure 
:ref:`simple-baseline-UDP75-latency` provide the performance, in the simple 
scenario, in terms of throughout and latency, respectively, of Wi-Fi APs when 
Wi-Fi coexists with WiFi, and a CBR of 75 Mbps is sent over a UDP transport.
We observe that as the nodes get closer, the throughput is reduced, due to the 
interference that the two networks generate to each other. The latency increases, 
and this makes reduce the throughout. The figures represent only one point, 
because only one CBR flow is considered with this traffic model, between the 
AP/eNB and the STA/UE.

.. _simple-baseline-UDP75-throughput:

.. figure:: figures-lteu/simple-baseline-UDP75-throughput.*
   :align: center

   Throughput performance, as a function of the distance between two Wi-Fi APs in the simple scenario. Wi-Fi coexists with Wi-Fi. UDP CBR 75 Mbps


.. _simple-baseline-UDP75-latency:

.. figure:: figures-lteu/simple-baseline-UDP75-latency.*
   :align: center

   Latency performance, as a function of the distance between two Wi-Fi APs in the simple scenario. Wi-Fi coexists with Wi-Fi.UDP CBR 75 Mbps



CSAT disabled
#############
In this subsection, we discuss the behavior of the LTE-U implementation when 
the CSAT algorithm is not enabled. This means that the duty cycle is set 
manually and remains fixed during 
the whole simulation time. We organize the subsection in different paragraphs, 
based on the traffic type that we send at application level. We consider the 
FTP application and a CBR application sent over UDP transport. 

FTP over UDP
~~~~~~~~~~~~~ 
We start the discussion on the performance results of the LTE-U scheme, with 
CSAT disabled, with a calibration exercise. In particular, we compare the 
results obtained with the implementation proposed for LTE-U, which another 
implementation of LTE-U, referred to as LTE-DC, where the duty cycle can only 
be set manually. This implementation was provided as an in initial estimation 
of LTE-U performance in the first phase of the project carried out with the WFA. 
This implementation is much less flexible than the current one described in 
this document, and it only allows to send CSAT periods of 40 msec. Adaptation 
of the ON period is not allowed, and puncturing is not modeled. Figure 
:ref:`simple-scenario_LTEDC-throughput` and Figure 
:ref:`simple-scenario-LTEDC-latency` provide the performance, in the simple 
scenario, in terms of throughout and latency, respectively, of W-Fi and LTE-DC. 
We pick a duty cycle of 50%. We show these results only with the purpose of 
showing calibration with a LTE-U implementation configured with exactly the 
same parameters. The LTE-U implementation with the same configuration as LTE-DC 
is shown in Figure :ref:`simple-scenario_CSAT40dis-throughput` and Figure 
:ref:`simple-scenario_CSAT40dis-latency`. In these curves we observe that at 
1000 mt and 50 mt distance LTE-U nodes and Wi-Fi do not interfere each other 
because they do not energy detect each other. At the distance of 50mt, 
consequently, we observe that the higher sensitivity of Wi-Fi to Wi-Fi, 
guaranteed by the preamble detection capabilities actually jeopardizes the 
performance of Wi-Fi more than what LTE-U does. At 10 mt distance, the Wi-Fi 
and LTE-U nodes energy detect each other and they interfere among each other. 
With respect to the maximum throughput achieved by LTE-U, we observe that it 
is limited to 80 msec. This is due to the fact that each point in the 
represented CDF represents the performance of 1 FTP flow. The file to be 
transmitted in one flow is approximately 500 KBytes. To transmit this amount 
of bytes at the LTE-U peak rate, approximately 30 msec are required. In these 
simulations, with 40 msec CSAT period, the ON period is only 20 msec, as we 
are considering Duty cycles of 50%. As a result, the LTE-U node always need 
to wait one OFF period to finalize the transmission of the flow. This increases 
the latency and so bounds the throughput.


.. _simple-scenario_LTEDC-throughput:

.. figure:: figures-lteu/simple-scenario_LTEDC-throughput.*
   :align: center

   Throughput performance, as a function of the distance between one Wi-Fi AP and one LTE-DC eNB, in the simple scenario. Wi-Fi coexists with LTE-DC.


.. _simple-scenario-LTEDC-latency:

.. figure:: figures-lteu/simple-scenario-LTEDC-latency.*
   :align: center

   Latency performance, as a function of the distance between one Wi-Fi AP and one LTE-DC eNB, in the simple scenario. Wi-Fi coexists with LTE-DC.


.. _simple-scenario_CSAT40dis-throughput:

.. figure:: figures-lteu/simple-scenario_CSAT40dis-throughput.*
   :align: center

   Throughput performance, as a function of the distance between one Wi-Fi AP and one LTE-U eNB, in the simple scenario. Wi-Fi coexists with LTE-U. CSAT period is 40 msec. Duty cycle is 50%.


.. _simple-scenario_CSAT40dis-latency:

.. figure:: figures-lteu/simple-scenario_CSAT40dis-latency.*
   :align: center

   Latency performance, as a function of the distance between one Wi-Fi AP and one LTE-U eNB, in the simple scenario. Wi-Fi coexists with LTE-U. CSAT period is 40 msec. Duty cycle is 50%.

We run the same simulations but with a higher CSAT period of 160 msec, so as to 
show that the LTE-U peak rate can now reach higher values, because it is 
possible during the ON period a flow is completely transmitted. Figure 
:ref:`simple-scenario_CSAT160dis-throughput` and Figure 
:ref:`simple-scenario_CSAT160dis-latency` show this effect. The peak 
rate now reaches 130 Mbps.

.. _simple-scenario_CSAT160dis-throughput:

.. figure:: figures-lteu/simple-scenario_CSAT160dis-throughput.*
   :align: center

   Throughput performance, as a function of the distance between one Wi-Fi AP and one LTE-U eNB, in the simple scenario. Wi-Fi coexists with LTE-U. CSAT period is 160 msec. Duty cycle is 50%.


.. _simple-scenario_CSAT160dis-latency:

.. figure:: figures-lteu/simple-scenario_CSAT160dis-latency.*
   :align: center

   Latency performance, as a function of the distance between one Wi-Fi AP and one LTE-U eNB, in the simple scenario. Wi-Fi coexists with LTE-U. CSAT period is 40 msec. Duty cycle is 50%.

UDP CBR
~~~~~~~~
In this paragraph, we show some simulations when the CSAT algorithm is disabled, 
and the application is a CBR, for different values of distance between the eNB 
and the AP. We observe that similarly to the FTP case, when the nodes are not 
interfering each other we reach approximately 75 Mbps with both networks. When 
the distance reduces, both throughput falls, as the latency increases due to 
the interfering effects. This is shown in Figure 
:ref:`simple-CSATdis-UDP75-dist-throughout` and Figure 
:ref:`simple-CSATdis-UDP75-dist-latency`

FTP over UDP
~~~~~~~~~~~~~ 
.. _simple-CSATdis-UDP75-dist-throughout:

.. figure:: figures-lteu/simple-CSATdis-UDP75-dist-throughout.*
   :align: center

   Throughput performance, as a function of the distance between one Wi-Fi AP and one LTE-U eNB, in the simple scenario. Wi-Fi coexists with LTE-U. CSAT period is 40 msec. Duty cycle is 50%


.. _simple-CSATdis-UDP75-dist-latency:

.. figure:: figures-lteu/simple-CSATdis-UDP75-dist-latency.*
   :align: center

   Latency performance, as a function of the distance between one Wi-Fi AP and one LTE-U eNB, 
   in the simple scenario. Wi-Fi coexists with LTE-U. CSAT period is 40 msec. Duty cycle is 50%

CSAT enabled
############
In this subsection, we show now results with CSAT algorithm enabled. We organize 
this section as done above, based on the application that we are considering. 
So, we show FTP UDP and CBR UDP results.

Figure :ref:`simple-scenario-CSAT160ena-throughout` and Figure 
:ref:`simple-scenario-CSATenab160-latency` show throughput and latency 
performance when CSAT is active, and the Wi-Fi AP and LTE-U eNB are at 10 mt 
distance. We observe that the Medium utilization is low to appreciate a balanced 
share of the channel. LTE-U sees the medium utilization of the channel quite low, 
so that the threshold of the MU does not affect the results, and the CSAT 
converges to the maximum duty cycle, i.e., to the maximum ON period. Due to the 
way the FTP application is implemented, by increasing the lambda value, it is 
not enough to increase the traffic generated. We leave as a future work to 
modify the FTP application in such a way that we can increase the rate 
generated, in order to inject more traffic.


.. _simple-scenario-CSAT160ena-throughout:

.. figure:: figures-lteu/simple-scenario-CSAT160ena-throughout.*
   :align: center

   Throughput performance, as a function of different values of MU threshold in the simple scenario. 
   Wi-Fi coexists with LTE-U. CSAT period is 160 msec. Distance between Wi-Fi AP and LTE-U eNB is 10 mt.


.. _simple-scenario-CSATenab160-latency:

.. figure:: figures-lteu/simple-scenario-CSATenab160-latency.*
   :align: center

   Latency performance, as a function of different values of MU threshold in the simple scenario. 
   Wi-Fi coexists with LTE-U. CSAT period is 160 msec. Distance between Wi-Fi AP and LTE-U eNB is 10 mt.

UDP CBR
~~~~~~~~
When considering UDP CBR, we show instead the opposite behavior that we have 
with FTP UDP application. While before the MU was low enough to allow LTE-U to 
transmit with the maximum ON period, now the MU of Wi-Fi is much higher, it 
converges to values around 0.68. For this reason, independently of the MU 
thresholds, the ON period converges to the minimum one. Since we have two nodes 
sharing the channel, the minimum time share of each one is 50%. This behavior 
is shown in Figure :ref:`simple-CSATenab-UDP75-dist-throughput` and Figure 
:ref:`simple-CSATena-UDP75-dist-latency`

.. _simple-CSATenab-UDP75-dist-throughput:

.. figure:: figures-lteu/simple-CSATenab-UDP75-dist-throughput.*
   :align: center

   Throughput performance, as a function of different values of MU threshold in the simple scenario. 
   Wi-Fi coexists with LTE-U. CSAT period is 160 msec. Distance between Wi-Fi AP and LTE-U eNB is 10 mt.


.. _simple-CSATena-UDP75-dist-latency:

.. figure:: figures-lteu/simple-CSATena-UDP75-dist-latency.*
   :align: center

   Latency performance, as a function of different values of MU threshold in the simple scenario. 
   Wi-Fi coexists with LTE-U. CSAT period is 160 msec. Distance between Wi-Fi AP and LTE-U eNB is 10 mt.


Indoor scenario
================
In this section we show results obtained in the indoor scenario as defined by 
3GPP in [TR36889]_. 
Similarly to the simple scenario, we show results considering that the CSAT is 
disabled and the duty cycle is manually selected, and results where the CSAT is 
activated. we show results obtained with FTP traffic, over both UDP and TCP 
transport. 

CSAT disabled
#############
We consider CSAT algorithm to be disabled. We show FTP performance when the 
application is sent over UDP and over TCP, to observe the impact of TCP traffic. 
When we consider TCP, the selected link layer is RLC-AM.

FTP over UDP
~~~~~~~~~~~~~ 
We show results considering CSAT algorithm disabled and we test different duty 
cycle values. We also show Wi-Fi over Wi-Fi performance. When the Duty cycle is 
little LTE-U reaches low values and Wi-Fi activity is not affected. LTE-U 
presents better results when the Duty Cycle is higher. Wi-Fi activity is not so 
high so it is not affected. These results are shown in Figure 
:ref:`indoor-CSATdis-FTP-throughout` and Figure :ref:`indoor-CSATdis-FTP-latency`.

.. _indoor-CSATdis-FTP-throughout:

.. figure:: figures-lteu/indoor-CSATdis-FTP-throughout.*
   :align: center

   Throughput performance, as a function of different values of Duty cycle in the indoor scenario. 
   Wi-Fi coexists with Wi-Fi and LTE-U. CSAT period is 160 msec. 


.. _indoor-CSATdis-FTP-latency:

.. figure:: figures-lteu/indoor-CSATdis-FTP-latency.*
   :align: center

   Latency performance, as a function of different values of Duty cycle in the indoor scenario. 
   Wi-Fi coexists with Wi-Fi and LTE-U. CSAT period is 160 msec. 

FTP over TCP
~~~~~~~~~~~~~
We consider a TCP configuration with segment size of 536 bytes and 1 as initial 
congestion window. Figure :ref:`indoor-CSAT_DIS_TCP536-throughput` and Figure 
:ref:`indoor-CSAT_DIS_TCP536-latency` show the trends when considering different 
duty cycles. Wi-Fi performance are more affected when LTE duty cycle is higher, 
as it was to expect. In general we always see, even in case of low LTE duty 
cycle, a certain impact on the Wi-Fi traffic. This happens because due to the 
lack of a carrier aggregation implementation, some overhead which would be sent 
in licensed band is here channeled through the unlicensed. An example are the 
UL-DCIs. Similarly, also the overhead of the RLC is sent over the unlicensed 
band, like the STATUS PDU and the retransmissions. This makes that the Wi-Fi 
traffic may be interrupted by these control messages and the throughput is 
slightly decreased. The LTE-U throughout also is directly proportional to the 
duty cycle. When the duty cycle is very low, the latency is extremely high and 
the throughput is close to zero. In the other cases, the throughput increases, 
but it is anyway bounded by approximately 11 Mbps. The reason is to find on the 
delays introduced by the LTE protocol stack. To transmit a FTP flow of 512 
Bytes, we need about 22 TCP Round Trip Time (RTT). Each RTT is the time between 
when a TCP segment is generated at the remote host, till when the corresponding 
ACK is received at the remote host. Due to standard delays in the LTE protocol 
stack, we need 4 ms to have the packet received at the UE MAC. Immediately 
after, that an ACK is generated at the remote host and it is queued at RLC: 
When the UE MAC is aware of the TCP ACK to be queued, it needs to send a 
Buffer Status Report (BSR) to the eNB to inform of the need to transmit. For 
the BSR to be received at the eNB MAC we need 4 additional msec. The eNB MAC 
then generates a UL DCI to inform the UE about when and how to transmit. 
Additional 4 msec are required. Finally, the UE RLC PDU has to be scheduled and 
received at the eNB MAC, for this me need other 4 ms. As a result, each segment 
RTT requires 15-16 msec. Which makes that the throughput has to be bounded by 
11 Mbps approximately. To increases the throughput we could send also UL data 
and this would allow to reduce the delay needed to send the BSR in UL, since 
those packets would be piggybacked with data.


.. _indoor-CSAT_DIS_TCP536-throughput:

.. figure:: figures-lteu/indoor-CSAT_DIS_TCP536-throughput.*
   :align: center

   Throughput performance, as a function of different values of Duty cycle in the indoor 
   scenario. Wi-Fi coexists with Wi-Fi and LTE-U. CSAT period is 160 msec. TCP transport is considered


.. _indoor-CSAT_DIS_TCP536-latency:

.. figure:: figures-lteu/indoor-CSAT_DIS_TCP536-latency.*
   :align: center

   Latency performance, as a function of different values of duty cycle in the indoor scenario. 
   Wi-Fi coexists with Wi-Fi and LTE-U. CSAT period is 160 msec. TCP transport is considered


CSAT enabled
############
We show results considering CSAT algorithm being enabled. 

FTP over UDP
~~~~~~~~~~~~~ 
We show the behavior for different values of MU threshold. The impact is not 
high since the MU measured by LTE-U with the FTP traffic model is not high.


.. _indoor-scenario-CSATenabled-throughput:

.. figure:: figures-lteu/indoor-scenario-CSATenabled-throughput.*
   :align: center

   Throughput performance, as a function of different values of MU threshold in the indoor scenario. 
   Wi-Fi coexists with Wi-Fi and LTE-U. CSAT period is 160 msec. FTP over UDP 


.. _indoor-scenario-CSATenabled-latency:

.. figure:: figures-lteu/indoor-scenario-CSATenabled-latency.*
   :align: center

   Latency performance, as a function of different values of MU threshold in the indoor scenario. 
   Wi-Fi coexists with Wi-Fi and LTE-U. CSAT period is 160 msec. FTP over UDP 

FTP over TCP
~~~~~~~~~~~~~
When testing the same traffic over TCP and RLC-AM, we observe similar trends 
as for the case when CSAT is not enabled. Wi-Fi is affected by the operation of LTE-U, 
but the same analysis should be done in a second moment when carrier aggregation 
is included in the model.


.. _indoor-CSAT_ENA_TCP1440-throughput:

.. figure:: figures-lteu/indoor-CSAT_ENA_TCP1440-throughput.*
   :align: center

   Throughput performance, as a function of different values of MU threshold in the indoor scenario. 
   Wi-Fi coexists with Wi-Fi and LTE-U. CSAT period is 160 msec. FTP over TCP 


.. _indoor-CSAT_ENA_TCP1440-latency:

.. figure:: figures-lteu/indoor-CSAT_ENA_TCP1440-latency.*
   :align: center

   Latency performance, as a function of different values of MU threshold in the indoor scenario. 
   Wi-Fi coexists with Wi-Fi and LTE-U. CSAT period is 160 msec. FTP over TCP
   


LTE-U SDL CA simulation campaigns
==================================
In this section we focus on LTE-U deployment which consist of supplemental downlink 
in unlicensed band, which is paired with a licensed LTE carrier as carrier 
aggregation mode in legacy LTE. We first define baseline performance by simulating Wi-Fi over 
Wi-Fi, and then we evaluate the performance of the system when we substitute operator A node 
with LTE-U. 

In the following subsections the results will be represented in the following 
way: for each simulation configuration which is characterized by the traffic 
type and application we show results for different values of distance and 
3 different configuration of nodes which are labeled in figures in the 
following way:

* `WiFi-WiFi` - both operators are WiFi

* `WiFi-LteU` - one operator is WiFi and another is Lte-U

* `WiFi-LTE` -  one operator is WiFi and another is LTE.

In the case of `WiFi-LteU` we consider 3 different deployment configurations of 
LTEU node:

* `0 PCC + 1 SCC`
* `1 PCC + 1 SCC`
* `1 PCC + 2 SCC`

`0 PCC + 1 SCC` is the deployment in which the LTE network operates only in 
unlicensed band. `1 PCC + 1 SCC` and `1 PCC + 2 SCC` are deployments in which 
respectively, 1 and 2, secondary carriers are paired with 1 licensed LTE carrier. In 
table :ref:`table_lte_operator_conf` is shown configuration of different 
deployments.

..  _table_lte_operator_conf:
.. csv-table:: LTE-U Operator configuration in SDL CA scenarios
   :header: "LTE-U deployment", "WiFi channels in use", "Bandwidth (MHz)"
   
   0 PCC + 1 SCC, 36,20
   1 PCC + 1 SCC, 36, 20 + 20
   1 PCC + 2 SCC, "34, 40", 20 + 20 + 20


The Wi-Fi operator is configured to operate always in the channel 36. This 
applies for both simulation scenarios, the simple and the indoor, and over all 
simulation campaigns. In general, we are mainly focused on the deployment 
configuration *1 PCC + 1 SCC*.

In the following sections we present the LTE-U SDL simulation campaigns 
for simple and indoor results. For each of these scenarios we show the 
performance for different configurations of traffic. We are mainly focused on 
the following applications and transport protocols: 

* FTP application over UDP,
* FTP application over TCP, and 
* of a Constant Bit Rate (CBR) traffic over UDP transport. 

Since with CA more bandwidth is available, we are configuring higher lambdas 
for FTP application, e.g. as 5 and 10, and in some cases we analyze the effect 
of the size of the file on the achieved performance. Similarly,  the rate of UDP 
traffic is increased in the simple scenario in order to saturate the secondary 
carrier. Similarly to the simulation campaigns of simple scenario with only 
unlicensed carrier, we analyze the performance in the LTE-U SDL CA simulation 
campaign for different distances between the operators.

For both examples, the simple and the indoor, we facilitate simulation campaign 
scripts that allow user to easily configure and run different scenarios. By using 
these scripts, user may also reproduce the results shown in the following sections. 
The scripts are placed in `laa-wifi-coexistence/campaigns` folder and in the 
following we provide separately more details on simulation campaign for the 
simple scenario and for the indoor.

*  **Reproducing the LTE-U SDL CA simple scenario campaign**. The simulation 
   campaign scripts are placed in `laa-wifi-coexistence/campaigns/lte-u-wifi-simple-ca-lteu`. 
   This folder contains the following files:  
    
    * config
    * plot_laa_wifi_simple-ca-ftp.sh
    * plot_laa_wifi_simple-ca-tcp.sh
    * plot_laa_wifi_simple-ca-udp.sh    
    * run-lte-u-wifi-simple-ca-ftp.sh
    * run-lte-u-wifi-simple-ca-tcp.sh
    * run-lte-u-wifi-simple-ca-udp.sh
  
    There are two files per type of the traffic application. One is for running and 
    another is for plotting the figures. E.g. `run-lte-u-wifi-simple-ca-udp.sh` 
    will run `UDP CBR` simulation campaign for laa-wifi-simple. The results will 
    be written to `results_udp` folder. By running a script 
    `plot_laa_wifi_simple-ca-udp.sh` it will be created `images_udp` folder. 
    In its subfolder `thumbnails` can be found `index.html` file that contains 
    the figures of the simulation UDP CBR simulation campaign. To configure 
    the simulation campaign parameters can be used `config` file.

*  **Reproducing the LTE-U SDL CA indoor simulation campaign results**. The 
   scripts are placed in `laa-wifi-coexistence/campaigns/lte-u-wifi-indoor-ca-lteu`. 
   This folder contains the following files:
  
    * config
    * plot_laa_wifi_indoor-ca-ftp.sh
    * plot_laa_wifi_indoor-ca-tcp.sh
    * plot_laa_wifi_indoor-ca-udp.sh
    * run-lte-u-wifi-indoor-ca-ftp.sh
    * run-lte-u-wifi-indoor-ca-tcp.sh
    * run-lte-u-wifi-indoor-ca-udp.sh

    The scripts for the indoor scenario can be used in the same way as the 
    scripts for simple scenario simulation campaign. 

Simple scenario with LTE-U SDL
##############################

In the continuation we list the common parameters of the simulation campaigns 
for the simple scenario:

.. csv-table:: laa-wifi-simple simulation campaigns parameters
   :header: "Parameter", "Value"
    
    Base duration (simulations with FTP model), 120 seconds
    Duration (simulations with UDP), 10 seconds
    Distance in laa-wifi-simple,"10 50 100 1000"
    CCM implementation,"RrSdlCcm"
    Number of secondary carrier (if 0 only unlicensed is used),"0 1 2"
    Retransmission over primary implementation option, Option C
    FTP application,"FTP Model 1"
    FTP Lambda,"5"
    UDP rates, "150, 300 and 450 Mbps"
    FTP file size,"512000 bytes"
    CSAT cycle duration, 160 ms
    Puncturing interval, 20 ms
    Puncturing duration, 1 ms
    Maximum allowed inter MIB time, 160 ms
    tOnMinInMilliSec,120 ms
    minOffTime, 20 ms
    alphaMU,0.8
    deltaTUp,0.05
    deltaTDown,0.05
    csatDutyCycle,0.5    
    MU threshold low,0.4
    MU threshold high,0.6
    LTE-U ED Threshold,-62dBm
    Wi-Fi Standard, 802.11n (full)
    Wi-Fi queue max size,2000 packets
    Wi-Fi ED Threshold,-62 dBm
    LDS interval, 160
    LTE/LTE-U antenna configuration, MIMO
    PDCCH duration, 2 symbols
    TCP segment size,1440
    TCP initial congestion window,10
    ns-3 RngRun,1


LTE-U SDL CA simple scenario with UDP CBR traffic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In figures :ref:`fig-simple-0PCC-1SCC-150Mbps` and 
:ref:`fig-simple-1PCC-1SCC-300Mbps` is shown the throughput performance for the 
simple scenario when the traffic is UDP CBR and the rate of CBR application is 
, respectively, 150 and 300 Mbps, and the total number of carriers for LTE-U 
and LTE operator is, respectively, 1 and 2. We first observe the case when 
the LTE-U and LTE cell operate only on a single carrier :ref:`fig-simple-0PCC-1SCC-150Mbps`. 
When the nodes are far away, Wi-Fi, LTE and LTE-U reach their maximum throughput. 
The Wi-Fi operator reaches around 116 Mbps, the LTE operator reaches 150 Mbps, 
while the LTE-U reaches around 123 Mbps, which is lower than LTE due to CSAT OFF 
time and AP scan. At the distance of 100 meters, we observe that 
when Wi-FI coexists with LTE or LTE-U its performance does not degrade. This is 
because the power of the interfering signal is much lower than of the signal of 
the originating cell. Additionally, the Wi-Fi operator sees the signal of 
the LTE/LTE-U with the power that is bellow the ED threshold, so it does 
not backoff. But, at 100m, LTE-U cell suffers a significant degradation of the 
performance since the power of the signal received from the Wi-Fi operator is 
above the energy levels necessary to perform successfully the preamble detection.
LTE-U cell observes a high medium utilization, around 0.88, thus its duty cycle 
drops to 0.5. When the distance reduces (in our example 10 m) throughput of 
Wi-Fi and LTE-U degrades, as the latency increases due to the interfering 
effects. In the case of Wi-Fi coexisting with LTE, its throughput goes to zero  
as it never finds the channel free for long enough to complete some its 
transmissions.

.. _fig-simple-0PCC-1SCC-150Mbps:

.. figure:: figures-lteu/scalaa2-results/simple/UDP-CBR-150Mbps.*
   :align: center
   :scale: 70 %
   
   UDP CBR 150 Mbps, NO PCC, only SCC. Simple scenario

In the figure :ref:`fig-simple-0PCC-1SCC-300Mbps` are shown results for the 
simulations in which LTE and LTE-U use aggregation of two carriers. While `LTeU` 
deployment contains of a regular LTE in primary cell, and LTE-U in the 
the secondary cell, the `LTE` deployment consist of regular LTE in both, primary 
and secondary, cells. Since more traffic is injected and more bandwidth is 
available, we observe a higher throughput performance of LTE-U and LTE cells. 
E.g. at 1000 m of distance LTE-U achieves around 272 Mbps, which corresponds 
to the sum of around 150 Mbps achieved in the primary cell and around 120 Mbps 
achieved in unlicensed band. The same holds for distances of 10 and 100 meters. 
The main trends of effect on Wi-Fi remain consistent with the case when only single 
carrier is deployed. In this simulation campaign, Wi-Fi over Wi-Fi achieves a 
higher throughput which is most probably due to a different behavior at the 
Wi-Fi MAC layer due to a limited MAC queue size and an increased packet 
arrival rate.  

.. _fig-simple-1PCC-1SCC-300Mbps:

.. figure:: figures-lteu/scalaa2-results/simple/UDP-CBR-300Mbps.*
   :align: center
   :scale: 70 %

   UDP CBR 300 Mbps, 1 PCC, 1 SCC. Simple scenario


LTE-U SDL CA simple scenario with FTP UDP traffic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If table :ref:`table-simple-0PCC-1SCC-FTPUDP` are shown results of the 
simulation campaign of the simple scenario when FTP UDP traffic is performed and 
when `0 PCC + 1 SCC` deployment is being used for LTE-U and LTE operator.
Similarly to the case of UDP traffic, in these curves we observe that at 
1000 m of distance operators do not not detect each other thus they do not 
interfere each other. At the distance of 50 m, we observe that the LTE-U 
performance is very similar to the case of 1000 m distance, and this is 
because LTE-U node detect low medium utilization at most of the time during the 
simulation duration, with an average of around 0.07, thus even if it Duty cycle 
varies over time, it converges to the maximum 0.875 value. This value is the 
same as in the case of 1000 m distance, thus the achieved performance is very 
similar. When the distance is 10 m, the effect of interference is much more 
prominent, which causes more retransmissions and in this case LTE-U detects 
an increased medium utilization at most of time during the simulation, with an 
average of around 0.2. Still, this medium utilization is still lower than the 
higher bound of allowed MU range, which is 0.4 - 0.6. Thus, the duty cycle 
again converges to 0.875, thus causing that LTE-U is not such a good neighbor to 
Wi-Fi as it is an another Wi-Fi operator. Differently from the previous campaign 
with UDP traffic we observe that when Wi-Fi coexist with regular LTE, it can 
achieve non zero throughput at small distances as 10 m. This is because the file 
transfer application will not cause that LTE cell occupies the channel constantly 
as the UDP CBR application, but rather for a smaller amount of times and thus 
leaving some channel time for a Wi-Fi to get in. 

.. |simple-FtpM1-1.5-lam-thr-11| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_1_5_10_512000_wifi_wifi_throughput.*
.. |simple-FtpM1-1.5-lam-thr-12| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_0_1_5_10_512000_lteu_wifi_throughput.*
.. |simple-FtpM1-1.5-lam-thr-13| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_0_1_5_10_512000_lte_wifi_throughput.*
.. |simple-FtpM1-1.5-lam-thr-21| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_1_5_50_512000_wifi_wifi_throughput.*
.. |simple-FtpM1-1.5-lam-thr-22| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_0_1_5_50_512000_lteu_wifi_throughput.*
.. |simple-FtpM1-1.5-lam-thr-23| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_0_1_5_50_512000_lte_wifi_throughput.*
.. |simple-FtpM1-1.5-lam-thr-31| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_1_5_1000_512000_wifi_wifi_throughput.*
.. |simple-FtpM1-1.5-lam-thr-32| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_0_1_5_1000_512000_lteu_wifi_throughput.*
.. |simple-FtpM1-1.5-lam-thr-33| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_0_1_5_1000_512000_lte_wifi_throughput.*

.. _table-simple-0PCC-1SCC-FTPUDP:

.. table:: Simple Scenario FTP UDP Low throughput (lambda = 1.5, file=512 KB), 0 PCC + 1 SCC

    +-------------------------------+-------------------------------+-------------------------------+
    | **Simple Scenario FTP UDP Low throughput (lambda = 1.5, file=512 KB), 0 PCC + 1 SCC**         |
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 10m                                                                                  |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-FtpM1-1.5-lam-thr-11| | |simple-FtpM1-1.5-lam-thr-12| | |simple-FtpM1-1.5-lam-thr-13| | 
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 50m                                                                                  |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-FtpM1-1.5-lam-thr-21| | |simple-FtpM1-1.5-lam-thr-22| | |simple-FtpM1-1.5-lam-thr-23| |
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 1000m                                                                                |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-FtpM1-1.5-lam-thr-31| | |simple-FtpM1-1.5-lam-thr-32| | |simple-FtpM1-1.5-lam-thr-33| |
    +-------------------------------+-------------------------------+-------------------------------+

.. |simple-FtpM1-10-lamb-thr-11| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_10_10_512000_wifi_wifi_throughput.*
.. |simple-FtpM1-10-lamb-thr-12| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_1_10_10_512000_lteu_wifi_throughput.*
.. |simple-FtpM1-10-lamb-thr-13| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_1_10_10_512000_lte_wifi_throughput.*
.. |simple-FtpM1-10-lamb-thr-21| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_10_50_512000_wifi_wifi_throughput.*
.. |simple-FtpM1-10-lamb-thr-22| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_1_10_50_512000_lteu_wifi_throughput.*
.. |simple-FtpM1-10-lamb-thr-23| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_1_10_50_512000_lte_wifi_throughput.*
.. |simple-FtpM1-10-lamb-thr-31| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_10_1000_512000_wifi_wifi_throughput.*
.. |simple-FtpM1-10-lamb-thr-32| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_1_10_1000_512000_lteu_wifi_throughput.*
.. |simple-FtpM1-10-lamb-thr-33| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_FtpM1_1_10_1000_512000_lte_wifi_throughput.*


In table :ref:`table-simple-1PCC-1SCC-FTPUDP` we show the performance of the 
simple scenario with an increased traffic load and an increased bandwidth for 
LTE-U and LTE operators. At the distance of 1000 m each operator reaches its 
maximum performance. With this FTP model and file size of 512 KB, LTE throughput 
is bounded by 270 Mbps approximately. We observe that at 50 m distance, the Wi-Fi 
performance is better when coexisting with LTE-U than with another Wi-Fi. This 
is because Wi-Fi does not backoff (LTE is below -62 dBm threshold) and 
interference is low compared to the useful signal. In this case, LTE-U 
observes on average the medium utilization of 0.23, which is lower than the 
higher bound of MU range, thus the duty cycle converges to the its higher bound 
which is 0.875. The average duty cycle is around 0.83 which is slightly lower 
than 0.875 which is the average duty cycle for the distance of 1000 m. Thus 
due to this difference there is a small difference between the performance of 
LTE-U operator for the distance of 50 and 1000 meters. 
However, at the distance of 10 m, the LTE-U cell observes the 
average medium utilization of around 0.32. Even if this average value is 
lower than the lower bound of the MU range (0.4-0.6), at some periods of time 
in the simulations a duty cycle value drops, and its average value over 
simulation time is 0.79. Because of decrease in duty cycle, many flows get 
delayed, and thus the performance of LTE-U cell degrades. We note that the 
performance of Wi-Fi in the case of 10 m of distance is very similar when 
coexisting with LTE-U as the one when coexisting with another Wi-Fi. 

.. _table-simple-1PCC-1SCC-FTPUDP:

.. table:: Simple Scenario FTP UDP High throughput (lambda = 10, file = 512KB), 1 PCC + 1 SCC

    +-------------------------------+-------------------------------+-------------------------------+
    | **Simple Scenario FTP UDP High throughput (lambda = 10, file = 512KB), 1 PCC + 1 SCC**        |
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 10m                                                                                  |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-FtpM1-10-lamb-thr-11| | |simple-FtpM1-10-lamb-thr-12| | |simple-FtpM1-10-lamb-thr-13| | 
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 50m                                                                                  |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-FtpM1-10-lamb-thr-21| | |simple-FtpM1-10-lamb-thr-22| | |simple-FtpM1-10-lamb-thr-23| |
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 1000m                                                                                |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-FtpM1-10-lamb-thr-31| | |simple-FtpM1-10-lamb-thr-32| | |simple-FtpM1-10-lamb-thr-33| |
    +-------------------------------+-------------------------------+-------------------------------+


LTE-U SDL CA simple scenario with FTP TCP traffic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In tables :ref:`table-simple-1PCC-1SCC-FTPTCP-512KB` and 
:ref:`table-simple-1PCC-1SCC-FTPTCP-5MB` we show the throughput result of 
the scenario simple simulation campaign when the traffic is FTP over TCP. We 
note that with the file with the file size of 512 KB LTE does not exceed 25 Mbps.
This is because when the file size is relatively small, the throughput 
performance is greatly affected by any minor latency in LTE stack or in the 
LTE-U CSAT. Thus, we have also analyzed the case of the larger file size, 
of 5.12 MB. When the file size is 512 KB and the distance 50 m we observe a 
benefit of having 1 PCC and 1 SCC deployment for both, LTE-U and Wi-Fi, 
operators. However, at the distance of 10 m, Wi-Fi operator is more 
affected when the neighbor is LTE-U than another Wi-Fi. In this case the 
average medium utilization observed by LTE-U is around 0.24 which leads to the 
duty cycle convergence to the highest value of 0.875, and in the average over 
simulation is around 0.86. Thus, Wi-Fi performance degrades due to effects of 
interference.

.. |simple-Tcp-5-lam-OpC-thr-11| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_5_10_512000_wifi_wifi_throughput.*
.. |simple-Tcp-5-lam-OpC-thr-12| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_10_512000_lteu_wifi_throughput.*
.. |simple-Tcp-5-lam-OpC-thr-13| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_10_512000_lte_wifi_throughput.*
.. |simple-Tcp-5-lam-OpC-thr-21| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_5_50_512000_wifi_wifi_throughput.*
.. |simple-Tcp-5-lam-OpC-thr-22| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_50_512000_lteu_wifi_throughput.*
.. |simple-Tcp-5-lam-OpC-thr-23| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_50_512000_lte_wifi_throughput.*
.. |simple-Tcp-5-lam-OpC-thr-31| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_5_1000_512000_wifi_wifi_throughput.*
.. |simple-Tcp-5-lam-OpC-thr-32| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_1000_512000_lteu_wifi_throughput.*
.. |simple-Tcp-5-lam-OpC-thr-33| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_1000_512000_lte_wifi_throughput.*


.. _table-simple-1PCC-1SCC-FTPTCP-512KB:

.. table:: Simple scenario FTP TCP, High throughput (lambda=5, file=512KB), 1 PCC + 1 SCC, Option C

    +-------------------------------+-------------------------------+-------------------------------+
    | **Simple scenario FTP TCP, High throughput (lambda=5, file=512KB), 1 PCC + 1 SCC, Option C**  |
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 10m                                                                                  |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-Tcp-5-lam-OpC-thr-11| | |simple-Tcp-5-lam-OpC-thr-12| | |simple-Tcp-5-lam-OpC-thr-13| | 
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 50m                                                                                  |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-Tcp-5-lam-OpC-thr-21| | |simple-Tcp-5-lam-OpC-thr-22| | |simple-Tcp-5-lam-OpC-thr-23| |
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 1000m                                                                                |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-Tcp-5-lam-OpC-thr-31| | |simple-Tcp-5-lam-OpC-thr-32| | |simple-Tcp-5-lam-OpC-thr-33| |
    +-------------------------------+-------------------------------+-------------------------------+

In table :ref:`table-simple-1PCC-1SCC-FTPTCP-5MB` are shown the results when 
the file size is 5.12 MB. When the file size is increased by a factor 10 the 
LTE achieves a much higher throughput, and the medium occupancy increases. 
E.g. at the distance of 10 m, LTE-U observes a medium utilization of 0.66, thus 
the duty cycle drops to 0.5, and in average is 0.55. But, even if the duty 
cycle is much lower comparing to the case of the simulation campaign with the 
file size of 512 KB, we observe that the performance of Wi-Fi is much more 
affected and that the most of flows achieve the throughput between 60 and 
80 Mbps, while in the previous case the Wi-Fi flows very achieving the throughput 
between 80 and 100 Mbps.
A degradation of performance in this case is due to much higher utilization of 
channel by LTE-U operator, which is having as a consequence an increase in 
interference among operators and Wi-Fi backoffs more. Finally, we conclude that  
with this configuration of FTP TCP traffic model, the LTE-U is a significantly 
better neighbor to Wi-Fi than another Wi-Fi.

In table :ref:`table-simple-TCP-options` we show comparison of different 
implementation options for "retransmission only over primary" requirement. We 
also provide the table :ref:`table_simple-TCP-options-numeric` in which are 
shown average throughput values of each operator. We observe that in the simple 
scenario and this specific configuration of parameters, OptionB provides the 
best performance for both operators. 

..  _table_simple-TCP-options-numeric:
.. csv-table:: A numeric values of comparison of different implementations of "retransmission only over primary" for the simple scenario
   :header: "Implementation option", "Average Wi-Fi throughput (Mbps)", "Average LTE-U throughput (Mbps)"

   Option A, 80.12, 21.37
   Option B, 81.36, 21.6
   Option C, 80.97, 21.45

.. |simple-Tcp-5-5120000-thr-11| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_5_10_5120000_wifi_wifi_throughput.*
.. |simple-Tcp-5-5120000-thr-12| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_10_5120000_lteu_wifi_throughput.*
.. |simple-Tcp-5-5120000-thr-13| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_10_5120000_lte_wifi_throughput.*
.. |simple-Tcp-5-5120000-thr-21| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_5_50_5120000_wifi_wifi_throughput.*
.. |simple-Tcp-5-5120000-thr-22| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_50_5120000_lteu_wifi_throughput.*
.. |simple-Tcp-5-5120000-thr-23| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_50_5120000_lte_wifi_throughput.*
.. |simple-Tcp-5-5120000-thr-31| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_5_1000_5120000_wifi_wifi_throughput.*
.. |simple-Tcp-5-5120000-thr-32| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_1000_5120000_lteu_wifi_throughput.*
.. |simple-Tcp-5-5120000-thr-33| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_1000_5120000_lte_wifi_throughput.*


.. _table-simple-1PCC-1SCC-FTPTCP-5MB:

.. table:: Simple scenario FTP TCP, High throughput (lambda=5, file=5.12 MB), 1 PCC + 1 SCC, Option C

    +-------------------------------+-------------------------------+-------------------------------+
    | **Simple scenario FTP TCP, High throughput (lambda=5, file=5.12MB), 1 PCC + 1 SCC, Option C** |
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 10m                                                                                  |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-Tcp-5-5120000-thr-11| | |simple-Tcp-5-5120000-thr-12| | |simple-Tcp-5-5120000-thr-13| | 
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 50m                                                                                  |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-Tcp-5-5120000-thr-21| | |simple-Tcp-5-5120000-thr-22| | |simple-Tcp-5-5120000-thr-23| |
    +-------------------------------+-------------------------------+-------------------------------+
    | Distance 1000m                                                                                |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-WiFi                     | WiFi-LteU                     |  WiFi-Lte                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-Tcp-5-5120000-thr-31| | |simple-Tcp-5-5120000-thr-32| | |simple-Tcp-5-5120000-thr-33| |
    +-------------------------------+-------------------------------+-------------------------------+



.. |simple-Tcp-5-lam-OpA-thr| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_10_512000_lteu_wifi_throughput_OptionA.*
.. |simple-Tcp-5-lam-OpB-thr| image:: figures-lteu/scalaa2-results/simple/indoor_ca_ftp_Tcp_1_5_10_512000_lteu_wifi_throughput_OptionB.*

.. _table-simple-TCP-options:

.. table:: Simple scenario FTP TCP with different implementations of "retransmission only over primary"

    +-------------------------------+-------------------------------+-------------------------------+
    | **Simple scenario FTP TCP, High throughput (lambda=5, file=512KB), 1 PCC + 1 SCC**            |
    +-------------------------------+-------------------------------+-------------------------------+
    | WiFi-LteU  - Distance 10m                                                                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | Option A                      | Option B                      |  Option C                     |
    +-------------------------------+-------------------------------+-------------------------------+
    | |simple-Tcp-5-lam-OpA-thr|    | |simple-Tcp-5-lam-OpB-thr|    | |simple-Tcp-5-lam-OpC-thr-12| | 
    +-------------------------------+-------------------------------+-------------------------------+

Indoor scenario with LTE-U SDL
##############################

In this section we show results obtained in the indoor scenario as defined by 
3GPP in [TR36889]_. 
Similarly to the simple scenario, we show results considering different 
deployment options for LTE-U and LTE operators, but here we focus more on 
the comparison of a deployment consisting of single unlicensed carrier 
(`O PCC + 1 SCC`) with the deployment consisting of single licensed and single 
unlicensed carrier (`1 PCC + 1 SCC`). We  organize this section by 
dividing results in different groups based on the type of traffic being 
configured, thus we have subsections for simulations with the CBR traffic over 
UDP transport, and the FTP traffic, over both UDP and TCP transport protocols. 

In the continuation we list default parameters used over all simulation 
campaigns for the indoor scenario:

.. csv-table:: laa-wifi-indoor simulation campaign parameters
   :header: "Parameter", "Value"
   
    Base duration (simulations with FTP model) ,120 seconds
    Duration (simulations with UDP model),10 seconds
    UDP rates,"1, 5, 10 and 20 Mbps"
    FTP lambda,"5"
    FTP application,"FTP Model 1"
    FTP file size,"512000 bytes"
    CCM implementation,"RrSdlCcm"
    Number of secondary carrier (if 0 only unlicensed is used),"0 1 2"
    Retransmission over primary implementation option,Option C
    CSAT cycle duration,160 ms
    Puncturing interval,20 ms
    Puncturing duration,1 ms
    Maximum allowed inter MIB time,160 ms
    tOnMinInMilliSec, 120 ms
    Minimum OFF time, 20 ms
    alphaMU,0.8
    deltaT up,0.05
    deltaT down,0.05
    CSAT duty cycle,0.5    
    MU threshold low,0.4
    MU threshold high,0.6
    LTE-U ED threshold,-62 dBm
    Wi-Fi standard,802.11n (full)
    Wi-Fi queue max size,2000 packets
    Wi-Fi ED threshold,-62 dBm
    LDS period,160 ms
    LTE/LTE-U antenna configuration, MIMO
    PDCCH duration,2 symbols
    TCP segment size,1440
    TCP initial congestion window,10
    ns-3 RngRun,1


Indoor UDP CBR
~~~~~~~~~~~~~~

In the following tables are shown the throughput and the latency for different rates 
of CBR applications: 1, 5, 10 and 20 Mbps. From the tables 
:ref:`table-indoor-thr-UDP-1Mbps` and :ref:`table-indoor-lat-UDP-1Mbps` we see that 
when the traffic is 1 Mbps, the LTE-U operator does not affect Wi-Fi even in a 
deployment with only unlicensed carrier. However, from the tables 
:ref:`table-indoor-thr-UDP-5Mbps` and :ref:`table-indoor-lat-UDP-5Mbps` we see that 
the WiFi performance starts to be affected by LTE-U with an increase of CBR rate, 
but still LTE-U is a good neighbor to Wi-Fi since its performance is very similar to the one achieved 
when coexisting with another Wi-Fi. In this scenario LTE-U observes medium 
utilization of around 0.81, hence its duty cycle converges to 0.5. Still the 
convergence is not immediate. Since the first indication of the medium 
occupancy being higher than the high MU threshold until duty cycle drops to 
0.5 it passes around 0.6 seconds. However, when UDP CBR rate is 10 Mbps, LTE-U 
starts to affect more Wi-Fi performance when only single 
carrier is being used. However, in the deployment `1 PCC + 1 SCC` LTE-U is a better 
neighbor to Wi-Fi than another Wi-Fi. Similarly to the case when the rate is 
5 Mbps, LTE-U observes medium utilization of around 0.81, and its duty cycle 
drops to 0.5. This time the delay in converging to the minimum value of duty 
cycle is around 0.3 seconds. The results for the CBR rate of 10 Mbps are shown 
in fugures :ref:`table-indoor-thr-UDP-10Mbps` and 
:ref:`table-indoor-lat-UDP10Mbps`. Finally in :ref:`table-indoor-thr-UDP-20Mbps` and 
:ref:`table-indoor-lat-UDP20Mbps` we show performance when CBR rate is 20Mbps. 
We see that in this case, LTE-U starts to affect Wi-Fi even in the 
configuration `1PCC + 1 SCC`. Still, seems that in this deployment there are 
less Wi-Fi flows with zero throughput than in the case when another operator is 
Wi-Fi. If we take as the coexistence evaluation metric the number of flows with 
non zero throughput value, then the best neighbor for Wi-Fi is the LTE-U operator 
operating only in unlicensed band (`0 PCC + 1 SCC` deployment).

.. |indoor-UDP-1Mbps-thr-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1Mbps_wifi_wifi_throughput.*
.. |indoor-UDP-1Mbps-thr-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_1Mbps_lteu_wifi_throughput.*
.. |indoor-UDP-1Mbps-thr-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_1Mbps_lte_wifi_throughput.*
.. |indoor-UDP-1Mbps-thr-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_1Mbps_lteu_wifi_throughput.*
.. |indoor-UDP-1Mbps-thr-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_1Mbps_lte_wifi_throughput.*

.. _table-indoor-thr-UDP-1Mbps:

.. table:: Indoor scenario UDP CBR Throughput (1 Mbps)

    +-----------------------------------------------------------------------------------+
    | **Indoor scenario UDP CBR Throughput (1 Mbps)**                                   |
    +-----------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-UDP-1Mbps-thr-11| | |indoor-UDP-1Mbps-thr-12| | |indoor-UDP-1Mbps-thr-13| |
    +---------------------------+---------------------------+---------------------------+
    | **1 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-UDP-1Mbps-thr-11| | |indoor-UDP-1Mbps-thr-22| | |indoor-UDP-1Mbps-thr-23| |
    +---------------------------+---------------------------+---------------------------+

.. |indoor-UDP-1Mbps-lat-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1Mbps_wifi_wifi_latency.*
.. |indoor-UDP-1Mbps-lat-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_1Mbps_lteu_wifi_latency.*
.. |indoor-UDP-1Mbps-lat-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_1Mbps_lte_wifi_latency.*
.. |indoor-UDP-1Mbps-lat-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_1Mbps_lteu_wifi_latency.*
.. |indoor-UDP-1Mbps-lat-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_1Mbps_lte_wifi_latency.*

.. _table-indoor-lat-UDP-1Mbps:

.. table:: Indoor scenario UDP CBR Latency (1 Mbps)

    +-----------------------------------------------------------------------------------+
    | **Indoor scenario UDP CBR Latency (1 Mbps)**                                      |
    +-----------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-UDP-1Mbps-lat-11| | |indoor-UDP-1Mbps-lat-12| | |indoor-UDP-1Mbps-lat-13| |
    +---------------------------+---------------------------+---------------------------+
    | **1 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-UDP-1Mbps-lat-11| | |indoor-UDP-1Mbps-lat-22| | |indoor-UDP-1Mbps-lat-23| |
    +---------------------------+---------------------------+---------------------------+


In the following is shown performance for the rate = 5Mbps:

.. |indoor-UDP-5Mbps-thr-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_5Mbps_wifi_wifi_throughput.*
.. |indoor-UDP-5Mbps-thr-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_5Mbps_lteu_wifi_throughput.*
.. |indoor-UDP-5Mbps-thr-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_5Mbps_lte_wifi_throughput.*
.. |indoor-UDP-5Mbps-thr-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_5Mbps_lteu_wifi_throughput.*
.. |indoor-UDP-5Mbps-thr-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_5Mbps_lte_wifi_throughput.*


.. _table-indoor-thr-UDP-5Mbps:

.. table:: Indoor scenario UDP CBR Throughput (5 Mbps)

    +-----------------------------------------------------------------------------------+
    | **Indoor scenario UDP CBR Throughput (5 Mbps)**                                   |
    +-----------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-UDP-5Mbps-thr-11| | |indoor-UDP-5Mbps-thr-12| | |indoor-UDP-5Mbps-thr-13| |
    +---------------------------+---------------------------+---------------------------+
    | **1 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-UDP-5Mbps-thr-11| | |indoor-UDP-5Mbps-thr-22| | |indoor-UDP-5Mbps-thr-23| |
    +---------------------------+---------------------------+---------------------------+

.. |indoor-UDP-5Mbps-lat-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_5Mbps_wifi_wifi_latency.*
.. |indoor-UDP-5Mbps-lat-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_5Mbps_lteu_wifi_latency.*
.. |indoor-UDP-5Mbps-lat-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_5Mbps_lte_wifi_latency.*
.. |indoor-UDP-5Mbps-lat-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_5Mbps_lteu_wifi_latency.*
.. |indoor-UDP-5Mbps-lat-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_5Mbps_lte_wifi_latency.*


.. _table-indoor-lat-UDP-5Mbps:

.. table:: Indoor scenario UDP CBR Latency (5  Mbps)

    +-----------------------------------------------------------------------------------+
    | **Indoor scenario UDP CBR Latency (5 Mbps)**                                      |
    +-----------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-UDP-5Mbps-lat-11| | |indoor-UDP-5Mbps-lat-12| | |indoor-UDP-5Mbps-lat-13| |
    +---------------------------+---------------------------+---------------------------+
    | **1 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-UDP-5Mbps-lat-11| | |indoor-UDP-5Mbps-lat-22| | |indoor-UDP-5Mbps-lat-23| |
    +---------------------------+---------------------------+---------------------------+

.. |indoor-UDP-10Mbps-thr-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_10Mbps_wifi_wifi_throughput.*
.. |indoor-UDP-10Mbps-thr-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_10Mbps_lteu_wifi_throughput.*
.. |indoor-UDP-10Mbps-thr-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_10Mbps_lte_wifi_throughput.*
.. |indoor-UDP-10Mbps-thr-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_10Mbps_lteu_wifi_throughput.*
.. |indoor-UDP-10Mbps-thr-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_10Mbps_lte_wifi_throughput.*


.. _table-indoor-thr-UDP-10Mbps:

.. table:: Indoor scenario UDP CBR Throughput (10 Mbps)

    +--------------------------------------------------------------------------------------+
    | **Indoor scenario UDP CBR Throughput (10 Mbps)**                                     |
    +--------------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                    |
    +----------------------------+----------------------------+----------------------------+
    | WiFi-WiFi                  | WiFi-LteU                  | WiFi-Lte                   |
    +----------------------------+----------------------------+----------------------------+
    | |indoor-UDP-10Mbps-thr-11| | |indoor-UDP-10Mbps-thr-12| | |indoor-UDP-10Mbps-thr-13| |
    +----------------------------+----------------------------+----------------------------+
    | **1 PCC + 1 SCC**                                                                    |
    +----------------------------+----------------------------+----------------------------+
    | WiFi-WiFi                  | WiFi-LteU                  | WiFi-Lte                   |
    +----------------------------+----------------------------+----------------------------+
    | |indoor-UDP-10Mbps-thr-11| | |indoor-UDP-10Mbps-thr-22| | |indoor-UDP-10Mbps-thr-23| |
    +----------------------------+----------------------------+----------------------------+

.. |indoor-UDP-10Mbps-lat-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_10Mbps_wifi_wifi_latency.*
.. |indoor-UDP-10Mbps-lat-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_10Mbps_lteu_wifi_latency.*
.. |indoor-UDP-10Mbps-lat-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_10Mbps_lte_wifi_latency.*
.. |indoor-UDP-10Mbps-lat-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_10Mbps_lteu_wifi_latency.*
.. |indoor-UDP-10Mbps-lat-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_10Mbps_lte_wifi_latency.*

.. _table-indoor-lat-UDP-10Mbps:

.. table:: Indoor scenario UDP CBR Latency (10 Mbps)

    +--------------------------------------------------------------------------------------+
    | **Indoor scenario UDP CBR Latency (10 Mbps)**                                        |
    +--------------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                    |
    +----------------------------+----------------------------+----------------------------+
    | WiFi-WiFi                  | WiFi-LteU                  | WiFi-Lte                   |
    +----------------------------+----------------------------+----------------------------+
    | |indoor-UDP-10Mbps-lat-11| | |indoor-UDP-10Mbps-lat-12| | |indoor-UDP-10Mbps-lat-13| |
    +----------------------------+----------------------------+----------------------------+
    | **1 PCC + 1 SCC**                                                                    |
    +----------------------------+----------------------------+----------------------------+
    | WiFi-WiFi                  | WiFi-LteU                  | WiFi-Lte                   |
    +----------------------------+----------------------------+----------------------------+
    | |indoor-UDP-10Mbps-lat-11| | |indoor-UDP-10Mbps-lat-22| | |indoor-UDP-10Mbps-lat-23| |
    +----------------------------+----------------------------+----------------------------+

.. |indoor-UDP-20Mbps-thr-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_20Mbps_wifi_wifi_throughput.*
.. |indoor-UDP-20Mbps-thr-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_20Mbps_lteu_wifi_throughput.*
.. |indoor-UDP-20Mbps-thr-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_20Mbps_lte_wifi_throughput.*
.. |indoor-UDP-20Mbps-thr-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_20Mbps_lteu_wifi_throughput.*
.. |indoor-UDP-20Mbps-thr-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_20Mbps_lte_wifi_throughput.*

.. _table-indoor-thr-UDP-20Mbps:

.. table:: Indoor scenario UDP CBR Throughput (20 Mbps)

    +--------------------------------------------------------------------------------------+
    | **Indoor scenario UDP CBR Throughput (20 Mbps)**                                     |
    +--------------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                    |
    +----------------------------+----------------------------+----------------------------+
    | WiFi-WiFi                  | WiFi-LteU                  | WiFi-Lte                   |
    +----------------------------+----------------------------+----------------------------+
    | |indoor-UDP-20Mbps-thr-11| | |indoor-UDP-20Mbps-thr-12| | |indoor-UDP-20Mbps-thr-13| |
    +----------------------------+----------------------------+----------------------------+
    | **1 PCC + 1 SCC**                                                                    |
    +----------------------------+----------------------------+----------------------------+
    | WiFi-WiFi                  | WiFi-LteU                  | WiFi-Lte                   |
    +----------------------------+----------------------------+----------------------------+
    | |indoor-UDP-20Mbps-thr-11| | |indoor-UDP-20Mbps-thr-22| | |indoor-UDP-20Mbps-thr-23| |
    +----------------------------+----------------------------+----------------------------+

.. |indoor-UDP-20Mbps-lat-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_20Mbps_wifi_wifi_latency.*
.. |indoor-UDP-20Mbps-lat-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_20Mbps_lteu_wifi_latency.*
.. |indoor-UDP-20Mbps-lat-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_0_20Mbps_lte_wifi_latency.*
.. |indoor-UDP-20Mbps-lat-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_20Mbps_lteu_wifi_latency.*
.. |indoor-UDP-20Mbps-lat-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_udp_Udp_1_20Mbps_lte_wifi_latency.*


.. _table-indoor-lat-UDP-20Mbps:

.. table:: Indoor scenario UDP CBR Latency (20 Mbps)

    +--------------------------------------------------------------------------------------+
    | **Indoor scenario UDP CBR Latency (20 Mbps)**                                        |
    +--------------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                    |
    +----------------------------+----------------------------+----------------------------+
    | WiFi-WiFi                  | WiFi-LteU                  | WiFi-Lte                   |
    +----------------------------+----------------------------+----------------------------+
    | |indoor-UDP-10Mbps-lat-11| | |indoor-UDP-10Mbps-lat-12| | |indoor-UDP-10Mbps-lat-13| |
    +----------------------------+----------------------------+----------------------------+
    | **1 PCC + 1 SCC**                                                                    |
    +----------------------------+----------------------------+----------------------------+
    | WiFi-WiFi                  | WiFi-LteU                  | WiFi-Lte                   |
    +----------------------------+----------------------------+----------------------------+
    | |indoor-UDP-10Mbps-lat-11| | |indoor-UDP-10Mbps-lat-22| | |indoor-UDP-10Mbps-lat-23| |
    +----------------------------+----------------------------+----------------------------+

Indoor scenario FTP UDP traffic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

From figures :ref:`table-indoor-thr-FTP-UDP` and :ref:`table-indoor-lat-FTP-UDP` 
we observe that in `0 PCC + 1 SCC` deployment Wi-Fi performance degrades when 
coexisting with LTE-U. However, in `1 PCC + 1 SCC` deployment LTE-U is good 
neighbor to Wi-Fi as performance achieved by Wi-Fi in this deployment is 
very similar to the one when the neighbor is another Wi-Fi. 

.. |indoor-FTP-5-lam-thr-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_5_512000_wifi_wifi_throughput.*   
.. |indoor-FTP-5-lam-thr-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_0_5_512000_lteu_wifi_throughput.*
.. |indoor-FTP-5-lam-thr-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_0_5_512000_lte_wifi_throughput.*
.. |indoor-FTP-5-lam-thr-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_1_5_512000_lteu_wifi_throughput.*
.. |indoor-FTP-5-lam-thr-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_1_5_512000_lte_wifi_throughput.*

.. |indoor-FTP-5-lam-lat-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_5_512000_wifi_wifi_latency.*   
.. |indoor-FTP-5-lam-lat-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_0_5_512000_lteu_wifi_latency.*
.. |indoor-FTP-5-lam-lat-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_0_5_512000_lte_wifi_latency.*
.. |indoor-FTP-5-lam-lat-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_1_5_512000_lteu_wifi_latency.*
.. |indoor-FTP-5-lam-lat-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_FtpM1_1_5_512000_lte_wifi_latency.*


.. _table-indoor-thr-FTP-UDP:

.. table:: Indoor scenario FTP UDP Throughput (lambda=5, file=512KB)

    +-----------------------------------------------------------------------------------+
    | **Indoor scenario FTP UDP Throughput (lambda=5, file=512KB)**                     |
    +-----------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-FTP-5-lam-thr-11| | |indoor-FTP-5-lam-thr-12| | |indoor-FTP-5-lam-thr-13| |
    +---------------------------+---------------------------+---------------------------+
    | **1 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-FTP-5-lam-thr-11| | |indoor-FTP-5-lam-thr-22| | |indoor-FTP-5-lam-thr-23| |
    +---------------------------+---------------------------+---------------------------+

.. _table-indoor-lat-FTP-UDP:

.. table:: Indoor scenario FTP UDP Latency (lambda=5, file=512KB) 
    
    +-----------------------------------------------------------------------------------+
    | **FTP UDP Latency (lambda=5)**                                                    |
    +-----------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-FTP-5-lam-lat-11| | |indoor-FTP-5-lam-lat-12| | |indoor-FTP-5-lam-lat-13| |
    +---------------------------+---------------------------+---------------------------+
    | **1 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-FTP-5-lam-lat-11| | |indoor-FTP-5-lam-lat-22| | |indoor-FTP-5-lam-lat-23| |
    +---------------------------+---------------------------+---------------------------+



Indoor scenario FTP TCP traffic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In figures :ref:`table-indoor-thr-FTP-TCP` and :ref:`table-indoor-thr-FTP-TCP` 
are shown throughput and latency results of simulation campaigns in which the 
traffic is FTP over TCP. Here we notice an important benefit for Wi-Fi of 
carrier aggregation and sending UL_DCI and LTE retransmissions over primary.
This makes that the Wi-Fi is being less interrupted and the throughput is 
increased in `1 PCC + 1 SCC` comparing to `0 PCC + 1SCC` deployment. Finally, 
in figure :ref:`table-indoor-TCP-options` we show comparison of different 
implementation of "retransmission over primary" requirement in the indoor 
scenario. From these curves seems that both `OptionB` and `OptionC` are 
better than the `OptionA`. But, the numerical values of the 
throughput average obtained from the flow monitor show that 
on average only `OptionC` provides better throughput for `LTE-U`, 
see table :ref:`table_indoor-TCP-options-numeric`. However, as this means that 
the unlicensed band is more efficiently used by LTE-U cell in order to achieve 
better performance, this affects Wi-Fi and its performance degrades.

..  _table_indoor-TCP-options-numeric:
.. csv-table:: A numeric values of comparison of different implementations of "retransmission only over primary for the indoor scenario"
   :header: "Implementation option", "Average Wi-Fi throughput (Mbps)", "Average LTE-U throughput (Mbps)"

   Option A, 77.05, 20.37 
   Option B, 79.02, 19.6
   Option C, 77.35, 20.45


.. |indoor-TCP-5-lam-thr-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_5_512000_wifi_wifi_throughput.*   
.. |indoor-TCP-5-lam-thr-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_0_5_512000_lteu_wifi_throughput.*
.. |indoor-TCP-5-lam-thr-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_0_5_512000_lte_wifi_throughput.*
.. |indoor-TCP-5-lam-thr-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_1_5_512000_lteu_wifi_throughput.*
.. |indoor-TCP-5-lam-thr-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_1_5_512000_lte_wifi_throughput.*

.. |indoor-TCP-5-lam-lat-11| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_5_512000_wifi_wifi_latency.*   
.. |indoor-TCP-5-lam-lat-12| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_0_5_512000_lteu_wifi_latency.*
.. |indoor-TCP-5-lam-lat-13| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_0_5_512000_lte_wifi_latency.*
.. |indoor-TCP-5-lam-lat-22| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_1_5_512000_lteu_wifi_latency.*
.. |indoor-TCP-5-lam-lat-23| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_1_5_512000_lte_wifi_latency.*


.. _table-indoor-thr-FTP-TCP:

.. table:: Indoor scenario FTP TCP Throughput (lambda=5, file=512KB)

    +-----------------------------------------------------------------------------------+
    | **Indoor scenario FTP TCP Throughput (lambda=5, file=512KB)**                     |
    +-----------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-TCP-5-lam-thr-11| | |indoor-TCP-5-lam-thr-12| | |indoor-TCP-5-lam-thr-13| |
    +---------------------------+---------------------------+---------------------------+
    | **1 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-TCP-5-lam-thr-11| | |indoor-TCP-5-lam-thr-22| | |indoor-TCP-5-lam-thr-23| |
    +---------------------------+---------------------------+---------------------------+
    
    
.. _table-indoor-lat-FTP-TCP:

.. table:: Indoor scenario FTP TCP Latency (lambda=5, file=512KB)
    
    +-----------------------------------------------------------------------------------+
    | **FTP TCP Latency (lambda=5)**                                                    |
    +-----------------------------------------------------------------------------------+
    | **0 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-TCP-5-lam-lat-11| | |indoor-TCP-5-lam-lat-12| | |indoor-TCP-5-lam-lat-13| |
    +---------------------------+---------------------------+---------------------------+
    | **1 PCC + 1 SCC**                                                                 |
    +---------------------------+---------------------------+---------------------------+
    | WiFi-WiFi                 | WiFi-LteU                 | WiFi-Lte                  |
    +---------------------------+---------------------------+---------------------------+
    | |indoor-TCP-5-lam-lat-11| | |indoor-TCP-5-lam-lat-22| | |indoor-TCP-5-lam-lat-23| |
    +---------------------------+---------------------------+---------------------------+

.. |indoor-Tcp-5-lam-OpA-thr| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_1_5_512000_lteu_wifi_throughput_OptionA.*
.. |indoor-Tcp-5-lam-OpB-thr| image:: figures-lteu/scalaa2-results/indoor/indoor_ca_ftp_Tcp_1_5_512000_lteu_wifi_throughput_OptionB.*

.. _table-indoor-TCP-options:

.. table:: Indoor scenario FTP TCP with different implementations of "retransmission only over primary"

    +-------------------------------+-------------------------------+---------------------------+
    | **Indoor scenario FTP TCP, Throughput (lambda=5, file=512KB), 1 PCC + 1 SCC**             |
    +-------------------------------+-------------------------------+---------------------------+
    | WiFi-LteU                                                                                 |
    +-------------------------------+-------------------------------+---------------------------+
    | Option A                      | Option B                      |  Option C                 |
    +-------------------------------+-------------------------------+---------------------------+
    | |indoor-Tcp-5-lam-OpA-thr|    | |indoor-Tcp-5-lam-OpB-thr|    | |indoor-Tcp-5-lam-thr-22| | 
    +-------------------------------+-------------------------------+---------------------------+


Profiling
*********
The profiling is performed by leveraging the ns-3 LTE module profiling framework. 
For more information on the profiling methodology see LTE module Profiling 
Documentation. In the following we provide a brief description of the 
profiling scenario. Two different simulation scripts are being used: 
``src/lte/examples/lena-profiling.cc`` and ``src/lte/examples/lena-simple-epc.cc``. 
The first script is providing the simulation scenario with E-UTRAN that 
contains a simplified RLC layer, which is behaving as a traffic generator.
This means that in this simulation scenario the network will be in saturation 
mode, and always will be some data to transmit. The objective here is to test 
the performance of E-UTRAN and specifically the layers bellow RLC. In the 
second script is added EPC, and are installed and configured applications in 
such a way to reproduce the saturation traffic model from the first scenario. 
In the second scenario, the objective is to check the performance of the 
simulation with a realistic RLC and the layers above, and the EPC.

The profiling was carried out on an Ubuntu 16.04.01 LTS operating system, 
and  the PC having processor Intel(R) Core(TM) i7-4790 CPU @ 3.60GH, 
and the RAM memory of 20GB. In the figure :ref:``fig-eutran-vs-eutranepc-5s`` 
and :ref:``fig-eutran-vs-eutranepc-10s`` are shown profiling results for the duration 
of simulations, respectively, 5 and 10 
seconds. We compare the profiling results of the original ns-3-dev repository, 
specifically ns-3.25 version, with the scalaa repository which is based on ns-3.25 
repository and is containing extensions developed in the scope of this 
project. Among, all the changes introduced 
during this project we foreseen that the finer grained physical model and the 
carrier aggregation are the modifications that might cause some increase in the 
simulation execution time. However, the profiling results do not show any 
notable increase in the execution time. From the results we see that there is 
a very small variation in the performance between ns-3-dev and scalaa. Note that some 
amount of variation may be introduced by the experimental environment, e.g  in 
the profiling framework being used it is not possible to remove effects of the 
processors' utilization by the system processes on the experimental PC being 
used for the profiling tests. Another,  cause for the small variations are different 
RNG streams that are caused by using different ns-3 repositories. Thus, we 
conclude that the code extensions introduced with this project are not 
affecting the simulation execution time in a typical use cases that are 
represented by the profiling simulation scenarios.

.. _fig-eutran-vs-eutranepc-5s:

.. figure:: figures-lteu/eutran-vs-eutranepc-5s.*
   :align: center
   
   EUTRAN vs EUTRAN + EPC simulation of 5 seconds, 1UE per eNb
   
   
.. _fig-eutran-vs-eutranepc-10s:

.. figure:: figures-lteu/eutran-vs-eutranepc-10s.*
   :align: center
   
   EUTRAN vs EUTRAN + EPC , simulation of 10 seconds, 1UE per eNb  




Open Issues and Future Work
***************************


.. [LTEUForum] LTE-U Forum  LTE-U SDL Coexistence Specifications v1.2 (2015-06), available online http://www.lteuforum.org/uploads/3/5/6/8/3568127/lte-u_forum_lte-u_sdl_coexistence_specifications_v1.2.pdf
.. [TR36889] 3GPP TR 36.889 "Study on Licensed-Assisted Access to Unlicensed Spectrum", (Release 12) TR 36.889v0.1.1 (2014-11), 3rd Generation Partnership Project, 2014.
.. [TR36814] 3GPP TR 36.814 "Further advancements for E-UTRA physical layer aspects", (Release 9), 3rd Generation Partnership Project, 2014.
.. [PhySimWifi] S. Papanastasiou, J. Mittag, E. Str m, H. Hartenstein, "Bridging the Gap between Physical Layer Emulation and Network Simulation," Proceedings of IEEE Wireless Communications and Networking Conference, Sydney, Australia, April 2010.  Available online at https://dsn.tm.kit.edu/english/ns3-physim.php.
.. [CSATQC] Qualcomm Research,  LTE in Unlicensed Spectrum: Harmonious Coexistence with Wi-Fi , white paper, June 2014
.. [Mezzavilla] M. Mezzavilla, M. Miozzo, M. Rossi, N. Baldo, M. Zorzi: "A Lightweight and Accurate Link Abstraction Model for System-Level Simulation of LTE Networks in ns-3", 2012.
.. [CSATALG] http://www.lteuforum.org/uploads/3/5/6/8/3568127/lte-u_coexistence_mechansim_qualcomm_may_28_2015.pdf
.. [LTEUFCSAT] LTU-U Forum, LTE-U CSAT Procedure TS v1.0 http://www.lteuforum.org/uploads/3/5/6/8/3568127/lte-u_forum_lte-u_sdl_csat_procedure_ts_v1.0.pdf
.. [TR36314] 3GPP TR 36.314 "E-UTRA Layer 2 - Measurements", (Release 13), 3rd Generation Partnership Project, 2016
.. [TR36133] 3GPP TR 36.133 "Requirements for support of radio resource management", (Release 10), 3rd Generation Partnership Project, 2011



