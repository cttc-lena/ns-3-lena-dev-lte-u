/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Nicola Baldo <nbaldo@cttc.es>
 * Biljana Bojovic <bbojovic@cttc.es> extensions to support dual-stripe operating on different channels, wifi nodes installation, channel scanning and channel selection algorithm
 */

#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/internet-module.h>
#include <ns3/lte-module.h>
#include <ns3/config-store-module.h>
#include <ns3/buildings-module.h>
#include <ns3/point-to-point-helper.h>
#include <ns3/applications-module.h>
#include <ns3/log.h>
#include <iomanip>
#include <ios>
#include <string>
#include <vector>
#include <ns3/spectrum-analyzer-helper.h>
#include <ns3/spectrum-model-ism2400MHz-res1MHz.h>
#include <ns3/scenario-helper.h>
#include <ns3/lte-u-wifi-coexistence-helper.h>

// The topology of this simulation program is inspired from
// 3GPP R4-092042, Section 4.2.1 Dual Stripe Model
// note that the term "apartments" used in that document matches with
// the term "room" used in the BuildingsMobilityModel

using namespace ns3;

enum ConfigMode_e
{
  CHANNEL_SCANNING,
  RUN_MODE
};

static ns3::GlobalValue g_nBlocks ("nBlocks",
                                   "Number of femtocell blocks",
                                   ns3::UintegerValue (1),
                                   ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_nApartmentsX ("nApartmentsX",
                                        "Number of apartments along the X axis in a femtocell block",
                                        ns3::UintegerValue (10),
                                        ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_nFloors ("nFloors",
                                   "Number of floors",
                                   ns3::UintegerValue (1),
                                   ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_nMacroEnbSites ("nMacroEnbSites",
                                          "How many macro sites there are",
                                          ns3::UintegerValue (3),
                                          ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_nMacroEnbSitesX ("nMacroEnbSitesX",
                                           "(minimum) number of sites along the X-axis of the hex grid",
                                           ns3::UintegerValue (1),
                                           ns3::MakeUintegerChecker<uint32_t> ());
static ns3::GlobalValue g_interSiteDistance ("interSiteDistance",
                                             "min distance between two nearby macro cell sites",
                                             ns3::DoubleValue (500),
                                             ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_areaMarginFactor ("areaMarginFactor",
                                            "how much the UE area extends outside the macrocell grid, "
                                            "expressed as fraction of the interSiteDistance",
                                            ns3::DoubleValue (0.5),
                                            ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_macroUeDensity ("macroUeDensity",
                                          "How many macrocell UEs there are per square meter",
                                          ns3::DoubleValue (0.00002),
                                          ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_homeEnbDeploymentRatio ("homeEnbDeploymentRatio",
                                                  "The HeNB deployment ratio as per 3GPP R4-092042",
                                                  ns3::DoubleValue (0.2),
                                                  ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_homeEnbActivationRatio ("homeEnbActivationRatio",
                                                  "The HeNB activation ratio as per 3GPP R4-092042",
                                                  ns3::DoubleValue (0.5),
                                                  ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_wifiApDeploymentRatio ("wifiApDeploymentRatio",
                                                  "The wifi AP deployment ratio",
                                                  ns3::DoubleValue (0.2),
                                                  ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_wifiApActivationRatio ("wifiApActivationRatio",
                                                  "The wif AP activation ratio",
                                                  ns3::DoubleValue (0.5),
                                                  ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_homeUesHomeEnbRatio ("homeUesHomeEnbRatio",
                                               "How many (on average) home UEs per HeNB there are in the simulation",
                                               ns3::DoubleValue (1.0),
                                               ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_wifiStaWifiApRatio ("wifStaWifiApRatio",
                                               "How many (on average) wifi STAs per wifi AP there are in the simulation",
                                               ns3::DoubleValue (1.0),
                                               ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_macroEnbTxPowerDbm ("macroEnbTxPowerDbm",
                                              "TX power [dBm] used by macro eNBs",
                                              ns3::DoubleValue (46.0),
                                              ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_homeEnbTxPowerDbm ("homeEnbTxPowerDbm",
                                             "TX power [dBm] used by HeNBs",
                                             ns3::DoubleValue (20.0),
                                             ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_macroEnbDlEarfcn ("macroEnbDlEarfcn",
                                            "DL EARFCN used by macro eNBs",
                                            ns3::UintegerValue (100),
                                            ns3::MakeUintegerChecker<uint16_t> ());
static ns3::GlobalValue g_homeEnbDlEarfcn ("homeEnbDlEarfcn",
                                           "DL EARFCN used by HeNBs",
                                           ns3::UintegerValue (100),
                                           ns3::MakeUintegerChecker<uint16_t> ());
static ns3::GlobalValue g_macroEnbBandwidth ("macroEnbBandwidth",
                                             "bandwidth [num RBs] used by macro eNBs",
                                             ns3::UintegerValue (100),
                                             ns3::MakeUintegerChecker<uint16_t> ());
static ns3::GlobalValue g_homeEnbBandwidth ("homeEnbBandwidth",
                                            "bandwidth [num RBs] used by HeNBs",
                                            ns3::UintegerValue (100),
                                            ns3::MakeUintegerChecker<uint16_t> ());
static ns3::GlobalValue g_simTime ("simTime",
                                   "Total duration of the simulation [s]",
                                   ns3::DoubleValue (1),
                                   ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_generateRem ("generateRem",
                                       "if true, will generate a REM and then abort the simulation;"
                                       "if false, will run the simulation normally (without generating any REM)",
                                       ns3::BooleanValue (false),
                                       ns3::MakeBooleanChecker ());
static ns3::GlobalValue g_spectrumAnalyzerEnabled ("spectrumAnalyzerEnabled",
                                       "if true, will generate a spectrum analyzer traces and then abort the simulation;"
                                       "if false, will run the simulation normally (without generating any spectrum analyzer)",
                                       ns3::BooleanValue (false),
                                       ns3::MakeBooleanChecker ());
static ns3::GlobalValue g_remRbId ("remRbId",
                                   "Resource Block Id of Data Channel, for which REM will be generated;"
                                   "default value is -1, what means REM will be averaged from all RBs of "
                                   "Control Channel",
                                   ns3::IntegerValue (-1),
                                   MakeIntegerChecker<int32_t> ());
static ns3::GlobalValue g_epc ("epc",
                               "If true, will setup the EPC to simulate an end-to-end topology, "
                               "with real IP applications over PDCP and RLC UM (or RLC AM by changing "
                               "the default value of EpsBearerToRlcMapping e.g. to RLC_AM_ALWAYS). "
                               "If false, only the LTE radio access will be simulated with RLC SM. ",
                               ns3::BooleanValue (false),
                               ns3::MakeBooleanChecker ());
static ns3::GlobalValue g_epcDl ("epcDl",
                                 "if true, will activate data flows in the downlink when EPC is being used. "
                                 "If false, downlink flows won't be activated. "
                                 "If EPC is not used, this parameter will be ignored.",
                                 ns3::BooleanValue (true),
                                 ns3::MakeBooleanChecker ());
static ns3::GlobalValue g_epcUl ("epcUl",
                                 "if true, will activate data flows in the uplink when EPC is being used. "
                                 "If false, uplink flows won't be activated. "
                                 "If EPC is not used, this parameter will be ignored.",
                                 ns3::BooleanValue (true),
                                 ns3::MakeBooleanChecker ());
static ns3::GlobalValue g_useUdp ("useUdp",
                                  "if true, the UdpClient application will be used. "
                                  "Otherwise, the BulkSend application will be used over a TCP connection. "
                                  "If EPC is not used, this parameter will be ignored.",
                                  ns3::BooleanValue (true),
                                  ns3::MakeBooleanChecker ());
static ns3::GlobalValue g_fadingTrace ("fadingTrace",
                                       "The path of the fading trace (by default no fading trace "
                                       "is loaded, i.e., fading is not considered)",
                                       ns3::StringValue (""),
                                       ns3::MakeStringChecker ());
static ns3::GlobalValue g_numBearersPerUe ("numBearersPerUe",
                                           "How many bearers per UE there are in the simulation",
                                           ns3::UintegerValue (1),
                                           ns3::MakeUintegerChecker<uint16_t> ());
static ns3::GlobalValue g_srsPeriodicity ("srsPeriodicity",
                                          "SRS Periodicity (has to be at least "
                                          "greater than the number of UEs per eNB)",
                                          ns3::UintegerValue (80),
                                          ns3::MakeUintegerChecker<uint16_t> ());
static ns3::GlobalValue g_outdoorUeMinSpeed ("outdoorUeMinSpeed",
                                             "Minimum speed value of macor UE with random waypoint model [m/s].",
                                             ns3::DoubleValue (0.0),
                                             ns3::MakeDoubleChecker<double> ());
static ns3::GlobalValue g_outdoorUeMaxSpeed ("outdoorUeMaxSpeed",
                                             "Maximum speed value of macor UE with random waypoint model [m/s].",
                                             ns3::DoubleValue (0.0),
                                             ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_pcap ("pcapEnabled",
                                "Whether to enable pcap trace files for Wi-Fi",
                                ns3::BooleanValue (false),
                                ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_configMode ("configMode",
                                       "In channel_scanning mode will perform channel scanning and then abort the simulation, "
                                       "In run_mode will run the simulation normally.",
                                        ns3::EnumValue (CHANNEL_SCANNING),
                                        ns3::MakeEnumChecker (CHANNEL_SCANNING, "scanMode",
                                                              RUN_MODE, "runMode"));

static ns3::GlobalValue g_monitorMode ("lteUMonitorMode",
                                       "monitor mode that will be used by lte-u nodes",
                                       ns3::EnumValue (LteUWifiCoexistenceHelper::REGULAR_WIFI_MONITOR),
                                       ns3::MakeEnumChecker(LteUWifiCoexistenceHelper::ENERGY_DETECTION_ONLY, "energy_detection_only",
                                                            LteUWifiCoexistenceHelper::REGULAR_WIFI_MONITOR, "regular_wifi"));

static ns3::GlobalValue g_lteUChannelAccessManagerInstallTime ("lteUChannelAccessManagerInstallTime",
                                                              "LTE channel access manager install time (seconds)",
                                                              ns3::DoubleValue (1),
                                                              ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_csatCycleDuration ("csatCycleDuration",
                                        "Duration of LTE-U CSAT cycle in number of subframes.",
                                        ns3::UintegerValue (320),
                                        ns3::MakeUintegerChecker<uint32_t> (40, MAX_CSAT_DURATION));

static ns3::GlobalValue g_csatDutyCycle ("csatDutyCycle",
                                        "Portion of ON time in CsatCycle. If csat is enabled this value is only used as inital value",
                                        ns3::DoubleValue (0.5),
                                        ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_puncturing ("puncturing",
                                        "CSAT parameter that defines the maximum number of consecutive ON subframes during the ON period.",
                                        ns3::UintegerValue (20),
                                        ns3::MakeUintegerChecker<uint32_t> (1, 20));

static ns3::GlobalValue g_puncturingDuration ("puncturingDuration",
                                        "CSAT parameter that defines the duration of puncturing in the number of subframes.",
                                        ns3::UintegerValue (1),
                                        ns3::MakeUintegerChecker<uint32_t> (1, 5));

static ns3::GlobalValue g_maxInterMibTime ("maxInterMibTime",
                                        "CSAT parameter that defines the maximum amount of time that can pass between two consecutive MIBs.",
                                        ns3::UintegerValue (160),
                                        ns3::MakeUintegerChecker<uint32_t> (10, 160));

static ns3::GlobalValue g_csatEnabled ("csatEnabled",
                                       "If true enable CSAT algorithm (duty cycle is update dynamically during simulation), "
                                       "If false use constant duty cycle during simulation.",
                                       ns3::BooleanValue (true),
                                      ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_tOnMinInMilliSec ("tOnMinInMilliSec",
                                        "CSAT parameter that defines the minimum total ON time during one CSAT cycle in number of subframes.",
                                        ns3::UintegerValue (360),
                                        ns3::MakeUintegerChecker<uint32_t> (4, MAX_CSAT_DURATION ));

static ns3::GlobalValue g_minOffTime ("minOffTime",
                                        "CSAT parameter that defines the minimum total off time during one CSAT cycle in number of subframes.",
                                        ns3::UintegerValue (20),
                                        ns3::MakeUintegerChecker<uint32_t> (20, 40));

static ns3::GlobalValue g_monitoringTime ("monitoringTime",
                                        "CSAT parameter that defines the The monitoring time in number of subframes.",
                                        ns3::UintegerValue (20),
                                        ns3::MakeUintegerChecker<uint32_t> (10, 40));

static ns3::GlobalValue g_alphaMU ("alphaMU",
                                   "A value used in CSAT algorithm for MU calculation.",
                                   ns3::DoubleValue (0.8),
                                   ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_thresholdMuLow ("thresholdMuLow",
                                          "The lower MU threshold value used in CSAT algorithm for duty cycle update as lower MU threshold.",
                                          ns3::DoubleValue (0.4),
                                          ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_thresholdMuHigh ("thresholdMuHigh",
                                           "The upper MU threshold value used in CSAT algorithm for the duty cycle update.",
                                           ns3::DoubleValue (0.6),
                                           ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_deltaTUp ("deltaTUp",
                                    "The delta value used in CSAT algorithm when duty cycle is being increased",
                                    ns3::DoubleValue (0.05),
                                     ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_deltaTDown ("deltaTDown",
                                      "The delta value used in CSAT algorithm when the duty cycle is being reduced.",
                                      ns3::DoubleValue (0.05),
                                      ns3::MakeDoubleChecker<double> (0.0,1.0));

static ns3::GlobalValue g_channelStatsFile ("channelStatsFile",
                                            "Name of the channel statistics file, in 'channel scanning mode' the statistics will be saved to this file, "
                                            "while in 'run mode' this file will be used as input to load information regarding channels.",
                                            ns3::StringValue ("channel-stats-all.tr"),
                                            ns3::MakeStringChecker());

static ns3::GlobalValue g_channelConfigFile ("channelConfigFile",
                                             "Name of the channel config file, in used to specify inital densities of lte-u eNbs and wifi APs per channel",
                                             ns3::StringValue ("channel-config.txt"),
                                             ns3::MakeStringChecker());

static ns3::GlobalValue g_useChannelConfigFile ("useChannelConfigFile",
                                       "If channel config file will be used to set configure which channels will be used, and what will be density of wifi and lte-u nodes per channel."
                                       "If false all enabled Wifi 5GHz channels will be used with randomly generated densities for wifi and lte-u.",
                                       ns3::BooleanValue (true),
                                      ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_outputDir ("outputDir",
                                     "directory where to store rem files",
                                     ns3::StringValue ("./"),
                                     ns3::MakeStringChecker ());




std::map< uint32_t, Ptr<RadioEnvironmentMapHelper> > g_remHelperList;

std::map< uint32_t, Wifi5GHzChannel > g_cellIdToWifiChannel;

uint32_t g_remHelperNotificationsCounter = 0;


struct InitialDensitiesPerChannel
{
  double homeEnbDensity;
  double wifiApDensity;
};

NS_LOG_COMPONENT_DEFINE ("ChannelSelectionDualStripe");

Wifi5GHzChannel GetChannelInfo (uint32_t channel)
{

  Wifi5GHzChannel chInfo;

  NS_ASSERT_MSG( channel==32 || channel==36 || channel==40 || channel==44 || channel==48 ||
                 channel==149 || channel==153 || channel==157 || channel==161 || channel==165, "Channel :"<<channel<<" not supported");

  for (uint32_t i = 0; i < NUM_5GHZ_WIFI_CHANNELS; i++)
    {
      if (g_wifi5GHzChannels[i].m_wifiChannelNumber == channel)
        {
          chInfo = g_wifi5GHzChannels[i];
          return chInfo;
        }
     }
  return chInfo;
}



bool AreOverlapping (Box a, Box b)
{
  return !((a.xMin > b.xMax) || (b.xMin > a.xMax) || (a.yMin > b.yMax) || (b.yMin > a.yMax));
}

class FemtocellBlockAllocator
{
public:
  FemtocellBlockAllocator (Box area, uint32_t nApartmentsX, uint32_t nFloors);
  void Create (uint32_t n);
  void Create ();
  Box GetArea();

private:
  bool OverlapsWithAnyPrevious (Box);
  Box m_area;
  uint32_t m_nApartmentsX;
  uint32_t m_nFloors;
  std::list<Box> m_previousBlocks;
  double m_xSize;
  double m_ySize;
  Ptr<UniformRandomVariable> m_xMinVar;
  Ptr<UniformRandomVariable> m_yMinVar;

};

Box FemtocellBlockAllocator::GetArea()
{
  return m_area;
}

FemtocellBlockAllocator::FemtocellBlockAllocator (Box area, uint32_t nApartmentsX, uint32_t nFloors)
  : m_area (area),
    m_nApartmentsX (nApartmentsX),
    m_nFloors (nFloors),
    m_xSize (nApartmentsX*10 + 20),
    m_ySize (70)
{
  m_xMinVar = CreateObject<UniformRandomVariable> ();
  m_xMinVar->SetAttribute ("Min", DoubleValue (area.xMin));
  m_xMinVar->SetAttribute ("Max", DoubleValue (area.xMax - m_xSize));
  m_yMinVar = CreateObject<UniformRandomVariable> ();
  m_yMinVar->SetAttribute ("Min", DoubleValue (area.yMin));
  m_yMinVar->SetAttribute ("Max", DoubleValue (area.yMax - m_ySize));
}

void
FemtocellBlockAllocator::Create (uint32_t n)
{
  for (uint32_t i = 0; i < n; ++i)
    {
      Create ();
    }
}

void
FemtocellBlockAllocator::Create ()
{
  Box box;
  uint32_t attempt = 0;
  do
    {
      NS_ASSERT_MSG (attempt < 100, "Too many failed attemtps to position apartment block. Too many blocks? Too small area?");
      box.xMin = m_xMinVar->GetValue ();
      box.xMax = box.xMin + m_xSize;
      box.yMin = m_yMinVar->GetValue ();
      box.yMax = box.yMin + m_ySize;
      ++attempt;
    }
  while (OverlapsWithAnyPrevious (box));

  NS_LOG_LOGIC ("allocated non overlapping block " << box);
  m_previousBlocks.push_back (box);
  Ptr<GridBuildingAllocator>  gridBuildingAllocator;
  gridBuildingAllocator = CreateObject<GridBuildingAllocator> ();
  gridBuildingAllocator->SetAttribute ("GridWidth", UintegerValue (1));
  gridBuildingAllocator->SetAttribute ("LengthX", DoubleValue (10*m_nApartmentsX));
  gridBuildingAllocator->SetAttribute ("LengthY", DoubleValue (10*2));
  gridBuildingAllocator->SetAttribute ("DeltaX", DoubleValue (10));
  gridBuildingAllocator->SetAttribute ("DeltaY", DoubleValue (10));
  gridBuildingAllocator->SetAttribute ("Height", DoubleValue (3*m_nFloors));
  gridBuildingAllocator->SetBuildingAttribute ("NRoomsX", UintegerValue (m_nApartmentsX));
  gridBuildingAllocator->SetBuildingAttribute ("NRoomsY", UintegerValue (2));
  gridBuildingAllocator->SetBuildingAttribute ("NFloors", UintegerValue (m_nFloors));
  gridBuildingAllocator->SetAttribute ("MinX", DoubleValue (box.xMin + 10));
  gridBuildingAllocator->SetAttribute ("MinY", DoubleValue (box.yMin + 10));
  gridBuildingAllocator->Create (2);
}

bool
FemtocellBlockAllocator::OverlapsWithAnyPrevious (Box box)
{
  for (std::list<Box>::iterator it = m_previousBlocks.begin (); it != m_previousBlocks.end (); ++it)
    {
      if (AreOverlapping (*it, box))
        {
          return true;
        }
    }
  return false;
}

void
PrintGnuplottableBuildingListToFile (std::string filename)
{
  std::ofstream outFile;
  outFile.open (filename.c_str (), std::ios_base::out | std::ios_base::trunc);
  if (!outFile.is_open ())
    {
      NS_LOG_ERROR ("Can't open file " << filename);
      return;
    }
  uint32_t index = 0;
  for (BuildingList::Iterator it = BuildingList::Begin (); it != BuildingList::End (); ++it)
    {
      ++index;
      Box box = (*it)->GetBoundaries ();
      outFile << "set object " << index
              << " rect from " << box.xMin  << "," << box.yMin
              << " to "   << box.xMax  << "," << box.yMax
              << " front fs empty "
              << std::endl;
    }
}


Box
GetBuildingsArea ()
{
  Box maxBox;

  for (BuildingList::Iterator it = BuildingList::Begin (); it != BuildingList::End (); ++it)
    {
      Box box = (*it)->GetBoundaries ();

      if (it==BuildingList::Begin ())
        {
          maxBox = box;
        }
      else
        {
          if (box.xMin < maxBox.xMin)
            {
              maxBox.xMin = box.xMin;
            }

          if (box.xMax > maxBox.xMax)
            {
              maxBox.xMax = box.xMax;
            }

          if (box.yMin < maxBox.yMin)
            {
              maxBox.yMin = box.yMin;
            }

          if (box.yMax > maxBox.yMax)
            {
              maxBox.yMax = box.yMax;
            }

          if (box.zMin < maxBox.zMin)
            {
              maxBox.zMin = box.zMin;
            }

          if (box.zMax > maxBox.zMax)
            {
              maxBox.zMax = box.zMax;
            }
        }
    }

  return maxBox;
}


void
PrintGnuplottableUeListToFile (std::string filename)
{
  std::ofstream outFile;
  outFile.open (filename.c_str (), std::ios_base::out | std::ios_base::trunc);
  if (!outFile.is_open ())
    {
      NS_LOG_ERROR ("Can't open file " << filename);
      return;
    }
  for (NodeList::Iterator it = NodeList::Begin (); it != NodeList::End (); ++it)
    {
      Ptr<Node> node = *it;
      int nDevs = node->GetNDevices ();
      for (int j = 0; j < nDevs; j++)
        {
          Ptr<LteUeNetDevice> uedev = node->GetDevice (j)->GetObject <LteUeNetDevice> ();

          if (uedev)
            {
              Vector pos = node->GetObject<MobilityModel> ()->GetPosition ();
              outFile << "set label \"" << uedev->GetImsi ()
                      << "\" at "<< pos.x << "," << pos.y << " left font \"Helvetica,4\" textcolor rgb \"grey\" front point pt 1 ps 0.3 lc rgb \"grey\" offset 0,0"
                      << std::endl;
            }
        }
    }
  outFile.close();
}


void
PrintGnuplottableUeListToFile (std::string filename, SpectrumModel sm)
{
  std::ofstream outFile;
  outFile.open (filename.c_str (), std::ios_base::out | std::ios_base::trunc);
  if (!outFile.is_open ())
    {
      NS_LOG_ERROR ("Can't open file " << filename);
      return;
    }
  for (NodeList::Iterator it = NodeList::Begin (); it != NodeList::End (); ++it)
    {
      Ptr<Node> node = *it;
      int nDevs = node->GetNDevices ();
      for (int j = 0; j < nDevs; j++)
        {
          Ptr<LteUeNetDevice> uedev = node->GetDevice (j)->GetObject <LteUeNetDevice> ();

          if (uedev)
            {
              Ptr<SpectrumModel> ueSm = LteSpectrumValueHelper::GetSpectrumModel(uedev->GetDlEarfcn(), 100);
              if (*ueSm!=sm)
                 continue;

              Vector pos = node->GetObject<MobilityModel> ()->GetPosition ();
              outFile << "set label \"" << uedev->GetImsi ()
                      << "\" at "<< pos.x << "," << pos.y << " left font \"Helvetica,4\" textcolor rgb \"grey\" front point pt 1 ps 0.3 lc rgb \"grey\" offset 0,0"
                      << std::endl;
            }
        }
    }
  outFile.close();
}


void
PrintGnuplottableEnbListToFile (std::string filename)
{
  std::ofstream outFile;
  outFile.open (filename.c_str (), std::ios_base::out | std::ios_base::trunc);
  if (!outFile.is_open ())
    {
      NS_LOG_ERROR ("Can't open file " << filename);
      return;
    }
  for (NodeList::Iterator it = NodeList::Begin (); it != NodeList::End (); ++it)
    {
      Ptr<Node> node = *it;
      int nDevs = node->GetNDevices ();
      for (int j = 0; j < nDevs; j++)
        {
          Ptr<LteEnbNetDevice> enbdev = node->GetDevice (j)->GetObject <LteEnbNetDevice> ();
          if (enbdev)
            {
              Vector pos = node->GetObject<MobilityModel> ()->GetPosition ();
              outFile << "set label \"" << enbdev->GetCellId ()
                      << "\" at "<< pos.x << "," << pos.y
                      << " left font \"Helvetica,4\" textcolor rgb \"white\" front  point pt 2 ps 0.3 lc rgb \"white\" offset 0,0"
                      << std::endl;
            }
        }
    }
}

void
PrintGnuplottableEnbListToFile (std::string filename, SpectrumModel sm)
{
  std::ofstream outFile;
  outFile.open (filename.c_str (), std::ios_base::out | std::ios_base::trunc);
  if (!outFile.is_open ())
    {
      NS_LOG_ERROR ("Can't open file " << filename);
      return;
    }
  for (NodeList::Iterator it = NodeList::Begin (); it != NodeList::End (); ++it)
    {
      Ptr<Node> node = *it;
      int nDevs = node->GetNDevices ();
      for (int j = 0; j < nDevs; j++)
        {
          Ptr<LteEnbNetDevice> enbdev = node->GetDevice (j)->GetObject <LteEnbNetDevice> ();
          if (enbdev)
            {
              Ptr<SpectrumModel> eNbSm = LteSpectrumValueHelper::GetSpectrumModel(enbdev->GetDlEarfcn(), enbdev->GetDlBandwidth());
              if (*eNbSm != sm)
                continue;
              Vector pos = node->GetObject<MobilityModel> ()->GetPosition ();
              outFile << "set label \"" << enbdev->GetCellId ()
                      << "\" at "<< pos.x << "," << pos.y
                      << " left font \"Helvetica,4\" textcolor rgb \"white\" front  point pt 2 ps 0.3 lc rgb \"white\" offset 0,0"
                      << std::endl;
            }
        }
    }
}


void
PrintPositionsToFile (std::string filename, std::map <uint32_t, Wifi5GHzChannel > & lteUNodeIdToBandInfo, std::map <uint32_t, Wifi5GHzChannel > & wifiNodeIdToBandInfo)
{
  std::ofstream outFile;
  outFile.open (filename.c_str (), std::ios_base::out | std::ios_base::trunc);
  if (!outFile.is_open ())
    {
      NS_LOG_ERROR ("Can't open file " << filename);
      return;
    }
  for (NodeList::Iterator it = NodeList::Begin (); it != NodeList::End (); ++it)
    {
      Ptr<Node> node = *it;

      if ((lteUNodeIdToBandInfo.find(node->GetId())!= lteUNodeIdToBandInfo.end()) || (wifiNodeIdToBandInfo.find(node->GetId())!= wifiNodeIdToBandInfo.end()))
        {

          std::string type = "";

          if (lteUNodeIdToBandInfo.find(node->GetId())!= lteUNodeIdToBandInfo.end())
            {
              type = "lte-u";
            }
          else
            {
              type = "wifi";
            }

          Vector pos = node->GetObject<MobilityModel> ()->GetPosition ();
          outFile << "node id :" <<node->GetId()
                      << "\t"<<type
                      << "\t"<< pos.x << "," << pos.y
                      << std::endl;
        }

    }
}

void
PrintRemPerCell (std::map<uint32_t, std::map<uint32_t, std::map<RemPointPos, double> > >& remPerCell)
{

  std::cout<<"\n PrintRemPerCell start."<<std::endl;
  StringValue stringValue;
  GlobalValue::GetValueByName ("outputDir", stringValue);
  std::string outputDir = stringValue.Get ();

  for (std::map<uint32_t, std::map<uint32_t, std::map<RemPointPos, double> > >::iterator itChannel = remPerCell.begin(); itChannel!= remPerCell.end(); itChannel++)
    {
      for (std::map<uint32_t, std::map<RemPointPos, double> > ::iterator itMapCellId = itChannel->second.begin(); itMapCellId!= itChannel->second.end(); itMapCellId++)
        {
          if (g_cellIdToWifiChannel.find(itMapCellId->first)==g_cellIdToWifiChannel.end())
            {
              continue;
            }
          if (g_cellIdToWifiChannel.find(itMapCellId->first)->second.m_wifiChannelNumber != itChannel->first)
            {
              continue;
            }

          std::stringstream remPerCellFileName;
          remPerCellFileName<<outputDir<<"channel-stats-per-cell-"<<itChannel->first<<"-"<<itMapCellId->first<<".rem";
          std::ofstream remPerCellStream;  ///< Stream the output to a file.
          remPerCellStream.open (remPerCellFileName.str().c_str());
          // check if there are all points from the grid, gnuplot needs all points from grid in order to generate figure
          if (itMapCellId->second.size() < g_remHelperList.find(itChannel->first)->second->GetAllRemPoints().size())
            {
              std::vector<RemPointPos> vec = g_remHelperList.find(itChannel->first)->second->GetAllRemPoints();
              for (uint32_t i = 0; i < vec.size(); i++)
                {
                  if (itMapCellId->second.find (vec.at(i)) == itMapCellId->second.end())
                   {
                     itMapCellId->second.insert(std::pair<RemPointPos, double>(vec.at(i),10e-17));
                   }
                }
            }
          for (std::map<RemPointPos, double> ::iterator itMapPosToPower = itMapCellId->second.begin(); itMapPosToPower!= itMapCellId->second.end(); itMapPosToPower++)
            {
              remPerCellStream  << itMapPosToPower->first.x << "\t"
                            << itMapPosToPower->first.y << "\t"
                            << itMapPosToPower->first.z << "\t"
                            << itMapPosToPower->second<<std::endl;; // cellId;
            }
          remPerCellStream.close();
        }
    }

  std::cout<<"\n PrintRemPerCell end."<<std::endl;
}

void
PrintChannelStatisticsPerChannel (bool printRemFilePerCell)
{
  std::cout<<"\n PrintChannelStatisticsPerChannel start."<<std::endl;

  StringValue stringValue;
  GlobalValue::GetValueByName ("outputDir", stringValue);
  std::string outputDir = stringValue.Get ();

  std::map<uint32_t, std::map<uint32_t, std::map<RemPointPos, double> > > tempChannelMap;

  for (std::map<uint32_t, Ptr<RadioEnvironmentMapHelper> >::iterator itDsh = g_remHelperList.begin(); itDsh!= g_remHelperList.end(); itDsh++ )
    {

      Ptr<RadioEnvironmentMapHelper> remHelper= itDsh->second;
      uint32_t channel = itDsh->first;

      std::stringstream remPerChanelFileName;
      remPerChanelFileName <<outputDir<<"channel-stats-"<<channel<<".rem";
      std::ofstream remPerChannelFileStream;  ///< Stream the output to a file.
      remPerChannelFileStream.open (remPerChanelFileName.str().c_str());
      NS_ASSERT_MSG (remPerChannelFileStream.is_open (), "Can't open file " << remPerChanelFileName.str().c_str());

      std::map<RemPointPos, std::vector<PowerPerCellIdLog> > logsPerPosition = remHelper->GetPowerPerCellIdMap();

      for (std::map<RemPointPos, std::vector<PowerPerCellIdLog> >::iterator itMap = logsPerPosition.begin(); itMap!= logsPerPosition.end(); itMap++)
        {
          remPerChannelFileStream
          << itMap->first.x << "\t"
          << itMap->first.y << "\t"
          << itMap->first.z;

          for (std::vector<PowerPerCellIdLog>::iterator it = itMap->second.begin(); it!= itMap->second.end(); it++)
            {
              remPerChannelFileStream
              << "\t"<<it->m_cellId // at which channel
              << "\t"<<it->m_power; // cellId

              // prepare map to print one rem file per cell id
              if (printRemFilePerCell)
                {
                  if (tempChannelMap.find(channel) == tempChannelMap.end())
                    {
                      std::map<RemPointPos, double> positionToPowerMap;
                      positionToPowerMap.insert(std::pair<RemPointPos,double> (itMap->first, it->m_power));

                      std::map<uint32_t, std::map<RemPointPos, double> > cellIdToRemPoints;
                      cellIdToRemPoints.insert(std::pair<uint32_t, std::map<RemPointPos, double > >(it->m_cellId, positionToPowerMap));
                      tempChannelMap.insert(std::pair<uint32_t, std::map<uint32_t, std::map<RemPointPos, double> > >(channel, cellIdToRemPoints));
                    }
                  else
                    {
                      if (tempChannelMap.find(channel)->second.find(it->m_cellId) == tempChannelMap.find(channel)->second.end())
                          {
                            std::map<RemPointPos, double> positionToPowerMap;
                            positionToPowerMap.insert(std::pair<RemPointPos,double> (itMap->first, it->m_power));
                            tempChannelMap.find(channel)->second.insert(std::pair<uint32_t, std::map<RemPointPos,double> > (it->m_cellId, positionToPowerMap));
                          }
                      else
                        {
                          tempChannelMap.find(channel)->second.find(it->m_cellId)->second.insert(std::pair<RemPointPos, double> (itMap->first, it->m_power));
                        }
                    }
                }
            }
          remPerChannelFileStream<<std::endl;
        }
      remPerChannelFileStream.close ();
    }

  if (printRemFilePerCell)
      {
        PrintRemPerCell (tempChannelMap);
      }
  std::cout<<"\n PrintChannelStatisticsPerChannel end."<<std::endl;
}


void PrintChannelStatisticsToSingleFile ()
{

  std::cout<<"\n PrintChannelStatisticsToSingleFile start."<<std::endl;
  StringValue stringValue;
  GlobalValue::GetValueByName ("channelStatsFile", stringValue);
  std::string fileName = stringValue.Get();
  GlobalValue::GetValueByName ("outputDir", stringValue);
  std::string outputDir = stringValue.Get ();

  std::ofstream powerPerCellIdOutputFile;  ///< Stream the output to a file.
  powerPerCellIdOutputFile.open (fileName.c_str());
  NS_ASSERT_MSG (powerPerCellIdOutputFile.is_open (), "Can't open file " << fileName);

  PrintGnuplottableBuildingListToFile (outputDir + "buildings.txt");
  PrintGnuplottableEnbListToFile (outputDir + "enbs.txt");
  PrintGnuplottableUeListToFile (outputDir + "ues.txt");

  for (std::map<uint32_t, Ptr<RadioEnvironmentMapHelper> >::iterator itDsh = g_remHelperList.begin(); itDsh!= g_remHelperList.end(); itDsh++ )
    {
      Ptr<RadioEnvironmentMapHelper> remHelper= itDsh->second;
      uint32_t channel = itDsh->first;

      std::map<RemPointPos, std::vector<PowerPerCellIdLog> > logsPerPosition = remHelper->GetPowerPerCellIdMap();
      for (std::map<RemPointPos, std::vector<PowerPerCellIdLog> >::iterator itMap = logsPerPosition.begin(); itMap!= logsPerPosition.end(); itMap++)
            {
              powerPerCellIdOutputFile
              << itMap->first.x << "\t"
              << itMap->first.y << "\t"
              << itMap->first.z << "\t"
              << channel << "\t";

              for (std::vector<PowerPerCellIdLog>::iterator it = itMap->second.begin(); it!= itMap->second.end(); it++)
                {
                  powerPerCellIdOutputFile
                  << "\t"<<it->m_cellId // at which channel
                  << "\t"<<it->m_power; // cellId
                }
              powerPerCellIdOutputFile<<std::endl;
            }
    }
  powerPerCellIdOutputFile.close ();
}


void LoadChannelConfiguration (std::map < uint32_t, InitialDensitiesPerChannel >& channelConfig)
{

  StringValue stringValue;
  GlobalValue::GetValueByName ("channelConfigFile", stringValue);
  std::string channelConfigFile = stringValue.Get();

  std::ifstream channelConfigStream;
  channelConfigStream.open (channelConfigFile.c_str () );
  if (! channelConfigStream.is_open () )
    {
      NS_FATAL_ERROR("Failed to open file:"<<channelConfigFile);
    }

  NS_LOG_INFO ("Load channel configuration from file:"<<channelConfigFile);

  int lineCount = 0;

  while (!channelConfigStream.eof ())
    {
      std::string line;
      std::istringstream lineBuffer;

      uint32_t channelNumber = 0;
      InitialDensitiesPerChannel densities;

      getline (channelConfigStream, line);

      // ignore empty lines
      if (line.empty ())
        {
          continue;
        }

      NS_ASSERT_MSG (!channelConfigStream.fail(),"Error reading input file in line:"<<lineCount<<". Line:"<<line);

      lineCount++;

      lineBuffer.str (line);
      lineBuffer >> channelNumber >> densities.homeEnbDensity >> densities.wifiApDensity;

      NS_ASSERT_MSG (!lineBuffer.fail(),"Error reading input file in line:"<<lineCount<<". Line:"<<line);

      channelConfig.insert(std::pair<uint32_t, InitialDensitiesPerChannel > (channelNumber, densities));
    }

  std::cout<<"\n Densities per channel loaded from config file:";
  for ( std::map <uint32_t, InitialDensitiesPerChannel >::iterator it = channelConfig.begin(); it!=channelConfig.end(); it++)
    {
       std::cout<<"\n "<<it->first<<"\t"<<it->second.homeEnbDensity<<"\t"<<it->second.wifiApDensity;
    }
  channelConfigStream.close ();

}

void LoadChannelStatisticsFile ( std::map <RemPointPos, std::map<uint32_t, std::map<uint32_t, double> > >& remMapAll)
{
  std::cout<<"\n Load channel statistics file"<<std::endl;

  StringValue stringValue;
  GlobalValue::GetValueByName ("channelStatsFile", stringValue);
  std::string channelStatsFile = stringValue.Get();

  std::ifstream channelStatsStream;
  channelStatsStream.open (channelStatsFile.c_str () );
  if (! channelStatsStream.is_open () )
    {
      NS_FATAL_ERROR("Failed to open file:"<<channelStatsFile);
    }

  int lineCount = 0;

  while (!channelStatsStream.eof ())
    {
      std::string line;
      std::istringstream lineBuffer;
      RemPointPos pos;
      uint32_t channel;
      std::map<uint32_t, double> cellIdToPower;

      getline (channelStatsStream, line);

      // ignore empty lines
      if (line.empty ())
        {
          continue;
        }

      NS_ASSERT_MSG (!channelStatsStream.fail(),"Error reading input file in line:"<<lineCount<<". Line:"<<line);
      lineCount++;
      lineBuffer.str (line);
      lineBuffer >> pos.x >> pos.y >>pos.z >> channel;
      NS_ASSERT_MSG (!lineBuffer.fail(),"Error reading input file in line:"<<lineCount<<". Line:"<<line);
      // read pairs of values (cellId, power)
      while (!lineBuffer.eof())
        {
          int32_t cellId = 0;
          double power = 0;
          lineBuffer >> cellId >> power;
          NS_ASSERT_MSG (!lineBuffer.fail(),"Error reading input file in line:"<<lineCount<<". Line:"<<line);
          cellIdToPower.insert(std::pair<uint32_t, double> (cellId, power));
        }
      NS_ASSERT_MSG (!lineBuffer.fail(),"Error reading input file in line:"<<lineCount<<". Line:"<<line);


      std::map <uint32_t, std::map<uint32_t, double> > channelToCellIdPowerMap;
      channelToCellIdPowerMap.insert(std::pair<uint32_t, std::map<uint32_t, double> > (channel, cellIdToPower));
      remMapAll.insert(std::pair<RemPointPos, std::map<uint32_t, std::map<uint32_t, double> > > (pos, channelToCellIdPowerMap));
    }
  channelStatsStream.close ();
}


Wifi5GHzChannel SelectChannel (std::map <uint32_t, Wifi5GHzChannel > & lteUNodeIdToBandInfo, const std::map <uint32_t, Wifi5GHzChannel > & wifiNodeIdToBandInfo,
                               std::vector <Wifi5GHzChannel> channelsInUse)
{
  uint32_t selectedChannel = 0;

  std::map<uint32_t, uint32_t> channelToCount;
  channelToCount.clear();

  for ( std::vector <Wifi5GHzChannel>::iterator it = channelsInUse.begin(); it != channelsInUse.end(); it++)
    {
      channelToCount.insert(std::pair<uint32_t, uint32_t> (it->m_wifiChannelNumber, 0));
    }

  for (std::map <uint32_t, Wifi5GHzChannel >::iterator it = lteUNodeIdToBandInfo.begin(); it != lteUNodeIdToBandInfo.end(); it++)
    {
      Wifi5GHzChannel chInfo = it->second;
      channelToCount.find(chInfo.m_wifiChannelNumber)->second++;
    }

  for (std::map <uint32_t, Wifi5GHzChannel >::const_iterator it = wifiNodeIdToBandInfo.begin(); it != wifiNodeIdToBandInfo.end(); it++)
    {
      Wifi5GHzChannel chInfo = it->second;
      channelToCount.find(chInfo.m_wifiChannelNumber)->second++;
    }

  // map has ordered elements, the smallest is first
  for (std::map<uint32_t, uint32_t>::iterator it = channelToCount.begin(); it != channelToCount.end(); it++)
    {
      if (it==channelToCount.begin())
        {
          selectedChannel = it->first;
        }
      else
        {
          if (it->second < channelToCount.find(selectedChannel)->second)
            {
              selectedChannel = it->first;
            }
        }
    }

  Wifi5GHzChannel channel = GetChannelInfo (selectedChannel);
  NS_LOG_INFO("Selected channel:"<<channel.m_wifiChannelNumber<<" with AP+LTE-U node count:"<<channelToCount.find(selectedChannel)->second);
  return channel;
};


class LenaDualStripeHelper
{

  Ptr<LteHexGridEnbTopologyHelper> lteHexGridEnbTopologyHelper;
  Ptr<LteUWifiCoexistenceHelper> lteUWifiCoexistenceHelper;
  Box macroUeBox;
  NodeContainer macroEnbs;
  NodeContainer macroUes;
  NodeContainer homeUes;
  NodeContainer wifiStas;
  NodeContainer remoteHostContainer;
  NodeContainer wifiRemoteHostContainer;
  NetDeviceContainer m_wifiP2pDevices;
  InternetStackHelper internet;
  NodeContainer ues;
  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ipv4InterfaceContainer ueIpIfaces;

public:

  LenaDualStripeHelper ();
  void Configure (Ptr<LteHelper> lteHelper, Ptr<EpcHelper> epcHelper, NodeContainer homeEnbs, NodeContainer wifiAps,
                  std::map <uint32_t, Wifi5GHzChannel >& lteUNodeIdToBandInfo, const std::map <uint32_t, Wifi5GHzChannel >& wifiNodeIdToBandInfo,
                  std::map <RemPointPos, std::map<uint32_t, std::map<uint32_t, double> > >& remMap, std::vector <Wifi5GHzChannel> channelsInUse);
  Box GetMacroUeBox();
};


Box
LenaDualStripeHelper::GetMacroUeBox()
{
  return macroUeBox;
}

LenaDualStripeHelper::LenaDualStripeHelper ()
{

}

void LenaDualStripeHelper::Configure ( Ptr<LteHelper> lteHelper, Ptr<EpcHelper> epcHelper, NodeContainer homeEnbs, NodeContainer wifiAps,
                                       std::map <uint32_t, Wifi5GHzChannel >& lteUNodeIdToBandInfo, const std::map <uint32_t, Wifi5GHzChannel >& wifiNodeIdToBandInfo,
                                       std::map <RemPointPos, std::map<uint32_t, std::map<uint32_t, double> > >& remMap, std::vector <Wifi5GHzChannel> channelsInUse)
{

  NS_LOG_INFO(" LteDualStripe lte-u nodes:"<<homeEnbs.GetN()<<"  wifiNodes:"<<wifiAps.GetN());

  // the scenario parameters get their values from the global attributes defined above
  UintegerValue uintegerValue;
  IntegerValue integerValue;
  DoubleValue doubleValue;
  BooleanValue booleanValue;
  StringValue stringValue;
  EnumValue enumValue;
  GlobalValue::GetValueByName ("nBlocks", uintegerValue);
  uint32_t nBlocks = uintegerValue.Get ();
  GlobalValue::GetValueByName ("nApartmentsX", uintegerValue);
  uint32_t nApartmentsX = uintegerValue.Get ();
  GlobalValue::GetValueByName ("nFloors", uintegerValue);
  uint32_t nFloors = uintegerValue.Get ();
  GlobalValue::GetValueByName ("nMacroEnbSites", uintegerValue);
  uint32_t nMacroEnbSites = uintegerValue.Get ();
  GlobalValue::GetValueByName ("homeUesHomeEnbRatio", doubleValue);
  double homeUesHomeEnbRatio = doubleValue.Get ();
  GlobalValue::GetValueByName ("wifStaWifiApRatio", doubleValue);
  double wifiStaWifiApRatio = doubleValue.Get ();
  GlobalValue::GetValueByName ("interSiteDistance", doubleValue);
  GlobalValue::GetValueByName ("macroUeDensity", doubleValue);
  double macroUeDensity = doubleValue.Get ();
  GlobalValue::GetValueByName ("macroEnbTxPowerDbm", doubleValue);
  double macroEnbTxPowerDbm = doubleValue.Get ();
  GlobalValue::GetValueByName ("homeEnbTxPowerDbm", doubleValue);
  double homeEnbTxPowerDbm = doubleValue.Get ();
  GlobalValue::GetValueByName ("macroEnbDlEarfcn", uintegerValue);
  uint32_t macroEnbDlEarfcn = uintegerValue.Get ();
  GlobalValue::GetValueByName ("macroEnbBandwidth", uintegerValue);
  uint16_t macroEnbBandwidth = uintegerValue.Get ();
  GlobalValue::GetValueByName ("homeEnbBandwidth", uintegerValue);
  uint16_t homeEnbBandwidth = uintegerValue.Get ();
  GlobalValue::GetValueByName ("epc", booleanValue);
  bool epc = booleanValue.Get ();
  GlobalValue::GetValueByName ("epcDl", booleanValue);
  bool epcDl = booleanValue.Get ();
  GlobalValue::GetValueByName ("epcUl", booleanValue);
  bool epcUl = booleanValue.Get ();
  GlobalValue::GetValueByName ("useUdp", booleanValue);
  bool useUdp = booleanValue.Get ();
  GlobalValue::GetValueByName ("fadingTrace", stringValue);
  std::string fadingTrace = stringValue.Get ();
  GlobalValue::GetValueByName ("numBearersPerUe", uintegerValue);
  uint16_t numBearersPerUe = uintegerValue.Get ();
  GlobalValue::GetValueByName ("srsPeriodicity", uintegerValue);
  uint16_t srsPeriodicity = uintegerValue.Get ();
  GlobalValue::GetValueByName ("outdoorUeMinSpeed", doubleValue);
  uint16_t outdoorUeMinSpeed = doubleValue.Get ();
  GlobalValue::GetValueByName ("outdoorUeMaxSpeed", doubleValue);
  uint16_t outdoorUeMaxSpeed = doubleValue.Get ();
  GlobalValue::GetValueByName ("configMode", enumValue);
  ConfigMode_e configMode = (ConfigMode_e) enumValue.Get ();
  Config::SetDefault ("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue (srsPeriodicity));

  GlobalValue::GetValueByName ("nMacroEnbSitesX", uintegerValue);
  uint32_t nMacroEnbSitesX = uintegerValue.Get ();
  GlobalValue::GetValueByName ("areaMarginFactor", doubleValue);
  double areaMarginFactor = doubleValue.Get ();
  GlobalValue::GetValueByName ("interSiteDistance", doubleValue);
  double interSiteDistance = doubleValue.Get ();

  // Specify some physical layer parameters that will be used below and
  // in the scenario helper.
  PhyParams phyParams;
  phyParams.m_bsTxGain = 5; // dB antenna gain
  phyParams.m_bsRxGain = 5; // dB antenna gain
  phyParams.m_bsTxPower = 18; // dBm
  phyParams.m_bsNoiseFigure = 5; // dB
  phyParams.m_ueTxGain = 0; // dB antenna gain
  phyParams.m_ueRxGain = 0; // dB antenna gain
  phyParams.m_ueTxPower = 18; // dBm
  phyParams.m_ueNoiseFigure = 9; // dB
  phyParams.m_channelNumber = 36;
  phyParams.m_receivers = 2;
  phyParams.m_transmitters = 2;

  double ueZ = 1.5;
  if (nMacroEnbSites > 0)
      {
        uint32_t currentSite = nMacroEnbSites -1;
        uint32_t biRowIndex = (currentSite / (nMacroEnbSitesX + nMacroEnbSitesX + 1));
        uint32_t biRowRemainder = currentSite % (nMacroEnbSitesX + nMacroEnbSitesX + 1);
        uint32_t rowIndex = biRowIndex*2 + 1;
        if (biRowRemainder >= nMacroEnbSitesX)
          {
            ++rowIndex;
          }
        uint32_t nMacroEnbSitesY = rowIndex;
        NS_LOG_LOGIC ("nMacroEnbSitesY = " << nMacroEnbSitesY);

        macroUeBox = Box (-areaMarginFactor*interSiteDistance,
                          (nMacroEnbSitesX + areaMarginFactor)*interSiteDistance,
                          -areaMarginFactor*interSiteDistance,
                          (nMacroEnbSitesY -1)*interSiteDistance*sqrt (0.75) + areaMarginFactor*interSiteDistance,
                          ueZ, ueZ);
      }
    else
      {
        // still need the box to place femtocell blocks
        macroUeBox = Box (0, 150, 0, 150, ueZ, ueZ);
      }

  lteUWifiCoexistenceHelper = CreateObject<LteUWifiCoexistenceHelper> ();


  // lena dual stripe helper classes
  lteHexGridEnbTopologyHelper = CreateObject<LteHexGridEnbTopologyHelper> ();
  lteHexGridEnbTopologyHelper->SetLteHelper (lteHelper);
  lteHexGridEnbTopologyHelper->SetAttribute ("InterSiteDistance", DoubleValue (interSiteDistance));
  lteHexGridEnbTopologyHelper->SetAttribute ("MinX", DoubleValue (interSiteDistance/2));
  lteHexGridEnbTopologyHelper->SetAttribute ("GridWidth", UintegerValue (nMacroEnbSitesX));

  // ip addresses for wifi p2p link
  Ipv4AddressHelper wifiIpv4hP2p ("27.0.0.0", "255.255.0.0");
  // ip addresses for wifi base stations
  Ipv4AddressHelper wifiIpv4hBs ("17.0.0.0", "255.255.0.0");
  // ip addresses for internet devices
  Ipv4AddressHelper ipv4hId ("1.0.0.0", "255.0.0.0");

  FemtocellBlockAllocator blockAllocator (macroUeBox, nApartmentsX, nFloors);
  blockAllocator.Create (nBlocks);

  double macroUeAreaSize = (macroUeBox.xMax - macroUeBox.xMin) * (macroUeBox.yMax - macroUeBox.yMin);
  uint32_t nMacroUes = round (macroUeAreaSize * macroUeDensity);
  NS_LOG_LOGIC ("nMacroUes = " << nMacroUes << " (density=" << macroUeDensity << ")");

  macroEnbs.Create (3 * nMacroEnbSites);
  std::cout<<"\n Macro eNbs count:"<<macroEnbs.GetN();

  macroUes.Create (nMacroUes);

  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");

  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::HybridBuildingsPropagationLossModel"));
  lteHelper->SetPathlossModelAttribute ("ShadowSigmaExtWalls", DoubleValue (0));
  lteHelper->SetPathlossModelAttribute ("ShadowSigmaOutdoor", DoubleValue (1));
  lteHelper->SetPathlossModelAttribute ("ShadowSigmaIndoor", DoubleValue (1.5));
  // use always LOS model
  lteHelper->SetPathlossModelAttribute ("Los2NlosThr", DoubleValue (1e6));
  lteHelper->SetSpectrumChannelType ("ns3::MultiModelSpectrumChannel");

  if (!fadingTrace.empty ())
    {
      lteHelper->SetAttribute ("FadingModel", StringValue ("ns3::TraceFadingLossModel"));
      lteHelper->SetFadingModelAttribute ("TraceFilename", StringValue (fadingTrace));
    }

  if (epc)
    {
      NS_LOG_LOGIC ("enabling EPC");
      lteHelper->SetEpcHelper (epcHelper);
    }

  // Macro eNBs in 3-sector hex grid

  mobility.Install (macroEnbs);
  BuildingsHelper::Install (macroEnbs);
  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (macroEnbTxPowerDbm));
  lteHelper->SetEnbAntennaModelType ("ns3::ParabolicAntennaModel");
  lteHelper->SetEnbAntennaModelAttribute ("Beamwidth",   DoubleValue (70));
  lteHelper->SetEnbAntennaModelAttribute ("MaxAttenuation",     DoubleValue (20.0));
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (macroEnbDlEarfcn));
  lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (18000));
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (macroEnbBandwidth));
  lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (macroEnbBandwidth));
  // needed for initial cell search
  lteHelper->SetUeDeviceAttribute ("DlEarfcn", UintegerValue (macroEnbDlEarfcn));

  NetDeviceContainer macroEnbDevs = lteHexGridEnbTopologyHelper->SetPositionAndInstallEnbDevice (macroEnbs);

  for (NetDeviceContainer::Iterator it = macroEnbDevs.Begin(); it!=macroEnbDevs.End(); it++)
  std::cout<<"\n Installed macro eNB with node Id:"<<(*it)->GetNode()->GetId()<<", cell id:"<<(*it)->GetObject<LteEnbNetDevice>()->GetCellId()<<" dl earfcn:"<<(*it)->GetObject<LteEnbNetDevice>()->GetDlEarfcn()<<std::endl;

  if (epc)
    {
      // this enables handover for macro eNBs
      lteHelper->AddX2Interface (macroEnbs);
    }

  Ptr<PositionAllocator> positionAlloc = CreateObject<RandomRoomPositionAllocator> ();
  mobility.SetPositionAllocator (positionAlloc);
  mobility.Install (homeEnbs);
  BuildingsHelper::Install (homeEnbs);

  mobility.Install (wifiAps);
  BuildingsHelper::Install (wifiAps);

  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (homeEnbTxPowerDbm));
  lteHelper->SetEnbAntennaModelType ("ns3::IsotropicAntennaModel");
  lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (18000));
  lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (homeEnbBandwidth));
  lteHelper->SetEnbDeviceAttribute ("CsgId", UintegerValue (1));
  lteHelper->SetEnbDeviceAttribute ("CsgIndication", BooleanValue (true));


  NetDeviceContainer homeEnbDevs;

  // clear original lteUNodeIfToBandInfo map, as channel selection will be used to determine which channel will be assigned to each node
  if (! (configMode == CHANNEL_SCANNING))
    {
        lteUNodeIdToBandInfo.clear();
    }

  for (NodeContainer::Iterator it = homeEnbs.Begin(); it!=homeEnbs.End(); it++)
    {
      uint32_t dlEarfcn = 0;
      uint32_t dlBandwidth = 0;
      Ptr<Node> homeEnb = (*it);
      NetDeviceContainer homeEnbDevice;

      Wifi5GHzChannel lteChInfo;

      if (configMode == CHANNEL_SCANNING)
        {
          NS_ASSERT_MSG (lteUNodeIdToBandInfo.find((*it)->GetId()) != lteUNodeIdToBandInfo.end(), "No channel information specified for node id:"<< (*it)->GetId());
          lteChInfo = lteUNodeIdToBandInfo.find((*it)->GetId())->second;
          dlEarfcn = lteChInfo.m_dlEarfcn;
          dlBandwidth = lteChInfo.m_dlBandwidthInNoOfRbs;
        }
      else
        {
          lteChInfo = SelectChannel (lteUNodeIdToBandInfo, wifiNodeIdToBandInfo, channelsInUse);
          dlEarfcn = lteChInfo.m_dlEarfcn;
          dlBandwidth = lteChInfo.m_dlBandwidthInNoOfRbs;
          //update nodeIdToBandInfo
          lteUNodeIdToBandInfo.insert (std::pair<uint32_t, Wifi5GHzChannel> ((*it)->GetId(), lteChInfo));
          std::cout<<"\n Node:"<<(*it)->GetId()<<" selected channel:"<<lteChInfo.m_wifiChannelNumber;
        }

      lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (dlEarfcn));
      lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (dlBandwidth));

      homeEnbDevice  = lteHelper->InstallEnbDevice (homeEnb);
      g_cellIdToWifiChannel.insert(std::pair<uint32_t, Wifi5GHzChannel>(homeEnbDevice.Get(0)->GetObject<LteEnbNetDevice>()->GetCellId(), lteChInfo));

      std::cout<<"\n Installed home eNb with node Id:"<<(*it)->GetId()<<", cell id:"<<homeEnbDevice.Get(0)->GetObject<LteEnbNetDevice>()->GetCellId()<<" channel:"<<lteChInfo.m_wifiChannelNumber <<" ,dl earfcn:"<<homeEnbDevice.Get(0)->GetObject<LteEnbNetDevice>()->GetDlEarfcn()<<std::endl;

      homeEnbDevs.Add (homeEnbDevice);
    }

  NS_LOG_LOGIC ("nHomeEnbs = " << homeEnbs.GetN());
  uint32_t nHomeUes = round (homeEnbs.GetN() * homeUesHomeEnbRatio);
  NS_LOG_LOGIC ("nHomeUes = " << nHomeUes);
  uint32_t nWifiSta = round (wifiAps.GetN() * wifiStaWifiApRatio);
  NS_LOG_LOGIC ("nWifiSta = " << nWifiSta);

  std::map <uint32_t, uint32_t> homeUeNodeIdToHomeEnbNodeId;
  NodeContainer::Iterator it = homeEnbs.Begin();
  // create one by one UE and place it in the same room where is placed its eNb
  while ((homeUes.GetN() < nHomeUes) && (it!=homeEnbs.End()))
    {
      NodeContainer ueNode;
      ueNode.Create(1);
      // home UEs located in the same apartment in which there are the Home eNBs
      positionAlloc = CreateObject<SameRoomPositionAllocator> ((*it));
      mobility.SetPositionAllocator (positionAlloc);
      mobility.Install (ueNode);
      BuildingsHelper::Install (ueNode);

      homeUes.Add(ueNode);

      // save information about which UE will be attached to which eNb
      uint32_t ueNodeId = ueNode.Get(0)->GetId();
      uint32_t eNbNodeId = (*it)->GetId();
      homeUeNodeIdToHomeEnbNodeId.insert(std::pair<uint32_t, uint32_t> (ueNodeId, eNbNodeId));

      it++;
      // start from the beginning until nHomeUes number of UEs are created
      if (it == homeEnbs.End())
        {
           it = homeEnbs.Begin();
        }
    }

  std::map <uint32_t, uint32_t> wifiStaNodeIdToWifiApNodeId;
  it = wifiAps.Begin();
 // create one by one STA and place it in the same room where is placed its AP
  while ((wifiStas.GetN() < nWifiSta) && (it != wifiAps.End()))
    {
      NodeContainer wifiStaNode;
      wifiStaNode.Create(1);
      // home UEs located in the same apartment in which there are the Home eNBs
      positionAlloc = CreateObject<SameRoomPositionAllocator> ((*it));
      mobility.SetPositionAllocator (positionAlloc);
      mobility.Install (wifiStaNode);
      BuildingsHelper::Install (wifiStaNode);

      wifiStas.Add(wifiStaNode);

      // save information about which UE will be attached to which eNb
      uint32_t wifiStaNodeId = wifiStaNode.Get(0)->GetId();
      uint32_t wifiApNodeId = (*it)->GetId();
      homeUeNodeIdToHomeEnbNodeId.insert(std::pair<uint32_t, uint32_t> (wifiStaNodeId, wifiApNodeId));

      it++;
      // start from the beginning until nHomeUes number of UEs are created
      if (it == wifiAps.End())
        {
           it = wifiAps.Begin();
        }
    }

  // set the home UE as a CSG member of the home eNodeBs
  lteHelper->SetUeDeviceAttribute ("CsgId", UintegerValue (1));

  NetDeviceContainer homeUeDevs;

  // set central frequency for initial cell search
  for (NodeContainer::Iterator it = homeUes.Begin(); it!=homeUes.End(); it++)
    {
      NS_ASSERT_MSG (homeUeNodeIdToHomeEnbNodeId.find((*it)->GetId())!= homeUeNodeIdToHomeEnbNodeId.end(), "eNb node id not found for UE node id:"<<(*it)->GetId());
      uint32_t eNbNodeId = homeUeNodeIdToHomeEnbNodeId.find((*it)->GetId())->second;
      //NS_ASSERT (homeEnbnodeIdToBandInfo)
      NS_ASSERT_MSG (lteUNodeIdToBandInfo.find(eNbNodeId)!= lteUNodeIdToBandInfo.end(), "not found channel info for eNb node id :"<<eNbNodeId);
      Wifi5GHzChannel lteChInfo;
      lteChInfo = lteUNodeIdToBandInfo.find(eNbNodeId)->second;
      lteHelper->SetUeDeviceAttribute ("DlEarfcn", UintegerValue (lteChInfo.m_dlEarfcn));
      NetDeviceContainer homeUeDev;
      homeUeDev = lteHelper->InstallUeDevice (*(it));
      homeUeDevs.Add(homeUeDev);
    }

  // macro Ues
  NS_LOG_LOGIC ("randomly allocating macro UEs in " << macroUeBox << " speedMin " << outdoorUeMinSpeed << " speedMax " << outdoorUeMaxSpeed);
  if (outdoorUeMaxSpeed!=0.0)
    {
      mobility.SetMobilityModel ("ns3::SteadyStateRandomWaypointMobilityModel");
      
      Config::SetDefault ("ns3::SteadyStateRandomWaypointMobilityModel::MinX", DoubleValue (macroUeBox.xMin));
      Config::SetDefault ("ns3::SteadyStateRandomWaypointMobilityModel::MinY", DoubleValue (macroUeBox.yMin));
      Config::SetDefault ("ns3::SteadyStateRandomWaypointMobilityModel::MaxX", DoubleValue (macroUeBox.xMax));
      Config::SetDefault ("ns3::SteadyStateRandomWaypointMobilityModel::MaxY", DoubleValue (macroUeBox.yMax));
      Config::SetDefault ("ns3::SteadyStateRandomWaypointMobilityModel::Z", DoubleValue (macroUeBox.zMin));
      Config::SetDefault ("ns3::SteadyStateRandomWaypointMobilityModel::MaxSpeed", DoubleValue (outdoorUeMaxSpeed));
      Config::SetDefault ("ns3::SteadyStateRandomWaypointMobilityModel::MinSpeed", DoubleValue (outdoorUeMinSpeed));

      // this is not used since SteadyStateRandomWaypointMobilityModel
      // takes care of initializing the positions;  however we need to
      // reset it since the previously used PositionAllocator
      // (SameRoom) will cause an error when used with homeDeploymentRatio=0
      positionAlloc = CreateObject<RandomBoxPositionAllocator> ();
      mobility.SetPositionAllocator (positionAlloc);
      mobility.Install (macroUes);

      // forcing initialization so we don't have to wait for Nodes to
      // start before positions are assigned (which is needed to
      // output node positions to file and to make AttachToClosestEnb work)
      for (NodeContainer::Iterator it = macroUes.Begin ();
           it != macroUes.End ();
           ++it)
        {
          (*it)->Initialize ();
        }
    }
    else
    {
      positionAlloc = CreateObject<RandomBoxPositionAllocator> ();
      Ptr<UniformRandomVariable> xVal = CreateObject<UniformRandomVariable> ();
      xVal->SetAttribute ("Min", DoubleValue (macroUeBox.xMin));
      xVal->SetAttribute ("Max", DoubleValue (macroUeBox.xMax));
      positionAlloc->SetAttribute ("X", PointerValue (xVal));
      Ptr<UniformRandomVariable> yVal = CreateObject<UniformRandomVariable> ();
      yVal->SetAttribute ("Min", DoubleValue (macroUeBox.yMin));
      yVal->SetAttribute ("Max", DoubleValue (macroUeBox.yMax));
      positionAlloc->SetAttribute ("Y", PointerValue (yVal));
      Ptr<UniformRandomVariable> zVal = CreateObject<UniformRandomVariable> ();
      zVal->SetAttribute ("Min", DoubleValue (macroUeBox.zMin));
      zVal->SetAttribute ("Max", DoubleValue (macroUeBox.zMax));
      positionAlloc->SetAttribute ("Z", PointerValue (zVal));
      mobility.SetPositionAllocator (positionAlloc);
      mobility.Install (macroUes);
    }
  BuildingsHelper::Install (macroUes);


  lteHelper->SetUeDeviceAttribute ("DlEarfcn", UintegerValue (macroEnbDlEarfcn));
  NetDeviceContainer macroUeDevs = lteHelper->InstallUeDevice (macroUes);


  // set channel MaxLossDb to discard all transmissions below -15dB SNR. 
  // The calculations assume -174 dBm/Hz noise PSD and 20 MHz bandwidth (73 dB)
  Ptr<SpectrumChannel> dlSpectrumChannel = lteHelper->GetDownlinkSpectrumChannel ();
  //           loss  = txpower -noisepower -snr ;   
  double dlMaxLossDb = std::max (macroEnbTxPowerDbm, homeEnbTxPowerDbm) 
                       -(-174.0 + 73.0 + 9.0) 
                       -(-15.0);
  dlSpectrumChannel->SetAttribute ("MaxLossDb", DoubleValue (dlMaxLossDb));
  Ptr<SpectrumChannel> ulSpectrumChannel = lteHelper->GetUplinkSpectrumChannel ();
  //           loss  = txpower -noisepower -snr ;   
  double ulMaxLossDb = std::max (macroEnbTxPowerDbm, homeEnbTxPowerDbm) 
                       -(-174.0 + 73.0 + 5.0) 
                       -(-15.0);
  ulSpectrumChannel->SetAttribute ("MaxLossDb", DoubleValue (ulMaxLossDb));


  Ipv4Address remoteHostAddr;
  
  Ptr<Node> remoteHost;
  NetDeviceContainer ueDevs;
  
  Ptr<Node> wifiRemoteHost;

  if (epc)
    {
      // it is important for counting LTE-U nodes, can be used only when there is EPC
      lteHelper->AddX2Interface (homeEnbs);

      NS_LOG_LOGIC ("setting up internet and remote host");

      // Create a single RemoteHost
      remoteHostContainer.Create (1);
      remoteHost = remoteHostContainer.Get (0);
      internet.Install (remoteHostContainer);

      // Create the Internet
      PointToPointHelper p2ph;
      p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
      p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
      p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
      Ptr<Node> pgw = epcHelper->GetPgwNode ();
      NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
      Ipv4InterfaceContainer internetIpIfaces = ipv4hId.Assign (internetDevices);
      // in this container, interface 0 is the pgw, 1 is the remoteHost
      remoteHostAddr = internetIpIfaces.GetAddress (1);

      Ipv4StaticRoutingHelper ipv4RoutingHelper;
      Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
      remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

      // for internetworking purposes, consider together home UEs and macro UEs
      ues.Add (homeUes);
      ues.Add (macroUes);
      ueDevs.Add (homeUeDevs);
      ueDevs.Add (macroUeDevs);

      // Install the IP stack on the UEs
      internet.Install (ues);
      ueIpIfaces = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueDevs));

      // attachment (needs to be done after IP stack configuration)
      // using initial cell selection
      lteHelper->Attach (macroUeDevs);

      // attach UEs in deterministic manner using map UeNodeIdToHomeEnbNodeId
      for (NetDeviceContainer::Iterator it = homeUeDevs.Begin(); it != homeUeDevs.End(); it++)
        {
          uint32_t ueNodeId = (*it)->GetNode()->GetId();
          NS_ASSERT_MSG (homeUeNodeIdToHomeEnbNodeId.find(ueNodeId)!= homeUeNodeIdToHomeEnbNodeId.end(), "eNb node id not found for UE node id:"<<ueNodeId);

          uint32_t homeEnbNodeId = homeUeNodeIdToHomeEnbNodeId.find(ueNodeId)->second;
          Ptr<NetDevice> homeEnbDevice =  homeEnbs.Get(homeEnbNodeId)->GetDevice(1);
          lteHelper->Attach((*it), homeEnbDevice);
        }
    }
  else
    {
      // macro UEs attached to the closest macro eNB
      lteHelper->AttachToClosestEnb (macroUeDevs, macroEnbDevs);

      // attach UEs in deterministic manner using map UeNodeIdToHomeEnbNodeId
      for (NetDeviceContainer::Iterator it = homeUeDevs.Begin(); it != homeUeDevs.End(); it++)
             {
               uint32_t ueNodeId = (*it)->GetNode()->GetId();
               NS_ASSERT_MSG (homeUeNodeIdToHomeEnbNodeId.find(ueNodeId)!= homeUeNodeIdToHomeEnbNodeId.end(), "eNb node id not found for UE node id:"<<ueNodeId);
               uint32_t homeEnbNodeId = homeUeNodeIdToHomeEnbNodeId.find(ueNodeId)->second;
               Ptr<Node> homeEnbNode = NodeList::GetNode (homeEnbNodeId);
               NS_ASSERT_MSG (homeEnbNode, "Lte-U node with NodeId:"<<homeEnbNodeId<<" does not exist.");
               Ptr<NetDevice> homeEnbDevice =  homeEnbNode->GetDevice(0);
               NS_ASSERT_MSG (homeEnbDevice, "Lte-U with NodeId:"<<homeEnbNodeId<<" does not have any LTE device installed");
               lteHelper->Attach((*it), homeEnbDevice);
             }
    }

  // Configure LTE-U
  ConfigureLteU (lteUWifiCoexistenceHelper, homeEnbDevs, phyParams);

  NetDeviceContainer wifiApDevs;
  NetDeviceContainer wifiStaDevs;
  PointToPointHelper p2pHelper;
  p2pHelper.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("1000Gb/s")));
  p2pHelper.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2pHelper.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));

  wifiRemoteHostContainer.Create (1);
  wifiRemoteHost = wifiRemoteHostContainer.Get (0);

  if (configMode == CHANNEL_SCANNING)
    {
      // replace wifi APs with LTE nodes, in order to be albe to use REM for channel selection
      Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (homeEnbTxPowerDbm));
      lteHelper->SetEnbAntennaModelType ("ns3::IsotropicAntennaModel");
      lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (18000));
      lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (homeEnbBandwidth));
      lteHelper->SetEnbDeviceAttribute ("CsgId", UintegerValue (1));
      lteHelper->SetEnbDeviceAttribute ("CsgIndication", BooleanValue (true));

      for (NodeContainer::Iterator it = wifiAps.Begin(); it!=wifiAps.End(); it++)
        {
          Ptr<Node> node = (*it);
          NetDeviceContainer wifiApDevice;
          NS_ASSERT_MSG (wifiNodeIdToBandInfo.find((*it)->GetId()) != wifiNodeIdToBandInfo.end(), "No channel information specified for node id:"<< (*it)->GetId());
          Wifi5GHzChannel lteChInfo = wifiNodeIdToBandInfo.find((*it)->GetId())->second;
          lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (lteChInfo.m_dlEarfcn));
          lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (lteChInfo.m_dlBandwidthInNoOfRbs));
          wifiApDevice  = lteHelper->InstallEnbDevice (node);
          g_cellIdToWifiChannel.insert(std::pair<uint32_t, Wifi5GHzChannel>(wifiApDevice.Get(0)->GetObject<LteEnbNetDevice>()->GetCellId(), lteChInfo));
          std::cout<<"\n Channel scanning mode, installed LTE device instead of wi-fi with node Id:"<<(*it)->GetId()<<", cell id:"<<wifiApDevice.Get(0)->GetObject<LteEnbNetDevice>()->GetCellId()<<" channel:"<<lteChInfo.m_wifiChannelNumber<<" dl earfcn:"<<wifiApDevice.Get(0)->GetObject<LteEnbNetDevice>()->GetDlEarfcn()<<std::endl;
          wifiApDevs.Add (wifiApDevice);
        }
    }
  else
    {
      internet.Install (wifiRemoteHostContainer);
      internet.Install (wifiStas);
      internet.Install (wifiAps);

      Ptr<SpectrumChannel> spectrumChannel = lteHelper->GetDownlinkSpectrumChannel ();
      wifiApDevs.Add (ConfigureWifiAp (wifiAps, phyParams, spectrumChannel, Ssid ("ns380211n")));
      // wifiStaDevs.Add (ConfigureWifiSta (wifiStas, phyParams, spectrumChannel, Ssid ("ns380211n")));

      for (uint32_t i = 0; i < wifiAps.GetN (); i++)
        {
          m_wifiP2pDevices.Add (p2pHelper.Install (wifiRemoteHost, wifiAps.Get (i)));
          // Add IP addresses to backhaul links
          Ipv4InterfaceContainer addresses = wifiIpv4hP2p.Assign (m_wifiP2pDevices);
          //std::cout<<"\n next network address:"<<
          wifiIpv4hP2p.NewNetwork ();//<<std::endl;
        }
      wifiIpv4hBs.Assign (wifiApDevs);

    }
  BuildingsHelper::MakeMobilityModelConsistent ();


  if (epc)
    {
      NS_LOG_LOGIC ("setting up applications");

      // Install and start applications on UEs and remote host
      uint16_t dlPort = 10000;
      uint16_t ulPort = 20000;

      // randomize a bit start times to avoid simulation artifacts
      // (e.g., buffer overflows due to packet transmissions happening
      // exactly at the same time) 
      Ptr<UniformRandomVariable> startTimeSeconds = CreateObject<UniformRandomVariable> ();
      if (useUdp)
        {
          startTimeSeconds->SetAttribute ("Min", DoubleValue (0));
          startTimeSeconds->SetAttribute ("Max", DoubleValue (0.010));
        }
      else
        {
          // TCP needs to be started late enough so that all UEs are connected
          // otherwise TCP SYN packets will get lost
          startTimeSeconds->SetAttribute ("Min", DoubleValue (0.100));
          startTimeSeconds->SetAttribute ("Max", DoubleValue (0.110));
        }

      for (uint32_t u = 0; u < ues.GetN (); ++u)
        {
          Ptr<Node> ue = ues.Get (u);
          // Set the default gateway for the UE
          Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ue->GetObject<Ipv4> ());
          ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);

          for (uint32_t b = 0; b < numBearersPerUe; ++b)
            {
              ++dlPort;
              ++ulPort;

              ApplicationContainer clientApps;
              ApplicationContainer serverApps;

              if (useUdp)
                {
                  if (epcDl)
                    {
                      NS_LOG_LOGIC ("installing UDP DL app for UE " << u);
                      UdpClientHelper dlClientHelper (ueIpIfaces.GetAddress (u), dlPort);
                      clientApps.Add (dlClientHelper.Install (remoteHost));
                      PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory", 
                                                           InetSocketAddress (Ipv4Address::GetAny (), dlPort));
                      serverApps.Add (dlPacketSinkHelper.Install (ue));
                    }
                  if (epcUl)
                    {
                      NS_LOG_LOGIC ("installing UDP UL app for UE " << u);
                      UdpClientHelper ulClientHelper (remoteHostAddr, ulPort);
                      clientApps.Add (ulClientHelper.Install (ue));
                      PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory", 
                                                           InetSocketAddress (Ipv4Address::GetAny (), ulPort));
                      serverApps.Add (ulPacketSinkHelper.Install (remoteHost));
                    }
                }
              else // use TCP
                {
                  if (epcDl)
                    {
                      NS_LOG_LOGIC ("installing TCP DL app for UE " << u);
                      BulkSendHelper dlClientHelper ("ns3::TcpSocketFactory",
                                                     InetSocketAddress (ueIpIfaces.GetAddress (u), dlPort));
                      dlClientHelper.SetAttribute ("MaxBytes", UintegerValue (0));
                      clientApps.Add (dlClientHelper.Install (remoteHost));
                      PacketSinkHelper dlPacketSinkHelper ("ns3::TcpSocketFactory", 
                                                           InetSocketAddress (Ipv4Address::GetAny (), dlPort));
                      serverApps.Add (dlPacketSinkHelper.Install (ue));
                    }
                  if (epcUl)
                    {
                      NS_LOG_LOGIC ("installing TCP UL app for UE " << u);
                      BulkSendHelper ulClientHelper ("ns3::TcpSocketFactory",
                                                     InetSocketAddress (remoteHostAddr, ulPort));
                      ulClientHelper.SetAttribute ("MaxBytes", UintegerValue (0));
                      clientApps.Add (ulClientHelper.Install (ue));
                      PacketSinkHelper ulPacketSinkHelper ("ns3::TcpSocketFactory", 
                                                           InetSocketAddress (Ipv4Address::GetAny (), ulPort));
                      serverApps.Add (ulPacketSinkHelper.Install (remoteHost));
                    }
                } // end if (useUdp)

              Ptr<EpcTft> tft = Create<EpcTft> ();
              if (epcDl)
                {
                  EpcTft::PacketFilter dlpf;
                  dlpf.localPortStart = dlPort;
                  dlpf.localPortEnd = dlPort;
                  tft->Add (dlpf); 
                }
              if (epcUl)
                {
                  EpcTft::PacketFilter ulpf;
                  ulpf.remotePortStart = ulPort;
                  ulpf.remotePortEnd = ulPort;
                  tft->Add (ulpf);
                }

              if (epcDl || epcUl)
                {
                  EpsBearer bearer (EpsBearer::NGBR_VIDEO_TCP_DEFAULT);
                  lteHelper->ActivateDedicatedEpsBearer (ueDevs.Get (u), bearer, tft);
                }
              Time startTime = Seconds (startTimeSeconds->GetValue ());
              serverApps.Start (startTime);
              clientApps.Start (startTime);

            } // end for b
        }

    } 
  else // (epc == false)
    {
      // for radio bearer activation purposes, consider together home UEs and macro UEs
      NetDeviceContainer ueDevs;
      ueDevs.Add (homeUeDevs);
      ueDevs.Add (macroUeDevs);
      for (uint32_t u = 0; u < ueDevs.GetN (); ++u)
        {
          Ptr<NetDevice> ueDev = ueDevs.Get (u);
          for (uint32_t b = 0; b < numBearersPerUe; ++b)
            {
              enum EpsBearer::Qci q = EpsBearer::NGBR_VIDEO_TCP_DEFAULT;
              EpsBearer bearer (q);
              lteHelper->ActivateDataRadioBearer (ueDev, bearer);
            }
        }
    }
}


void CheckIfAllRemHelpersHaveFinished ()
{
  g_remHelperNotificationsCounter++;

  std::cout<<"\n"<<Simulator::Now()<<" RemHelper finished. Total finished:"<<g_remHelperNotificationsCounter<<" out of :"<<g_remHelperList.size()<<std::endl;
  // when all rem helpers have finished stop simulation
  if (g_remHelperNotificationsCounter == g_remHelperList.size())
    {
      Simulator::Stop();
    }
}


int
main (int argc, char *argv[])
{

  // change some default attributes so that they are reasonable for
  // this scenario, but do this before processing command line
  // arguments, so that the user is allowed to override these settings
  Config::SetDefault ("ns3::UdpClient::Interval", TimeValue (MilliSeconds (1)));
  Config::SetDefault ("ns3::UdpClient::MaxPackets", UintegerValue (1000000));
  Config::SetDefault ("ns3::LteRlcUm::MaxTxBufferSize", UintegerValue (10 * 1024));

  CommandLine cmd;
  cmd.Parse (argc, argv);
  ConfigStore inputConfig;
  inputConfig.ConfigureDefaults ();
  // parse again so you can override input file default values via command line
  cmd.Parse (argc, argv);

  UintegerValue uintegerValue;
  IntegerValue integerValue;
  DoubleValue doubleValue;
  BooleanValue booleanValue;
  StringValue stringValue;
  EnumValue enumValue;

  GlobalValue::GetValueByName ("configMode", enumValue);
  ConfigMode_e configMode = (ConfigMode_e) enumValue.Get ();
  GlobalValue::GetValueByName ("nBlocks", uintegerValue);
  uint32_t nBlocks = uintegerValue.Get ();
  GlobalValue::GetValueByName ("nApartmentsX", uintegerValue);
  uint32_t nApartmentsX = uintegerValue.Get ();
  GlobalValue::GetValueByName ("nFloors", uintegerValue);
  uint32_t nFloors = uintegerValue.Get ();
  GlobalValue::GetValueByName ("homeEnbActivationRatio", doubleValue);
  double homeEnbActivationRatio = doubleValue.Get ();
  GlobalValue::GetValueByName ("wifiApActivationRatio", doubleValue);
  double wifiApActivationRatio = doubleValue.Get ();
  GlobalValue::GetValueByName ("spectrumAnalyzerEnabled", booleanValue);
  bool spectrumAnalyzerEnabled = booleanValue.Get ();
  GlobalValue::GetValueByName ("useChannelConfigFile", booleanValue);
  bool useChannelConfigFile = booleanValue.Get();
  GlobalValue::GetValueByName ("simTime", doubleValue);
  double simTime = doubleValue.Get ();
  GlobalValue::GetValueByName ("generateRem", booleanValue);
  bool generateRem = booleanValue.Get ();
  GlobalValue::GetValueByName ("outputDir", stringValue);
  std::string outputDir = stringValue.Get ();

  GlobalValue::GetValueByName ("csatCycleDuration", uintegerValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::CsatCycleDuration", uintegerValue);
  GlobalValue::GetValueByName ("csatDutyCycle", doubleValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::CsatDutyCycle", doubleValue);
  GlobalValue::GetValueByName ("puncturing", uintegerValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::Puncturing", uintegerValue);
  GlobalValue::GetValueByName ("puncturingDuration", uintegerValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::PuncturingDuration", uintegerValue);
  GlobalValue::GetValueByName ("maxInterMibTime", uintegerValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::MaxInterMibTime", uintegerValue);
  GlobalValue::GetValueByName ("csatEnabled", booleanValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::CsatEnabled", booleanValue);
  GlobalValue::GetValueByName ("tOnMinInMilliSec", uintegerValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::TOnMinInMilliSec", uintegerValue);
  GlobalValue::GetValueByName ("minOffTime", uintegerValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::MinOffTime", uintegerValue);
  GlobalValue::GetValueByName ("monitoringTime", uintegerValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::MonitoringTime", uintegerValue);
  GlobalValue::GetValueByName ("alphaMU", doubleValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::AlphaMU", doubleValue);
  GlobalValue::GetValueByName ("thresholdMuLow", doubleValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::ThresholdMuLow", doubleValue);
  GlobalValue::GetValueByName ("thresholdMuHigh", doubleValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::ThresholdMuHigh", doubleValue);
  GlobalValue::GetValueByName ("deltaTUp", doubleValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::DeltaTUp", doubleValue);
  GlobalValue::GetValueByName ("deltaTDown", doubleValue);
  Config::SetDefault ("ns3::LteUCoexistenceManager::DeltaTDown", doubleValue);

  Ptr<UniformRandomVariable> lteUDeploymentRatioRanVar = CreateObject<UniformRandomVariable> ();
  lteUDeploymentRatioRanVar->SetAttribute ("Min", DoubleValue (0));
  lteUDeploymentRatioRanVar->SetAttribute ("Max", DoubleValue (1));

  Ptr<UniformRandomVariable> wifiUDeploymentRatioRanVar = CreateObject<UniformRandomVariable> ();
  wifiUDeploymentRatioRanVar->SetAttribute ("Min", DoubleValue (0));
  wifiUDeploymentRatioRanVar->SetAttribute ("Max", DoubleValue (1));

  NodeContainer allHomeEnbs;
  NodeContainer allWifiAps;

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<EpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();

  std::map <uint32_t, Wifi5GHzChannel > wifiNodeIdToBandInfo;
  std::map <uint32_t, Wifi5GHzChannel > lteUNodeIdToBandInfo;

  std::map < uint32_t, InitialDensitiesPerChannel > initialNodeDensitiesMap;
  std::vector <Wifi5GHzChannel> channelsInUse;

  if (!useChannelConfigFile)
    {
      for (uint32_t i = 0; i < NUM_5GHZ_WIFI_CHANNELS; i++)
        {
          double lteUDeploymentRatio = lteUDeploymentRatioRanVar->GetValue ();
          double wifiDeploymentRatio = wifiUDeploymentRatioRanVar->GetValue ();

          InitialDensitiesPerChannel densities;
          densities.homeEnbDensity = lteUDeploymentRatio;
          densities.wifiApDensity = wifiDeploymentRatio;

          initialNodeDensitiesMap.insert(std::pair<uint32_t, InitialDensitiesPerChannel > (g_wifi5GHzChannels[i].m_wifiChannelNumber, densities));

          uint32_t nHomeEnbs = round (4 * nApartmentsX * nBlocks * nFloors * lteUDeploymentRatio * homeEnbActivationRatio);
          uint32_t nWifiAps = round (4 * nApartmentsX * nBlocks * nFloors * wifiDeploymentRatio * wifiApActivationRatio);

          std::cout<<"\n Channel:"<<g_wifi5GHzChannels[i].m_wifiChannelNumber<<" lte-U nodes:"<<nHomeEnbs<< " ,wifi nodes:"<<nWifiAps;

          NodeContainer homeEnbs;
          NodeContainer homeUes;
          NodeContainer wifiAps;
          NodeContainer wifiStas;

          homeEnbs.Create (nHomeEnbs);
          wifiAps.Create (nWifiAps);

          channelsInUse.push_back(g_wifi5GHzChannels[i]);

          // if channel scanning mode save information to which channel
          for (NodeContainer::Iterator it = homeEnbs.Begin(); it!=homeEnbs.End(); it++)
            {
              Wifi5GHzChannel chInfo = g_wifi5GHzChannels[i];
              uint32_t nodeId = (*it)->GetId();
              lteUNodeIdToBandInfo.insert(std::pair <uint32_t, Wifi5GHzChannel> (nodeId, chInfo));
            }

          for (NodeContainer::Iterator it = wifiAps.Begin(); it!=wifiAps.End(); it++)
            {
              Wifi5GHzChannel chInfo = g_wifi5GHzChannels[i];
              uint32_t nodeId = (*it)->GetId();
              wifiNodeIdToBandInfo.insert(std::pair <uint32_t, Wifi5GHzChannel> (nodeId, chInfo));
            }

          allHomeEnbs.Add (homeEnbs);
          allWifiAps.Add (wifiAps);
        }
    }
  else
    {
      LoadChannelConfiguration (initialNodeDensitiesMap);

      for (std::map < uint32_t, InitialDensitiesPerChannel >::iterator itConfigMap = initialNodeDensitiesMap.begin(); itConfigMap != initialNodeDensitiesMap.end(); itConfigMap++)
        {
          double lteUDeploymentRatio = itConfigMap->second.homeEnbDensity;
          double wifiDeploymentRatio = itConfigMap->second.wifiApDensity;

          uint32_t nHomeEnbs = round (4 * nApartmentsX * nBlocks * nFloors * lteUDeploymentRatio * homeEnbActivationRatio);
          uint32_t nWifiAps = round (4 * nApartmentsX * nBlocks * nFloors * wifiDeploymentRatio * wifiApActivationRatio);

          std::cout<<"\n Channel:"<<itConfigMap->first<<" lte-U nodes:"<<nHomeEnbs<< " ,wifi nodes:"<<nWifiAps;

          NodeContainer homeEnbs;
          NodeContainer wifiAps;

          homeEnbs.Create (nHomeEnbs);
          wifiAps.Create (nWifiAps);

          channelsInUse.push_back(GetChannelInfo(itConfigMap->first));

          // if channel scanning mode save information to which channel
          for (NodeContainer::Iterator it = homeEnbs.Begin(); it!=homeEnbs.End(); it++)
            {
              Wifi5GHzChannel chInfo = GetChannelInfo (itConfigMap->first);
              uint32_t nodeId = (*it)->GetId();
              lteUNodeIdToBandInfo.insert(std::pair <uint32_t, Wifi5GHzChannel> (nodeId, chInfo));
            }

          for (NodeContainer::Iterator it = wifiAps.Begin(); it!=wifiAps.End(); it++)
            {
              Wifi5GHzChannel chInfo = GetChannelInfo (itConfigMap ->first);
              uint32_t nodeId = (*it)->GetId();
              wifiNodeIdToBandInfo.insert(std::pair <uint32_t, Wifi5GHzChannel> (nodeId, chInfo));
            }

          allHomeEnbs.Add (homeEnbs);
          allWifiAps.Add (wifiAps);
        }
    }

  NS_LOG_INFO ("Total number of created LTE-U nodes is:"<<allHomeEnbs.GetN()<<" , and WIFI APs is:"<<allWifiAps.GetN());

  std::cout<<"\nTotal number of created LTE-U nodes is:"<<allHomeEnbs.GetN()<<" , and WIFI is:"<<allWifiAps.GetN();


  std::map <RemPointPos, std::map<uint32_t, std::map<uint32_t, double> > > remMapAll;

  if (configMode == RUN_MODE)
    {
      LoadChannelStatisticsFile (remMapAll);
    }

  LenaDualStripeHelper dualStripeHelper;
  dualStripeHelper.Configure (lteHelper, epcHelper, allHomeEnbs, allWifiAps, lteUNodeIdToBandInfo, wifiNodeIdToBandInfo, remMapAll, channelsInUse);

  NodeContainer spectrumAnalyzerNodes;

  for ( std::vector <Wifi5GHzChannel>::iterator it = channelsInUse.begin(); it != channelsInUse.end(); it++)
    {
      uint32_t dlEarfcn = (*it).m_dlEarfcn;
      uint32_t dlBandwidthInNoOfRbs = (*it).m_dlBandwidthInNoOfRbs;
      uint32_t channelNumber = (*it).m_wifiChannelNumber;

      if (generateRem)
        {
          std::stringstream buildingsFileName, enbsFileName, uesFileName, remHelperOutputFileName;
          buildingsFileName<<outputDir<<"buildings-"<<channelNumber<<".txt";
          enbsFileName<<outputDir<<"enbs-"<<channelNumber<<".txt";
          uesFileName<<outputDir<<"ues-"<<channelNumber<<".txt";

          Ptr<SpectrumModel> sm = LteSpectrumValueHelper::GetSpectrumModel (dlEarfcn, dlBandwidthInNoOfRbs);
          PrintGnuplottableBuildingListToFile (buildingsFileName.str());
          PrintGnuplottableEnbListToFile (enbsFileName.str(), *sm);
          PrintGnuplottableUeListToFile (uesFileName.str(), *sm);

          // we want to select only area in which are buildings
          Box maxBox = GetBuildingsArea();

          remHelperOutputFileName<<outputDir<<"lena-dual-stripe-"<<channelNumber<<".rem";
          Ptr <RadioEnvironmentMapHelper> remHelper = CreateObject<RadioEnvironmentMapHelper> ();
          remHelper->SetAttribute ("Earfcn", UintegerValue (dlEarfcn));
          remHelper->SetAttribute ("Bandwidth", UintegerValue (dlBandwidthInNoOfRbs));
          remHelper->SetAttribute ("OutputFile", StringValue (remHelperOutputFileName.str()));

          /*remHelper->SetAttribute ("XMin", DoubleValue (dualStripeHelper.GetMacroUeBox().xMin));
          remHelper->SetAttribute ("XMax", DoubleValue (dualStripeHelper.GetMacroUeBox().xMax));
          remHelper->SetAttribute ("YMin", DoubleValue (dualStripeHelper.GetMacroUeBox().yMin));
          remHelper->SetAttribute ("YMax", DoubleValue (dualStripeHelper.GetMacroUeBox().yMax));*/

          remHelper->SetAttribute ("XMin", DoubleValue (maxBox.xMin));
          remHelper->SetAttribute ("XMax", DoubleValue (maxBox.xMax));
          remHelper->SetAttribute ("YMin", DoubleValue (maxBox.yMin));
          remHelper->SetAttribute ("YMax", DoubleValue (maxBox.yMax));

          remHelper->SetAttribute ("Z", DoubleValue (1.5));

          remHelper->SetAttribute ("XRes", UintegerValue (300));
          remHelper->SetAttribute ("YRes", UintegerValue (300));
          // since we will have in general several rem helpers per simlation, we cannot activate this function, because not all rem helper will finish at the same time
          remHelper->SetAttribute ("StopWhenDone", BooleanValue (false));
          // since we might have 10 remhelpers working at the same time we need to reduce maximum number of points per iteartion
          remHelper->SetAttribute ("MaxPointsPerIteration", UintegerValue (2000));

          remHelper->TraceConnectWithoutContext ("NotifyWhenDone", MakeCallback(&CheckIfAllRemHelpersHaveFinished));

          GlobalValue::GetValueByName ("remRbId", integerValue);
          int32_t remRbId = integerValue.Get ();

          if (remRbId >= 0)
            {
              remHelper->SetAttribute ("UseDataChannel", BooleanValue (true));
              remHelper->SetAttribute ("RbId", IntegerValue (remRbId));
            }
          remHelper->Install (lteHelper->GetDownlinkSpectrumChannel());
          g_remHelperList.insert ( std::pair<uint32_t, Ptr<RadioEnvironmentMapHelper> > (channelNumber, remHelper));
        }
      else
        {
          if (spectrumAnalyzerEnabled)
            {
              /////////////////////////////////
              // Configure spectrum analyzer
              ///////////////////////////////
              NodeContainer saNode;
              saNode.Create (1);
              SpectrumAnalyzerHelper spectrumAnalyzerHelper;
              spectrumAnalyzerHelper.SetChannel (lteHelper->GetDownlinkSpectrumChannel());
              spectrumAnalyzerHelper.SetRxSpectrumModel (LteSpectrumValueHelper::GetSpectrumModel (dlEarfcn, dlBandwidthInNoOfRbs));
              spectrumAnalyzerHelper.SetPhyAttribute ("Resolution", TimeValue (MilliSeconds (2)));
              spectrumAnalyzerHelper.SetPhyAttribute ("NoisePowerSpectralDensity", DoubleValue (1e-15));  // -120 dBm/Hz

              std::stringstream saOutputFileName;
              saOutputFileName<<outputDir<<"spectrum-analyzer-"<<channelNumber<<".tr";
              std::string spectrumAnalzerOutput = saOutputFileName.str();
              spectrumAnalyzerHelper.EnableAsciiAll (spectrumAnalzerOutput);
              spectrumAnalyzerHelper.SetUseDevIdInOutputFilename(false);
              NetDeviceContainer spectrumAnalyzerDevices1 = spectrumAnalyzerHelper.Install (saNode);

              spectrumAnalyzerNodes.Add (saNode);
            }
        }
    }

 // lteHelper->EnableMacTraces ();
 // lteHelper->EnableRlcTraces ();

  LogComponentEnable ("ChannelSelectionDualStripe", LOG_LEVEL_ALL);

  Simulator::Stop (Seconds (simTime));

  Simulator::Run ();

  if (configMode == CHANNEL_SCANNING || generateRem)
    {
       PrintChannelStatisticsToSingleFile ();
       PrintChannelStatisticsPerChannel (true);
    }

  PrintPositionsToFile (outputDir + "test-positions.txt", lteUNodeIdToBandInfo, wifiNodeIdToBandInfo);

  Simulator::Destroy ();

  return 0;
}
