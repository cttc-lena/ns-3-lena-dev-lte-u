/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2019 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es>
 */

/**
 * This example represents validation of LAA-WIFI performance against the analytical model and the
 * testbed whose results are presented in the following paper:
 *
 * Morteza Mehrnoush, Vanlin Sathya, Sumit Roy, and Monisha Ghosh. 2018.
 * Analytical Modeling of Wi-Fi and LTE-LAA Coexistence:
 * Throughput and Impact of Energy Detection Threshold.
 * IEEE/ACM Trans. Netw. 26, 4 (August 2018), 1990-2003.
 * DOI: https://doi.org/10.1109/TNET.2018.2856901
 *
 *
 * According to this paper there are 3 topologies to be validated:
 *
 * 1)
 *    OPERATOR A ....... d.......OPERATOR B
 *      (AP)                      (AP or LAA)
 *
 * 2)
 *
 *     OPERATOR A  ...  d  ... OPERATOR A
 *        (AP)                     (AP)
 *          |                        |
 *          |                        |
 *          |                        |
 *          |                        |
 *       d  |                     d  |
 *          |                        |
 *          |                        |
 *          |                        |
 *          |                        |
 *     OPERATOR B  ...  d  ...  OPERATOR B
 *     (AP or LAA)              (AP or LAA)
 *
 *
 * 3)
 * *   OPERATOR A ...   d  ...  OPERATOR A
 *        (AP)                     (AP)
 *          |                        |
 *          |                        |
 *          |                        |
 *          |                        |
 *       d  |                     d  |
 *          |                        |
 *          |                        |
 *          |                        |
 *          |                        |
 *     OPERATOR B  ...  d   ... OPERATOR B
 *     (AP or LAA)              (AP or LAA)
 *          |                        |
 *          |                        |
 *          |                        |
 *          |                        |
 *       d  |                     d  |
 *          |                        |
 *          |                        |
 *          |                        |
 *          |                        |
 *     OPERATOR A  ...  d   ... OPERATOR A
 *         (AP)                    (AP)
 *
 *  According to this paper d is 4 feet which is equal to 1.2192 meters.
 *
 */

#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/internet-module.h>
#include <ns3/point-to-point-module.h>
#include <ns3/lte-module.h>
#include <ns3/wifi-module.h>
#include <ns3/config-store-module.h>
#include <ns3/spectrum-module.h>
#include <ns3/applications-module.h>
#include <ns3/flow-monitor-module.h>
#include <ns3/propagation-module.h>
#include <ns3/scenario-helper.h>
#include <ns3/laa-wifi-coexistence-helper.h>
#include <ns3/lbt-access-manager.h>

using namespace ns3;

enum ValidationCase_e{
  CASE1,
  CASE2,
  CASE3
};

enum ValidationScenario_e{
  SCENARIO_2_NODES,
  SCENARIO_4_NODES,
  SCENARIO_6_NODES
};

NS_LOG_COMPONENT_DEFINE ("LaaWifiCoexistenceSimple");

static ns3::GlobalValue g_duration ("duration",
                                    "Data transfer duration (seconds)",
                                    ns3::DoubleValue (10),
                                    ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_validationCase ("validationCase",
                                           "Can be Case 1, Case 2 or Case 3.",
                                           ns3::EnumValue (CASE1),
                                           ns3::MakeEnumChecker (CASE1, "Case1",
                                                                 CASE2, "Case2",
                                                                 CASE3, "Case3"
                                                                              ));

static ns3::GlobalValue g_validationScenario ("validationScenario",
                                              "Scenario is defined by the number of nodes competing for the channel in downlink",
                                               ns3::EnumValue (SCENARIO_2_NODES),
                                               ns3::MakeEnumChecker (SCENARIO_2_NODES, "Scenario2Nodes",
                                                                     SCENARIO_4_NODES, "Scenario4Nodes",
                                                                     SCENARIO_6_NODES, "Scenario6Nodes"
                                                                     ));

static ns3::GlobalValue g_useReservationSignal ("useReservationSignal",
                                                "Defines whether reservation signal will be used when used channel access manager at LTE eNb",
                                                ns3::BooleanValue (true),
                                                ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_laaEdThreshold ("laaEdThreshold",
                                          "CCA-ED threshold for channel access manager (dBm)",
                                          ns3::DoubleValue (-72.0),
                                          ns3::MakeDoubleChecker<double> (-100.0, -50.0));

static ns3::GlobalValue g_simTag ("simTag",
                                  "tag to be appended to output filenames to distinguish simulation campaigns",
                                  ns3::StringValue ("default"),
                                  ns3::MakeStringChecker ());

static ns3::GlobalValue g_outputDir ("outputDir",
                                     "directory where to store simulation results",
                                     ns3::StringValue ("./"),
                                     ns3::MakeStringChecker ());

static ns3::GlobalValue g_shadowingEnabled ("shadowingEnabled",
                                           "If true, shadowing will be enabled, and if false, it will be disabled.",
                                           ns3::BooleanValue (false),
                                           ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_wifiDataRate ("wifiDataRate",
                                       "Data rate to be used by Wifi, expected values are: 9, 18, 54Mbps",
                                        ns3::DataRateValue (DataRate ("9Mbps")),
                                        ns3::MakeDataRateChecker());

static ns3::GlobalValue g_wifiToUseConstantRateManager ("wifiUseContantRateManager",
                                                        "If true, wifi will use constantRateManager",
                                                        ns3::BooleanValue (true),
                                                        ns3::MakeBooleanChecker ());


int
main (int argc, char *argv[])
{  
  CommandLine cmd;
  cmd.Parse (argc, argv);
  ConfigStore inputConfig;
  inputConfig.ConfigureDefaults ();
  // parse again so you can override input file default values via command line
  cmd.Parse (argc, argv);

  DoubleValue doubleValue;
  EnumValue enumValue;
  BooleanValue booleanValue;
  StringValue stringValue;
  DataRateValue dataRateValue;

  GlobalValue::GetValueByName ("duration", doubleValue);
  double duration = doubleValue.Get ();
  GlobalValue::GetValueByName ("laaEdThreshold", doubleValue);
  double laaEdThreshold = doubleValue.Get ();

  GlobalValue::GetValueByName ("simTag", stringValue);
  std::string simTag = stringValue.Get ();
  GlobalValue::GetValueByName ("outputDir", stringValue);
  std::string outputDir = stringValue.Get ();
  GlobalValue::GetValueByName ("shadowingEnabled", booleanValue);
  bool shadowingEnabled = booleanValue.Get ();
  GlobalValue::GetValueByName ("useReservationSignal", booleanValue);
  bool useReservationSignal = booleanValue.Get ();
  GlobalValue::GetValueByName ("validationCase", enumValue);
  enum ValidationCase_e validationCase = (ValidationCase_e) enumValue.Get ();
  GlobalValue::GetValueByName ("validationScenario", enumValue);
  enum ValidationScenario_e validationScenario = (ValidationScenario_e) enumValue.Get ();
  GlobalValue::GetValueByName ("wifiDataRate", dataRateValue);
  DataRate wifiDataRate = dataRateValue.Get ();

  enum Config_e cellConfigA = WIFI;
  enum Config_e cellConfigB = WIFI;

  Config::SetDefault ("ns3::ChannelAccessManager::EnergyDetectionThreshold", DoubleValue (laaEdThreshold));
  Config::SetDefault ("ns3::LaaWifiCoexistenceHelper::ChannelAccessManagerType", StringValue ("ns3::LbtAccessManager"));
  Config::SetDefault ("ns3::LbtAccessManager::UseReservationSignal", BooleanValue(useReservationSignal));
  //Config::SetDefault ("ns3::LbtAccessManager::CwUpdateRule", EnumValue (LbtAccessManager::ANY_NACK));
  Config::SetDefault ("ns3::LbtAccessManager::CwUpdateRule", EnumValue (LbtAccessManager::NACKS_80_PERCENT));
  // According the paper the transport if full buffer traffic which in our simulation is full buffer CBR
  enum Transport_e transport = ONOFF;

  Config::SetDefault ("ns3::OnOffApplication::PacketSize",  UintegerValue (2048));
  Config::SetDefault ("ns3::PfFfMacScheduler::FixedMcs", BooleanValue (true));
  Config::SetDefault ("ns3::PfFfMacScheduler::OptimizedResourceAllocationInDownlink", BooleanValue (false));
  Config::SetDefault ("ns3::PfFfMacScheduler::HarqEnabled", BooleanValue (true));
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", UintegerValue (65535));
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", UintegerValue (3000));
  Config::SetDefault ("ns3::WifiRemoteStationManager::IsLowLatency", BooleanValue (false));
  // we need to configure for CASE 2 non-default CW parameters for Wi-Fi, so we use this new attribute to enable such behaviour
  Config::SetDefault ("ns3::WifiMac::UseCustomCwParameters", BooleanValue (true));
  // allow transmission of MPDU of 2048 bytes as defined in the paper, without segmentation
  Config::SetDefault ("ns3::CsmaNetDevice::Mtu", UintegerValue (2500));
  Config::SetDefault ("ns3::PointToPointEpcHelper::S1uLinkMtu", UintegerValue (3500));
  GlobalValue::BindFailSafe ("wifiStandard", EnumValue (St80211a));
  // the maximum number of retransmission attempts for a DATA packet.
  Config::SetDefault ("ns3::WifiRemoteStationManager::MaxSlrc",  UintegerValue (2));
  GlobalValue::BindFailSafe ("drsEnabled", BooleanValue (false));

  GlobalValue::BindFailSafe ("wifiQueueMaxSize", UintegerValue (10000));
  GlobalValue::BindFailSafe ("wifiQueueMaxDelay", UintegerValue (10000));

  Config::SetDefault ("ns3::OnOffApplication::DataRate", DataRateValue (wifiDataRate.GetBitRate()*3));

  //Config::SetDefault ("ns3::OnOffApplication::MaxBytes", UintegerValue (10000));
  if (wifiDataRate == DataRate ("9Mbps"))
    {
      Config::SetDefault ("ns3::ConstantRateWifiManager::DataMode", WifiModeValue (WifiPhy::GetOfdmRate9Mbps ()));
      Config::SetDefault ("ns3::ConstantRateWifiManager::ControlMode", WifiModeValue (WifiPhy::GetOfdmRate6Mbps ()));
      /**
       * Table of number of physical resource blocks (NPRB), TBS index (ITBS), and
       * their associated transport block size. Taken from 3GPP TS 36.213 v8.8.0
       * Table 7.1.7.2.1-1: _Transport block size table (dimension 27×110)
       * And taking into account MCS to ITBS mapping
       * static const int McsToItbs[29] = {
       *0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 10, 11, 12, 13, 14, 15, 15, 16, 17, 18,
       *19, 20, 21, 22, 23, 24, 25, 26};
       */
      Config::SetDefault ("ns3::PfFfMacScheduler::FixedMcsValue", UintegerValue (4));
    }
  else if (wifiDataRate == DataRate ("18Mbps"))
    {
      //configure WiFi data rate
      Config::SetDefault ("ns3::ConstantRateWifiManager::DataMode", WifiModeValue (WifiPhy::GetOfdmRate18Mbps()));
      Config::SetDefault ("ns3::ConstantRateWifiManager::ControlMode", WifiModeValue (WifiPhy::GetOfdmRate6Mbps()));
      Config::SetDefault ("ns3::PfFfMacScheduler::FixedMcsValue", UintegerValue (8));
    }
  else if (wifiDataRate == DataRate ("54Mbps"))
    {
      Config::SetDefault ("ns3::ConstantRateWifiManager::DataMode", WifiModeValue (WifiPhy::GetOfdmRate54Mbps ()));
      Config::SetDefault ("ns3::ConstantRateWifiManager::ControlMode", WifiModeValue (WifiPhy::GetOfdmRate6Mbps ()));
      Config::SetDefault ("ns3::PfFfMacScheduler::FixedMcsValue", UintegerValue (27));
    }
  else
    {
      NS_ABORT_MSG ("Unsupported wifi data rate");
    }


  if (validationCase == CASE1)
    {
      cellConfigA = WIFI;
      cellConfigB = WIFI;
      // the minimum size of the contention window of wifi
      // according to the reference validation paper the minimum Wo is 16
      Config::SetDefault ("ns3::Dcf::MinCw",  UintegerValue (15));
      // according to the reference validation paper the maximum CW depends on the
      // maximum retransmission stage which is in CASE1 in the reference paper selected to be 6
      // and that value corresponds to the maximum CW of 1023
      Config::SetDefault ("ns3::Dcf::MaxCw", UintegerValue (1023));
    }
  else if (validationCase == CASE2)
    {
      cellConfigA = WIFI;
      cellConfigB = LAA;
      // to validate LTE throughput use regular LTE, bellow configured with duty cycle 1
      // LAA configuration parameters that depend on the scenario case
      // LAA uses priority class 1
      // Priority class 1 parameters: miCw=3, maxCw=7, txop=2
      Config::SetDefault ("ns3::LbtAccessManager::Txop", TimeValue (MilliSeconds (3)));
      Config::SetDefault ("ns3::LbtAccessManager::MinCw", UintegerValue (3));
      Config::SetDefault ("ns3::LbtAccessManager::MaxCw", UintegerValue (7));
      //Config::SetDefault ("ns3::LbtAccessManager::DeferTime", TimeValue (MicroSeconds(25)));
      // the minimum size of the contention window of wifi
      //According to the reference paper since the W0 = 4 min CW is 3
      Config::SetDefault ("ns3::Dcf::MinCw",  UintegerValue (3));
      //According to the reference paper since the W0 = 4 max CW is 7
      // [0, (2^i)*W0 - 1]
      // where maximum stage m is equal to 1
      Config::SetDefault ("ns3::Dcf::MaxCw", UintegerValue (7));
    }
  else if (validationCase == CASE3)
    {
      cellConfigA = WIFI;
      cellConfigB = LAA;
      // LAA configuration parameters that depend on the scenario case
      // LAA uses priority class 3
      // Priority class 3 parameters: minCw=15, maxCw=63, txop=8
      Config::SetDefault ("ns3::LbtAccessManager::Txop", TimeValue (MilliSeconds (9)));
      Config::SetDefault ("ns3::LbtAccessManager::MinCw", UintegerValue (15));
      Config::SetDefault ("ns3::LbtAccessManager::MaxCw", UintegerValue (63));
      //Config::SetDefault ("ns3::LbtAccessManager::DeferTime", TimeValue (MicroSeconds(34)));
      // the minimum size of the contention window of wifi
      //According to the reference paper since the W0 = 16 min CW is 15
      Config::SetDefault ("ns3::Dcf::MinCw",  UintegerValue (15));
      //According to the reference paper since the W0 = 16 max CW is
      // [0, (2^i)*W0 - 1]
      // where maximum stage m is equal to 2
      Config::SetDefault ("ns3::Dcf::MaxCw", UintegerValue (63));
    }
  else
    {
      NS_ABORT_MSG ("Undefined case");
    }

  // LAA should have fixed MCS depending on the rate that is being tested

  NodeContainer bsNodesOperatorA, bsNodesOperatorB;
  NodeContainer ueNodesOperatorA, ueNodesOperatorB;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();

  // According the paper by Mehrnoush et al. the distance between the
  // AP/LAA BS and the STA/UE is not relevant, but we must set it up
  // for the simulation purposes. According to the scenario this distance
  // should not impact the performance.
  // Additionally, the distance between the
  // nodes competing for the channel is 4 feet, which corresponds to
  // 1.2192 meters
  double distance1 = 1.2192; //between APs/LAAs
  double distance2 = 1.2192*2.5; //between AP/LAA and its STA/UE

  if (validationScenario == SCENARIO_2_NODES)
    {
      bsNodesOperatorA.Create (1);
      bsNodesOperatorB.Create (1);
      ueNodesOperatorA.Create (1);
      ueNodesOperatorB.Create (1);

      positionAlloc->Add (Vector (0,                     0.0, 0.0)); // AP of Operator A
      positionAlloc->Add (Vector (            distance1, 0.0, 0.0)); // AP/LAA of Operator B

      positionAlloc->Add (Vector (          - distance2, 0.0, 0.0));  // STA of Operator A
      positionAlloc->Add (Vector (distance1 + distance2, 0.0, 0.0));  // UE/STA of Operator B

    }
  else if (validationScenario == SCENARIO_4_NODES)
    {
      if (validationCase == CASE3)
       {
         Config::SetDefault ("ns3::LbtAccessManager::CwUpdateRule", EnumValue (LbtAccessManager::ANY_NACK));
         Config::SetDefault ("ns3::LbtAccessManager::Txop", TimeValue (MilliSeconds (8)));
      }

      bsNodesOperatorA.Create (2);
      bsNodesOperatorB.Create (2);
      ueNodesOperatorA.Create (2);
      ueNodesOperatorB.Create (2);

      positionAlloc->Add (Vector (      0.0,        0.0,     0.0)); // AP of Operator A
      positionAlloc->Add (Vector (distance1,        0.0,     0.0)); // AP of Operator A
      positionAlloc->Add (Vector (      0.0, -distance1,     0.0)); // AP/LAA of Operator B
      positionAlloc->Add (Vector (distance1, -distance1,     0.0)); // AP/LAA of Operator B

      positionAlloc->Add (Vector (          - distance2,        0.0, 0.0));  // STA of Operator A
      positionAlloc->Add (Vector (distance1 + distance2,        0.0, 0.0));  // STA of Operator A

      positionAlloc->Add (Vector (          - distance2, -distance1, 0.0));  // UE/STA of Operator B
      positionAlloc->Add (Vector (distance1 + distance2, -distance1, 0.0));  // UE/STA of Operator B

    }
  else if (validationScenario == SCENARIO_6_NODES)
    {
      // spread load over nodes
      if (validationCase == CASE3)
        {
          Config::SetDefault ("ns3::OnOffApplication::DataRate", DataRateValue (wifiDataRate.GetBitRate()*0.7));
          Config::SetDefault ("ns3::LbtAccessManager::Txop", TimeValue (MilliSeconds (8)));
        }

      bsNodesOperatorA.Create (4);
      bsNodesOperatorB.Create (2);
      ueNodesOperatorA.Create (4);
      ueNodesOperatorB.Create (2);

      positionAlloc->Add (Vector (      0.0,          0.0,      0.0)); // AP of Operator A
      positionAlloc->Add (Vector (distance1,          0.0,      0.0)); // AP of Operator A

      positionAlloc->Add (Vector (      0.0, -2 * distance1,     0.0)); // AP of Operator A
      positionAlloc->Add (Vector (distance1, -2 * distance1,     0.0)); // AP of Operator A

      positionAlloc->Add (Vector (      0.0,   - distance1,        0.0)); // AP/LAA of Operator B
      positionAlloc->Add (Vector (distance1,   - distance1,        0.0)); // AP/LAA of Operator B

      positionAlloc->Add (Vector (          - distance2,        0.0, 0.0));  // STA of Operator A
      positionAlloc->Add (Vector (distance1 + distance2,        0.0, 0.0));  // STA of Operator A
      positionAlloc->Add (Vector (          - distance2,  -2 * distance1, 0.0));  // STA of Operator A
      positionAlloc->Add (Vector (distance1 + distance2,  -2 * distance1, 0.0));  // STA of Operator A


      positionAlloc->Add (Vector (          - distance2, - distance1, 0.0));  // UE/STA of Operator B
      positionAlloc->Add (Vector (distance1 + distance2, - distance1, 0.0));  // UE/STA of Operator B

    }
  else
    {
      NS_ABORT_MSG("Scenario not supported");
    }

  NodeContainer allWirelessNodes = NodeContainer (bsNodesOperatorA, bsNodesOperatorB, ueNodesOperatorA, ueNodesOperatorB);

  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetPositionAllocator (positionAlloc);
  mobility.Install (allWirelessNodes);

  Time durationTime = Seconds (duration);

  if (!shadowingEnabled)
    {
      // we want deterministic behavior in this simple scenario, so we disable shadowing
      Config::SetDefault ("ns3::Ieee80211axIndoorPropagationLossModel::Sigma", DoubleValue (0));
    }
  else
    {  // enable shadowing
      Config::SetDefault ("ns3::Ieee80211axIndoorPropagationLossModel::Sigma", DoubleValue (2));
    }

  // Specify some physical layer parameters that will be used below and
  // in the scenario helper.
  PhyParams phyParams;
  phyParams.m_bsTxGain = 0; // dB antenna gain
  //phyParams.m_enbTxGain = 5; // dBi antenna gain
  phyParams.m_bsRxGain = 0; // dB antenna gain, apply to Wi-Fi only
  phyParams.m_bsTxPower = 18; // dBm
  //phyParams.m_enbTxPower = 18; // dBm
  phyParams.m_bsNoiseFigure = 5; // dB
  phyParams.m_ueTxGain = 0; // dB antenna gain
  phyParams.m_ueRxGain = 0; // dB antenna gain
  phyParams.m_ueTxPower = 18; // dBm
  phyParams.m_ueNoiseFigure = 9; // dB
  phyParams.m_channelNumber = 36; // the channel to be used according to the paper
  phyParams.m_receivers = 2;
  phyParams.m_transmitters = 2;
  
  std::string str = outputDir + "/validation_results";

  std::ofstream outFile;
  outFile.open (str.c_str(), std::ofstream::out | std::ofstream:: app);
  if (!outFile.is_open ())
    {
      NS_LOG_ERROR ("Can't open file ");
      return 0;
    }
  outFile.setf (std::ios_base::fixed);

  if (validationScenario == SCENARIO_2_NODES)
    {
      outFile << " " <<"SCENARIO_2_NODES";
    }
  else if (validationScenario == SCENARIO_4_NODES)
    {
      outFile << " " <<"SCENARIO_4_NODES";
    }
  else
    {
      outFile << " " <<"SCENARIO_6_NODES";
    }

  if (wifiDataRate == DataRate ("9Mbps"))
    {
      outFile << " " <<"9Mbps";
    }
  else if (wifiDataRate == DataRate ("18Mbps"))
    {
      outFile << " " <<"18Mbps";
    }
  else
    {
      outFile << " " <<"54Mbps";
    }

  if (validationCase == CASE1)
    {
      outFile << " " <<"Case1";
    }
  else if (validationCase == CASE2)
   {
      outFile << " "<<"Case2 ";
   }
  else
   {
     outFile << " "<<"Case3 ";
   }

  outFile.close ();

  ConfigureAndRunScenario (cellConfigA, 
                           cellConfigB, 
                           bsNodesOperatorA, bsNodesOperatorB,
                           ueNodesOperatorA, ueNodesOperatorB,
                           phyParams, durationTime, 
                           transport,
                           "ns3::Ieee80211axIndoorPropagationLossModel",
                           //"ns3::ItuInhPropagationLossModel",
                           false,
                           1,
                           false,
                           outputDir + "/laa_wifi_simple_" + simTag,
                           "");
  
  return 0;
}
