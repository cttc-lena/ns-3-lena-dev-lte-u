/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Nicola Baldo <nbaldo@cttc.es>
 * Modified by Biljana Bojovic <bbojovic@cttc.es>
 */

//
//  This program is an extension of the example 'laa-wifi-simple', extended
//  to include a LBT node and allow the LTE networks to experiment with
//  LBT modes.
// 
//  This example program can be used to experiment with wireless coexistence
//  in a simple scenario.  The scenario consists of two cells whose radio
//  coverage overlaps but representing, notionally, two operators in the
//  same region whose transmissions may impact mechanisms such as clear
//  channel assessment and adaptive modulation and coding.
//  The technologies used are LTE Licensed Assisted Access (LAA)
//  operating on EARFCN 255444 (5.180 GHz), and Wi-Fi 802.11n
//  operating on channel 36 (5.180 GHz).
//
//  The notional scenario consists of two operators A and B, each
//  operating a single cell, and each cell consisting of a base
//  station (BS) and a user equipment (UE), as represented in the following
//  diagram:
//
//
//    BS A  .  .  .  d2 .  .  .  UE B
//     |                          |
//  d1 |            BS C       d1 |
//     |                          |
//    UE A  .  .  .  d2 .  .  .  BS B
//
//  cell A              cell B
//
//  where 'd1' and 'd2' distances (in meters) separate the nodes.
//
//  When using LTE, the BS is modeled as an eNB and the UE as a UE.
//  When using Wi-Fi, the BS is modeled as an AP and the UE as a STA.
//
//  In addition, both BS are connected to a "backhaul" client node that
//  originates data transfer in the downlink direction from client to UE(s).
//  The links from client to BS are modeled as well provisioned; low latency
//  links.
//
//  The figure may be redrawn as follows:
//
//     +-------------------------------- client
//     |                                    |
//    BS A  . . .  d2  . . .  UE B          |
//     |                        |           |
//  d1 |          BS C       d1 |           |
//     |                        |           |
//    UE A  . . .  d2  . . .  BS B----------+
//
//
//  In general, the program can be configured at run-time by passing
//  command-line arguments.  The command
//  ./waf --run "laa-wifi-coexistence-simple --help"
//  will display all of the available run-time help options, and in
//  particular, the command
//  ./waf --run "laa-wifi-coexistence-simple --PrintGlobals" should
//  display the following:
//
// Global values:
//
//    --ChannelAccessManager=[BasicLbt]
//        Default, DutyCyccle or BasicLbt
//    --ChecksumEnabled=[false]
//        A global switch to enable all checksums for all protocols
//    --RngRun=[1]
//        The run number used to modify the global seed
//    --RngSeed=[1]
//        The global seed of all rng streams
//    --SchedulerType=[ns3::MapScheduler]
//        The object class to use as the scheduler implementation
//    --SimulatorImplementationType=[ns3::DefaultSimulatorImpl]
//        The object class to use as the simulator implementation
//    --cellConfigA=[Wifi]
//        Lte, Wifi or Laa
//    --cellConfigB=[LTE node with LAA functionalities.]
//        Lte, Wifi or Laa
//    --clientStartTimeSeconds=[3]
//        Client start time (seconds)
//    --d1=[10]
//        intra-cell separation (e.g. AP to STA)
//    --d2=[10]
//        inter-cell separation
//    --duration=[1]
//        Data transfer duration (seconds)
//    --dutyCyclePeriod=[20]
//        Total duty cycle period that consists of ON and OFF period.
//    --generateRem=[false]
//        if true, will generate a REM and then abort the simulation;if false, will run the simulation normally (without generating any REM)
//    --laaNodeEnabled=[false]
//        Whether to install laa node
//    --laaNodePosition=[25:5:0]
//        Laa node position
//    --lteDutyCycle=[1]
//        Duty cycle value to be used for LTE
//    --onTime=[10]
//        Duration of transmission period during duty cycle period
//    --outputDir=[./]
//        directory where to store simulation results
//    --pcapEnabled=[false]
//        Whether to enable pcap trace files for Wi-Fi
//    --remDir=[./]
//        directory where to save REM-related output files
//    --serverLingerTimeSeconds=[5]
//        Server linger time (seconds)
//    --serverStartTimeSeconds=[3]
//        Server start time (seconds)
//    --simTag=[default]
//        tag to be appended to output filenames to distinguish simulation campaigns
//    --simulationLingerTimeSeconds=[5]
//        Simulation linger time (seconds)
//    --transport=[Udp]
//        whether to use Udp or Tcp
//    --txTime=[40]
//        Transmission time parameter for basic channel access manager.
//    --waitTime=[1]
//        Wait time parameter for basic channel access manager.
//

#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/internet-module.h>
#include <ns3/point-to-point-module.h>
#include <ns3/lte-module.h>
#include <ns3/wifi-module.h>
#include <ns3/config-store-module.h>
#include <ns3/spectrum-module.h>
#include <ns3/applications-module.h>
#include <ns3/flow-monitor-module.h>
#include <ns3/propagation-module.h>
#include <ns3/scenario-helper.h>
#include <ns3/laa-wifi-coexistence-helper.h>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("LaaWifiSimpleEdpd");

// Global Values are used in place of command line arguments so that these
// values may be managed in the ns-3 ConfigStore system.

static ns3::GlobalValue g_channelAccessManager ("ChannelAccessManager",
                                                "Default, DutyCycle, BasicLbt, Lbt, or OnlyListen",
                                                ns3::EnumValue (Lbt),
                                                ns3::MakeEnumChecker (Default, "Default",
                                                                      DutyCycle, "DutyCycle",
                                                                      BasicLbt, "BasicLbt",
                                                                      Lbt, "Lbt",
                                                                      OnlyListen, "OnlyListen" 
                                                                      ));
// The last option, 'OnlyListen', is used to run a topology in which 
// the LAA node that only listens is installed in the middle, and then 
// the other cells, A and B, should be configured to be either LTE or WIFI

static ns3::GlobalValue g_laaNodeEnabled ("laaNodeEnabled", // node in the middle of topology, that only listens
                                          "Whether to install laa node",
                                          ns3::BooleanValue (false),
                                          ns3::MakeBooleanChecker ()
                                          );

static ns3::GlobalValue g_laaNodePosition ("laaNodePosition", // position of laa node that only listens
                                           "Laa node position",
                                           ns3::VectorValue (Vector (50 / 2, 10 / 2, 0.0)),
                                           ns3::MakeVectorChecker ()
                                           );
static ns3::GlobalValue g_d1 ("d1",
                              "intra-cell separation (e.g. AP to STA)",
                              ns3::DoubleValue (100),
                              ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_d2 ("d2",
                              "inter-cell separation",
                              ns3::DoubleValue (10000),
                              ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_txTime ("txTime",
                                  "Transmission time parameter for basic channel access manager.",
                                  ns3::DoubleValue (40),
                                  ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_waitTime ("waitTime",
                                    "Wait time parameter for basic channel access manager.",
                                    ns3::DoubleValue (1),
                                    ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_dutyCycleOnDuration ("dutyCycleOnDuration",
                                               "Duration of transmission period during duty cycle period",
                                               ns3::DoubleValue (10),
                                               ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_dutyCycleOnStartTime ("dutyCycleOnStartTime",
                                                "Duration of transmission period during duty cycle period",
                                                ns3::DoubleValue (0),
                                                ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_dutyCyclePeriod ("dutyCyclePeriod",
                                           "Total duty cycle duration, consists of ON and OFF period.",
                                           ns3::DoubleValue (20),
                                           ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_duration ("duration",
                                    "Data transfer duration (seconds)",
                                    ns3::DoubleValue (1),
                                    ns3::MakeDoubleChecker<double> ());

static ns3::GlobalValue g_cellConfigA ("cellConfigA",
                                       "Lte, Wifi or Laa",
                                       ns3::EnumValue (WIFI),
                                       ns3::MakeEnumChecker (WIFI, "Wifi",
                                                             LTE, "Lte",
                                                             LAA, "Laa"));
static ns3::GlobalValue g_cellConfigB ("cellConfigB",
                                       "Lte, Wifi or Laa",
                                       ns3::EnumValue (LAA),
                                       ns3::MakeEnumChecker (WIFI, "Wifi",
                                                             LTE, "Lte",
                                                             LAA, "Laa"));

static ns3::GlobalValue g_lbtTxop ("lbtTxop",
                                       "Value between 4 and 20 milliseconds",
                                       ns3::IntegerValue(4),
                                       ns3::MakeIntegerChecker<uint32_t>());

static ns3::GlobalValue g_pcap ("pcapEnabled",
                                "Whether to enable pcap trace files for Wi-Fi",
                                ns3::BooleanValue (false),
                                ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_transport ("transport",
                                     "whether to use Udp or Tcp",
                                     ns3::EnumValue (UDP),
                                     ns3::MakeEnumChecker (UDP, "Udp",
                                                           TCP, "Tcp"));

static ns3::GlobalValue g_lteDutyCycle ("lteDutyCycle",
                                        "Duty cycle value to be used for LTE",
                                        ns3::DoubleValue (1),
                                        ns3::MakeDoubleChecker<double> (0.0, 1.0));

static ns3::GlobalValue g_generateRem ("generateRem",
                                       "if true, will generate a REM and then abort the simulation;"
                                       "if false, will run the simulation normally (without generating any REM)",
                                       ns3::BooleanValue (false),
                                       ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_generateCwStatistics ("generateCwStatistics",
                                                "Defines whether CW statistics will be generated",
                                                ns3::BooleanValue (true),
                                                ns3::MakeBooleanChecker ());

static ns3::GlobalValue g_simTag ("simTag",
                                  "tag to be appended to output filenames to distinguish simulation campaigns",
                                  ns3::StringValue ("default"),
                                  ns3::MakeStringChecker ());

static ns3::GlobalValue g_outputDir ("outputDir",
                                     "directory where to store simulation results",
                                     ns3::StringValue ("./"),
                                     ns3::MakeStringChecker ());

static ns3::GlobalValue g_useReservationSignal ("useReservationSignal",
                                                "Defines whether reservation signal will be used at LTE eNb",
                                                ns3::BooleanValue (true),
                                                ns3::MakeBooleanChecker ());


// Global variables for use in callbacks.
double g_signalDbmAvg[2];
double g_noiseDbmAvg[2];
uint32_t g_samples[2];
uint16_t g_channelNumber[2];
uint32_t g_rate[2];

int
main (int argc, char *argv[])
{
  CommandLine cmd;
  cmd.Parse (argc, argv);
  ConfigStore inputConfig;
  inputConfig.ConfigureDefaults ();
  // parse again so you can override input file default values via command line
  cmd.Parse (argc, argv);

  DoubleValue doubleValue;
  EnumValue enumValue;
  BooleanValue booleanValue;
  StringValue stringValue;
  IntegerValue integerValue;

  GlobalValue::GetValueByName ("ChannelAccessManager", enumValue);
  enum Config_ChannelAccessManager channelAccessManager = (Config_ChannelAccessManager) enumValue.Get ();
  GlobalValue::GetValueByName ("txTime",doubleValue);
  double txTime = doubleValue.Get ();
  GlobalValue::GetValueByName ("waitTime",doubleValue);
  double waitTime = doubleValue.Get ();
  GlobalValue::GetValueByName ("dutyCycleOnDuration",doubleValue);
  double dutyCycleOnDuration = doubleValue.Get ();
  GlobalValue::GetValueByName ("dutyCycleOnStartTime",doubleValue);
  double dutyCycleOnStartTime = doubleValue.Get ();
  GlobalValue::GetValueByName ("dutyCyclePeriod",doubleValue);
  double dutyCyclePeriod = doubleValue.Get ();
  GlobalValue::GetValueByName ("d1", doubleValue);
  double d1 = doubleValue.Get ();
  GlobalValue::GetValueByName ("d2", doubleValue);
  double d2 = doubleValue.Get ();
  GlobalValue::GetValueByName ("cellConfigA", enumValue);
  enum Config_e cellConfigA = (Config_e) enumValue.Get ();
  GlobalValue::GetValueByName ("cellConfigB", enumValue);
  enum Config_e cellConfigB = (Config_e) enumValue.Get ();
  GlobalValue::GetValueByName ("duration", doubleValue);
  double duration = doubleValue.Get ();
  GlobalValue::GetValueByName ("transport", enumValue);
  enum Transport_e transport = (Transport_e) enumValue.Get ();
  GlobalValue::GetValueByName ("lteDutyCycle", doubleValue);
  double lteDutyCycle = doubleValue.Get ();
  GlobalValue::GetValueByName ("generateRem", booleanValue);
  bool generateRem = booleanValue.Get ();
  GlobalValue::GetValueByName ("simTag", stringValue);
  std::string simTag = stringValue.Get ();
  GlobalValue::GetValueByName ("outputDir", stringValue);
  std::string outputDir = stringValue.Get ();
  GlobalValue::GetValueByName ("lbtTxop", integerValue);
  double lbtTxop = integerValue.Get ();
  GlobalValue::GetValueByName ("useReservationSignal", booleanValue);
  bool useReservationSignal = booleanValue.Get ();

  // Create nodes and containers
  NodeContainer bsNodesA, bsNodesB;  // for APs and eNBs
  NodeContainer ueNodesA, ueNodesB;  // for STAs and UEs
  NodeContainer allWirelessNodes;  // container to hold all wireless nodes
  // Each network A and B gets one type of node each
  bsNodesA.Create (1);
  bsNodesB.Create (1);
  ueNodesA.Create (1);
  ueNodesB.Create (1);
  allWirelessNodes = NodeContainer (bsNodesA, bsNodesB, ueNodesA, ueNodesB);

  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0));   // eNB1/AP in cell 0
  positionAlloc->Add (Vector (d2, d1, 0.0)); // AP in cell 1
  positionAlloc->Add (Vector (0.0, d1, 0.0));  // UE1/STA in cell 0
  positionAlloc->Add (Vector (d2, 0.0, 0.0));  // STA in cell 1
  positionAlloc->Add (Vector (d2 / 2, d1 / 2, 0.0)); // LAA node in the middle of the topology

  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetPositionAllocator (positionAlloc);
  mobility.Install (allWirelessNodes);

  bool disableApps = false;
  Time durationTime = Seconds (duration);



  switch (channelAccessManager)
    {
    case Lbt:
      Config::SetDefault ("ns3::LaaWifiCoexistenceHelper::ChannelAccessManagerType", StringValue ("ns3::LbtAccessManager"));
      Config::SetDefault ("ns3::LbtAccessManager::Txop", TimeValue (MilliSeconds(lbtTxop)));
      Config::SetDefault ("ns3::LbtAccessManager::UseReservationSignal", BooleanValue(useReservationSignal));
      break;
    case BasicLbt:
      Config::SetDefault ("ns3::LaaWifiCoexistenceHelper::ChannelAccessManagerType", StringValue ("ns3::BasicLbtAccessManager"));
      Config::SetDefault ("ns3::BasicLbtAccessManager::TxTime", TimeValue (MilliSeconds (txTime)));
      Config::SetDefault ("ns3::BasicLbtAccessManager::WaitTime",TimeValue (MilliSeconds (waitTime)));
      std::cout << "\n LTE eNb is using a basic LBT channel access mechanism with txTime=" << txTime << "ms and waitTime=" << waitTime << " ms.";
      break;
    case DutyCycle:
      Config::SetDefault ("ns3::LaaWifiCoexistenceHelper::ChannelAccessManagerType", StringValue("ns3::DutyCycleAccessManager"));
      Config::SetDefault ("ns3::DutyCycleAccessManager::OnDuration", TimeValue (MilliSeconds (dutyCycleOnDuration)));
      Config::SetDefault ("ns3::DutyCycleAccessManager::OnStartTime",TimeValue (MilliSeconds (dutyCycleOnStartTime)));
      Config::SetDefault ("ns3::DutyCycleAccessManager::DutyCyclePeriod",TimeValue (MilliSeconds (dutyCyclePeriod)));
      std::cout << "\n LTE eNb is using a duty cycle channel access mechanism with onDuration=" << dutyCycleOnDuration << "ms and dutyCyclePeriod=" << dutyCyclePeriod << " ms.";
      break;
    case OnlyListen: // it is used if laaNodeEnabled=true, then one laa node is installed in the middle of the topology and that node will just listen
      Config::SetDefault ("ns3::LaaWifiCoexistenceHelper::ChannelAccessManagerType", StringValue ("ns3::LbtAccessManager"));
      break;
    default:
      //default LTE channel access manager will be used, LTE always transmits
      break;
    }

  // REM settings tuned to get a nice figure for this specific scenario
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::OutputFile", StringValue ("laa-wifi-simple.rem"));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::XMin", DoubleValue (-50));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::XMax", DoubleValue (250));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::YMin", DoubleValue (-50));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::YMax", DoubleValue (250));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::XRes", UintegerValue (600));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::YRes", UintegerValue (600));
  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::Z", DoubleValue (1.5));

  //TBD: add this to be global value config parameter
  //Config::SetDefault ("ns3::DcfCwTrace::GenerateCWTraces", BooleanValue(true));


  // we want deterministic behavior in this simple scenario, so we disable shadowing
  Config::SetDefault ("ns3::Ieee80211axIndoorPropagationLossModel::Sigma", DoubleValue (0));

  // Specify some physical layer parameters that will be used below and
  // in the scenario helper.
  PhyParams phyParams;
  phyParams.m_bsTxGain = 5; // dB antenna gain
  phyParams.m_bsRxGain = 5; // dB antenna gain
  phyParams.m_bsTxPower = 18; // dBm
  phyParams.m_bsNoiseFigure = 5; // dB
  phyParams.m_ueTxGain = 0; // dB antenna gain
  phyParams.m_ueRxGain = 0; // dB antenna gain
  phyParams.m_ueTxPower = 18; // dBm
  phyParams.m_ueNoiseFigure = 9; // dB

  // calculate rx power corresponding to d2 for logging purposes
  // note: a separate propagation loss model instance is used, make
  // sure the settings are consistent with the ones used for the
  // simulation
  const uint32_t earfcn = 255444;
  double dlFreq = LteSpectrumValueHelper::GetCarrierFrequency (earfcn);
  Ptr<PropagationLossModel> plm = CreateObject<Ieee80211axIndoorPropagationLossModel> ();
  plm->SetAttribute ("Frequency", DoubleValue (dlFreq));
  double txPowerFactors = phyParams.m_bsTxGain + phyParams.m_ueRxGain +
    phyParams.m_bsTxPower;
  double rxPowerDbmD1 = plm->CalcRxPower (txPowerFactors,
                                          bsNodesA.Get (0)->GetObject<MobilityModel> (),
                                          ueNodesA.Get (0)->GetObject<MobilityModel> ());
  double rxPowerDbmD2 = plm->CalcRxPower (txPowerFactors,
                                          bsNodesA.Get (0)->GetObject<MobilityModel> (),
                                          ueNodesB.Get (0)->GetObject<MobilityModel> ());


  std::ostringstream simulationParams;
  simulationParams << d1 << " " << d2 << " "
                   << rxPowerDbmD1 << " "
                   << rxPowerDbmD2 << " "
                   << lteDutyCycle << " ";

  //LogComponentEnable("DutyCycleAccessManager", LOG_LEVEL_ALL);
  //LogComponentEnable("BasicLbtAccessManager", LOG_LEVEL_ALL);

  ConfigureAndRunScenario (cellConfigA, cellConfigB, bsNodesA, bsNodesB, ueNodesA, ueNodesB, phyParams, durationTime, transport,
                           "ns3::Ieee80211axIndoorPropagationLossModel", disableApps, lteDutyCycle, generateRem, outputDir + "/laa_wifi_simple_edpd" + simTag, simulationParams.str ());

  return 0;
}
