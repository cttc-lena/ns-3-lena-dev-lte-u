#!/bin/bash
#
# Copyright (c) 2016 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Author: Biljana Bojovic <bbojovic@cttc.es>
#

source ../utils/shot
source ../utils/common
source config

IMGDIR=images_ftp
THUMBNAILS=true
NOX11=true

BASE_OPTIONS=""
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 1 pt 4 lt 1"
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 2 pt 7 lt 2"
BASE_OPTIONS="${BASE_OPTIONS} ; set style increment ;"
BASE_OPTIONS="${BASE_OPTIONS} ; set pointsize 2 ; set grid;"

outputDir=`pwd`/results_ftp

set -o errexit

transport=FtpM1

##############################################################
### a) Lte_U over Wifi
##############################################################
#  Latency CDF
##############################################################

for ftpFileSize in ${ftpFileSizeList} ; do
  for d2 in ${d2_list} ; do
    for ftpLambda in ${ftpLambdas} ; do
      for numberOfSecondaryCarriers in ${numberOfSecondaryCarriersList} ; do
       simTag="ca_ftp_${transport}_${numberOfSecondaryCarriers}_${ftpLambda}_${d2}_${ftpFileSize}_lteu_wifi"
       imgTag="indoor_"`echo "${simTag}" | tr '.' '_'`
       TRAFFIC=`print_traffic_model ${transport}`    
       TITLE="a)FTP=${ftpLambda}-d=${d2}-Nscc=${numberOfSecondaryCarriers}_${ftpFileSize}"
       for OPERATOR in A B ; do
         LATENCY_COLUMN=9
         CURRENT=${outputDir}/laa_wifi_simple_${simTag}_operator${OPERATOR}
            `../utils/cdf.sh $LATENCY_COLUMN $CURRENT > ${outputDir}/cdf_latency_${simTag}_${OPERATOR}`
       done
       index=0           
       for OPERATOR in A B ; do
           FILES[$index]=${outputDir}/cdf_latency_${simTag}_${OPERATOR}
           YCOLS[$index]='($2)'    
           XCOLS[$index]='($1)'
           LABELS[$index]=`print_operator_lteu_wifi $OPERATOR`
           
           index=`expr $index + 1`
       done

       PLOTTYPE="with linespoints"
       XLABEL="Latency [ms]"
       YLABEL="CDF"
       RANGE=$LATENCY_CDF_RANGE
       OPTIONS="$BASE_OPTIONS ; set key bottom right"
       IMGFILENAME="${imgTag}_latency"
       plot

       unset FILES
       unset LABELS
       unset YCOLS
       unset XCOLS
      done 
    done
  done
done
##############################################################
#  Throughput CDF
##############################################################
for ftpFileSize in ${ftpFileSizeList} ; do
  for d2 in ${d2_list} ; do
    for ftpLambda in ${ftpLambdas} ; do
      for numberOfSecondaryCarriers in ${numberOfSecondaryCarriersList} ; do
       simTag="ca_ftp_${transport}_${numberOfSecondaryCarriers}_${ftpLambda}_${d2}_${ftpFileSize}_lteu_wifi"
       imgTag="indoor_"`echo "${simTag}" | tr '.' '_'`
       TRAFFIC=`print_traffic_model ${transport}`    
       TITLE="a)FTP=${ftpLambda}_d=${d2}-Nscc=${numberOfSecondaryCarriers}_${ftpFileSize}"
       for OPERATOR in A B ; do
           THROUGHPUT_COLUMN=8
           CURRENT=${outputDir}/laa_wifi_simple_${simTag}_operator${OPERATOR}
           `../utils/cdf.sh $THROUGHPUT_COLUMN $CURRENT > ${outputDir}/cdf_throughput_${simTag}_${OPERATOR}`
       done
       index=0           
       for OPERATOR in A B ; do
           FILES[$index]=${outputDir}/cdf_throughput_${simTag}_${OPERATOR}
           YCOLS[$index]='($2)'    
           XCOLS[$index]='($1)'
           LABELS[$index]=`print_operator_lteu_wifi $OPERATOR`
           
           index=`expr $index + 1`
       done
       PLOTTYPE="with linespoints"
       XLABEL="Throughput [Mbps]"
       YLABEL="CDF"
       RANGE=$THROUGHPUT_CDF_RANGE
       OPTIONS="$BASE_OPTIONS ; set key bottom right"
       IMGFILENAME="${imgTag}_throughput"
       plot

       unset FILES
       unset LABELS
       unset YCOLS
       unset XCOLS
      done
    done
  done
done


##############################################################
### b) Wifi over Wifi
##############################################################
#  Latency CDF
##############################################################

for ftpFileSize in ${ftpFileSizeList} ; do
  for d2 in ${d2_list} ; do
    for ftpLambda in ${ftpLambdas} ; do
       simTag="ca_ftp_${transport}_${ftpLambda}_${d2}_${ftpFileSize}_wifi_wifi"
       imgTag="indoor_"`echo "${simTag}" | tr '.' '_'`
       TRAFFIC=`print_traffic_model ${transport}`    
       TITLE="b)FTP=${ftpLambda}-d=${d2}_${ftpFileSize}"
       for OPERATOR in A B ; do
         LATENCY_COLUMN=9
         CURRENT=${outputDir}/laa_wifi_simple_${simTag}_operator${OPERATOR}
            `../utils/cdf.sh $LATENCY_COLUMN $CURRENT > ${outputDir}/cdf_latency_${simTag}_${OPERATOR}`
       done
       index=0           
       for OPERATOR in A B ; do
           FILES[$index]=${outputDir}/cdf_latency_${simTag}_${OPERATOR}
           YCOLS[$index]='($2)'    
           XCOLS[$index]='($1)'
           LABELS[$index]=`print_operator_wifi_wifi $OPERATOR`
           
           index=`expr $index + 1`
       done

       PLOTTYPE="with linespoints"
       XLABEL="Latency [ms]"
       YLABEL="CDF"
       RANGE=$LATENCY_CDF_RANGE
       OPTIONS="$BASE_OPTIONS ; set key bottom right"
       IMGFILENAME="${imgTag}_latency"
       plot

       unset FILES
       unset LABELS
       unset YCOLS
       unset XCOLS
    done 
  done
done
##############################################################
#  Throughput CDF
##############################################################

for ftpFileSize in ${ftpFileSizeList} ; do
  for d2 in ${d2_list} ; do
    for ftpLambda in ${ftpLambdas} ; do
       simTag="ca_ftp_${transport}_${ftpLambda}_${d2}_${ftpFileSize}_wifi_wifi"
       imgTag="indoor_"`echo "${simTag}" | tr '.' '_'`
       TRAFFIC=`print_traffic_model ${transport}`    
       TITLE="b)FTP=${ftpLambda}-d=${d2}_${ftpFileSize}"
       for OPERATOR in A B ; do
           THROUGHPUT_COLUMN=8
           CURRENT=${outputDir}/laa_wifi_simple_${simTag}_operator${OPERATOR}
           `../utils/cdf.sh $THROUGHPUT_COLUMN $CURRENT > ${outputDir}/cdf_throughput_${simTag}_${OPERATOR}`
       done
       index=0           
       for OPERATOR in A B ; do
           FILES[$index]=${outputDir}/cdf_throughput_${simTag}_${OPERATOR}
           YCOLS[$index]='($2)'    
           XCOLS[$index]='($1)'
           LABELS[$index]=`print_operator_wifi_wifi $OPERATOR`
           
           index=`expr $index + 1`
       done
       PLOTTYPE="with linespoints"
       XLABEL="Throughput [Mbps]"
       YLABEL="CDF"
       RANGE=$THROUGHPUT_CDF_RANGE
       OPTIONS="$BASE_OPTIONS ; set key bottom right"
       IMGFILENAME="${imgTag}_throughput"
       plot

       unset FILES
       unset LABELS
       unset YCOLS
       unset XCOLS
    done
  done  
done


##############################################################
### c) Lte over Wifi
##############################################################
#  Latency CDF
##############################################################

for ftpFileSize in ${ftpFileSizeList} ; do
  for d2 in ${d2_list} ; do
    for ftpLambda in ${ftpLambdas} ; do
      for numberOfSecondaryCarriers in ${numberOfSecondaryCarriersList} ; do
      simTag="ca_ftp_${transport}_${numberOfSecondaryCarriers}_${ftpLambda}_${d2}_${ftpFileSize}_lte_wifi"
       imgTag="indoor_"`echo "${simTag}" | tr '.' '_'`
       TRAFFIC=`print_traffic_model ${transport}`    
       TITLE="c)FTP=${ftpLambda}-d=${d2}-Nscc=${numberOfSecondaryCarriers}_${ftpFileSize}"
       for OPERATOR in A B ; do
         LATENCY_COLUMN=9
         CURRENT=${outputDir}/laa_wifi_simple_${simTag}_operator${OPERATOR}
            `../utils/cdf.sh $LATENCY_COLUMN $CURRENT > ${outputDir}/cdf_latency_${simTag}_${OPERATOR}`
       done
       index=0           
       for OPERATOR in A B ; do
           FILES[$index]=${outputDir}/cdf_latency_${simTag}_${OPERATOR}
           YCOLS[$index]='($2)'    
           XCOLS[$index]='($1)'
           LABELS[$index]=`print_operator_lte_wifi $OPERATOR`
           
           index=`expr $index + 1`
       done

       PLOTTYPE="with linespoints"
       XLABEL="Latency [ms]"
       YLABEL="CDF"
       RANGE=$LATENCY_CDF_RANGE
       OPTIONS="$BASE_OPTIONS ; set key bottom right"
       IMGFILENAME="${imgTag}_latency"
       plot

       unset FILES
       unset LABELS
       unset YCOLS
       unset XCOLS
      done 
    done
  done
done
##############################################################
#  Throughput CDF
##############################################################

for ftpFileSize in ${ftpFileSizeList} ; do
  for d2 in ${d2_list} ; do
    for ftpLambda in ${ftpLambdas} ; do
      for numberOfSecondaryCarriers in ${numberOfSecondaryCarriersList} ; do
       simTag="ca_ftp_${transport}_${numberOfSecondaryCarriers}_${ftpLambda}_${d2}_${ftpFileSize}_lte_wifi"
       imgTag="indoor_"`echo "${simTag}" | tr '.' '_'`
       TRAFFIC=`print_traffic_model ${transport}`    
       TITLE="c)FTP=${ftpLambda}-d=${d2}-Nscc=${numberOfSecondaryCarriers}_${ftpFileSize}"
       for OPERATOR in A B ; do
           THROUGHPUT_COLUMN=8
           CURRENT=${outputDir}/laa_wifi_simple_${simTag}_operator${OPERATOR}
           `../utils/cdf.sh $THROUGHPUT_COLUMN $CURRENT > ${outputDir}/cdf_throughput_${simTag}_${OPERATOR}`
       done
       index=0           
       for OPERATOR in A B ; do
           FILES[$index]=${outputDir}/cdf_throughput_${simTag}_${OPERATOR}
           YCOLS[$index]='($2)'    
           XCOLS[$index]='($1)'
           LABELS[$index]=`print_operator_lte_wifi $OPERATOR`
           
           index=`expr $index + 1`
       done
       PLOTTYPE="with linespoints"
       XLABEL="Throughput [Mbps]"
       YLABEL="CDF"
       RANGE=$THROUGHPUT_CDF_RANGE
       OPTIONS="$BASE_OPTIONS ; set key bottom right"
       IMGFILENAME="${imgTag}_throughput"
       plot

       unset FILES
       unset LABELS
       unset YCOLS
       unset XCOLS
      done
    done
  done
done


../utils/shot_thumbnails.sh $IMGDIR/thumbnails "Scenario laa-wifi-simple with FTP traffic, 3 different scenarios a) lteu - wifi  b) wifi-wifi c) lte - wifi "

