#!/bin/bash

source ../utils/shot
source ../utils/common

IMGDIR=images
THUMBNAILS=true
NOX11=true

BASE_OPTIONS=""
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 1 pt 8 lt 1"
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 2 pt 9 lt 2"
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 3 pt 4 lt 3"
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 4 pt 7 lt 4"
BASE_OPTIONS="${BASE_OPTIONS} ; set style increment ;"

BASE_OPTIONS="${BASE_OPTIONS} ; set pointsize 2 ; set grid;"

set -o errexit


transport=Tcp
ftpLambda=1.5
lteDutyCycle=0.5


imgTag="indoor_comparison_"`echo "${transport}_ftpLambda_${ftpLambda}_lteDutyCycle_${lteDutyCycle}" | tr '.' '_'`


##############################################################
#  Latency CDF
##############################################################

index=0	    


# ----------------
# Wi-Fi over Wi-Fi
# ----------------

simTag="ftpLambda_${ftpLambda}_wifiOnly_${transport}"

for OPERATOR in A B ; do
    FILES[$index]=../laa-wifi-indoor-wifionly/results/cdf_latency_${simTag}_${OPERATOR}
    if test ! -f ${FILES[$index]} ; then
	echo "cannot find ${FILES[$index]}, did you run the laa-wifi-indoor-wifionly campaign and its plotting script?"
	exit 1
    fi
    YCOLS[$index]='($2)'    
    XCOLS[$index]='($1)'
    LABELS[$index]="Wi-Fi over Wi-Fi "`print_operator_wifi_wifi $OPERATOR`
    
    index=`expr $index + 1`
done


# ----------------
# LTE over Wi-Fi
# ----------------

simTag="ftpLambda_${ftpLambda}_lteDutyCycle_${lteDutyCycle}_${transport}"

for OPERATOR in A B ; do
    FILES[$index]=results/cdf_latency_${simTag}_${OPERATOR}
    if test ! -f ${FILES[$index]} ; then
	echo "cannot find ${FILES[$index]}, did you run ./plot_laa_wifi_indoor.sh ?"
	exit 1
    fi
    YCOLS[$index]='($2)'    
    XCOLS[$index]='($1)'
    LABELS[$index]="LTE-DC over Wi-Fi "`print_operator_laa_wifi $OPERATOR`
    
    index=`expr $index + 1`
done


TRAFFIC=`print_traffic_model ${transport}`    
TITLE="laa-wifi-indoor, ftpLambda=${ftpLambda}, lteDutyCycle=${lteDutyCycle}, ${TRAFFIC}"


PLOTTYPE="with linespoints"
XLABEL="Latency [ms]"
YLABEL="CDF"
RANGE=$LATENCY_CDF_RANGE
OPTIONS="$BASE_OPTIONS ; set key bottom right"
IMGFILENAME="${imgTag}_latency"
plot



##############################################################
#  Throughput CDF
##############################################################

jndex=0
while test $jndex -lt $index ; do
    FILES[$jndex]=`echo -n "${FILES[$jndex]}" | sed 's/latency/throughput/'`
    XCOLS[$jndex]='($1)'
    jndex=`expr $jndex + 1`
done


PLOTTYPE="with linespoints"
XLABEL="Throughput [Mbps]"
YLABEL="CDF"
RANGE=$THROUGHPUT_CDF_RANGE
OPTIONS="$BASE_OPTIONS ; set key bottom right"
IMGFILENAME="${imgTag}_throughput"
plot


../utils/shot_thumbnails.sh $IMGDIR/thumbnails "laa-wifi-indoor results "

