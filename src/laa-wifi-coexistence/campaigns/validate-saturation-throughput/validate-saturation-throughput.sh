#!/bin/bash

#
# Copyright (c) 2015 University of Washington
# Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Authors: Tom Henderson <tomh@tomh.org> and Nicola Baldo <nbaldo@cttc.es>
#

control_c()
{
  echo "exiting"
  exit $?
}

trap control_c SIGINT

if test ! -f ../../../../waf ; then
    echo "please run this program from within the directory `dirname $0`, like this:"
    echo "cd `dirname $0`"
    echo "./`basename $0`"
    exit 1
fi


set -x
set -o errexit

outputDir=`pwd`/results
mkdir -p "${outputDir}"
rm -f "${outputDir}"/laa_wifi_simple_*_operator?
rm -f "${outputDir}"/time_stats

# need this as otherwise waf won't find the executables
cd ../../../../

# Fix d2 to 1000 meters to ensure isolation between networks
d2=1000

# RngRun governs the random number seeding; the default value is 1, and 
# setting to other integer values will seed the simulation differently
RngRun=1

for transport in Udp ; do

    for lteDutyCycle in 1.0 ; do
	
	simTag="validate-saturation-d2_${d2}"

	for d1 in 10 20 50 75 85 95 105 115 125 150 175 200 225 250 275 300 350 400 450 500 ; do 
	    /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats -a \
		./waf --run laa-wifi-simple --command="%s --cellConfigA=Lte --cellConfigB=Wifi --d1=${d1} --d2=${d2} --lteDutyCycle=${lteDutyCycle} --simTag=${simTag} --transport=${transport} --outputDir=${outputDir} --RngRun=${RngRun}"
	done 
	
    done

done
