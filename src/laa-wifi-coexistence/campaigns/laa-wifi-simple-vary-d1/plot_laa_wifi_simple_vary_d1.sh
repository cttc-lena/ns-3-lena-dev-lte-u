#!/bin/bash

source ../utils/shot
source ../utils/common

IMGDIR=images
THUMBNAILS=true
NOX11=true


BASE_OPTIONS=""
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 1 pt 4 lt 1"
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 2 pt 7 lt 2"
BASE_OPTIONS="${BASE_OPTIONS} ; set style increment ;"

BASE_OPTIONS="${BASE_OPTIONS} ; set pointsize 2 ; set grid;"

set -o errexit



############################################
# varying d1, fixed d2 and duty cycle
############################################


d2=1000

for transport in Udp Tcp ; do

    for lteDutyCycle in 0.5 1.0 ; do

	simTag="d2_${d2}_lteDutyCycle_${lteDutyCycle}_${transport}"
	imgTag=`echo "${transport}_d2_${d2}_lteDutyCycle_${lteDutyCycle}" | tr '.' '_'`

	TRAFFIC=`print_traffic_model ${transport}`    
	TITLE="laa-wifi-simple, d2=${d2}, lteDutyCycle=${lteDutyCycle}, ${TRAFFIC}"

	index=0	    
	for OPERATOR in A B ; do
	    FILES[$index]=results/laa_wifi_simple_${simTag}_operator${OPERATOR}
	    YCOLS[$index]='($13)'    
	    XCOLS[$index]='($3)'
	    LABELS[$index]=`print_operator_laa_wifi $OPERATOR`
	    
	    index=`expr $index + 1`
	done

	PLOTTYPE="with linespoints"
	XLABEL="RX power [dBm]"
	YLABEL="Throughput [Mbps]"
	RANGE="[*:*][0:`print_max_throughput_range_laa_wifi_simple $transport`]"
	OPTIONS="$BASE_OPTIONS ; set key top left"
	IMGFILENAME="${imgTag}_throughput_vs_rxpower"
	plot

	jndex=0
	while test $jndex -lt $index ; do
	    XCOLS[$jndex]='($1)'
	    jndex=`expr $jndex + 1`
	done

	XLABEL="d1 [m]"
	OPTIONS="$BASE_OPTIONS ; set key top right"
	IMGFILENAME="${imgTag}_throughput_vs_d1"
	plot


	unset FILES
	unset LABELS
	unset YCOLS
	unset XCOLS

    done

done

../utils/shot_thumbnails.sh $IMGDIR/thumbnails "laa-wifi-simple results vary d1"

