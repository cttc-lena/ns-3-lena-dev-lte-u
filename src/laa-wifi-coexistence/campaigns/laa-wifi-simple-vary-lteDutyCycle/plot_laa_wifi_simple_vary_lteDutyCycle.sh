#!/bin/bash

source ../utils/shot
source ../utils/common

IMGDIR=images
THUMBNAILS=true
NOX11=true


BASE_OPTIONS=""
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 1 pt 4 lt 1"
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 2 pt 7 lt 2"
BASE_OPTIONS="${BASE_OPTIONS} ; set style increment ;"

BASE_OPTIONS="${BASE_OPTIONS} ; set pointsize 2 ; set grid;"

set -o errexit


############################################
# varying lteDutyCycle, fixed d1 and d2
############################################



d1=20


for transport in Udp Tcp ; do
    
    for d2 in 10 20 50 100 ; do 

	simTag="d1_${d1}_d2_${d2}_${transport}"    
	imgTag=`echo "${transport}_d1_${d1}_d2_${d2}" | tr '.' '_'`    

	TRAFFIC=`print_traffic_model ${transport}`    
	TITLE="laa-wifi-simple, d1=${d1}, d2=${d2}, ${TRAFFIC}"

	index=0	    
	for OPERATOR in A B ; do
	    FILES[$index]=results/laa_wifi_simple_${simTag}_operator${OPERATOR}
	    YCOLS[$index]='($13)'    
	    XCOLS[$index]='($5*100)'
	    LABELS[$index]=`print_operator_laa_wifi $OPERATOR`
	    
	    index=`expr $index + 1`
	done

	PLOTTYPE="with linespoints"
	XLABEL="duty cycle [%]"
	YLABEL="Throughput [Mbps]"
	RANGE="[*:*][0:`print_max_throughput_range_laa_wifi_simple $transport`]"
	OPTIONS="$BASE_OPTIONS ; set key top center"
	IMGFILENAME="${imgTag}_throughput_vs_lteDutyCycle"
	plot

	unset FILES
	unset LABELS
	unset YCOLS
	unset XCOLS

    done

done


../utils/shot_thumbnails.sh $IMGDIR/thumbnails "laa-wifi-simple results vary lteDutyCycle"

