#!/bin/bash

#
# Copyright (c) 2015 University of Washington
# Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Authors: Tom Henderson <tomh@tomh.org> and Nicola Baldo <nbaldo@cttc.es> and Biljana Bojovic <bbojovic@cttc.es>
#

source config

control_c()
{
  echo "exiting"
  exit $?
}

trap control_c SIGINT

if test ! -f ../../../../waf ; then
  echo "please run this program from within the directory `dirname $0`, like this:"
  echo "cd `dirname $0`"
  echo "./`basename $0`"
  exit 1
fi

set -x
set -o errexit

outputDir=`pwd`/results
mkdir -p "${outputDir}"
rm -f "${outputDir}"/laa_wifi_indoor_csat_enabled_${transport}*_operator?
rm -f "${outputDir}"/time_stats
rm -f "${outputDir}"/laa_wifi_indoor_csat_enabled_${transport}*_log

# need this as otherwise waf won't find the executables
cd ../../../../

for ftpLambda in ${ftpLambdas} ; do
for transport in ${transports} ; do
for thrIndex in ${!muThresholdsLow[@]} ; do
	    simTag="csat_enabled_${transport}_lambda_${ftpLambda}_${duration}_${muThresholdsLow[${thrIndex}]}_${muThresholdsHigh[${thrIndex}]}"
            /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats -a \
            ./waf --run laa-wifi-indoor --command="%s --cellConfigA=Lte_U --cellConfigB=Wifi --logPhyNodeId=${logPhyNodeId} --logPhyArrivals=${logPhyArrivals} --logBeaconArrivals=${logBeaconArrivals} --logBeaconNodeId=${logBeaconNodeId} --logWifiRetries=${logWifiRetries} --logWifiFailRetries=${logWifiFailRetries} --logBackoffChanges=${logBackoffChanges} --logTxops=${logTxops} --logCsatDutyCycle=${logCsatDutyCycle} --transport=${transport} --duration=${duration} --simTag=${simTag} --outputDir=${outputDir} --RngRun=${RngRun}  --wifiStandard=${wifiStandard} --ftpLambda=${ftpLambda} --csatCycleDuration=${csatCycleDuration} --csatDutyCycle=${csatDutyCycle} --puncturing=${puncturing} --puncturingDuration=${puncturingDuration} --maxInterMibTime=${maxInterMibTime} --tOnMinInMilliSec=${tOnMinInMilliSec} --minOffTime=${minOffTime} --alphaMU=${alphaMU} --thresholdMuLow=${muThresholdsLow[${thrIndex}]} --thresholdMuHigh=${muThresholdsHigh[${thrIndex}]} --deltaTUp=${deltaTUp} --deltaTDown=${deltaTDown} --wifiQueueMaxSize=${wifiQueueMaxSize} --ns3::LteEnbRrc::DefaultTransmissionMode=${laaMimo} --ns3::TcpSocket::SegmentSize=${tcpSegSize} --ns3::TcpSocket::InitialCwnd=${tcpInitialCw} --lteUEdThreshold=${lteUEdThreshold} --wifiEdThreshold=${wifiEdThreshold} --ldsPeriod=${ldsPeriod} --pdcchDuration=${pdcchDuration} --assignDutyCycle=${assignDutyCycle}"
done
done
done

