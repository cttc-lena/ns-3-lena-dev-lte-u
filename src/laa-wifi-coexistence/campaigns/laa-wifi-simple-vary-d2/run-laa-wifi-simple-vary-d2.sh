#!/bin/bash

#
# Copyright (c) 2015 University of Washington
# Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Authors: Tom Henderson <tomh@tomh.org> and Nicola Baldo <nbaldo@cttc.es>
#

control_c()
{
  echo "exiting"
  exit $?
}

trap control_c SIGINT

set -x
set -o errexit

outputDir=`pwd`/results
mkdir -p "${outputDir}"
rm -f "${outputDir}"/laa_wifi_simple_*_operator?
rm -f "${outputDir}"/time_stats

# need this as otherwise waf won't find the executables
cd ../../../../

d1=20

for transport in Udp Tcp ; do

    for lteDutyCycle in 0.5 1.0 ; do

	simTag="d1_${d1}_lteDutyCycle_${lteDutyCycle}_${transport}"	

	for d2 in  2 3 5 7 10 20 30 50 70 100 200 300 500 ; do 
	    /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats -a \
		./waf --run laa-wifi-simple --command="%s --cellConfigA=Lte --cellConfigB=Wifi --d1=${d1} --d2=${d2} --lteDutyCycle=${lteDutyCycle} --simTag=${simTag} --transport=${transport} --outputDir=${outputDir}"
	done 
	
    done

done