#!/bin/bash

#
# Copyright (c) 2015 University of Washington
# Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Authors: Tom Henderson <tomh@tomh.org> and Nicola Baldo <nbaldo@cttc.es>
#


control_c()
{
  echo "exiting"
  exit $?
}

trap control_c SIGINT

set -x
set -o errexit

if test ! -f ../../../../waf ; then
    echo "please run this program from within the directory `dirname $0`, like this:"
    echo "cd `dirname $0`"
    echo "./`basename $0`"
    exit 1
fi

xRes=200
yRes=200

# note: setting this to xRes*yRes guarantees no pathloss randomness artifacts
# (i.e., NLOS/LOS and shadowing values are never repeated), 
# however it increases memory consumption
maxPointsPerIteration=40000

# manual calculation of REM dimensions valid for 7 sites
xMin=-250
xMax=1335
yMin=-360
yMax=1220

# manual calculation of REM dimensions valid for 7 sites but showing larger lower left corner
# to see where pathloss starts degrading
# xMin=-2250
# xMax=1335
# yMin=-2360
# yMax=1220

 # zoomed-in map highlighting 3 sites
 # xMin=0
 # xMax=600
 # yMin=0
 # yMax=600

remDir=`pwd`/rem
mkdir -p $remDir

remBasename=${remDir}/laa-wifi-outdoor-rem_${xMin}_${xMax}_${yMin}_${yMax}_${xRes}x${yRes}
topologyBasename=${remDir}/laa-wifi-outdoor-topology_${xMin}_${xMax}_${yMin}_${yMax}_${xRes}x${yRes}



# need this as otherwise waf won't find the executables
cd ../../../../


test "x$NORUN" = x && nice -n 19 ./waf --run laa-wifi-outdoor --command="%s --generateRem=1 \
--ns3::RadioEnvironmentMapHelper::XRes=${xRes} --ns3::RadioEnvironmentMapHelper::YRes=${yRes} \
--ns3::RadioEnvironmentMapHelper::XMin=${xMin} --ns3::RadioEnvironmentMapHelper::XMax=${xMax} \
--ns3::RadioEnvironmentMapHelper::YMin=${yMin} --ns3::RadioEnvironmentMapHelper::YMax=${yMax} \
--ns3::RadioEnvironmentMapHelper::MaxPointsPerIteration=40000 \
--ns3::RadioEnvironmentMapHelper::OutputFile=${remBasename}.rem \
--remDir=${remDir} "

cd $remDir

gnuplot -persist <<EOF

set pointsize 1.0

set linetype 1 lc rgb "white"
set style fill empty border lt 1
set style arrow 1 front lt 1



load "laa-wifi-outdoor-topology.gnuplot"
load "bs_A.gnuplot"
load "bs_B.gnuplot" 
load "ue_A.gnuplot" 
load "ue_B.gnuplot"

set term x11
set view map;
set xlabel "X"
set ylabel "Y"
set cblabel "SINR (dB)"
set key bottom left box opaque reverse Left width 2
set cbrange [-5:30]


plot [${xMin}:${xMax}][${yMin}:${yMax}][*:*] "${remBasename}.rem" using (\$1):(\$2):(\$4 > 0 ? 10*log10(\$4) : -100) with image title "" , \
NaN with points pt 5 lc rgb "cyan"        title "BS operator A", \
NaN with points pt 4 lc rgb "cyan"        title "UE operator A", \
NaN with points pt 7 lc rgb "chartreuse" title "BS operator B", \
NaN with points pt 6 lc rgb "chartreuse" title "UE operator B"


#plot [-250:1335][-360:1220][*:*] -2e6 with points
#plot [0:500][-360:250][*:*] -2e6 with points

set term postscript eps color size 9,8 fontscale 1.5
set output "${remBasename}.eps"
replot

set term png size 1200,1066 font "Helvetica,16"
set output "${remBasename}.png"
replot

reset 

set pointsize 1.0

set linetype 1 lc rgb "black"
set style fill empty border lt 1
set style arrow 1 front lt 1

load "laa-wifi-outdoor-topology.gnuplot"
load "bs_A.gnuplot"
load "bs_B.gnuplot" 
load "ue_A.gnuplot" 
load "ue_B.gnuplot"

set term x11
set view map;
set xlabel "X"
set ylabel "Y"
set key bottom left box opaque reverse Left width 2

plot [${xMin}:${xMax}][${yMin}:${yMax}][*:*] \
NaN with points pt 5 lc rgb "cyan"        title "BS operator A", \
NaN with points pt 4 lc rgb "cyan"        title "UE operator A", \
NaN with points pt 7 lc rgb "chartreuse" title "BS operator B", \
NaN with points pt 6 lc rgb "chartreuse" title "UE operator B"


set term postscript eps color size 9,8 fontscale 1.5
set output "${topologyBasename}.eps"
replot

set term png size 1200,1066 font "Helvetica,16"
set output "${topologyBasename}.png"
replot



EOF

echo "check for images in $remDir"


