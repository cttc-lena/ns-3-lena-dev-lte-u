#!/bin/bash

source ../utils/shot
source ../utils/common

IMGDIR=images
THUMBNAILS=true
NOX11=true


BASE_OPTIONS=""
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 1 pt 4 lt 1"
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 2 pt 7 lt 2"
BASE_OPTIONS="${BASE_OPTIONS} ; set style increment ;"

BASE_OPTIONS="${BASE_OPTIONS} ; set pointsize 2 ; set grid;"

set -o errexit

##############################################################
#  Latency CDF
##############################################################

transport=Tcp

for ftpLambda in 0.5 1.5 ; do
    
    simTag="ftpLambda_${ftpLambda}_${transport}"

    imgTag="outdoor_"`echo "${transport}_ftpLambda_${ftpLambda}_wifiOnly" | tr '.' '_'`

    TRAFFIC=`print_traffic_model ${transport}`    
    TITLE="laa-wifi-outdoor, ftpLambda=${ftpLambda}, wifionly, ${TRAFFIC}"

    for OPERATOR in A B ; do
        LATENCY_COLUMN=9
	CURRENT=results-wifi-wifi/laa_wifi_outdoor_${simTag}_operator${OPERATOR}
        `../utils/cdf.sh $LATENCY_COLUMN $CURRENT > results-wifi-wifi/cdf_latency_${simTag}_${OPERATOR}`
    done
    index=0	    
    for OPERATOR in A B ; do
	FILES[$index]=results-wifi-wifi/cdf_latency_${simTag}_${OPERATOR}
	YCOLS[$index]='($2)'    
	XCOLS[$index]='($1)'
	LABELS[$index]=`print_operator_wifi_wifi $OPERATOR`
	
	index=`expr $index + 1`
    done

    PLOTTYPE="with linespoints"
    XLABEL="Latency [ms]"
    YLABEL="CDF"
    RANGE="[*:*][0:1]"
    OPTIONS="$BASE_OPTIONS ; set key bottom right"
    IMGFILENAME="${imgTag}_latency"
    plot

    unset FILES
    unset LABELS
    unset YCOLS
    unset XCOLS

done

##############################################################
#  Throughput CDF
##############################################################

transport=Tcp

for ftpLambda in 0.5 1.5 ; do

    simTag="ftpLambda_${ftpLambda}_${transport}"

    imgTag="outdoor_"`echo "${transport}_ftpLambda_${ftpLambda}_wifiOnly" | tr '.' '_'`

    TRAFFIC=`print_traffic_model ${transport}`    
    TITLE="laa-wifi-outdoor, ftpLambda=${ftpLambda}, wifi only, ${TRAFFIC}"

    for OPERATOR in A B ; do
        THROUGHPUT_COLUMN=8
	CURRENT=results-wifi-wifi/laa_wifi_outdoor_${simTag}_operator${OPERATOR}
        `../utils/cdf.sh $THROUGHPUT_COLUMN $CURRENT > results-wifi-wifi/cdf_throughput_${simTag}_${OPERATOR}`
    done
    index=0	    
    for OPERATOR in A B ; do
	FILES[$index]=results-wifi-wifi/cdf_throughput_${simTag}_${OPERATOR}
	YCOLS[$index]='($2)'    
	XCOLS[$index]='($1)'
	LABELS[$index]=`print_operator_wifi_wifi $OPERATOR`
	
	index=`expr $index + 1`
    done

    PLOTTYPE="with linespoints"
    XLABEL="Throughput [Mbps]"
    YLABEL="CDF"
    RANGE="[*:*][0:1]"
    OPTIONS="$BASE_OPTIONS ; set key bottom right"
    IMGFILENAME="${imgTag}_throughput"
    plot

    unset FILES
    unset LABELS
    unset YCOLS
    unset XCOLS

done
../utils/shot_thumbnails.sh $IMGDIR/thumbnails "laa-wifi-outdoor results "

