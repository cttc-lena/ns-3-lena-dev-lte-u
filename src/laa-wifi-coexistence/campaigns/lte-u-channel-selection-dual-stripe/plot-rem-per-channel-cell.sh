#!/bin/bash
#
# Copyright (c) 2016 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author: Biljana Bojovic <bbojovic@cttc.es>
#


imagesDir=`pwd`/images
mkdir -p "${imagesDir}"
resultsDir=`pwd`/results

#channels="32 36 40 44 48 149 153 157 161 165"
channels="32 36 165"

#we need to specify cell ids in order to plot per cell id
#cells="13 15"

# could be used to print everything without specifying cell id
cell=10
while [ $cell -lt 30 ]
do
for channel in $channels ; do
if [ -f "${resultsDir}/channel-stats-per-cell-${channel}-${cell}.rem" ];
then
export rem_file=${resultsDir}/channel-stats-per-cell-${channel}-${cell}.rem
export png_file=${imagesDir}/channel-stats-per-cell-${channel}-${cell}.png
gnuplot -p gnuplot/print-re-tx-power.gnuplot
fi
done
cell=`expr $cell + 1`
done

