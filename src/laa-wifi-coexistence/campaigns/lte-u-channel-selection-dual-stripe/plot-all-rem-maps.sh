#!/bin/bash
#
# Copyright (c) 2016 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author: Biljana Bojovic <bbojovic@cttc.es>
#

imagesDir=`pwd`/images
mkdir -p "${imagesDir}"
resultsDir=`pwd`/results

channels="32 36 40 44 48 149 153 157 161 165"

for channel in $channels ; do
if [ -f "${resultsDir}/lena-dual-stripe-${channel}.rem" ];
then
export rem_file=${resultsDir}/lena-dual-stripe-${channel}.rem
export png_file=${imagesDir}/lena-dual-stripe-${channel}.png
gnuplot -p ${resultsDir}/enbs-${channel}.txt ${resultsDir}/buildings-${channel}.txt gnuplot/plot-all-rem-maps.gnuplot
fi
done;
