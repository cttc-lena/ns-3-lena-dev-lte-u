#!/bin/bash

#
# Copyright (c) 2015 University of Washington
# Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Authors: Tom Henderson <tomh@tomh.org> and Nicola Baldo <nbaldo@cttc.es> and Biljana Bojovic <bbojovic@cttc.es>
#
source config
control_c()
{
  echo "exiting"
  exit $?
}
trap control_c SIGINT

if test ! -f ../../../../waf ; then
  echo "please run this program from within the directory `dirname $0`, like this:"
  echo "cd `dirname $0`"
  echo "./`basename $0`"
  exit 1
fi

set -x
set -o errexit

currentDir=`pwd`
outputDir=`pwd`/results/
mkdir -p "${outputDir}"

# need this as otherwise waf won't find the executables
cd ../../../../

/usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats -a \
            ./waf --run channel-selection-dual-stripe --command="%s --simTime=${simTime} --outputDir=${outputDir} --generateRem=${generateRem} --configMode=${configMode} --csatCycleDuration=${csatCycleDuration} --csatDutyCycle=${csatDutyCycle} --channelStatsFile=${currentDir}/${channelStatsFile} --channelConfigFile=${currentDir}/${channelConfigFile} --useChannelConfigFile=${useChannelConfigFile}"

