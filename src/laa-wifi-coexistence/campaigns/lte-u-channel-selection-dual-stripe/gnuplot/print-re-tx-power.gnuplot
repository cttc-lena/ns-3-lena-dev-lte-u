#! /usr/bin/gnuplot

set terminal png
set pm3d map
set palette

rem_file=system("echo $rem_file")
png_file=system("echo $png_file")

set cbrange [-70:-150]

set view map;
set xlabel "X"
set ylabel "Y"
set cblabel "Received power (dB)"
unset key
set output png_file
plot rem_file using ($1):($2):(10*log10($4)) with image


