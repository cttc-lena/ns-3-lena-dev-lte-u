#!/bin/bash

#
# Copyright (c) 2015 University of Washington
# Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# 
# Authors: Tom Henderson <tomh@tomh.org>, Nicola Baldo <nbaldo@cttc.es> and Biljana Bojovic <bbojovic@cttc.es>
#

source config

control_c()
{
  echo "exiting"
  exit $?
}

trap control_c SIGINT
set -x
set -o errexit

outputDir=`pwd`/results
mkdir -p "${outputDir}"
rm -f "${outputDir}"/laa_wifi_simple_wifi_wifi_*_operator?
rm -f "${outputDir}"/time_stats
rm -f "${outputDir}"/laa_wifi_indoor_wifi_wifi_*_log

# need this as otherwise waf won't find the executables
cd ../../../../

# wifi- wifi 
for transport in ${transportType} ; do
    for d2 in ${d2_distances} ; do 
	simTag="wifi_wifi_d2_${d2}_${transport}"	
	    /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats -a \
		./waf --run laa-wifi-simple --command="%s --cellConfigA=Wifi --cellConfigB=Wifi --d2=${d2} --ftpLambda=${ftpLambda} --simTag=${simTag} --transport=${transport} --outputDir=${outputDir} --wifiStandard=${wifiStandard} --udpRate=${udpRate} --logBackoffChanges=${logBackoffChanges} --logPhyArrivals=${logPhyArrivals} --logWifiRetries=${logWifiRetries} --duration=${duration} --logTxops=${logTxops} --wifiQueueMaxSize=${wifiQueueMaxSize} --ns3::TcpSocket::SegmentSize=${tcpSegSize} --ns3::TcpSocket::InitialCwnd=${tcpInitialCw} --wifiEdThreshold=${wifiEdThreshold}"
    done
done

