/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es>
 *
 */

#ifndef TEST_LTE_U_WIFI_SIMPLE_H
#define TEST_LTE_U_WIFI_SIMPLE_H

#include "ns3/test.h"

using namespace ns3;


/**
 * Test that energy detection works properly.
 * Test scenario topology consists of two nodes: LTE-U node and wifi AP. LTE-U node is not transmitting, while wifi AP is sending periodic beacons.
 *
 *
 * 		LTE-U --------------------(d)---------------------WIFI AP
 * 		(ed)
 *
 *  Test suite is checking if by lowering energy detection threshold (ed) at LTE-U node can be sensed wifi at higher distances. If LTE-U node can detect energy from wifi AP then is expected to capture all
 *  transmissions from wifi AP, thus it is counted if all beacons have been detected. Otherwise, if LTE-U node cannot detect energy from wifi AP, then it should not detect any beacon. In order to make
 *  a deterministic behavior, the shadowing variance is set to zero.
 *
 */

class LteUWifiSimpleEdTestSuite : public TestSuite
{
public:
  LteUWifiSimpleEdTestSuite ();
};


class LteUWifiSimpleEdTestCase : public TestCase
{
public:
  LteUWifiSimpleEdTestCase (double distance, double rxPowerForDistance, double edThreshold, bool energyDetected);
  virtual ~LteUWifiSimpleEdTestCase ();
  void EnergyDetected (double duration);
  std::string BuildNameString (double distance,double rxPowerForDistance, double edThreshold, bool energyDetected);


private:
  virtual void DoRun (void);
  double m_d;
  double m_edThreshold;
  bool m_energyShouldBeDetected;
  double m_beaconsDetectedCount;
};

#endif /* TEST_LTE_U_WIFI_SIMPLE_H */
