/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011-2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es>
 *
 */

#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/internet-module.h>
#include <ns3/point-to-point-module.h>
#include <ns3/lte-module.h>
#include <ns3/wifi-module.h>
#include <ns3/config-store-module.h>
#include <ns3/spectrum-module.h>
#include <ns3/applications-module.h>
#include <ns3/flow-monitor-module.h>
#include <ns3/propagation-module.h>
#include <ns3/scenario-helper.h>
#include <ns3/lte-u-wifi-coexistence-helper.h>
#include "ns3/traced-value.h"
#include "ns3/trace-source-accessor.h"
#include "test-lte-u-wifi-simple-bd.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("LteUApScan");

/**
 * Tests that "AP scan" is working properly. Test scenario topology consists of LTE-U node and various number of wifi APs.
 * It is tested if LTE-U node can count APs based on beacon detection.
 */

class LteUApScanTestSuite : public TestSuite
{
public:
  LteUApScanTestSuite ();
};


class LteUApScanTestCase : public TestCase
{
public:
  LteUApScanTestCase (std::string, uint32_t numberOfAps);
  virtual ~LteUApScanTestCase ();
  void ApScan (uint32_t a, uint32_t b);


private:
  virtual void DoRun (void);
  uint32_t m_numberOfInstalledAPs;
  uint32_t m_apCount;
};

/**
 * TestSuite
 */

LteUApScanTestSuite::LteUApScanTestSuite ()
: TestSuite ("lte-u-ap-scan", EXAMPLE)
{
  AddTestCase (new LteUApScanTestCase ("1 AP", 1), TestCase::QUICK);
  AddTestCase (new LteUApScanTestCase ("5 APs", 5), TestCase::QUICK);
  AddTestCase (new LteUApScanTestCase ("10 APs", 10), TestCase::QUICK);
  AddTestCase (new LteUApScanTestCase ("36 APs", 36), TestCase::QUICK);
 // AddTestCase (new LteUApScanTestCase ("50 APs", 50), TestCase::QUICK);
 // AddTestCase (new LteUApScanTestCase ("100 APs", 100), TestCase::QUICK);
}

static LteUApScanTestSuite lteUWifiSimpleEdTestSuite;


/**
 * TestCase
 */

LteUApScanTestCase::LteUApScanTestCase (std::string name, uint32_t numberOfAps)
: TestCase (name), m_numberOfInstalledAPs (numberOfAps)
{
  m_apCount = 0;
}

LteUApScanTestCase::~LteUApScanTestCase ()
{
}

void
LteUApScanTestCase::ApScan (uint32_t oldValue, uint32_t newValue)
{
  NS_LOG_FUNCTION (this);
  m_apCount = newValue;

  // perform test
  NS_TEST_ASSERT_MSG_EQ(m_apCount, m_numberOfInstalledAPs, "Not received signals of all beacons.");

}


void
LteUApScanTestCase::DoRun (void)
{

  Time beaconInterval = MilliSeconds(40);
  Time duration = Seconds(3);

  // Create nodes and containers
  NodeContainer lteUNode, wifiApNodes;  // for APs and eNBs
  // create lteU node
  lteUNode.Create (1);
  // create wifi APs
  wifiApNodes.Create (m_numberOfInstalledAPs);

  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");

  // set position for Lte-U node
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0));   // eNodeB

  mobility.SetPositionAllocator (positionAlloc);
  mobility.Install(lteUNode);

  // set positoin allocator for wifi APs
  Ptr<UniformDiscPositionAllocator> discAlloc = Create<UniformDiscPositionAllocator>();
  discAlloc->SetX(100);
  discAlloc->SetY(200);
  discAlloc->SetRho(10);

  mobility.SetPositionAllocator (discAlloc);
  mobility.Install(wifiApNodes);

  PhyParams phyParams;
  phyParams.m_bsTxGain = 5;
  phyParams.m_bsRxGain = 5;
  phyParams.m_bsTxPower = 18;
  phyParams.m_bsNoiseFigure = 5;
  phyParams.m_receivers = 2;
  phyParams.m_transmitters = 2;
  phyParams.m_channelNumber = 36;

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Config::SetDefault("ns3::SpectrumWifiPhy::TxGain", DoubleValue(phyParams.m_bsTxGain));
  Config::SetDefault("ns3::SpectrumWifiPhy::RxGain", DoubleValue(phyParams.m_bsRxGain));
  Config::SetDefault("ns3::SpectrumWifiPhy::TxPowerStart", DoubleValue (phyParams.m_bsTxPower));
  Config::SetDefault("ns3::SpectrumWifiPhy::TxPowerEnd", DoubleValue (phyParams.m_bsTxPower));
  Config::SetDefault("ns3::SpectrumWifiPhy::RxNoiseFigure",DoubleValue(phyParams.m_bsNoiseFigure));
  Config::SetDefault("ns3::SpectrumWifiPhy::Receivers", UintegerValue (phyParams.m_receivers));
  Config::SetDefault("ns3::SpectrumWifiPhy::Transmitters", UintegerValue (phyParams.m_transmitters));

  // we want deterministic behavior in this simple scenario, so we disable shadowing
  Config::SetDefault ("ns3::Ieee80211axIndoorPropagationLossModel::Sigma", DoubleValue (0));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::Ieee80211axIndoorPropagationLossModel"));
  // call in order to initialize propagation loss model
  lteHelper->Initialize ();
  // LTE-U DL transmission @5180 MHz
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (255444));
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (100));
  NetDeviceContainer lteUNetDevice = lteHelper->InstallEnbDevice (lteUNode);
  // disable any transmission from LTE device, we want to have only wifi AP transmitting beacons
  Config::SetDefault ("ns3::ChannelAccessManager::GrantDuration", TimeValue(Seconds(0)));
  Config::SetDefault ("ns3::LteUCoexistenceManager::CsatEnabled", BooleanValue (true));

  // instantiate lte-u coexistence helper
  Ptr<LteUWifiCoexistenceHelper> lteUWifiCoexistenceHelper = Create<LteUWifiCoexistenceHelper> ();
  lteUWifiCoexistenceHelper->SetMonitorMode(LteUWifiCoexistenceHelper::REGULAR_WIFI_MONITOR);
  lteUWifiCoexistenceHelper->ConfigureLteUSingleWifiChannel(lteUNetDevice, phyParams);
  // connect to trace source of lte-u coexistence manager
  Ptr<LteUCoexistenceManager> lteManager = DynamicCast<LteUCoexistenceManager >(lteUNetDevice.Get(0)->GetObject<LteEnbNetDevice> ()->GetPhy ()->GetChannelAccessManager());
  lteManager->TraceConnectWithoutContext("ApCount", MakeCallback(&LteUApScanTestCase::ApScan, this));

  // spectrum channel to attach wifi AP
  Ptr<SpectrumChannel> spectrumChannel = lteHelper->GetDownlinkSpectrumChannel ();
  // set wifi AP to the same spectrum channel
  SpectrumWifiPhyHelper spectrumPhy = SpectrumWifiPhyHelper::Default ();
  spectrumPhy.SetChannel (spectrumChannel);
  WifiHelper wifi;
  // this setting will be used during WifiHeler::Install to set frequency of SpectrumWifiPhy
  wifi.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
  wifi.SetRemoteStationManager ("ns3::IdealWifiManager");
  // configure beacons generation, disable jitter
  Config::SetDefault ("ns3::ApWifiMac::BeaconInterval", TimeValue(beaconInterval));
  HtWifiMacHelper mac = HtWifiMacHelper::Default ();
  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (Ssid ("ns380211n-B")),
        	   "EnableBeaconJitter", BooleanValue (true));
  uint32_t channelNumber = 36;
  spectrumPhy.SetChannelNumber (channelNumber);

  for (uint32_t i = 0; i < wifiApNodes.GetN(); i++ )
    {
      wifi.Install (spectrumPhy, mac, wifiApNodes.Get (i));
    }

  Simulator::Stop (duration);
  Simulator::Run ();
   Simulator::Destroy ();
}
