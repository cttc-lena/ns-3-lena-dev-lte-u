/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Author: Biljana Bojovic <bbojovic@cttc.es> and Lorenza Giupponi <lgiupponi@cttc.es>
 */

#include <sstream>
#include "ns3/test.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/string.h"
#include "ns3/double.h"
#include "ns3/enum.h"
#include "ns3/boolean.h"
#include "ns3/mobility-helper.h"
#include "ns3/lte-helper.h"
#include "ns3/ff-mac-scheduler.h"
#include "ns3/lte-enb-rrc.h"
#include "ns3/lte-enb-phy.h"
#include "ns3/lte-enb-net-device.h"
#include "ns3/lte-ue-phy.h"
#include "ns3/lte-ue-net-device.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/lte-module.h"
#include "ns3/wifi-module.h"
#include "ns3/applications-module.h"
#include "ns3/internet-module.h"
#include "ns3/propagation-module.h"
#include "ns3/config-store-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-u-wifi-coexistence-helper.h"
#include "ns3/lte-u-coexistence-manager.h"
#include "ns3/scenario-helper.h"
#include <ns3/lte-control-messages.h>


using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("LteUCsatTest");

/*
*  Test that LTEU node transmits according to CSAT mask: check if puncturing is done properly, and that MIB and LDS are sent according to its configuration.
*
*     UE
*      |
*   d1 |
*      |     d2
*     LTE-U  _ _ _  wifi monitor
*      .
*      .
*      .p2p
*      .
*     client
*
*
*/


class LteUCsatTestSuite : public TestSuite
{
public:
	LteUCsatTestSuite ();
};


class LteUCsatTestCase : public TestCase
{
 public:

  LteUCsatTestCase (uint32_t puncturing,  uint32_t puncturingDuration, uint32_t m_csatCycleDuration, uint32_t offset, double dutyCycle, uint32_t mibInterval,  uint32_t maxInterMibTime,  uint32_t ldsInterval,
                    bool startWithOnPeriod, bool sendCtsToSelf);

  virtual ~LteUCsatTestCase ();

  std::string BuildNameString (uint32_t puncturing, uint32_t puncturingDuration, uint32_t m_csatCycleDuration, uint32_t offset, double dutyCycle, uint32_t mibInterval, uint32_t maxInterMibTime, uint32_t ldsInterval,
                               bool startWithOnPeriod, bool sendCtsToSelf);

  void CtrlMessageCheck (std::list<Ptr<LteControlMessage> > list);
  void DlSchedulingCheck (uint32_t frameNo, uint32_t subframeNo, uint16_t rnti, uint8_t mcsTb1, uint16_t sizeTb1, uint8_t mcsTb2, uint16_t sizeTb2);


private:

  virtual void DoRun (void);
  // test variables
  Time m_startTime;
  Time m_stopTime;
  Time m_lastMibSent;
  Time m_lastLdsSent;
  Time m_lastCtrlMessageTransmitted;
  Time m_lastMacDlScheduling;
  uint32_t m_consecutiveDlPhy;
  uint32_t m_consecutiveDlMac;
  // csat parameters
  uint32_t m_puncturing;
  uint32_t m_puncturingDuration;
  uint32_t m_csatCycleDuration;
  uint32_t m_offset;
  double m_dutyCycle;
  uint32_t m_mibInterval;
  uint32_t m_maxInterMibTime;
  uint32_t m_ldsInterval;
  bool m_startsWithOnPeriod;
  bool m_sendCtsToSelf;
  Time m_lteUCoexistenceManagerInstallTime;

};


void
LteTestCtrlMessageCheckCallback (LteUCsatTestCase *testcase, std::string path, std::list<Ptr<LteControlMessage> > list )
{
  testcase->CtrlMessageCheck (list);
}

void
LteTestMacDlSchedulingCheckCallback (LteUCsatTestCase *testcase, std::string path, uint32_t frameNo, uint32_t subframeNo, uint16_t rnti,uint8_t mcsTb1, uint16_t sizeTb1, uint8_t mcsTb2, uint16_t sizeTb2 )
{
  testcase->DlSchedulingCheck (frameNo, subframeNo, rnti, mcsTb1, sizeTb1, mcsTb2, sizeTb2);
}


/**
 * TestSuite
 */

LteUCsatTestSuite::LteUCsatTestSuite ()
  : TestSuite ("csat-test", SYSTEM)
{

  // test different comb of puncturing and LDS interval for puncturing duration of 1
  AddTestCase (new LteUCsatTestCase ( 5, 1, 320, 0,  0.5, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 5, 1, 320, 0,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 1, 320, 0,  0.5, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 1, 320, 0,  0.5, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 1, 320, 0,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 1, 320, 0,  0.5, 10, 10, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (15, 1, 320, 0,  0.5, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 1, 320, 0,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 1, 320, 0,  0.5, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 1, 320, 0,  0.5, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 1, 320, 0,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 1, 320, 0,  0.5, 10, 10, 160, true, false), TestCase::QUICK);

  // test different comb of puncturing, and LDS interval, for puncturing duration of 2
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 0,  0.5, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 0,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 0,  0.5, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 0,  0.5, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 0,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 0,  0.5, 10, 10, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 0,  0.5, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 0,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 0,  0.5, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 0,  0.5, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 0,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 0,  0.5, 10, 10, 160, true, false), TestCase::QUICK);

  // test different comb of puncturing and LDS interval for duty cycle = 0.2
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 0,  0.2, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 0,  0.2, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 0,  0.2, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 0,  0.2, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 0,  0.2, 10, 10,  80, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 0,  0.2, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 0,  0.2, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 0,  0.2, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 0,  0.2, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 0,  0.2, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 0,  0.2, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 0,  0.2, 10, 10, 160, true, false), TestCase::QUICK);

  //test different comb of puncturing, LDS interval and duty cycle = 0.8
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 0,  0.8, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 0,  0.8, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 0,  0.8, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 0,  0.8, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 0,  0.8, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 0,  0.8, 10, 10, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 0,  0.8, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 0,  0.8, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 0,  0.8, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 0,  0.8, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 0,  0.8, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 0,  0.8, 10, 10, 160, true, false), TestCase::QUICK);

  // test different comb of puncturing, LDS interval and duty cycle = 0.5
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 100,  0.5, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 100,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 2, 320, 100,  0.5, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 100,  0.5, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 100,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 2, 320, 100,  0.5, 10, 10, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 100,  0.5, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 100,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 2, 320, 100,  0.5, 10, 10, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 100,  0.5, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 100,  0.5, 10, 10,  80, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 2, 320, 100,  0.5, 10, 10, 160, true, false), TestCase::QUICK);

  // test for different puncturing values and duty cycle = 0.9 and puncturing duration = 1
  AddTestCase (new LteUCsatTestCase ( 1,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 2,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 3,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 4,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 6,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 7,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 8,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 9,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (10,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (11,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (12,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (13,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (14,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (16,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (17,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (18,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (19,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20,  1, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);

  // test for different puncturing values and duty cycle = 0.9, and puncturing duration = 2
  AddTestCase (new LteUCsatTestCase ( 1,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 2,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 3,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 4,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 6,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 7,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 8,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 9,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (10,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (11,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (12,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (13,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (14,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (16,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (17,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (18,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (19,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20,  2, 320, 0,  0.9, 10, 10,  40, true, false), TestCase::QUICK);

  // test for different puncturing values and maximum inter-MIB time = 160, and LDS interval = 160
  AddTestCase (new LteUCsatTestCase ( 1,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 2,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 3,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 4,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 6,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 7,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 8,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 9,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (10,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (11,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (12,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (13,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (14,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (16,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (17,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (18,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (19,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20,  2, 320, 0,  0.9, 10, 160,  160, true, false), TestCase::QUICK);

  // test for different puncturing values and maximum inter-MIB time = 80, and LDS interval = 160
  AddTestCase (new LteUCsatTestCase ( 1,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 2,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 3,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 4,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 6,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 7,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 8,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 9,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (10,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (11,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (12,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (13,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (14,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (16,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (17,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (18,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (19,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::QUICK);

  // test for different puncturing values and maximum inter-MIB time = 40, and LDS interval = 160
  AddTestCase (new LteUCsatTestCase ( 1,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 2,  2, 320, 0,  0.9, 10, 80,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 3,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 4,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 6,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 7,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 8,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 9,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (10,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (11,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (12,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (13,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (14,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (16,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (17,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (18,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (19,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20,  2, 320, 0,  0.9, 10, 40,  160, true, false), TestCase::QUICK);

  // test for different durations of csat cycle and puncturing
  AddTestCase (new LteUCsatTestCase ( 5, 1,   40, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 5, 1,   80, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 1,  120, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 1,  360, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 1,  480, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase ( 5, 1,  520, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 1,  760, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase ( 5, 1, 1280, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (10, 1,   40, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 1,   80, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 1,  120, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (10, 1,  360, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 1,  480, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 1,  520, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (10, 1,  760, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (10, 1, 1280, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (15, 1,   40, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 1,   80, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 1,  120, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 1,  360, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 1,  480, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 1,  520, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (15, 1,  760, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (15, 1, 1280, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (20, 1,   40, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 1,   80, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 1,  120, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (20, 1,  360, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 1,  480, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 1,  520, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);
  AddTestCase (new LteUCsatTestCase (20, 1,  760, 0,  0.5, 10, 160, 160, true, false), TestCase::EXTENSIVE);
  AddTestCase (new LteUCsatTestCase (20, 1, 1280, 0,  0.5, 10, 160, 160, true, false), TestCase::QUICK);

}


static LteUCsatTestSuite LteUCsatTestSuite;


/**
 * TestCase
 */
LteUCsatTestCase::LteUCsatTestCase (uint32_t puncturing, uint32_t puncturingDuration, uint32_t csatCycleDuration, uint32_t offset, double dutyCycle, uint32_t mibInterval, uint32_t maxInterMibTime, uint32_t ldsInterval,
                                    bool startWithOnPeriod, bool sendCtsToSelf)
  :TestCase (BuildNameString (puncturing, puncturingDuration, csatCycleDuration, offset, dutyCycle, mibInterval, maxInterMibTime, ldsInterval, startWithOnPeriod, sendCtsToSelf)),
   m_puncturing(puncturing), m_puncturingDuration (puncturingDuration), m_csatCycleDuration (csatCycleDuration), m_offset(offset), m_dutyCycle (dutyCycle), m_mibInterval (mibInterval),
   m_maxInterMibTime (maxInterMibTime), m_ldsInterval (ldsInterval), m_startsWithOnPeriod (startWithOnPeriod), m_sendCtsToSelf (sendCtsToSelf)
{
  m_lastMibSent = Seconds(0);
  m_lastLdsSent = Seconds(0);
  m_lastCtrlMessageTransmitted = Seconds(0);
  m_lastMacDlScheduling = Seconds(0);
  m_consecutiveDlPhy = 0;
  m_consecutiveDlMac = 0;
  m_startTime = Seconds(0);
  m_stopTime = Seconds(1);
  m_lteUCoexistenceManagerInstallTime = Seconds(0.005);

}

LteUCsatTestCase::~LteUCsatTestCase ()
{
}

/**
 * TestCase
 */

std::string
LteUCsatTestCase::BuildNameString (uint32_t puncturing, uint32_t puncturingDuration, uint32_t m_csatCycleDuration, uint32_t offset, double dutyCycle, uint32_t mibInterval,  uint32_t maxInterMibTime,
                                   uint32_t ldsInterval, bool startWithOnPeriod, bool sendCtsToSelf)
{
  std::ostringstream oss;
  oss << "puncturing="<<puncturing<< ", puncturingDuration="<<puncturingDuration<<", m_csatCycleDuration="<<m_csatCycleDuration<<",  offset="<<offset <<", dutyCycle="<<dutyCycle<<",  mibInterval="<<mibInterval <<",  maxInterMibTime="<<maxInterMibTime <<
      ", ldsInterval="<<ldsInterval<<", startWithOnPeriod="<<startWithOnPeriod<<", sendCtsToSelf="<<sendCtsToSelf;
  return oss.str ();
}

void
LteUCsatTestCase::DoRun (void)
{
  NS_LOG_INFO (this << GetName ());

  NodeContainer eNbNode, ueNode, clientNode, allNodes;//, monitorNode;
  eNbNode.Create(1);
  ueNode.Create(1);
  //monitorNode.Create(1);
  clientNode.Create (1);
  allNodes = NodeContainer (eNbNode, ueNode);//, monitorNode);

  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0));   // eNb
  positionAlloc->Add (Vector (0.0, 10, 0.0));  // ue
  positionAlloc->Add (Vector (10, 0.0, 0.0));  // LteU monitor

  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetPositionAllocator (positionAlloc);
  mobility.Install (allNodes);

  PhyParams phyParams;
  phyParams.m_bsTxGain = 5; // dB antenna gain
  phyParams.m_bsRxGain = 5; // dB antenna gain
  phyParams.m_bsTxPower = 18; // dBm
  phyParams.m_bsNoiseFigure = 5; // dB
  phyParams.m_ueTxGain = 0; // dB antenna gain
  phyParams.m_ueRxGain = 0; // dB antenna gain
  phyParams.m_ueTxPower = 18; // dBm
  phyParams.m_ueNoiseFigure = 9; // dB
  phyParams.m_channelNumber = 36;
  phyParams.m_receivers = 1;
  phyParams.m_transmitters = 1;

  // we want deterministic behavior in this simple scenario, so we disable shadowing
  Config::SetDefault ("ns3::Ieee80211axIndoorPropagationLossModel::Sigma", DoubleValue (0));
  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (phyParams.m_bsTxPower));
  Config::SetDefault ("ns3::LteUePhy::TxPower", DoubleValue (phyParams.m_ueTxPower));
  Config::SetDefault ("ns3::LteEnbRrc::EpsBearerToRlcMapping", EnumValue (LteEnbRrc::RLC_UM_ALWAYS));
  //Disable Uplink Power Control
  Config::SetDefault ("ns3::LteUePhy::EnableUplinkPowerControl", BooleanValue (false));

  // configure LteUCoexistenceManager
  Config::SetDefault ("ns3::LteUCoexistenceManager::Puncturing", UintegerValue (m_puncturing));
  Config::SetDefault ("ns3::LteUCoexistenceManager::PuncturingDuration", UintegerValue (m_puncturingDuration));
  Config::SetDefault ("ns3::LteUCoexistenceManager::CsatDutyCycle", DoubleValue (m_dutyCycle));
  Config::SetDefault ("ns3::LteUCoexistenceManager::MaxInterMibTime", UintegerValue (m_maxInterMibTime));
  Config::SetDefault ("ns3::LteUCoexistenceManager::Offset", UintegerValue (m_offset));
  Config::SetDefault ("ns3::LteUCoexistenceManager::CsatCycleDuration", UintegerValue (m_csatCycleDuration));
  Config::SetDefault ("ns3::LteUCoexistenceManager::StartWithOnPeriod", BooleanValue (m_startsWithOnPeriod));
  Config::SetDefault ("ns3::LteUCoexistenceManager::SendCtsToSelf", BooleanValue (m_sendCtsToSelf));

  // configure LteEnbPhy parameters
  Config::SetDefault ("ns3::LteEnbPhy::MibPeriod", UintegerValue (m_mibInterval));
  Config::SetDefault ("ns3::LteEnbPhy::LdsPeriod", UintegerValue (m_ldsInterval));

  NetDeviceContainer eNbDevice;
  NetDeviceContainer ueDevice;

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));
  lteHelper->SetAttribute ("UseIdealRrc", BooleanValue (true));
  lteHelper->SetAttribute ("UsePdschForCqiGeneration", BooleanValue (true));

  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);
  lteHelper->Initialize ();

  // LTE requires some Internet stack configuration prior to device installation
  // Wifi configures it after device installation
  InternetStackHelper internetStackHelper;
  // Install an internet stack to all the nodes with IP
  internetStackHelper.Install (ueNode);
  internetStackHelper.Install (clientNode);

  lteHelper->SetEnbDeviceAttribute ("CsgIndication", BooleanValue (true));
  lteHelper->SetEnbDeviceAttribute ("CsgId", UintegerValue (1));
  lteHelper->SetUeDeviceAttribute ("CsgId", UintegerValue (1));
  Ipv4AddressHelper internetIpv4Helper;
  internetIpv4Helper.SetBase ("1.0.0.0", "255.0.0.0");

  // configure client node
  Ptr<Node> pgw = epcHelper->GetPgwNode ();
  PointToPointHelper p2pHelper;
  p2pHelper.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2pHelper.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2pHelper.SetChannelAttribute ("Delay", TimeValue (Seconds (0.0)));
  NetDeviceContainer clientDevice = p2pHelper.Install (pgw, clientNode.Get(0));
  Ipv4InterfaceContainer internetIpIfaces = internetIpv4Helper.Assign (clientDevice);
  // interface 0 is localhost, 1 is the p2p device
  //Ipv4Address clientNodeAddr = internetIpIfaces.GetAddress (1);
  // make LTE and network reachable from the client node
  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> clientNodeStaticRouting = ipv4RoutingHelper.GetStaticRouting (clientNode.Get(0)->GetObject<Ipv4> ());
  clientNodeStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  // LTE-U DL transmission @5180 MHz
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (255444));
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (100));
  // needed for initial cell search
  lteHelper->SetUeDeviceAttribute ("DlEarfcn", UintegerValue (255444));
  // LTE calibration
  lteHelper->SetEnbAntennaModelType ("ns3::IsotropicAntennaModel");
  lteHelper->SetEnbAntennaModelAttribute ("Gain",   DoubleValue (phyParams.m_bsTxGain));

  // Create Devices and install them in the Nodes (eNBs and UEs)
  eNbDevice = lteHelper->InstallEnbDevice (eNbNode);
  ueDevice = lteHelper->InstallUeDevice (ueNode);
  Ptr<LteUWifiCoexistenceHelper> lteUWifiCoexistenceHelper = CreateObject<LteUWifiCoexistenceHelper> ();
  //lteUWifiCoexistenceHelper->SetMonitorMode(LteUWifiCoexistenceHelper::ENERGY_DETECTION_ONLY);
  lteUWifiCoexistenceHelper->SetMonitorMode(LteUWifiCoexistenceHelper::REGULAR_WIFI_MONITOR);

  // configure LteU node
  Simulator::Schedule (m_lteUCoexistenceManagerInstallTime, &LteUWifiCoexistenceHelper::ConfigureLteUSingleWifiChannel, lteUWifiCoexistenceHelper, eNbDevice, phyParams);
  //lteUWifiCoexistenceHelper->ConfigureLteUSingleWifiChannel(eNbDevice, phyParams);


  // configure monitor node
  // get LTE channel
  Ptr<LteEnbNetDevice> lteEnbNetDevice = eNbDevice.Get(0)->GetObject<LteEnbNetDevice> ();
  Ptr<SpectrumChannel> downlinkSpectrumChannel = lteEnbNetDevice->GetPhy ()->GetDownlinkSpectrumPhy ()->GetChannel ();
  SpectrumWifiPhyHelper spectrumPhy = SpectrumWifiPhyHelper::Default ();
  uint32_t channelNumber = 36;
  spectrumPhy.SetChannelNumber (channelNumber);
  spectrumPhy.SetChannel (downlinkSpectrumChannel);
  WifiHelper wifi;
  wifi.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
  HtWifiMacHelper mac = HtWifiMacHelper::Default ();
  spectrumPhy.Set ("ShortGuardEnabled", BooleanValue (false));
  spectrumPhy.Set ("TxGain", DoubleValue (phyParams.m_ueTxGain));
  spectrumPhy.Set ("RxGain", DoubleValue (phyParams.m_ueRxGain));
  spectrumPhy.Set ("TxPowerStart", DoubleValue (phyParams.m_ueTxPower));
  spectrumPhy.Set ("TxPowerEnd", DoubleValue (phyParams.m_ueTxPower));
  spectrumPhy.Set ("RxNoiseFigure", DoubleValue (phyParams.m_ueNoiseFigure));
  spectrumPhy.Set ("Receivers", UintegerValue (2));
  spectrumPhy.Set ("Transmitters", UintegerValue (2));
  wifi.SetRemoteStationManager ("ns3::IdealWifiManager");
  //which implements a Wi-Fi MAC that does not perform any kind of beacon generation, probing, or association
  mac.SetType ("ns3::AdhocWifiMac");
  // wifi device that is doing monitoring
  //Ptr<NetDevice> monitorDevice = (wifi.Install (spectrumPhy, mac, monitorNode.Get(0))).Get (0);

  // configure UE device
  epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueDevice));
  // set the default gateway for the UE
  Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ueNode.Get(0)->GetObject<Ipv4> ());
  ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
  lteHelper->Attach (ueDevice);

  lteHelper->EnableMacTraces ();

  // configure application at UE
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("17.0.0.0", "255.255.0.0");
  ipv4.Assign (eNbDevice);
  Ipv4InterfaceContainer ipUe = ipv4.Assign (ueDevice);

  ApplicationContainer serverApps, clientApps;
  serverApps.Add (ConfigureUdpServers (ueNode, m_startTime, m_stopTime));
  clientApps.Add (ConfigureUdpClients (clientNode, ipUe, m_startTime, m_stopTime, MicroSeconds(102)));

  Config::Connect ("/NodeList/0/DeviceList/0/LteEnbPhy/CtrlMsgTransmission",
                   MakeBoundCallback (&LteTestCtrlMessageCheckCallback, this));

  Config::Connect ("/NodeList/0/DeviceList/0/LteEnbMac/DlScheduling",
                   MakeBoundCallback (&LteTestMacDlSchedulingCheckCallback, this));

 // LogComponentEnable("LteUCoexistenceManager", LOG_LEVEL_DEBUG);
 // LogComponentEnable("LteUCoexistenceManager", LOG_LEVEL_WARN);

  Simulator::Stop (m_stopTime);
  Simulator::Run ();
  Simulator::Destroy ();
}


void
LteUCsatTestCase::CtrlMessageCheck (std::list<Ptr<LteControlMessage> > ctrlMsg)
{
  NS_LOG_INFO(this<<"Ctrl message at:"<<Now().GetMicroSeconds()<<", count:"<<ctrlMsg.size());

  if (Simulator::Now() >= m_lteUCoexistenceManagerInstallTime + MilliSeconds(10))
    {
      // check MIB and LDS intervals
      if (ctrlMsg.size () > 0)
        {
          std::list<Ptr<LteControlMessage> >::iterator it;
          it = ctrlMsg.begin ();
          while (it != ctrlMsg.end ())
            {
              Ptr<LteControlMessage> msg = (*it);
              if (msg->GetMessageType() == LteControlMessage::MIB)
                {
                  if (m_lastMibSent!=Seconds(0))
                    {
                      NS_TEST_ASSERT_MSG_LT_OR_EQ(Simulator::Now().GetMilliSeconds()- m_lastMibSent.GetMilliSeconds(), m_maxInterMibTime, "Mib was not sent for :"<<Simulator::Now()- m_lastMibSent);
                    }
                  m_lastMibSent = Simulator::Now();
                }
              else if (msg->GetMessageType() == LteControlMessage::LDS)
                {
                  if (m_lastLdsSent!=Seconds(0))
                    {
                      NS_TEST_ASSERT_MSG_LT_OR_EQ(Simulator::Now().GetMilliSeconds()- m_lastLdsSent.GetMilliSeconds(), m_ldsInterval, "Lds was not sent for :"<<Simulator::Now()- m_lastLdsSent);
                    }
                  m_lastLdsSent = Simulator::Now();
                }
              it++;
            }
        }

      if ((Simulator::Now() - m_lastCtrlMessageTransmitted).GetMilliSeconds() == 1)
        {
          m_consecutiveDlPhy++;
        }
      else
        {
          m_consecutiveDlPhy = 0;
        }

      m_lastCtrlMessageTransmitted = Simulator::Now();
      NS_TEST_ASSERT_MSG_LT_OR_EQ(m_consecutiveDlPhy, m_puncturing, "Wrong puncturing:"<<m_consecutiveDlPhy);
    }
}


void
LteUCsatTestCase::DlSchedulingCheck (uint32_t frameNo, uint32_t subframeNo, uint16_t rnti,uint8_t mcsTb1, uint16_t sizeTb1, uint8_t mcsTb2, uint16_t sizeTb2)
{
  NS_LOG_FUNCTION (frameNo << subframeNo << rnti << (uint32_t) mcsTb1 << sizeTb1 << (uint32_t) mcsTb2 << sizeTb2);

  // need to allow for RRC connection establishment + CQI feedback reception + persistent data transmission
  // consider that the ABS pattern might make attachment & connection establishment longer
  if (Simulator::Now () > m_lteUCoexistenceManagerInstallTime)
    {
      if ((Simulator::Now() - m_lastMacDlScheduling).GetMilliSeconds() == 1)
        {
          m_consecutiveDlMac ++;
        }
      else
        {
          m_consecutiveDlMac = 0;
        }

      m_lastMacDlScheduling = Simulator::Now();
      NS_TEST_ASSERT_MSG_LT_OR_EQ(m_consecutiveDlMac, m_puncturing, "Wrong puncturing detected in MAC:"<<m_consecutiveDlMac);
    }
}

