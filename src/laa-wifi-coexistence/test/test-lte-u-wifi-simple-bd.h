/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es>
 *
 */

#ifndef TEST_LTE_U_WIFI_SIMPLE_BD_H
#define TEST_LTE_U_WIFI_SIMPLE_BD_H

#include "ns3/test.h"

using namespace ns3;


/**
 * Test that beacon detection works properly. Test scenario topology consists of LTE-U node and various number of wifi APs. It is tested if LTE-U node can count APs based on
 * beacon detection.
 */

class LteUWifiSimpleBdTestSuite : public TestSuite
{
public:
  LteUWifiSimpleBdTestSuite ();
};


class LteUWifiSimpleBdTestCase : public TestCase
{
public:
  LteUWifiSimpleBdTestCase (std::string, uint32_t numberOfAps);
  virtual ~LteUWifiSimpleBdTestCase ();
  void BeaconDetected (Mac48Address address);
  void NavDuration(Time duration);

private:
  virtual void DoRun (void);
  uint32_t m_numberOfInstalledAPs;
  std::set<Mac48Address> m_detectedBSSIDs;
  Time m_totalNavDuration;

};

#endif /* TEST_LTE_U_WIFI_SIMPLE_BD_H */
