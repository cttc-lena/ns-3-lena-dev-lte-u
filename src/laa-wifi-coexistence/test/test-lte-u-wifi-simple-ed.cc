/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011-2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es>
 *
 */

#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/internet-module.h>
#include <ns3/point-to-point-module.h>
#include <ns3/lte-module.h>
#include <ns3/wifi-module.h>
#include <ns3/config-store-module.h>
#include <ns3/spectrum-module.h>
#include <ns3/applications-module.h>
#include <ns3/flow-monitor-module.h>
#include <ns3/propagation-module.h>
#include <ns3/scenario-helper.h>
#include <ns3/lte-u-wifi-coexistence-helper.h>
#include "ns3/traced-value.h"
#include "ns3/trace-source-accessor.h"
#include "test-lte-u-wifi-simple-ed.h"



using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("LteUWifiSimpleEdTest");

// Calculate RxPower according to the propagation model being used, Ieee80211axIndoorPropagationLossModel
double CalcRxPower (double txPowerDbm, double distance)
{
  double frequency = 5e9; // 5GHz
  double d = std::max (distance, 1.0);
  double lossDb =  40.05 + 20*std::log10 (frequency/(2.4*1e9)) + 20*std::log10(std::min(d, 10.0)) + ((d>10) ? 35*std::log10(d/10) : 0);
  return txPowerDbm - lossDb - 0; // there is no shadowing in this test
}

/**
 * TestSuite
 */

LteUWifiSimpleEdTestSuite::LteUWifiSimpleEdTestSuite ()
: TestSuite ("lte-u-wifi-simple-ed", SYSTEM)
{

  double txPowerDbm = 18;
  double txGainDb = 0;
  double rxGainDb = 0;

  // distance 10m
  double distance = 10.000000;
  double caclulatedRxPower = CalcRxPower (txPowerDbm, distance ) + txGainDb + rxGainDb;
  // set ed threshold little bit lower and little bit higher, when lower it the signals should be detected, when higher it shall not be detected
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower - 2.0,  true), TestCase::QUICK);
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower + 2.0, false), TestCase::QUICK);

  //distance 20m
  distance = 20.0000;
  caclulatedRxPower = CalcRxPower (txPowerDbm, distance ) + txGainDb + rxGainDb;
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower - 2.0,  true), TestCase::QUICK);
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower + 2.0, false), TestCase::QUICK);

  //distance 50m
  distance = 50.0000;
  caclulatedRxPower = CalcRxPower (txPowerDbm, distance ) + txGainDb + rxGainDb;
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower - 2.0,  true), TestCase::QUICK);
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower + 2.0, false), TestCase::QUICK);

  //distance 100m
  distance = 100.0000;
  caclulatedRxPower = CalcRxPower (txPowerDbm, distance ) + txGainDb + rxGainDb;
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower - 2.0,  true), TestCase::QUICK);
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower + 2.0, false), TestCase::QUICK);

  //distance 200m
  distance = 200.0000;
  caclulatedRxPower = CalcRxPower (txPowerDbm, distance ) + txGainDb + rxGainDb;
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower - 2.0,  true), TestCase::QUICK);
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower + 2.0, false), TestCase::QUICK);

  //distance 400m
  distance = 400.0000;
  caclulatedRxPower = CalcRxPower (txPowerDbm, distance ) + txGainDb + rxGainDb;
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower - 2.0,  true), TestCase::QUICK);
  AddTestCase (new LteUWifiSimpleEdTestCase ( distance, caclulatedRxPower, caclulatedRxPower + 2.0, false), TestCase::QUICK);

}

static LteUWifiSimpleEdTestSuite lteUWifiSimpleEdTestSuite;


/**
 * TestCase
 */

std::string
LteUWifiSimpleEdTestCase::BuildNameString (double distance, double rxPowerForDistance, double edThreshold, bool energyDetected)
{
  std::ostringstream oss;
  oss << "distance="<<distance<<", rxPower="<<rxPowerForDistance<<",  ed="<<edThreshold << (energyDetected ? ", energy detected" : ", not detected");
  return oss.str ();
}


LteUWifiSimpleEdTestCase::LteUWifiSimpleEdTestCase (double distance, double rxPowerForDistance, double edThreshold, bool energyDetected)
: TestCase (BuildNameString(distance, rxPowerForDistance, edThreshold, energyDetected)),
  m_d (distance),
  m_edThreshold (edThreshold),
  m_energyShouldBeDetected (energyDetected)
{
  m_beaconsDetectedCount = 0;
}

LteUWifiSimpleEdTestCase::~LteUWifiSimpleEdTestCase ()
{
}

void
LteUWifiSimpleEdTestCase::EnergyDetected(double duration)
{
  m_beaconsDetectedCount ++;
}


void
LteUWifiSimpleEdTestCase::DoRun (void)
{

  Time beaconInterval = MilliSeconds(10);
  Time duration = Seconds(0.100);
  double expectedBeacons = duration/beaconInterval;

  // Create nodes and containers
  NodeContainer lteUNode, wifiApNode;  // for APs and eNBs
  NodeContainer allWirelessNodes;  // container to hold all wireless nodes
  // Each network A and B gets one type of node each
  lteUNode.Create (1);
  wifiApNode.Create (1);
  allWirelessNodes = NodeContainer (lteUNode, wifiApNode);

  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0));   // eNodeB
  positionAlloc->Add (Vector (m_d, 0.0, 0.0)); // wifi AP
  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetPositionAllocator (positionAlloc);
  mobility.Install (allWirelessNodes);

  PhyParams phyParams;
  phyParams.m_ueTxGain = 0;
  phyParams.m_ueRxGain = 0;
  phyParams.m_ueTxPower = 18;
  phyParams.m_ueNoiseFigure = 9;
  phyParams.m_receivers = 2;
  phyParams.m_transmitters = 2;
  phyParams.m_channelNumber = 36;

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Config::SetDefault("ns3::SpectrumWifiPhy::TxGain", DoubleValue(phyParams.m_ueTxGain));
  Config::SetDefault("ns3::SpectrumWifiPhy::RxGain", DoubleValue(phyParams.m_ueRxGain));
  Config::SetDefault("ns3::SpectrumWifiPhy::TxPowerStart", DoubleValue (phyParams.m_ueTxPower));
  Config::SetDefault("ns3::SpectrumWifiPhy::TxPowerEnd", DoubleValue (phyParams.m_ueTxPower));
  Config::SetDefault("ns3::SpectrumWifiPhy::RxNoiseFigure",DoubleValue(phyParams.m_ueNoiseFigure));
  Config::SetDefault("ns3::SpectrumWifiPhy::Receivers", UintegerValue (phyParams.m_receivers));
  Config::SetDefault("ns3::SpectrumWifiPhy::Transmitters", UintegerValue (phyParams.m_transmitters));

  // we want deterministic behavior in this simple scenario, so we disable shadowing
  Config::SetDefault ("ns3::Ieee80211axIndoorPropagationLossModel::Sigma", DoubleValue (0));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::Ieee80211axIndoorPropagationLossModel"));
  // call in order to initialize propagation loss model
  lteHelper->Initialize ();
  // LTE-U DL transmission @5180 MHz
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (255444));
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (100));
  NetDeviceContainer lteUNetDevice = lteHelper->InstallEnbDevice (lteUNode);
  // disable any transmission from LTE device, we want to have only wifi AP transmitting beacons
  Config::SetDefault ("ns3::ChannelAccessManager::GrantDuration", TimeValue(Seconds(0)));
  // instantiate lte-u coexistence helper
  Ptr<LteUWifiCoexistenceHelper> lteUWifiCoexistenceHelper = Create<LteUWifiCoexistenceHelper> ();
  lteUWifiCoexistenceHelper->SetMonitorMode(LteUWifiCoexistenceHelper::ENERGY_DETECTION_ONLY);
  lteUWifiCoexistenceHelper->SetEdThreshold(m_edThreshold);
  lteUWifiCoexistenceHelper->SetSilent();
  lteUWifiCoexistenceHelper->ConfigureLteUSingleWifiChannel(lteUNetDevice, phyParams);
  // connect to trace source of lte-u coexistence manager
  Ptr<LteUCoexistenceManager> lteManager = DynamicCast<LteUCoexistenceManager >(lteUNetDevice.Get(0)->GetObject<LteEnbNetDevice> ()->GetPhy ()->GetChannelAccessManager());
  lteManager->TraceConnectWithoutContext("RxDuration",MakeCallback(&LteUWifiSimpleEdTestCase::EnergyDetected, this));
  // spectrum channel to attach wifi AP
  Ptr<SpectrumChannel> spectrumChannel = lteHelper->GetDownlinkSpectrumChannel ();
  // set wifi AP to the same spectrum channel
  SpectrumWifiPhyHelper spectrumPhy = SpectrumWifiPhyHelper::Default ();
  spectrumPhy.SetChannel (spectrumChannel);
  WifiHelper wifi;
  // this setting will be used during WifiHeler::Install to set frequency of SpectrumWifiPhy
  wifi.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
  wifi.SetRemoteStationManager ("ns3::IdealWifiManager");
  // configure beacons generation, disable jitter
  Config::SetDefault ("ns3::ApWifiMac::BeaconInterval", TimeValue(beaconInterval));
  HtWifiMacHelper mac = HtWifiMacHelper::Default ();
  mac.SetType ("ns3::ApWifiMac",
               "Ssid", SsidValue (Ssid ("ns380211n-B")),
        	   "EnableBeaconJitter", BooleanValue (false));
  uint32_t channelNumber = 36;
  spectrumPhy.SetChannelNumber (channelNumber);
  wifi.Install (spectrumPhy, mac, wifiApNode.Get (0));

  Simulator::Stop (duration);
  Simulator::Run ();

  // perform test
  if (m_energyShouldBeDetected)
    {
      NS_TEST_ASSERT_MSG_EQ(m_beaconsDetectedCount, expectedBeacons, "Not received signals of all beacons.");
    }
  else
    {
      NS_TEST_ASSERT_MSG_EQ(m_beaconsDetectedCount, 0,"Received signals of all beacons when nothing should be received");
    }

   Simulator::Destroy ();
}
