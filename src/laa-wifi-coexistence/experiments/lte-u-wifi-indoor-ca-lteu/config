#!/bin/bash
#
# Copyright (c) 2016 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Authors: Biljana Bojovic <bbojovic@cttc.es>
#

# main simulation parameters
base_duration=120
duration=10
ftpLambdas="1.5 2.5"
udpRates="1Mbps 10Mbps"
#ftp file size in bytes, the default is 512000
ftpFileSizeList="512000 5120000"

# Secondary carrier configuration. Default value is "RrSdlCcm" - use secondary carrier opportunistically,
# the secondary cells are deactivated when signal from the primary is bellow a certain threshold.  
# Retransmissions, signaling, and GBR traffic goe only over primary; uplink goes over primary.
# Currently, the other option for CCM is "RrCcm" - which splits 50/50 traffic in uplink and downlink 
# among available carrriers (primary and secondaries)
ccmImpl="RrSdlCcm"
# Number of secondary carriers
numberOfSecondaryCarriersList="0 1 2"
# Implementation choice of retransmission over primary requiremenent. Implementation options are OptionA, OptionB and OptionC. 
# Desciption of different options:
# OptionA - CCM forwards BSR only to primary if RLC retransmission queue is not empty
# OptionB - CCM forwards BSR to primary and secondary (retransmission only to primary, and transmission
#              divides among primary and secondary, intercept transmission opportunities, i.e. if coming
#              from secondary forward only if RLC retransmission queue is empty
# OptionC - CCM forward BSR to primary and secondary as in Option B, but do not intercept transmission
#              opportunities, lets RLC decide how to use transmission opportunity coming from the carrier
retxOverPrimaryImpl=OptionC

#csat parameters
csatCycleDuration=160
puncturing=20
puncturingDuration=1
maxInterMibTime=160
tOnMinInMilliSec=120
minOffTime=20
alphaMU=0.8
deltaTUp=0.05
deltaTDown=0.05
csatDutyCycle=0.5
csatDutyCycleList="0.1 0.5 0.9"  # used when csat is disabled

# whether to manually assign duty cycle when configuring LTE-U
assignDutyCycle=0

#csat adaptation
set -a muThresholdsLow
set -a muThresholdsHigh
muThresholdsLow=(0.1)
muThresholdsHigh=(0.3)

#lte-u edThreshold
lteUEdThreshold=-62

#wifi parameters
wifiStandard=80211nFull
wifiQueueMaxSize=2000
wifiEdThreshold=-62

#lte parameters
ldsPeriod=160
# LTE default transmission mode
laaMimo=2
# PDCCH duration in number of symbols
pdcchDuration=2

# logging
logBeaconArrivals=1
logBeaconNodeId=1
logPhyNodeId=4
logWifiRetries=1
logWifiFailRetries=1
logPhyArrivals=1
logBackoffChanges=1
logTxops=1
logCsatDutyCycle=1
logBackoffChanges=1

# change TCP segment size from default of 536
tcpSegSize=1440
# TCP initial congestion window defaults to 1; change it here
tcpInitialCw=10

#other
RngRun=1
OPERATORS="A B"

# specific plotting parameters
LATENCY_CDF_RANGE="[0:1500][0:1]"
THROUGHPUT_CDF_RANGE="[0:50][0:1]"
