#!/bin/bash

#
# Copyright (c) 2016 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author: Biljana Bojovic <bbojovic@cttc.es>
#

source config

control_c()
{
  echo "exiting"
  exit $?
}

trap control_c SIGINT
set -x
set -o errexit

outputDir=`pwd`/results_ftp
mkdir -p "${outputDir}"
rm -f "${outputDir}"/laa_wifi_indoor_ca_ftp_*_operator?
rm -f "${outputDir}"/time_stats_ca_ftp
rm -f "${outputDir}"/laa_wifi_indoor_ca_ftp_*_log

# need this as otherwise waf won't find the executables
cd ../../../../

# This script will run simulation campaigns for 3 different network topologies of laa-wifi-indoor scenario. The type of transport protocol is UDP and traffic is CBR.
transport=FtpM1

# lteu over wifi
for ftpFileSize in ${ftpFileSizeList} ; do
  for ftpLambda in ${ftpLambdas} ; do
    for numberOfSecondaryCarriers in ${numberOfSecondaryCarriersList} ; do
        duration=$(echo "$base_duration/$ftpLambda" | bc)
        simTag="ca_ftp_${transport}_${numberOfSecondaryCarriers}_${ftpLambda}_${ftpFileSize}_lteu_wifi"
        /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats_ca_udp -a \
        ./waf --run laa-wifi-indoor --command="%s --cellConfigA=Lte_U --cellConfigB=Wifi --csatEnabled=1 --simTag=${simTag} --ftpFileSize=${ftpFileSize} --transport=${transport} --outputDir=${outputDir} --wifiStandard=${wifiStandard} --ftpLambda=${ftpLambda} --logBackoffChanges=${logBackoffChanges} --logPhyArrivals=${logPhyArrivals} --logWifiRetries=${logWifiRetries} --duration=${duration} --logTxops=${logTxops} --logCsatDutyCycle=${logCsatDutyCycle} --wifiQueueMaxSize=${wifiQueueMaxSize} --ns3::LteEnbRrc::DefaultTransmissionMode=${laaMimo} --ns3::TcpSocket::SegmentSize=${tcpSegSize} --ns3::TcpSocket::InitialCwnd=${tcpInitialCw}  --lteUEdThreshold=${lteUEdThreshold} --wifiEdThreshold=${wifiEdThreshold} --ldsPeriod=${ldsPeriod} --pdcchDuration=${pdcchDuration}  --assignDutyCycle=${assignDutyCycle} --numberOfSecondaryCarriers=${numberOfSecondaryCarriers} --ccmImpl=${ccmImpl}"
    done
  done
done

# wifi over wifi
for ftpFileSize in ${ftpFileSizeList} ; do
  for ftpLambda in ${ftpLambdas} ; do
      duration=$(echo "$base_duration/$ftpLambda" | bc)
        simTag="ca_ftp_${transport}_${ftpLambda}_${ftpFileSize}_wifi_wifi"
        /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats_ca_udp -a \
        ./waf --run laa-wifi-indoor --command="%s --cellConfigA=Wifi --cellConfigB=Wifi --simTag=${simTag} --ftpFileSize=${ftpFileSize} --transport=${transport} --outputDir=${outputDir} --wifiStandard=${wifiStandard} --ftpLambda=${ftpLambda} --logBackoffChanges=${logBackoffChanges} --logPhyArrivals=${logPhyArrivals} --logWifiRetries=${logWifiRetries} --duration=${duration} --wifiQueueMaxSize=${wifiQueueMaxSize} --ns3::TcpSocket::SegmentSize=${tcpSegSize} --ns3::TcpSocket::InitialCwnd=${tcpInitialCw}  --wifiEdThreshold=${wifiEdThreshold} "   
  done
done

# lte over wifi
for ftpFileSize in ${ftpFileSizeList} ; do
  for ftpLambda in ${ftpLambdas} ; do
    for numberOfSecondaryCarriers in ${numberOfSecondaryCarriersList} ; do
        duration=$(echo "$base_duration/$ftpLambda" | bc)    
        simTag="ca_ftp_${transport}_${numberOfSecondaryCarriers}_${ftpLambda}_${ftpFileSize}_lte_wifi"
        /usr/bin/time -f '%e %U %S %K %M %x %C' -o "${outputDir}"/time_stats_ca_udp -a \
        ./waf --run laa-wifi-indoor --command="%s --cellConfigA=Lte --cellConfigB=Wifi --simTag=${simTag} --ftpFileSize=${ftpFileSize} --transport=${transport} --outputDir=${outputDir} --wifiStandard=${wifiStandard} --ftpLambda=${ftpLambda} --logBackoffChanges=${logBackoffChanges} --logPhyArrivals=${logPhyArrivals} --logWifiRetries=${logWifiRetries} --duration=${duration} --wifiQueueMaxSize=${wifiQueueMaxSize} --ns3::LteEnbRrc::DefaultTransmissionMode=${laaMimo} --ns3::TcpSocket::SegmentSize=${tcpSegSize} --ns3::TcpSocket::InitialCwnd=${tcpInitialCw}  --wifiEdThreshold=${wifiEdThreshold} --ldsPeriod=${ldsPeriod} --pdcchDuration=${pdcchDuration}  --numberOfSecondaryCarriers=${numberOfSecondaryCarriers} --ccmImpl=${ccmImpl}"    
    done
  done
done

