set terminal png
unset surface
set pm3d at s
set palette
set key off
set view 50,50
set xlabel "time (ms)"
set ylabel "freq (MHz)"
set zlabel "PSD (dBW/Hz)" offset 15,0,0
set output "channel-selection.png"
splot "./spectrum-analyzer-36.tr" using ($1*1000.0):($2/1e6):(10*log10($3)) , \
      "./spectrum-analyzer-40.tr" using ($1*1000.0):($2/1e6):(10*log10($3)) , \
      "./spectrum-analyzer-44.tr" using ($1*1000.0):($2/1e6):(10*log10($3)) , \
      "./spectrum-analyzer-48.tr" using ($1*1000.0):($2/1e6):(10*log10($3)) , \
      "./spectrum-analyzer-149.tr" using ($1*1000.0):($2/1e6):(10*log10($3)) , \
      "./spectrum-analyzer-153.tr" using ($1*1000.0):($2/1e6):(10*log10($3)) , \
      "./spectrum-analyzer-157.tr" using ($1*1000.0):($2/1e6):(10*log10($3)) , \
      "./spectrum-analyzer-161.tr" using ($1*1000.0):($2/1e6):(10*log10($3)) , \
      "./spectrum-analyzer-165.tr" using ($1*1000.0):($2/1e6):(10*log10($3))
set term x11
replot
pause -1
