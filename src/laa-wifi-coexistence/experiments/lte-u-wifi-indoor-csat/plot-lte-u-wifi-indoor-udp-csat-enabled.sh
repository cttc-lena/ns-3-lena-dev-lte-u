#!/bin/bash
#
# Copyright (c) 2015 University of Washington
# Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation;
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Authors: Tom Henderson <tomh@tomh.org> and Nicola Baldo <nbaldo@cttc.es> and Biljana Bojovic <bbojovic@cttc.es>
#

source ../utils/shot
source ../utils/common
source config

IMGDIR=images
THUMBNAILS=true
NOX11=true

BASE_OPTIONS=""
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 1 pt 4 lt 1"
BASE_OPTIONS="${BASE_OPTIONS} ; set style line 2 pt 7 lt 2"
BASE_OPTIONS="${BASE_OPTIONS} ; set style increment ;"
BASE_OPTIONS="${BASE_OPTIONS} ; set pointsize 2 ; set grid;"

set -o errexit

##############################################################
#  Latency CDF  TCP
##############################################################

for thrIndex in ${!muThresholdsLow[@]} ; do
for transport in ${transports} ; do
	
        simTag="csat_enabled_${transport}_${udpRate}_${duration}_${muThresholdsLow[${thrIndex}]}_${muThresholdsHigh[${thrIndex}]}"
	imgTag="indoor_"`echo "${simTag}" | tr '.' '_'`

	TRAFFIC=`print_traffic_model ${transport}`    
	TITLE="csat_enabled_${transport}_${udpRate}_MU1_${muThresholdsLow[${thrIndex}]}_MU2_${muThresholdsHigh[${thrIndex}]}"

        for OPERATOR in A B ; do
            LATENCY_COLUMN=9
	    CURRENT=results/laa_wifi_indoor_${simTag}_operator${OPERATOR}
            `../utils/cdf.sh $LATENCY_COLUMN $CURRENT > results/cdf_latency_${simTag}_${OPERATOR}`
        done
	index=0	    
	for OPERATOR in A B ; do
	    FILES[$index]=results/cdf_latency_${simTag}_${OPERATOR}
	    YCOLS[$index]='($2)'    
	    XCOLS[$index]='($1)'
	    LABELS[$index]=`print_operator_laa_wifi $OPERATOR`
	    
	    index=`expr $index + 1`
	done

	PLOTTYPE="with linespoints"
	XLABEL="Latency [ms]"
	YLABEL="CDF"
	RANGE=$LATENCY_CDF_RANGE
	OPTIONS="$BASE_OPTIONS ; set key bottom right"
	IMGFILENAME="${imgTag}_latency"
	plot

	unset FILES
	unset LABELS
	unset YCOLS
	unset XCOLS

done
done

##############################################################
#  Throughput CDF TCP
##############################################################

for thrIndex in ${!muThresholdsLow[@]} ; do
for transport in ${transports} ; do
	
        simTag="csat_enabled_${transport}_${udpRate}_${duration}_${muThresholdsLow[${thrIndex}]}_${muThresholdsHigh[${thrIndex}]}"
	imgTag="indoor_"`echo "${simTag}" | tr '.' '_'`
	TRAFFIC=`print_traffic_model ${transport}`
	TITLE="csat_enabled_${transport}_${udpRate}_MU1_${muThresholdsLow[${thrIndex}]}_MU2_${muThresholdsHigh[${thrIndex}]}"

        for OPERATOR in A B ; do
            THROUGHPUT_COLUMN=8
	    CURRENT=results/laa_wifi_indoor_${simTag}_operator${OPERATOR}
            `../utils/cdf.sh $THROUGHPUT_COLUMN $CURRENT > results/cdf_throughput_${simTag}_${OPERATOR}`
        done
	index=0	    
	for OPERATOR in A B ; do
	    FILES[$index]=results/cdf_throughput_${simTag}_${OPERATOR}
	    YCOLS[$index]='($2)'    
	    XCOLS[$index]='($1)'
	    LABELS[$index]=`print_operator_laa_wifi $OPERATOR`
	    
	    index=`expr $index + 1`
	done

	PLOTTYPE="with linespoints"
	XLABEL="Throughput [Mbps]"
	YLABEL="CDF"
	RANGE=$THROUGHPUT_CDF_RANGE
	OPTIONS="$BASE_OPTIONS ; set key bottom right"
	IMGFILENAME="${imgTag}_throughput"
	plot

	unset FILES
	unset LABELS
	unset YCOLS
	unset XCOLS

done
done

../utils/shot_thumbnails.sh $IMGDIR/thumbnails "laa-wifi-indoor: transport=${transport}"

