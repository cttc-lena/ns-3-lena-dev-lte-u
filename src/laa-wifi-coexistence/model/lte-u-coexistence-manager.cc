/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es>
 *
 * Acknowledgments: partially based on work done in collaboration with Tom Henderson <tomh@tomh.org> from University of Washington in the context of WALAA project.
 *
 */

#include "lte-u-coexistence-manager.h"
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/spectrum-wifi-phy.h"
#include "ns3/wifi-mac-trailer.h"
#include "ns3/scenario-helper.h"

namespace ns3 {

  NS_LOG_COMPONENT_DEFINE ("LteUCoexistenceManager");

  NS_OBJECT_ENSURE_REGISTERED (LteUCoexistenceManager);


  /**
   * Listener for PHY events.
   */

  class LteUPhyListener : public ns3::WifiPhyListener
  {

  public:
    /**
     * Create a PhyListener for the given DcfManager.
     *
     * \param dcf
     */
    LteUPhyListener (ns3::LteUCoexistenceManager *lteu) : m_coexistenceManager (lteu)
    {
    }

    virtual ~LteUPhyListener ()
    {
    }

    /*
     * Notify of rx event, and its estimated duration.
     */
    virtual void NotifyRxStart (Time duration)
    {
      NS_LOG_FUNCTION (this << duration.GetSeconds ());
      m_coexistenceManager->NotifyRxStart (duration);
    }

    virtual void NotifyRxEndOk (void)
    {
      NS_LOG_FUNCTION (this);
      //NS_FATAL_ERROR ("Unimplemented");
    }

    virtual void NotifyRxEndError (void)
    {
      // currently not implemented, not needed this notification
      NS_LOG_FUNCTION (this);
      //NS_FATAL_ERROR ("Unimplemented");
    }

    virtual void NotifyTxStart (Time duration, double txPowerDbm)
    {
      // wifi is in monitor mode, this shall never happen
      NS_LOG_FUNCTION (this);
      //NS_FATAL_ERROR ("Unimplemented");
    }

    /*
     * Notify of cca busy event, and its estimated duration.
     */
    virtual void NotifyMaybeCcaBusyStart (Time duration)
    {
      NS_LOG_FUNCTION (this << duration.GetSeconds ());
      m_coexistenceManager->NotifyMaybeCcaBusyStart (duration);
    }

    virtual void NotifySwitchingStart (Time duration)
    {
      // wifi is in monitor mode, this should never happen
      NS_LOG_FUNCTION (this);
      //NS_FATAL_ERROR ("Unimplemented");
    }

    virtual void NotifySleep (void)
    {
      // currently not implemented, not needed this notification
      NS_LOG_FUNCTION (this);
      //NS_FATAL_ERROR ("Unimplemented");
    }

    virtual void NotifyWakeup (void)
    {
      // currently not implemented, not needed this notification
      NS_LOG_FUNCTION (this);
      //NS_FATAL_ERROR ("Unimplemented");
    }

  private:

    LteUCoexistenceManager *m_coexistenceManager;

  };

  /**
   * Listener for NAV events. Forwards notifcations to LteUCoexistenceManager.
   */
  class LteUMacLowListener : public ns3::MacLowDcfListener
  {

  public:

    /**
     * Create MacLowListener.
     * \param dcf
     */
    LteUMacLowListener (ns3::LteUCoexistenceManager *lteUCoexistenceManager): m_lbtCoexistenceManager (lteUCoexistenceManager)
  {
  }

    virtual ~LteUMacLowListener ()
    {
    }

    virtual void NavStart (Time duration)
    {
      NS_LOG_FUNCTION (this);
      m_lbtCoexistenceManager->NavStart (duration);

    }

    virtual void NavReset (Time duration)
    {
      // currently not implemented, not needed this notification
      NS_LOG_FUNCTION (this);
      m_lbtCoexistenceManager->NavStart (duration);
    }

    virtual void AckTimeoutStart (Time duration)
    {
      // currently not implemented, not needed this notification
      NS_LOG_FUNCTION (this);
      NS_FATAL_ERROR ("Unimplemented");
    }

    virtual void AckTimeoutReset ()
    {
      // currently not implemented, not needed this notification
      NS_LOG_FUNCTION (this);
      NS_FATAL_ERROR ("Unimplemented");
    }

    virtual void CtsTimeoutStart (Time duration)
    {
      // currently not implemented, not needed this notification
      NS_LOG_FUNCTION (this);
      NS_FATAL_ERROR ("Unimplemented");
    }

    virtual void CtsTimeoutReset ()
    {
      // currently not implemented, not needed this notification
      NS_LOG_FUNCTION (this);
      NS_FATAL_ERROR ("Unimplemented");
    }

    virtual void NavHeader (WifiMacHeader wifiMacHeader)
    {
      if (wifiMacHeader.IsBeacon())
        {
          m_lbtCoexistenceManager->BeaconDetected (wifiMacHeader.GetAddr3());
        }

      m_lbtCoexistenceManager->MacHeaderDuration (wifiMacHeader.GetDuration());
    }

  private:

    ns3::LteUCoexistenceManager *m_lbtCoexistenceManager;

  };

  TypeId
  LteUCoexistenceManager::GetTypeId (void)
  {
    static TypeId tid = TypeId ("ns3::LteUCoexistenceManager")
        .SetParent<ChannelAccessManager> ()
        .SetGroupName ("laa-wifi-coexistence")
        .AddAttribute ("EnergyDetectionMode",
                       "If true only energy detection will be performed, packet reception will be disabled, "
                       "so no preamble or beacon will be received.",
                       BooleanValue (false),
                       MakeBooleanAccessor(&LteUCoexistenceManager::m_energyDetectionMode),
                       MakeBooleanChecker())
       .AddAttribute ("CsatCycleDuration",
                      "Duration of CSAT cycle in number of subframes.",
                       UintegerValue (160),
                       MakeUintegerAccessor(&LteUCoexistenceManager::m_csatCycleDuration),
                       MakeUintegerChecker<uint32_t> (40, MAX_CSAT_DURATION))
       .AddAttribute ("CsatDutyCycle",
                      "Portion of ON time in CsatCycle.",
                      DoubleValue (0.5),
                      MakeDoubleAccessor (&LteUCoexistenceManager::m_dutyCycle),
                      MakeDoubleChecker<double> (0.0,1.0))
       .AddAttribute ("Puncturing",
                      "The maximum number of consecutive ON subframes during the ON period.",
                      UintegerValue (20),
                      MakeUintegerAccessor (&LteUCoexistenceManager::m_puncturing),
                      MakeUintegerChecker<uint32_t> (1, 20))
       .AddAttribute ("PuncturingDuration",
                     "Duration of puncturing in the number of subframes.",
                      UintegerValue (1),
                      MakeUintegerAccessor (&LteUCoexistenceManager::m_puncturingDuration),
                      MakeUintegerChecker<uint32_t> (1, 5))
       .AddAttribute ("StartWithOnPeriod",
                      "If true start csat cycle with ON period, if false, start with OFF period",
                      BooleanValue (true),
                      MakeBooleanAccessor(&LteUCoexistenceManager::m_startWithOnPeriod),
                      MakeBooleanChecker())
       .AddAttribute ("Offset",
                      "Number of consecutive OFF subframes at the beginning of csat cycle after which starts "
                      "ON or OFF period depending on configuration",
                      UintegerValue (0),
                      MakeUintegerAccessor (&LteUCoexistenceManager::m_offset),
                      MakeUintegerChecker<uint32_t> (0, MAX_CSAT_DURATION/2))
      .AddAttribute ("MaxInterMibTime",
                     "The maximum amount of time that can pass between two consecutive MIBs.",
                     UintegerValue (160),
                     MakeUintegerAccessor(&LteUCoexistenceManager::m_maxInterMibTime),
                     MakeUintegerChecker<uint32_t> (10, 160))
      .AddAttribute ("CsatEnabled",
                     "If true enable CSAT, if false use the default duty cycle.",
                     BooleanValue (false),
                     MakeBooleanAccessor(&LteUCoexistenceManager::m_csatEnabled),
                     MakeBooleanChecker())
      .AddAttribute ("TOnMinInMilliSec",
                    "TOnMinInMilliSec variable used in CSAT formula for duty cycle update.",
                     UintegerValue (120),
                     MakeUintegerAccessor (&LteUCoexistenceManager::m_tOnMinInMilliSec),
                     MakeUintegerChecker<uint32_t> (4, MAX_CSAT_DURATION))
      .AddAttribute ("MinOnTime",
                     "The minimum total on time during one CSAT cycle in number of subframes.",
                      UintegerValue (4),
                      MakeUintegerAccessor (&LteUCoexistenceManager::m_minOnTime),
                      MakeUintegerChecker<uint32_t> (4, 10))
      .AddAttribute ("MinOffTime",
                     "The minimum total off time during one CSAT cycle in number of subframes.",
                      UintegerValue (20),
                      MakeUintegerAccessor (&LteUCoexistenceManager::m_minOffTime),
                      MakeUintegerChecker<uint32_t> (0, 40))
      .AddAttribute ("MonitoringTime",
                     "The monitoring time in number of subframes.",
                      UintegerValue (20),
                      MakeUintegerAccessor (&LteUCoexistenceManager::m_monitoringTime),
                      MakeUintegerChecker<uint32_t> (10, 40))
      .AddAttribute ("AlphaMU",
                    "A constant value used for MU calculation.",
                     DoubleValue (0.8),
                     MakeDoubleAccessor (&LteUCoexistenceManager::m_alphaMu),
                     MakeDoubleChecker<double> (0.0,1.0))
      .AddAttribute ("ThresholdMuLow",
                     "The lower MU threshold for CSAT adaptation.",
                     DoubleValue (0.4),
                     MakeDoubleAccessor (&LteUCoexistenceManager::m_thresholdMuLow),
                     MakeDoubleChecker<double> (0.0,1.0))
      .AddAttribute ("ThresholdMuHigh",
                     "The upper MU threshold for CSAT adaptation.",
                     DoubleValue (0.6),
                     MakeDoubleAccessor (&LteUCoexistenceManager::m_thresholdMuHigh),
                     MakeDoubleChecker<double> (0.0,1.0))
      .AddAttribute ("DeltaTUp",
                     "The delta value used when the duty cycle is being increased.",
                      DoubleValue (0.05),
                      MakeDoubleAccessor (&LteUCoexistenceManager::m_deltaTUp),
                      MakeDoubleChecker<double> (0.0,1.0))
      .AddAttribute ("DeltaTDown",
                     "The delta value used when the duty cycle is being reduced.",
                      DoubleValue (0.05),
                      MakeDoubleAccessor (&LteUCoexistenceManager::m_deltaTDown),
                      MakeDoubleChecker<double> (0.0,1.0))
      .AddAttribute ("ApScanInterval",
                     "The AP scan interval in number of CSAT cycles.",
                      UintegerValue (16),
                      MakeUintegerAccessor (&LteUCoexistenceManager::m_apScanInterval),
                      MakeUintegerChecker<uint32_t> (2, 40))
      .AddAttribute ("ApScanDuration",
                     "The AP scan duration in number of subframes.",
                      UintegerValue (160),
                      MakeUintegerAccessor (&LteUCoexistenceManager::m_apScanDuration),
                      MakeUintegerChecker<uint32_t> (120, 360))
      .AddTraceSource ("ApCount",
                      "The AP counter.",
                      MakeTraceSourceAccessor(&LteUCoexistenceManager::m_apCount),
                      "ns3::TracedValue::Uint32Callback")
      .AddTraceSource ("RxDuration",
                      "an estimated duration of rx signal",
                      MakeTraceSourceAccessor(&LteUCoexistenceManager::m_rxDuration),
                      "ns3::RxDurationTracedCallback")
      .AddTraceSource ("NavDuration",
                      "an estimated duration of nav",
                      MakeTraceSourceAccessor(&LteUCoexistenceManager::m_navDuration),
                      "ns3::NavDurationTracedCallback")
      .AddTraceSource ("BeaconDetected",
                      "reports the address of the AP of the received beacon",
                      MakeTraceSourceAccessor(&LteUCoexistenceManager::m_beaconDetected),
                      "ns3::BeaconDetectedTracedCallback")
      .AddAttribute ("SendCtsToSelf",
                     "If true Cts-to-self will be send just before starting transmission",
                      BooleanValue (true),
                      MakeBooleanAccessor(&LteUCoexistenceManager::m_sendCtsToSelf),
                      MakeBooleanChecker())
      .AddAttribute ("CcaMUCalculation",
                     "If true MU calculation will be based on duration obtained from PLCP, if false it will be based on duration obtained from MAC header.",
                      BooleanValue (true),
                      MakeBooleanAccessor(&LteUCoexistenceManager::m_ccaMuCalculation),
                      MakeBooleanChecker())
      .AddTraceSource ("CsatDutyCycleTrace",
                       "Reports when duty cycle is updated: duty cycle value, new on time, t_on_min, MU.",
                        MakeTraceSourceAccessor (&LteUCoexistenceManager::m_dutyCycleTraceSource),
                       "ns3::LteUCoexistenceManager::ReportCsatDutyCycleTracedCallback")
      .AddTraceSource ("ActiveOnTimeExtendedTrace",
                       "Reports when activeOnTime is updated: old value, new value, and current csat end time",
                        MakeTraceSourceAccessor (&LteUCoexistenceManager::m_activeOnTimeExtendedTraceSource),
                       "ns3::LteUCoexistenceManager::ReportActiveOnTimeAndCsatEndTimeTracedCallback")
      .AddAttribute ("PuncturingEnabled",
                     "If true enable puncturing, if false disable it.",
                      BooleanValue (true),
                      MakeBooleanAccessor(&LteUCoexistenceManager::m_puncturingEnabled),
                      MakeBooleanChecker())
     .AddConstructor<LteUCoexistenceManager> ();
    return tid;
  }

  void
  LteUCoexistenceManager::SetTOnMinInMilliSec (uint32_t tOnMinInMilliSec)
  {
    m_tOnMinInMilliSec = tOnMinInMilliSec;
  }
  
  uint32_t
  LteUCoexistenceManager::GetTOnMinInMilliSec ()
  {
    return m_tOnMinInMilliSec;
  }

  void
  LteUCoexistenceManager::SetDutyCycle (double lteDutyCycle)
  {
    NS_LOG_DEBUG("Duty cycle updated. Old value:"<<m_dutyCycle<<", new value:"<<lteDutyCycle);
    if ((lteDutyCycle >= (double) m_minOnTime /(double) m_csatCycleDuration) && (lteDutyCycle <= (double)(m_csatCycleDuration - m_minOffTime)/(double)m_csatCycleDuration ))
      {
        m_dutyCycle = lteDutyCycle;
      }
    else if (lteDutyCycle < (double)m_minOnTime /(double)m_csatCycleDuration)
      {
        m_dutyCycle = (double)m_minOnTime/(double)m_csatCycleDuration;
      }
    else if (lteDutyCycle > (double)(m_csatCycleDuration - m_minOffTime)/(double)m_csatCycleDuration)
      {
        m_dutyCycle = (double)(m_csatCycleDuration - m_minOffTime)/(double)m_csatCycleDuration;
      }

  }
  

  double
  LteUCoexistenceManager::GetDutyCycle ()
  {
    return m_dutyCycle;
  }


  void
  LteUCoexistenceManager::SetActiveOnTime (bool activeOnTime)
  {
    ChannelAccessManager::SetActiveOnTime(activeOnTime);
  }

  bool
  LteUCoexistenceManager::DoUpdateDutyCycle (double delta)
  {
    NS_LOG_FUNCTION(this);

    if ((delta == 0) || (GetDutyCycle() + delta > 1) || (GetDutyCycle() + delta < 0))
      return false;

    uint32_t newOnTime = floor(m_csatCycleDuration * (GetDutyCycle() + delta));

    if (delta > 0)
      {
        newOnTime = std::min(newOnTime, GetMaxOnTime());
      }
    else
      {
        newOnTime = std::max(newOnTime, GetMinOnTime());
      }

    SetDutyCycle((double)newOnTime/(double)m_csatCycleDuration);

    return false;
  }


  void
  LteUCoexistenceManager::SetWifiDevice ( Ptr<NetDevice> device )
  {
    m_device = device;
  }

  void
  LteUCoexistenceManager::SetEdThreshold(double edThreshold)
  {
    m_edThreshold = edThreshold;
  }

  LteUCoexistenceManager::LteUCoexistenceManager ()
  : ChannelAccessManager(),
    m_phyListener (0),
    m_macLowListener (0)
  {
    NS_LOG_FUNCTION (this);
    m_csatHasStarted = false;
    m_csatCycleStarts = 1;
    m_lastMibTxOpSubframe = 0;
    m_lastLdsTxOpSubframe = 0;
    m_isInMonitoring = false;
    m_monitoringDuration = 0;
    m_channelOccupancy = Seconds(0);
    m_lastMediumUtilization = 0;
    m_csatTimeInAdvance = Seconds (0); //initialized once that csat is started
    m_apScanTimer = 0;
    m_isInApScan = false;
  }

  LteUCoexistenceManager::~LteUCoexistenceManager ()
  {
    NS_LOG_FUNCTION (this);
    delete m_phyListener;
    delete m_macLowListener;
    m_phyListener = 0;
    m_macLowListener = 0;
  }

  void
  LteUCoexistenceManager::SetWifiPhy (Ptr<SpectrumWifiPhy> phy)
  {
    NS_LOG_FUNCTION (this << phy);
    phy->SetCcaMode1Threshold(m_edThreshold);
    if (m_energyDetectionMode)
      phy->SetAttribute("DisableWifiReception", BooleanValue(true));
    m_wifiPhy = phy;
    SetupPhyListener (phy);
  }

  void
  LteUCoexistenceManager::SetupPhyListener (Ptr<SpectrumWifiPhy> phy)
  {
    NS_LOG_FUNCTION (this << phy);
    if (m_phyListener != 0)
      {
        NS_LOG_DEBUG ("Deleting previous listener " << m_phyListener);
        phy->UnregisterListener (m_phyListener);
        delete m_phyListener;
      }
    //phy->SetCcaMode1Threshold(m_edThreshold);
    if (m_energyDetectionMode)
      {
        phy->SetAttribute("DisableWifiReception", BooleanValue(true));
      }
    else
     {
    	phy->TraceConnectWithoutContext("EnergyRxDuration", MakeCallback(&LteUCoexistenceManager::EnergyRxDuration, this));
     }
    m_phyListener = new LteUPhyListener (this);
    phy->RegisterListener(m_phyListener);
  }

  void
  LteUCoexistenceManager::SetupLowListener (Ptr<MacLow> low)
  {
    NS_LOG_FUNCTION (this << low);
    if (m_macLowListener != 0)
      {
        delete m_macLowListener;
      }
    m_macLowListener = new LteUMacLowListener (this);
    low->SetNavHeaderForward(true);
    low->RegisterDcfListener (m_macLowListener);
    m_macLow = low;
  }

  void
  LteUCoexistenceManager::NotifyRxStart (Time duration)
  {
    NS_LOG_FUNCTION (this << duration.GetSeconds ());
    m_rxDuration (duration.GetSeconds());
  }

  void
  LteUCoexistenceManager::NotifyMaybeCcaBusyStart (Time duration)
  {
    NS_LOG_FUNCTION (this << duration.GetSeconds ());
    m_rxDuration (duration.GetSeconds());
  }

  void
  LteUCoexistenceManager::NavStart (Time duration)
  {
    NS_LOG_FUNCTION (this << duration.GetSeconds ());
    m_navDuration (duration);
  }

  void
  LteUCoexistenceManager::SetEnergyDetectionMode (bool energyDetection)
  {
    NS_LOG_FUNCTION (this);
    m_energyDetectionMode = energyDetection;
  }

  void
  LteUCoexistenceManager::BeaconDetected (Mac48Address apAddress)
  {
    NS_LOG_FUNCTION (this);
    m_beaconDetected (apAddress);
    // insert in the set of beacons
    //if (m_isInMonitoring || m_isInApScan) // allow ap detection also during monitoring times, not only during ap scan
    if (m_isInApScan) // allow ap detection only during ap scan
      {
        m_wifiAps.insert(apAddress);
      }
    NS_LOG_INFO ("Number of detected AP:"<<m_wifiAps.size()<<" , current time:"<<Simulator::Now());
  }

  void
  LteUCoexistenceManager::CalculateChannelOccupancy (Time duration)
  {
    if (m_isInMonitoring)
      {
        m_channelOccupancy += duration;
        // uncomment this code if you need the output file stats of channel occupancy durations
        /*if (duration != Seconds(0))
        {
          std::ofstream outFile;
          outFile.open ("channel_occupancy.txt", std::ofstream::out | std::ofstream::app);
          outFile.setf (std::ios_base::fixed);

          if (!outFile.is_open ())
           {
             NS_LOG_ERROR ("Can't open file ");
             return;
           }
           outFile <<Simulator::Now().GetSeconds () << " ";
           outFile << duration.GetSeconds() << std::endl;
        }*/
      }
  }

  void LteUCoexistenceManager::EnergyRxDuration (Time duration)
  {
     if (m_isInMonitoring && m_ccaMuCalculation)
       {
         CalculateChannelOccupancy(duration);
       }
  }


  void
  LteUCoexistenceManager::MacHeaderDuration (Time duration)
  {
    if (m_isInMonitoring && !m_ccaMuCalculation)
      {
        CalculateChannelOccupancy (duration);
      }
  }

  void
  LteUCoexistenceManager::UpdateApCounter ()
  {
    m_apCount = m_wifiAps.size();
  }

  void
  LteUCoexistenceManager::SetLteEnbPhy (Ptr<LteEnbPhy> lteEnbPhy)
  {
    NS_LOG_FUNCTION (this);
    m_lteEnbPhy = lteEnbPhy;
  }

  void
  LteUCoexistenceManager::DoRequestAccess ()
  {
    NS_LOG_FUNCTION (this);
    NS_ASSERT_MSG (!m_accessGrantedCallback.IsNull (), "Access granted callback is not initialized!");
    ChannelAccessManager::DoRequestAccess();
  }

  Time LteUCoexistenceManager::GetDelayTillNextSubframeBoundary ()
  {
    NS_ASSERT_MSG (m_lteEnbPhy->GetTtiBegin() + Seconds(m_lteEnbPhy->GetTti()) - Simulator::Now() > 0, "Issue, time in LteUCoexistenceManager not in sync with LteEnbPhy");
    return m_lteEnbPhy->GetTtiBegin() + Seconds(m_lteEnbPhy->GetTti()) - Simulator::Now();
  }

  void
  LteUCoexistenceManager::ActiveOnTimeTrace (bool activeOnTime)
  {
    m_activeOnTimeExtendedTraceSource (activeOnTime, Simulator::Now() + MilliSeconds(m_csatCycleDuration));
  }

  void
  LteUCoexistenceManager::DoScheduleTxOpNotifications (std::bitset<MAX_CSAT_DURATION>& csatPattern, bool isAPscanPeriod)
  {
   NS_LOG_FUNCTION (this);
   NS_ASSERT_MSG(csatPattern.any(),"No transmission scheduled in next csat cycle");

   Time timeUntilNextCycleStart = GetDelayTillNextSubframeBoundary () + MilliSeconds(m_lteEnbPhy->GetMacChDelay());
   Simulator::Schedule (timeUntilNextCycleStart, &LteUCoexistenceManager::ActiveOnTimeTrace, this, IsActiveOnTime());
   // set only ctrl tranmission if not active on time
   m_lteEnbPhy->SetOnlyCtrl(!IsActiveOnTime());

   uint32_t index = 0;
   uint32_t txopStarts = 0;
   uint32_t txopDuration = 0;
   uint32_t monitoringDuration = 0;
   uint32_t monitoringStarts = 0;

   while (index < m_csatCycleDuration)
    {
       if (csatPattern[index] == 1)
         {
           txopDuration ++;
           if (txopDuration == 1)
             {
               txopStarts = index;
             }
          }
       else
         {
           monitoringDuration++;
           if (monitoringDuration ==1)
             {
               monitoringStarts = index;
             }
         }

       // notify lte-enb-phy of transmission opportunities. Because of mac-to-phy delay, next csat is created in advance and notifications are sent to account for this delay
       if ((txopDuration > 0) && ((index == m_csatCycleDuration - 1) || (csatPattern[index] == 0)))
         {
           if (m_puncturingEnabled)
             NS_ASSERT_MSG (txopDuration <= m_puncturing, "Wrong puncturing");
           Simulator::Schedule (MilliSeconds (txopStarts), &LteUCoexistenceManager::Notify, this, MilliSeconds (txopDuration));
           txopDuration = 0;
           txopStarts = 0;
         }

       // notify itself when to do monitoring and for how much time
       if (m_csatEnabled)
         {
           if ((monitoringDuration > 0) && ((index == m_csatCycleDuration - 1) || (csatPattern[index] == 1)))
             {
               if (isAPscanPeriod)
                 {
                   Simulator::Schedule ( timeUntilNextCycleStart + MilliSeconds (monitoringStarts) , &LteUCoexistenceManager::DoPerformApScan, this, monitoringDuration);
                 }
               else
                 {
                   Simulator::Schedule ( timeUntilNextCycleStart + MilliSeconds (monitoringStarts) , &LteUCoexistenceManager::DoPerformMonitoring, this, monitoringDuration);
                 }
               monitoringDuration = 0;
               monitoringStarts = 0;
             }
         }

       index++;
   }
  }

  void
  LteUCoexistenceManager::Notify (Time grantDuration)
  {
    NS_LOG_FUNCTION (this);
    NS_LOG_INFO ("Scheduled txop starting at:" << Simulator::Now() << " with duration equal to:"<<grantDuration);

    //SendCtsToSelf(grantDuration);

    SetGrantDuration(grantDuration);
    LteUCoexistenceManager::DoRequestAccess();
  }

  void
  LteUCoexistenceManager::DoPerformMonitoring (uint32_t durationInNumberOfSubframes)
  {
    NS_LOG_FUNCTION (this);
    NS_ASSERT_MSG (m_csatEnabled && !m_isInMonitoring, "Either csat not enabled or already in monitoring.");
    m_isInMonitoring = true;
    uint32_t alreadyMonitored = m_monitoringDuration;
    uint32_t numberOfMonitoringBlocks = (alreadyMonitored + durationInNumberOfSubframes) / m_monitoringTime;
    // update the monitoring duration
    m_monitoringDuration = (alreadyMonitored + durationInNumberOfSubframes) % m_monitoringTime;
    //NS_LOG_DEBUG ("Already in monitoring for:"<<alreadyMonitored<<", and will monitoring be during:"<<durationInNumberOfSubframes <<", new monitoring duration:"<<m_monitoringDuration);
    for (uint32_t n = 1; n <= numberOfMonitoringBlocks; n++)
      {
        Simulator::Schedule(MilliSeconds(n * m_monitoringTime - alreadyMonitored), &LteUCoexistenceManager::AdaptCsat, this);
      }
    Simulator::Schedule(MilliSeconds(durationInNumberOfSubframes), &LteUCoexistenceManager::DoStopMonitoring, this);
  }

  void
  LteUCoexistenceManager::DoStopMonitoring ()
  {
    NS_LOG_FUNCTION (this);
    NS_ASSERT_MSG(m_isInMonitoring, "Already stopped monitoring.");
    m_isInMonitoring = false;
  }

  void
  LteUCoexistenceManager::DoPerformApScan (uint32_t durationInNumberOfSubframes)
  {
    NS_LOG_FUNCTION (this);
    m_isInApScan = true;
    Simulator::Schedule (MilliSeconds(durationInNumberOfSubframes), &LteUCoexistenceManager::DoStopApScan, this);
  }

  void
  LteUCoexistenceManager::DoStopApScan ()
  {
    NS_LOG_FUNCTION (this);
    NS_ASSERT_MSG (m_isInApScan, "Already stopped monitoring.");
    m_isInApScan = false;
  }

  void
  LteUCoexistenceManager::ResetApScanStats()
  {
    NS_LOG_FUNCTION (this);
    m_wifiAps.clear();
  }

  uint32_t
  LteUCoexistenceManager::GetMaxOnTime ()
  {
    NS_ASSERT_MSG(m_csatCycleDuration - m_minOffTime, "Invalid duration of CSAT cycle, shorter than min OFF time.");
    return m_csatCycleDuration - m_minOffTime;
  }

  uint32_t
  LteUCoexistenceManager::GetLteNodesNum ()
  {
    NS_LOG_FUNCTION (this);
    PointerValue pointerValue;
    m_lteEnbPhy->GetDevice()->GetObject<LteEnbNetDevice>()->GetAttribute("LteAnr", pointerValue);
    Ptr<LteAnr> anr = pointerValue.Get<LteAnr>();
    NS_ASSERT_MSG(anr, "Neighbour table pointer is null, check if X2 interface is enabled in LteHelper.");
    return anr->GetNeighbourRelationTableSize();
  }


  uint32_t
  LteUCoexistenceManager::GetMinOnTime()
  {
    NS_LOG_FUNCTION (this);
    uint32_t lteUNodes = GetLteNodesNum();
    uint32_t minOn =  ((lteUNodes + 1) * m_csatCycleDuration) / (m_wifiAps.size() + lteUNodes + 1);
    uint32_t newValue = std::min (m_tOnMinInMilliSec, minOn);
    NS_LOG_DEBUG ("GetMinOnTime, when lteUNodesNum:"<<lteUNodes<<" wifiNodesNum:"<<m_wifiAps.size()<<" minOn:"<<minOn<<" and T_ON,min="<<newValue);
    return newValue;
  }

  void
  LteUCoexistenceManager::AdaptCsat ()
  {
    NS_LOG_FUNCTION (this);

    // update MU
    double mediumUtilization = m_channelOccupancy.GetSeconds() / MilliSeconds (m_monitoringTime).GetSeconds();
    if (mediumUtilization != 0)
      {
        m_lastMediumUtilization =  m_alphaMu * mediumUtilization  +  (1 - m_alphaMu) * m_lastMediumUtilization;
      }
    NS_LOG_DEBUG ("Medium utilization: "<< mediumUtilization <<", after filtering the last medium utilization is:"<<m_lastMediumUtilization);

    // update duty cycle
    if (m_lastMediumUtilization < m_thresholdMuLow)
      {
        DoUpdateDutyCycle (m_deltaTUp);
      }
    else if (m_lastMediumUtilization > m_thresholdMuHigh)
      {
        DoUpdateDutyCycle (-m_deltaTDown);
      }
    else
      {
        NS_LOG_DEBUG("T_ON is not updated since MU is inside the threshold boundaries.");
      }
    m_dutyCycleTraceSource ( m_lteEnbPhy->GetComponentCarrierId(), GetDutyCycle(), floor(m_csatCycleDuration * GetDutyCycle()), GetMinOnTime(), m_lastMediumUtilization);
    m_channelOccupancy = Seconds (0);
    //m_numLteNodes = 0;
    //m_numWifiNodes = 0;
  }

  void
  LteUCoexistenceManager::SendCtsToSelf(Time grantDuration)
  {
    NS_LOG_FUNCTION (this);
  }

  bool
  LteUCoexistenceManager::IsMibSubframe (uint32_t subframeIndex)
  {
    NS_LOG_FUNCTION (this);
    if ((m_csatCycleStarts + subframeIndex) % m_lteEnbPhy->GetMibInterval() == 1)
      {
        return true;
      }
    else
      {
        return false;
      }
  }

  bool
  LteUCoexistenceManager::IsLdsSubframe (uint32_t subframeIndex)
  {
    NS_LOG_FUNCTION (this);
    if ((m_csatCycleStarts + subframeIndex) % m_lteEnbPhy->GetLdsInterval() == 6)
      {
        return true;
      }
    else
      {
        return false;
      }
  }

  bool
  LteUCoexistenceManager::ScheduleNextMib (uint32_t subframeIndex, std::bitset<MAX_CSAT_DURATION>& ctrlMask)
  {
    NS_LOG_FUNCTION (this);
    NS_ASSERT_MSG(subframeIndex < m_csatCycleDuration, "Invalid subframe index:"<<subframeIndex);
    NS_ASSERT_MSG( m_lastMibTxOpSubframe == 0 || (m_lastMibTxOpSubframe + m_maxInterMibTime >= m_csatCycleStarts + subframeIndex),
                   "Exceeded maximum inter MIB time at:"<<m_csatCycleStarts + subframeIndex << ", now:"<<Simulator::Now().GetMilliSeconds());

    uint32_t index = subframeIndex;
    while (index < m_csatCycleDuration )
      {
        if (IsMibSubframe(index))
          {
            ctrlMask[index] = 1;
            m_lastMibTxOpSubframe = m_csatCycleStarts + index;
            NS_LOG_INFO ("Scheduled MIB at : "<<m_csatCycleStarts + index <<" ,next mib to be sent at latest:"<<m_lastMibTxOpSubframe + m_maxInterMibTime<<" ,csat cycle ends:"<<
                         m_csatCycleStarts +  m_csatCycleDuration);
            return true;
          }
        index++;
      }
    return false;
  };


  uint32_t
  LteUCoexistenceManager::SetPuncturing (uint32_t pos, uint32_t puncturing, uint32_t puncturingDuration, std::bitset<MAX_CSAT_DURATION>& puncturingMask, std::bitset<MAX_CSAT_DURATION>& ctrlMask)
  {
    uint32_t consequtiveSubframes = 0;
    uint32_t puncturingSubframes = 0;
    uint32_t subframeIndex = pos;

    while (subframeIndex < pos + puncturing + puncturingDuration && subframeIndex < m_csatCycleDuration)
      {
        if ((m_dutyCycle * m_csatCycleDuration) - (double)((puncturingMask|ctrlMask).count())==0)
          {
            NS_LOG_INFO ("No more ON subframes for this CSAT cycle.");
            break;
          }
        if (m_puncturingEnabled)
          {
            NS_ASSERT_MSG( pos + puncturing + puncturingDuration <= m_csatCycleDuration, "Puncturing position exceeds csat cycle!");
            if (consequtiveSubframes < puncturing)
              {
                consequtiveSubframes ++;
                puncturingMask [subframeIndex] = 1;
                // update record about last mib txop
                if (IsMibSubframe(subframeIndex))
                  {
                    //update ctrl map, important because for duty cycle sanity check, when some reduction of on time can be made, so to know where are ctrl bits
                    ctrlMask [subframeIndex] = 1;
                    if (m_lastMibTxOpSubframe < m_csatCycleStarts + subframeIndex)
                      {
                        m_lastMibTxOpSubframe = m_csatCycleStarts + subframeIndex;
                      }
                    NS_LOG_INFO ("Scheduled MIB (with data) at : "<<m_csatCycleStarts + subframeIndex<<" ,next mib to be sent at latest:"<<m_lastMibTxOpSubframe + m_maxInterMibTime<< " ,csat cycle ends:"<<m_csatCycleStarts + m_csatCycleDuration);
                  }
              }
            else
              {
                NS_ASSERT_MSG(ctrlMask[subframeIndex]==0, "Puncturing on ctrl bit");
                puncturingMask [subframeIndex] = 0;
                puncturingSubframes++;
                if (puncturingSubframes == puncturingDuration)
                  {
                    consequtiveSubframes = 0;
                    puncturingSubframes = 0;
                  }
              }
          }
        else
          {
            puncturingMask [subframeIndex] = 1;
            if (IsMibSubframe(subframeIndex))
              {
                ctrlMask [subframeIndex] = 1;
                if (m_lastMibTxOpSubframe < m_csatCycleStarts + subframeIndex)
                  {
                    m_lastMibTxOpSubframe = m_csatCycleStarts + subframeIndex;
                  }
              }

          }
        subframeIndex++;
      }

    NS_LOG_INFO("After  puncturing:"<<puncturingMask);
    NS_LOG_INFO("Ctrl     bit mask:"<<ctrlMask);
    return subframeIndex;
  }


  uint32_t
  LteUCoexistenceManager::FindNextCtrlBit (const uint32_t pos, uint32_t offset, std::bitset<MAX_CSAT_DURATION>& ctrlMask)
  {
    NS_ASSERT_MSG (pos < m_csatCycleDuration, "The maximum value of position parameter can be the size of current duty cycle. Provided value:"<<pos<<" csat cycle:"<<m_csatCycleDuration);
    uint32_t retVal = 0;
    uint32_t subframeIndex = pos;
    while (subframeIndex < pos + offset && subframeIndex < m_csatCycleDuration)
      {
        if (ctrlMask[subframeIndex]==1)
          {
            retVal = subframeIndex;
            break;
          }
        subframeIndex++;
      }
    return retVal;
  }

  uint32_t
  LteUCoexistenceManager::CountOnBitsAfter (uint32_t pos, std::bitset<MAX_CSAT_DURATION>& bitset)
  {
    NS_ASSERT_MSG (pos>=0, "Position out of range.");
    uint32_t retVal = 0;
    uint32_t index = pos;
    while (index < m_csatCycleDuration)
      {
        if (bitset[index] == 1)
          {
            retVal++;
          }
        else
          {
            break;
          }
        index++;
      }
    return retVal;
  };


  uint32_t
  LteUCoexistenceManager::CountOnBitsBefore (uint32_t pos, std::bitset<MAX_CSAT_DURATION>& bitset)
  {
    NS_ASSERT_MSG (pos>=0, "Position out of range.");
    uint32_t retVal = 0;
    uint32_t index = pos;
    while (index > 1)
      {
        if (bitset[index] == 1)
          {
            retVal++;
          }
        else
          {
            break;
          }
        index--;
      }
    return retVal;
  };


  void
  LteUCoexistenceManager::CreateInitialMibAndLdsMask (std::bitset<MAX_CSAT_DURATION>& ctrlMask, uint32_t csatCycleDuration, uint32_t offset)
  {
    NS_LOG_FUNCTION (this);
    // create initial mib and lds mask
    NS_ASSERT_MSG(csatCycleDuration >= 40, "Ctrl mask size has to be at least 40.");

    if ((m_lastMibTxOpSubframe == 0))
      {
        if (offset>=0)
            {
              ctrlMask [0] = 1;
              m_lastMibTxOpSubframe = m_csatCycleStarts;
            }
      }

    // set MIB
    while ( m_lastMibTxOpSubframe + m_maxInterMibTime < m_csatCycleStarts + offset)
     {
       ctrlMask [m_lastMibTxOpSubframe - m_csatCycleStarts + m_maxInterMibTime] = 1;
       m_lastMibTxOpSubframe += m_maxInterMibTime;

     };


    if ((m_lastLdsTxOpSubframe == 0))
      {
        uint32_t firstLdsTxOpSubframe = m_lteEnbPhy->GetLdsInterval() - m_csatCycleStarts  % m_lteEnbPhy->GetLdsInterval() + 6;
        ctrlMask [firstLdsTxOpSubframe] = 1;
        m_lastLdsTxOpSubframe = m_csatCycleStarts + firstLdsTxOpSubframe;
      };

    // set LDS
    while ( m_lastLdsTxOpSubframe + m_lteEnbPhy -> GetLdsInterval() < m_csatCycleStarts +  csatCycleDuration )
      {
        ctrlMask [m_lastLdsTxOpSubframe - m_csatCycleStarts + m_lteEnbPhy -> GetLdsInterval()] = 1;
        m_lastLdsTxOpSubframe += m_lteEnbPhy -> GetLdsInterval();
      };


    NS_LOG_INFO("CtrlMask:"<<ctrlMask);
  }


  // create offset mask
  void
  LteUCoexistenceManager::CreateOffsetMask (std::bitset<MAX_CSAT_DURATION>& offsetMask)
  {
    NS_LOG_FUNCTION (this);
    uint32_t subframeIndex = 0;
    offsetMask.set();

    while (subframeIndex < m_offset)
      {
        offsetMask.reset(subframeIndex);
        subframeIndex++;
      }
    NS_LOG_INFO("OffsMask:"<<offsetMask);
  }

  // generate puncturing
  void
  LteUCoexistenceManager::GenerateOnTimeAndPuncturing (std::bitset<MAX_CSAT_DURATION>& puncturingMask, std::bitset<MAX_CSAT_DURATION>& ctrlMask)
  {
    NS_LOG_FUNCTION (this);
    uint32_t subframeIndex = m_offset;
    puncturingMask.reset();

    if (!m_puncturingEnabled)
      {
        SetPuncturing (subframeIndex, m_csatCycleDuration, m_puncturingDuration, puncturingMask, ctrlMask);
        return;
      }

    while ((subframeIndex < m_csatCycleDuration) &&  // while in csat cycle
          ((double)((puncturingMask|ctrlMask).count())/(double)m_csatCycleDuration < m_dutyCycle) &&  // while duty cycle is not exceeded
          (m_csatCycleDuration - (puncturingMask|ctrlMask).count() > m_minOffTime))  // while min off time is not reached
      {
        uint32_t next = subframeIndex;
        while ((next < m_csatCycleDuration) &&  //while in csat cycle
               (next < subframeIndex + m_puncturing + m_puncturingDuration) &&  // and  while in next on period and puncturing
               (m_lastMibTxOpSubframe + m_maxInterMibTime >= m_csatCycleStarts + subframeIndex)  &&   // and if there is some mib that has to be sent during next on period or next puncturing
               (m_lastMibTxOpSubframe + m_maxInterMibTime < m_csatCycleStarts + subframeIndex + m_puncturing + m_puncturingDuration))
          {
            ScheduleNextMib (next, ctrlMask);
            next += m_maxInterMibTime;
          }
        /*uint32_t puncturingReduction = 0;*/
        uint32_t ctrlBit = subframeIndex;

        uint32_t onSubframes = m_puncturing;
        // Before setting ON bits and puncturing, we need to check if there is some ctrl bit set in puncturing subframes,
        // and if yes we need to reduce the ON bits, so that puncturing can be done before ctrl bit.
        // Every time that puncturing is shifted it is necessary to do this check.
        while (onSubframes > 0)
          {
            // if in current csat cycle there are no subframes left to set ON bits and puncturing
            if ((subframeIndex + onSubframes + m_puncturingDuration >= m_csatCycleDuration) && (ctrlBit != m_csatCycleDuration))
              {
                ctrlBit = m_csatCycleDuration;
              }
            else
              { //check if there is ctrl bit set in puncturing subframes
                ctrlBit = FindNextCtrlBit (subframeIndex + onSubframes, m_puncturingDuration, ctrlMask);
              }
            // Check if it is necessary to reduce the ON period, so the puncturing will be shifted.
            // And every time ON is reduced and puncturing shifted it is necessary to check if there are ctrl bits in new puncturing subframes.
            if (((ctrlBit >= subframeIndex + onSubframes) && (ctrlBit < subframeIndex + onSubframes + m_puncturingDuration))
                || (ctrlBit == m_csatCycleDuration))
              {
                // there is not enough space to do puncturing
                if (ctrlBit - m_puncturingDuration <= subframeIndex)
                  {
                    onSubframes = 0;
                  }
                else
                  {
                    // reduce ON bits
                    onSubframes = ctrlBit - m_puncturingDuration -  subframeIndex;
                  }
              }
            else
              {
                break;
              }
          };

        NS_LOG_INFO("puncturing:"<<m_puncturing<<" ,puncturingDuration:"<<m_puncturingDuration);

        if (onSubframes > 0) // if we cannot set any ON bit before next obligatory CTRL bit just shift subframeIndex to that ctrl frame and continue generating puncturing
          {
            subframeIndex = SetPuncturing (subframeIndex, onSubframes, m_puncturingDuration, puncturingMask, ctrlMask);
          }
        else
          {
            subframeIndex = ctrlBit;
          }
      }
    NS_LOG_INFO ("PuncMask:" << puncturingMask);
  }


  // check mibs after puncturing until the end of csat cycle
  void
  LteUCoexistenceManager::MibInOffPeriodSanityCheck (std::bitset<MAX_CSAT_DURATION>& ctrlMask)
  {

    NS_ASSERT_MSG(m_lastMibTxOpSubframe!=0," Last mib index is not valid");

    uint32_t mibSubframeIndex = m_lastMibTxOpSubframe - m_csatCycleStarts + m_maxInterMibTime;

    while ( m_lastMibTxOpSubframe + m_maxInterMibTime < m_csatCycleStarts + m_csatCycleDuration)
      {
        NS_LOG_INFO ("Mib in off period, start from:" << m_csatCycleStarts + mibSubframeIndex);
        bool mibScheduled = ScheduleNextMib (mibSubframeIndex, ctrlMask);
        NS_ASSERT_MSG (mibScheduled, "Mib could not be set at:"<<mibSubframeIndex);
        mibSubframeIndex+=m_maxInterMibTime;
      };
    NS_LOG_INFO ("CtrlMask:"<<ctrlMask);
  }

  // sanity check, after adding mibs maybe on period is has occupied too much time
  void
  LteUCoexistenceManager::DutyCycleSanityCheck (std::bitset<MAX_CSAT_DURATION>& puncturingMask, std::bitset<MAX_CSAT_DURATION>& ctrlMask)
  {
    NS_LOG_FUNCTION (this);
    uint32_t subframeIndex =  m_csatCycleDuration;

    if ((puncturingMask|ctrlMask).count() > m_dutyCycle * m_csatCycleDuration)
      {
        uint32_t onPeriodReduction = 0;
        NS_LOG_INFO ("ON period to be reduced for: "<< ceil(((puncturingMask|ctrlMask).count()) - m_dutyCycle * m_csatCycleDuration));
        // find last on time in puncturing and start reducing from there
        while (((puncturingMask|ctrlMask).count() > m_dutyCycle * m_csatCycleDuration)
            && (subframeIndex > 0) && (m_csatCycleDuration - (puncturingMask|ctrlMask).count() > m_minOffTime))
          {
            if (puncturingMask[subframeIndex]==1 && ctrlMask[subframeIndex]==0)
              {
                puncturingMask[subframeIndex]=0;
                onPeriodReduction++;
              }
            subframeIndex--;
          }
        NS_LOG_INFO ("ON period reduced for: "<< onPeriodReduction);
        NS_LOG_INFO ("PuncMask:" << puncturingMask);
      }
  }

  // puncturing sanity check
  void
  LteUCoexistenceManager::PuncturingSanityCheck (std::bitset<MAX_CSAT_DURATION>& csatBitmask)
  {
    NS_LOG_FUNCTION (this);
    uint32_t subframeIndex = 0;
    uint32_t puncturingTest = 0;

    while (subframeIndex < m_csatCycleDuration)
      {
        if (csatBitmask[subframeIndex]==1)
          {
            puncturingTest++;
            NS_ASSERT_MSG(puncturingTest <= m_puncturing, "Wrong puncturing");
          }
        else
          {
            puncturingTest = 0;
          }
        subframeIndex++;
      }
  }


  void
  LteUCoexistenceManager::StartCsat()
   {
     NS_LOG_FUNCTION (this);
     NS_ASSERT_MSG (m_lteEnbPhy, "LteUCoexistence manager not attached to eNb.");

     if (!m_csatHasStarted)
       {
         /**
          * CSAT cycle will start at the beginning of the next frame. Because of "mac-to-phy" delay, CSAT transmission opportunities have to be notified to LTE-MAC at least "mac-to-phy delay" earlier.
          * So, the transmission opportunity bitmask will be generated at least "mac-to-phy delay" time before the beginning of the corresponding CSAT cycle. All transmission opportunities have to be notified
          * in advance for at least "mac-phy-delay" time. To avoid notifying MAC and PHY exactly at subframe boundary, the time between transmission opportunity notification is increased for additional time,
          * which default value is set to half of subframe time (0.5ms).
          */
         m_csatTimeInAdvance = MilliSeconds (m_lteEnbPhy->GetMacChDelay()) + Seconds (0.0005);

         m_csatHasStarted = true;

         Time timeUntilTheEndOfCurrentFrame = Seconds (MilliSeconds(10).GetSeconds() - (std::fmod(Simulator::Now().GetSeconds(), MilliSeconds(10).GetSeconds())));

         if (timeUntilTheEndOfCurrentFrame < m_csatTimeInAdvance)
           {
             Simulator::Schedule (timeUntilTheEndOfCurrentFrame + MilliSeconds(10) - m_csatTimeInAdvance, &LteUCoexistenceManager::DoGetReadyForNextCsatCycle, this);
           }
         else
           {
             Simulator::Schedule (timeUntilTheEndOfCurrentFrame - m_csatTimeInAdvance , &LteUCoexistenceManager::DoGetReadyForNextCsatCycle, this);
           }
       }
     else
       {
         NS_LOG_ERROR(this<<"Csat has already been started.");
       }
   }

  // prepare csat mask for next duty cycle
  void
  LteUCoexistenceManager::DoGetReadyForNextCsatCycle()
  {
    NS_LOG_FUNCTION (this);
    NS_ASSERT_MSG( m_lteEnbPhy, "LteEnbPhy not set!");


    TimeValue timeValue;
    m_lteEnbPhy->GetAttribute("ChannelAccessManagerStartTime", timeValue);

    if (Simulator::Now().GetSeconds() <  timeValue.Get().GetSeconds())
      {
        Simulator::Schedule(MilliSeconds(m_csatCycleDuration), &LteUCoexistenceManager::DoGetReadyForNextCsatCycle, this);
        NS_LOG_INFO("Skipping LTE-U coexistence manager preparation for next cycle as the coexistence manager should not start yet.");
        return;
      }


    // calculate subframe number at which the cycle starts, since this function is executed before subframe boundary increase subframe number for 1, and account for mac-phy delay
    //  n - current absolute subframe number = m_lteEnbPhy->GetFrameNumber() * 10 + m_lteEnbPhy->GetSubframeNumber()
    //  n + 1  = next subframe
    //  n + 1 + mac-phy-delay = account for mac-phy delay
    m_csatCycleStarts = m_lteEnbPhy->GetFrameNumber() * 10 + m_lteEnbPhy->GetSubframeNumber() + 1 + m_lteEnbPhy->GetMacChDelay();

    std::bitset<MAX_CSAT_DURATION> ctrlMask;
    ctrlMask.reset();
    std::bitset<MAX_CSAT_DURATION> puncturingMask;
    puncturingMask.reset();
    std::bitset<MAX_CSAT_DURATION> csatPattern;
    csatPattern.reset();

    // check if it is time to perform AP scan
    if (m_csatEnabled && m_apScanTimer == 0)
      {
        m_apScanTimer = m_apScanInterval;
        ResetApScanStats();
        // in AP scan all subframes are OFF except MIB and LDS
        CreateInitialMibAndLdsMask (csatPattern, m_apScanDuration, m_apScanDuration);
        DoScheduleTxOpNotifications(csatPattern, true);
        Simulator::Schedule (MilliSeconds(m_apScanDuration), &LteUCoexistenceManager::UpdateApCounter, this);
        Simulator::Schedule (MilliSeconds(m_apScanDuration), &LteUCoexistenceManager::DoGetReadyForNextCsatCycle, this);
      }
    else
      {
        if (m_csatEnabled)
          {
            m_apScanTimer--;
          }
        if (!m_startWithOnPeriod)
          {
            // if start with OFF period update the offset accordingly, count when it starts ON period, account also for puncturing
            uint32_t onPeriodWithPuncturing = (ceil(m_csatCycleDuration * m_dutyCycle) / m_puncturing) * (m_puncturing + m_puncturingDuration);
            uint32_t newOffset = m_csatCycleDuration - onPeriodWithPuncturing;

            if (newOffset < m_offset)
              {
                NS_LOG_DEBUG ( "New offset value is lower than the configured offset.");
              }
            m_offset = newOffset;
          };

        // generate obligatory MIB and SIB bit mask
        CreateInitialMibAndLdsMask (ctrlMask, m_csatCycleDuration, m_offset);

        // m_activeOnTime is parameter used to enable and disable scheduling of data subframes, when this parameters is set to false only mandatory CTRL frames will be scheduled
        if (IsActiveOnTime())
          {
           // generate csat on period
           GenerateOnTimeAndPuncturing (puncturingMask, ctrlMask);
          }

        // mib ctrl message interval sanity checks
        MibInOffPeriodSanityCheck (ctrlMask);

        // m_activeOnTime is parameter used to enable and disable scheduling of data subframes, when this parameters is set to false only mandatory CTRL frames will be scheduled
        if (IsActiveOnTime())
          {
            // sanity check that duty cycle is not exceeded, if yes, reduce duty cycle
            DutyCycleSanityCheck (puncturingMask, ctrlMask);
          }
         // assign value to csatPattern
         csatPattern = puncturingMask | ctrlMask;

         if (IsActiveOnTime() && m_puncturingEnabled)
         {
            // final sanity check that resulting csat mask follows puncturing constraints
            PuncturingSanityCheck (csatPattern);
          }

        // schedule notifications for next CSAT
        DoScheduleTxOpNotifications (csatPattern, false);
        // schedule next CSAT
        Simulator::Schedule(MilliSeconds(m_csatCycleDuration), &LteUCoexistenceManager::DoGetReadyForNextCsatCycle, this);
      }

  }

} // ns3 namespace
