/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2019 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Biljana Bojovic <bbojovic@cttc.es>
 */

#ifndef COEXISTENCE_COMMON_H_
#define COEXISTENCE_COMMON_H_

namespace ns3 {

static const struct Wifi5GHzChannel
{
  uint32_t m_wifiChannelNumber;
  uint32_t m_dlEarfcn;
  uint32_t m_dlBandwidthInNoOfRbs;
} g_wifi5GHzChannels[] = {
  { 32, 255244, 100},
  { 36, 255444, 100},
  { 40, 255644, 100},
  { 44, 255844, 100},
  { 48, 256044, 100},
  { 149, 261094, 100},
  { 153, 261294, 100},
  { 157, 261494, 100},
  { 161, 261694, 100},
  { 165, 261894, 100}
};

#define NUM_5GHZ_WIFI_CHANNELS (sizeof (g_wifi5GHzChannels) / sizeof (Wifi5GHzChannel))

enum Config_e
{
  WIFI,
  LTE,
  LAA,
  LTE_U
};

enum Transport_e
{
  FTP,
  FTP_M1,
  TCP,
  UDP,
  ONOFF,
  ChangeLoadUdpTcp,
  OPA_UDP_OPB_VOIP,
  OPA_UDP_OPB_FTP,
  FTP_M1_OPB_HALF_LAMBDA,
  TCP_M1_OPB_HALF_LAMBDA
};

enum ComponentCarrierManager_e
{
  RR_CCM,
  RR_SDL_CCM
};

enum Standard_e
{
  St80211a,
  St80211n,
  St80211nFull
};

struct PhyParams
{
  double m_bsTxGain; // dBi
  double m_bsRxGain; // dBi
  double m_bsTxPower; // dBm
  double m_bsNoiseFigure; // dB
  double m_ueTxGain; // dB
  double m_ueRxGain; // dB
  double m_ueTxPower; // dBm
  double m_ueNoiseFigure; // dB
  uint32_t m_channelNumber;
  uint32_t m_receivers;
  uint32_t m_transmitters;
};

}; // end of ns-3 namespace

#endif
