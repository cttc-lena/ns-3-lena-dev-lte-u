/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)

 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Biljana Bojovic <bbojovic@cttc.es>
 *
 * Acknowledgments: partially based on work done in collaboration with Tom Henderson <tomh@tomh.org> from University of Washington in the context of WALAA project.
 *
 */

#ifndef LTEU_COEX_MANAGER_H
#define LTEU_COEX_MANAGER_H

#include <ns3/object.h>
#include <ns3/nstime.h>
#include <ns3/callback.h>
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/dcf-manager.h"
#include "ns3/wifi-mac.h"
#include "ns3/mac-low.h"
#include "ns3/lte-enb-phy.h"
#include "ns3/lte-enb-mac.h"
#include "ns3/random-variable-stream.h"
#include "ns3/ff-mac-common.h"
#include "ns3/channel-access-manager.h"
#include <ns3/lte-enb-net-device.h>
#include <ns3/lte-anr.h>
//#include "ns3/scenario-helper.h"

namespace ns3 {

static const uint32_t MAX_CSAT_DURATION = 1280;

class SpectrumWifiPhy;
class WifiMac;
class MacLow;
class LteUPhyListener;
class LteUMacLowListener;

class LteUCoexistenceManager:public ChannelAccessManager
{
  friend class LteUPhyListener;
  friend class LteUMacLowListener;

public:

  static TypeId GetTypeId (void);
  LteUCoexistenceManager ();
  virtual ~LteUCoexistenceManager ();
  void SetEdThreshold(double edThreshold);
  void SetWifiPhy (Ptr<SpectrumWifiPhy> phy);
  void SetupPhyListener (Ptr<SpectrumWifiPhy> phy);
  void SetupLowListener (Ptr<MacLow> low);
  void SetEnergyDetectionMode(bool energyDetection);
  void SetLteEnbPhy(Ptr<LteEnbPhy> lteEnbPhy);
  void SetTOnMinInMilliSec (uint32_t tOnMinInMilliSec);
  uint32_t GetTOnMinInMilliSec ();
  void SetDutyCycle (double lteDutyCycle);
  double GetDutyCycle ();
  void SetWifiDevice ( Ptr<NetDevice> device );
  /*
     * \brief Starts csat.
     */
  void StartCsat();

  /**
   * TracedCallback signature for duty cycle CSAT trace.
   *
   * \param [in] ccId
   * \param [in] duty cycle
   * \param [in] on time in number of ms
   * \param [in] min on time in number of ms
   * \param [in] medium utilization
   */
  typedef void (* ReportCsatDutyCycleTracedCallback)
      (const uint8_t ccId, const double dutyCycle, const uint32_t tOnTime, const uint32_t tOnMin, const double mediumUtilization);

  /**
   * TracedCallback signature for active on time trace. It is reported at the beginning of every csat period and reports active on time value
   * for that csat period, if true - csat has regular on time, if false, the csat has T_ON set to 0, which means that only mandatory MIB and LDS
   * are being transmitted. Additionally, it reports the time at which current csat period ends, in order to provide the information of until
   * what time will be used the reported value of "activeOnTime" variable.
   *
   * \param [in] activeOnTimeOld is value of active on time used for this csat cycle
   * \param [in] csatEndTime is the time at which the current csat period ends
   */
  typedef void (* ReportActiveOnTimeAndCsatEndTimeTracedCallback)
      (const bool activeOnTimeOld, const Time csatEndTime);

  /*
   * \brief used to enable/disable channel access for some periods of time,
  * \param activeOnTime when set to true channel access manager default behavior will be enabled, otherwise channel access will be disabled
  */
  virtual void SetActiveOnTime (bool activeOnTime);


private:

  bool DoUpdateDutyCycle (double delta);

  void AdaptCsat ();

  /*
   * \brief Prepares next csat cycle.
   */
  void DoGetReadyForNextCsatCycle();
  /*
  * \brief Schedules transmission opportunity notification for next csat cycle.
  * \param csatPattern the bitmask to be used to generate and scheduler transmission opportunities
  * \param isApScanPeriod whether the cycle is AP scan period
  */
  void DoScheduleTxOpNotifications (std::bitset<MAX_CSAT_DURATION>& csatPattern, bool isApScanPeriod);
  /*
   * \brief Sets grant duration and notifies about grant.
   * \param grantDuration The duration of the grant.
   */
  void Notify (Time grantDuration);

  /*
   * \brief Schedules MU updates after each "monitoringTime"
   * \param durationInNumberOfSubframe duration of monitoring in number of subframes
   */
  void DoPerformMonitoring (uint32_t durationInNumberOfSubframes);

  void DoStopMonitoring ();

  void DoPerformApScan (uint32_t durationInNumberOfSubframes);

  void DoStopApScan ();

  void ResetApScanStats();

  /*
   * The function returns the maximum number of ON subframes for the next CSAT that can be assigned for the minimum OFF time and the duty cycle.
   *
   * \return the maximum number of ON subframes
   */
  uint32_t GetMaxOnTime ();

  /*
   * Using LTE-ANR functionality checks what is number of neighbours
   */
  uint32_t GetLteNodesNum ();

  /*
   * Calculates minimum ON time.
   * returns minimum ON time in number of subframes
   */
  uint32_t GetMinOnTime ();

  /*
   * \brief Send ctsToSelf before each lte transmission opportunity.
   */
  void SendCtsToSelf (Time grantDuration);
  /*
   * \brief Notifies lte-enb-phy that the grant is available.
   */
  virtual void DoRequestAccess ();

  Time GetDelayTillNextSubframeBoundary();

  void ActiveOnTimeTrace (bool activeOnTime);

  // functions used by listeners to notify about events
  void NotifyRxStart (Time duration);
  void NotifyMaybeCcaBusyStart (Time duration);
  void NavStart (Time duration);
  void BeaconDetected (Mac48Address apAddress);
  void CalculateChannelOccupancy (Time duration);
  void EnergyRxDuration (Time duration);
  void MacHeaderDuration (Time duration);
  void UpdateApCounter ();
  /*
  *   \brief Creates an initial ctrl bitmask by setting bits to 1 when mib and lds have to be transmitted.
  *   \param ctrlMask the bitmask that will be updated
  *   \param duration of csat cycle - we add this parameter since this function can be used not only for CSAT cycle, but also for AP scan period
  *   \param offset the number of OFF subframes at the beginning of CSAT cycle
  */
  void CreateInitialMibAndLdsMask (std::bitset<MAX_CSAT_DURATION>& ctrlMask, uint32_t csatCycleDuration, uint32_t offset);

  /*
   *  \brief Creates an offset mask.
   *  \param offsetMask the bitmask that will be created
   */
  void CreateOffsetMask (std::bitset<MAX_CSAT_DURATION>& offsetMask);

  /*
   * \brief Creates the puncturing mask that contains ON period that consists of bits set to 1 separated that are followed by puncturing bits that are set to 0.
   *  Maximum number of consecutive ON bits is defined by member attribute "m_puncturing" and the duration of puncturing is defined by the member attribute "m_puncturingDuration".
   * \param puncturingMask The bitmask that will be updated with generated ON period.
   * \param ctrlMask The ctrlMask that will be updated when MIB timer expires.
   */
  void GenerateOnTimeAndPuncturing (std::bitset<MAX_CSAT_DURATION>& puncturingMask, std::bitset<MAX_CSAT_DURATION>& ctrlMask);

  /*
   * \brief Performs the duty cycle sanity check which consists in checking if the assigned ON subframes in CSAT CYCLE
   * are not exceeding the allowed number of ON subframes, which is defined by "duty cycle" and "min OFF time".
   * If ON suframes occupy more than is allowed, the subframes which exceed will be discounted from the puncturing bitmask
   * by setting 1 bits to 0.
   * \param puncturingMask - mask that will be checked and modified if necessary
   * \param ctrlMask - mask that contains control bitmask
   */
  void DutyCycleSanityCheck (std::bitset<MAX_CSAT_DURATION>& puncturingMask, std::bitset<MAX_CSAT_DURATION>& ctrlMask);

  /*
   * Schedules MIB transmission opportunities in OFF period.
   * \param ctrlMask ctrl mask that contains transmission opportunities for ctrl
   */
  void MibInOffPeriodSanityCheck (std::bitset<MAX_CSAT_DURATION> & ctrlMask);

  /*
   * Checks that the number of consecutive ON bits never exceeds the limit defined by the "puncturing" parameter.
   * \param csatBitmask to be checked
   */
  void PuncturingSanityCheck (std::bitset<MAX_CSAT_DURATION>& csatBitmask);

  /**
   *  \brief Checks if the subframe at the given position corresponds to the subframe in which MIB message can be sent.
   * \param subframeIndex the index of subframe
   * \returns true if the subframe at position subframeIndex in csat cycle is subframe in which MIB message can be sent, otherwise returns false.
   */
  bool IsMibSubframe (uint32_t subframeIndex);

  /**
   * \brief Checks if the subframe at the given position corresponds to the subframe in which LDS message can be sent.
   * \param subframeIndex index of subframe
   * \returns true if the subframe at position subframeIndex in csat cycle is subframe in which LDS message will be sent, otherwise returns false.
   */
  bool IsLdsSubframe (uint32_t subframeIndex);

  /**
   * \brief Updates the ctrl bitmask by adding one transmission opportunity for the MIB.
   *  Typically used when MIB timer expires to schedule the next transmission opportunity for MIB.
   * \returns true if is scheduled transmission opportunity for MIB, otherwise returns false
   * \param subframeIndex index of subframe from which which to start to check if is MIB subframe.
   * \param ctrlMask a bitmask that will be updated by adding transmission opportunity for MIB.
   */
  bool ScheduleNextMib (uint32_t subframeIndex, std::bitset<MAX_CSAT_DURATION>& ctrlMask);

  /**
   * \brief Function will start from the given position and set the ON(1) bits in provided bitset until "puncturing" value is reached,
   *  then it will start to set OFF(0) bits until it reaches "puncturingDuration".
   * \param pos the position at which to start to update bitmask
   * \param puncturing the number of bits in bitset that will be set to ON values starting from provided position
   * \param puncturingDuration the number of bits in bitset mask that will be set to OFF and will follow ON bits
   * \param bitset the bitset that will be updated
   * \param ctrlMast to be updated
   * \returns index of subframe in csat cycle at which the puncturing has finished.
   */
  uint32_t SetPuncturing (uint32_t pos, uint32_t puncturing, uint32_t puncturingDuration, std::bitset<MAX_CSAT_DURATION>& bitset, std::bitset<MAX_CSAT_DURATION>& ctrlMask);

  /**
   * \brief Searches for the first bit set to 1 starting from the given position until offset is reached.
   * \param pos the position at which to start to check if the bit is set to 1
   * \param offset the number of bits to check
   * \param bitset the bitset in which will be searched for the bit
   * \returns 0 if ctrl bit is not found, otherwise an index of subframe in csat cycle at which the ctrl bit is found
   */
  uint32_t FindNextCtrlBit (uint32_t pos, uint32_t offset, std::bitset<MAX_CSAT_DURATION>& ctrlMask);

  /**
   * \brief Counts bits set to 1 starting from the given position until the first bit set to 0 or until is reached the end of csat cycle.
   * \param pos the position at which to start to count bits set to 1
   * \param bitset the bitset in which will be searched for the bit
   * \returns a number of consecutive bits set to 1
   */
  uint32_t CountOnBitsAfter (uint32_t pos, std::bitset<MAX_CSAT_DURATION>& bitset);

  /**
   * \brief Counts bits set to 1 starting from the given position until the first bit set to 0 or until is reached the beginning of the csat cycle.
   * \param pos the position at which to start to count bits set to 1
   * \param bitset the bitset in which will be searched for the bit
   * \returnsa a number of consecutive bits set to 1
   */
  uint32_t CountOnBitsBefore (uint32_t pos, std::bitset<MAX_CSAT_DURATION>& bitset);

  Ptr<SpectrumWifiPhy> m_wifiPhy;
  Ptr<MacLow> m_macLow;
  LteUPhyListener* m_phyListener;
  LteUMacLowListener* m_macLowListener;
  TracedCallback<double > m_rxDuration;
  TracedValue<uint32_t> m_apCount;
  TracedCallback<Time> m_navDuration;
  TracedCallback<Mac48Address> m_beaconDetected;


  /*
   *  Trace source that reports when duty cycle is updated: duty cycle value, new ON time, t_on_min, MU.
   */
  TracedCallback < uint8_t, double, uint32_t, uint32_t, double > m_dutyCycleTraceSource;

  /*
   * Trace source that reports when active on time variable is updated: active on time value, the time at which the current csat period ends.
  */
  TracedCallback< bool, Time> m_activeOnTimeExtendedTraceSource;

  bool m_energyDetectionMode;
  Ptr<LteEnbPhy> m_lteEnbPhy;
  Ptr<NetDevice>  m_device;

  // bitmask parameters
  uint32_t m_csatCycleDuration;
  /*
   * Time at which next csatCycleStart in number of subframes.
   */
  uint32_t m_csatCycleStarts;
  // maximum number of consecutive ON subframes
  uint32_t m_puncturing;
  // duration of puncturing in number of subframes
  uint32_t m_puncturingDuration;
  // defines at which subframe in csat cycle to start ON, it is allowed to have CTRL bit in offset
  uint32_t m_offset;
  // portion of ON time in CSAT cycle
  double m_dutyCycle;
  // whether to start with on period
  bool m_startWithOnPeriod;
  // maximum time that is allowed to pass between two consecutive MIBs ( in number of subframes)
  uint32_t m_maxInterMibTime;

  // csat parameters
  // used to set if the csat algorithm will be used to adapt CSAT
  bool m_csatEnabled;
  // max number in millisecond that the variable tOnMin can take: tOnMin = min( tOnMinInMilliSec, tOnMin forumula output) 
  uint32_t m_tOnMinInMilliSec;
  // minimum OFF time during CSAT cycle (in number of subframes)
  uint32_t m_minOffTime;

  uint32_t m_minOnTime;
  // allow starting csat only once
  bool m_csatHasStarted;
  //whether to send cts to self
  bool m_sendCtsToSelf;
  //whether to calculate MU based on CCA. If false it will calculate MU based on duration obtained from MAC header.
  bool m_ccaMuCalculation;
  // absolute subframe number of the last subframe at which is scheduled txop for MIB
  uint32_t m_lastMibTxOpSubframe;
  // absolute subframe number of the last subframe at which is scheduled txop for LDS
  uint32_t m_lastLdsTxOpSubframe;
  // the interval of AP scan in number of CSAT cycles
  uint32_t m_apScanInterval;
  // the duration of AP scan in number of subframes
  uint32_t m_apScanDuration;
  // whether is in AP scan
  bool m_isInApScan;
  // whether is enabled puncturing
  bool m_puncturingEnabled;

  // csat algorithm parameters
  double m_alphaMu;
  double m_thresholdMuLow;
  double m_thresholdMuHigh;
  double m_deltaTUp;
  double m_deltaTDown;
  //defines single monitoring time
  uint32_t m_monitoringTime;
  //variable used to distinguish monitoring mode from transmit mode
  bool m_isInMonitoring;
  // counts current monitoring duraiton in number of subframes
  uint32_t m_monitoringDuration;
  // channel occupancy
  Time m_channelOccupancy;
  // last MU
  double m_lastMediumUtilization;
  // set of MAC addresses of detected APs
  std::set< Mac48Address > m_wifiAps;

  // Time between transmission opportunity notification and its usage at LTE MAC, necessary because of mac-to-phy delay.
  Time m_csatTimeInAdvance;
  // to know when to trigger AP scan
  uint32_t m_apScanTimer;

};

} // namespace ns3

#endif /* LTEUCOEXISTENCEMANAGER_H_ */
