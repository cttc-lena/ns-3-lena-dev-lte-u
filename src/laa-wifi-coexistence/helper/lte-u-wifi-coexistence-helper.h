/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es>
 *
 * Acknowledgments: partially based on work done in collaboration with Tom Henderson <tomh@tomh.org> from University of Washington in the context of WALAA project.
 *
 */

#ifndef LTE_U_WIFI_COEXISTENCE_HELPER_H
#define LTE_U_WIFI_COEXISTENCE_HELPER_H

#include <ns3/object.h>
#include <ns3/config.h>
#include <ns3/simulator.h>
#include <ns3/names.h>
#include <ns3/net-device.h>
#include <ns3/net-device-container.h>
#include <ns3/node.h>
#include <ns3/node-container.h>
#include <ns3/lte-u-coexistence-manager.h>
#include <ns3/vector.h>
#include <ns3/coexistence-common.h>

namespace ns3 {

class LteUCoexistenceManager;

/*
 * This class provides functionality for creating and configuring LTE-U nodes.
 */
class LteUWifiCoexistenceHelper : public Object
{

public:

  /*
   * Defines the monitor mode of wifi device at LTE-U:
   * - if set to ENERGY_DETECTION_ONLY, all signals will be treated in the same way, wifi and non-wifi signals,
   * this means that packet reception will be disabled, so no preamble detection will be performed.
   * - if set to REGULDAR_WIFI_MONITOR, for non-wifi signals will be performed simple energy detection, while for
   * wifi signals will be performed preamble and beacon detection
   *
   */
  enum MonitorMode
  {
    ENERGY_DETECTION_ONLY,
    REGULAR_WIFI_MONITOR
  };

  /**
   * Constructor
   */
  LteUWifiCoexistenceHelper ();

  /**
   * Destructor
   */
  ~LteUWifiCoexistenceHelper ();

  /**
   *  Register this type.
   *  \return The object TypeId.
   */
  static TypeId GetTypeId (void);

  virtual void DoDispose ();

  /**
   *  Configure LTE to be LTE-U. LTE will operate only in unlicensed carrier. This function will install a wifi device at LTE eNb to monitor unlicensed channel. Set wifi listeners.
   * \param enbDevices The enbDevices to which will be attached wifi devices.
   * \param phyParams PhyParams to use in configuration
   */
  void ConfigureLteUSingleWifiChannel (NetDeviceContainer enbDevices, struct PhyParams phyParams);
  //void ConfigureLteUSingleWifiChannel (NetDeviceContainer enbDevices, struct PhyParams phyParams, bool assignDutyCycle=false);

  /**
   *  Configures LTE-U on a specified carrier. Installs a wifi device at LTE eNb to monitor at the specified carrier. Set wifi listeners.
   * \param enbDevices The enbDevices to which will be attached wifi devices.
   * \param phyParams PhyParams to use in configuration
   */
  void ConfigureLteUCarrierOnWifiChannel (NetDeviceContainer enbDevices, struct PhyParams phyParams, uint8_t carrierIndex);

  /*
   *  lte-u node to use several wifi channels
   */
  void ConfigureLteUMultiWifiChannel (NetDeviceContainer enbDevices, std::vector< struct PhyParams > phyParams);

  void SetAssignDutyCycle (bool assignDutyCycle);

  void SetAssignTOnMinInMilliSec (bool assignTOnMinInMilliSec);
  
  void SetMonitorMode (MonitorMode monitorMode);

  void SetEdThreshold (double edThreshold);

  void SetSilent ();

private:

  MonitorMode m_monitorMode;
  double m_edThreshold;
  double m_isSilent;
  bool m_assignDutyCycle;
  bool m_assignTOnMinInMilliSec;


};

}

#endif // LTE_U_WIFI_COEXISTENCE_HELPER_H









