/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Biljana Bojovic <bbojovic@cttc.es> and Tom Henderson <tomh@tomh.org>
 *
 * Acknowledgments: partially based on work done in collaboration with Tom Henderson <tomh@tomh.org> from University of Washington in the context of WALAA project.
 *
 */

#include <ns3/lte-u-wifi-coexistence-helper.h>
#include <ns3/log.h>
#include <ns3/lte-enb-net-device.h>
#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/internet-module.h>
#include <ns3/point-to-point-module.h>
#include <ns3/lte-module.h>
#include <ns3/wifi-module.h>
#include <ns3/spectrum-module.h>
#include <ns3/applications-module.h>
#include <ns3/internet-module.h>
#include <ns3/propagation-module.h>
#include <ns3/config-store-module.h>
#include <ns3/flow-monitor-module.h>


namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("LteUWifiCoexistenceHelper");

NS_OBJECT_ENSURE_REGISTERED (LteUWifiCoexistenceHelper);

LteUWifiCoexistenceHelper::LteUWifiCoexistenceHelper ():m_monitorMode (REGULAR_WIFI_MONITOR)
{
  NS_LOG_FUNCTION (this);
  m_isSilent = false;
  m_assignDutyCycle = false;
}

LteUWifiCoexistenceHelper::~LteUWifiCoexistenceHelper ()
{
  NS_LOG_FUNCTION (this);
}

TypeId 
LteUWifiCoexistenceHelper::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::LteUWifiCoexistenceHelper")
    .SetParent<Object> ()
    .SetGroupName ("laa-wifi-coexistence")
    .AddAttribute("MonitorMode",
                   "Energy detection only or regular wi-fi monitoring",
                   EnumValue (LteUWifiCoexistenceHelper::REGULAR_WIFI_MONITOR),
                   MakeEnumAccessor (&LteUWifiCoexistenceHelper::m_monitorMode),
                   MakeEnumChecker (REGULAR_WIFI_MONITOR, "regular_wifi_monitor",
                                    ENERGY_DETECTION_ONLY, "energy_detection_only"))
   .AddAttribute("EdThreshold",
                 "Energy detection threshold used when ENERGY_DETECTION_ONLY mode is being used.",
                  DoubleValue (-72.0),
                  MakeDoubleAccessor(&LteUWifiCoexistenceHelper::m_edThreshold),
                  MakeDoubleChecker<double>())
   /*
   .AddAttribute("assignDutyCycle",
                 "Assign duty cycle manually. Only works when csat is disabled. Otherwise it is only changing initial duty cycle",
                  BooleanValue (false),
                  MakeBooleanAccessor(&LteUWifiCoexistenceHelper::m_assignDutyCycle),
                  MakeBooleanChecker<bool>())
                  */
   .AddConstructor<LteUWifiCoexistenceHelper>();
  return tid;

}

void 
LteUWifiCoexistenceHelper::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  Object::DoDispose ();
}

void
LteUWifiCoexistenceHelper::ConfigureLteUSingleWifiChannel (NetDeviceContainer enbDevices, struct PhyParams phyParams)
{
  ConfigureLteUCarrierOnWifiChannel (enbDevices, phyParams, 0);
}

void 
LteUWifiCoexistenceHelper::ConfigureLteUCarrierOnWifiChannel (NetDeviceContainer enbDevices, struct PhyParams phyParams, uint8_t carrierIndex)
{

  NS_LOG_FUNCTION (this);
  NS_ASSERT_MSG (enbDevices.GetN () > 0, "empty enb device container");
  int counter = 0;
  
  for (NetDeviceContainer::Iterator i = enbDevices.Begin (); i != enbDevices.End (); ++i)
    {
      Ptr<Node> node = (*i)->GetNode ();
      // we need a spectrum channel in order to install wifi device on the same instance of spectrum channel
      Ptr<LteEnbNetDevice> lteEnbNetDevice = (*i)->GetObject<LteEnbNetDevice> ();
      uint8_t numberOfComponentCarriers = lteEnbNetDevice->GetCcMap().size();
      NS_ABORT_MSG_UNLESS (carrierIndex < numberOfComponentCarriers, "Attempt to configure LTE-U on a component carrier that does not exist.");
      Ptr<SpectrumChannel> downlinkSpectrumChannel = lteEnbNetDevice->GetPhy (carrierIndex)->GetDownlinkSpectrumPhy ()->GetChannel ();
      SpectrumWifiPhyHelper spectrumPhyHelper = SpectrumWifiPhyHelper::Default ();
      spectrumPhyHelper.SetChannel (downlinkSpectrumChannel);
      WifiHelper wifi;
      // this setting will be used during WifiHeler::Install to set frequency of SpectrumWifiPhy
      wifi.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
      HtWifiMacHelper mac = HtWifiMacHelper::Default ();
      wifi.SetRemoteStationManager ("ns3::IdealWifiManager");
      //which implements a Wi-Fi MAC that does not perform any kind of beacon generation, probing, or association
      mac.SetType ("ns3::AdhocWifiMac");
      // parameters that will be used to created wifi instance of SpectrumWifiPhy
      spectrumPhyHelper.SetChannelNumber (phyParams.m_channelNumber);
      spectrumPhyHelper.Set ("ShortGuardEnabled", BooleanValue (false));
      spectrumPhyHelper.Set ("ChannelWidth", UintegerValue (20));
      spectrumPhyHelper.Set ("TxGain", DoubleValue (phyParams.m_ueTxGain));
      spectrumPhyHelper.Set ("RxGain", DoubleValue (phyParams.m_ueRxGain));
      spectrumPhyHelper.Set ("TxPowerStart", DoubleValue (phyParams.m_ueTxPower));
      spectrumPhyHelper.Set ("TxPowerEnd", DoubleValue (phyParams.m_ueTxPower));
      spectrumPhyHelper.Set ("RxNoiseFigure", DoubleValue (phyParams.m_ueNoiseFigure));
      spectrumPhyHelper.Set ("Receivers", UintegerValue (phyParams.m_receivers));
      spectrumPhyHelper.Set ("Transmitters", UintegerValue (phyParams.m_transmitters));
      // setup wifi device that is doing monitoring, create SpectrumWifiPhy instance for wifi
      Ptr<NetDevice> monitor = (wifi.Install (spectrumPhyHelper, mac, node)).Get (0);
      Ptr<WifiPhy> wifiPhy = monitor->GetObject<WifiNetDevice> ()->GetPhy ();
      Ptr<SpectrumWifiPhy> spectrumWifiPhy = DynamicCast<SpectrumWifiPhy> (wifiPhy);
      Ptr<LteEnbPhy> ltePhy = (*i)->GetObject<LteEnbNetDevice> ()->GetPhy (carrierIndex);
      Ptr<WifiMac> wifiMac = monitor->GetObject<WifiNetDevice>()->GetMac();
      Ptr<RegularWifiMac> regularWifiMac = DynamicCast<RegularWifiMac> (wifiMac);
      Ptr<MacLow> macLow = regularWifiMac->GetMacLow();

      ltePhy->SetCamType (LteEnbPhy::LTE_U);

      Ptr<LteUCoexistenceManager> lteUCoexistenceManager = CreateObject<LteUCoexistenceManager> ();

      if (m_monitorMode == ENERGY_DETECTION_ONLY)
        {
          lteUCoexistenceManager->SetEnergyDetectionMode(true);
          lteUCoexistenceManager->SetEdThreshold(m_edThreshold);
          lteUCoexistenceManager->SetWifiPhy (spectrumWifiPhy);
          lteUCoexistenceManager->SetLteEnbPhy(ltePhy);
          lteUCoexistenceManager->SetWifiDevice(monitor);
          if (!m_isSilent)
            lteUCoexistenceManager->StartCsat();

        }
      else if (m_monitorMode == REGULAR_WIFI_MONITOR)
        {
          lteUCoexistenceManager->SetEnergyDetectionMode(false);
          lteUCoexistenceManager->SetWifiPhy (spectrumWifiPhy);
          lteUCoexistenceManager->SetupLowListener(macLow);
          lteUCoexistenceManager->SetLteEnbPhy(ltePhy);
          lteUCoexistenceManager->SetWifiDevice(monitor);
          lteUCoexistenceManager->StartCsat();
        }
      else
        {
          NS_FATAL_ERROR("Unknown monitor mode");
        }

      ltePhy->SetChannelAccessManager (lteUCoexistenceManager);

      // Temp workaround: To assign tOnMinInMilliSec separately so that
      // tOnMin update does not affect in the hidden node simulation
      if (m_assignTOnMinInMilliSec)
        {
          UintegerValue uintegerValue;
          BooleanValue booleanValue;
          GlobalValue::GetValueByName ("tOnMinInMilliSecMid", uintegerValue);
          uint32_t tOnMinInMilliSecMid = uintegerValue.Get();
          GlobalValue::GetValueByName ("tOnMinInMilliSecRight", uintegerValue);
          uint32_t tOnMinInMilliSecRight = uintegerValue.Get();
          if( counter % 2 == 0)
            {
              DynamicCast<LteUCoexistenceManager>(ltePhy->GetChannelAccessManager ())->SetTOnMinInMilliSec(tOnMinInMilliSecMid);
              NS_LOG_DEBUG("assign LTE-U node " << counter
                           << " with tOnMinInMilliSec "
                           << DynamicCast<LteUCoexistenceManager>(ltePhy->GetChannelAccessManager ())->GetTOnMinInMilliSec()
                           << std::endl);
            }
          else
            {
              DynamicCast<LteUCoexistenceManager>(ltePhy->GetChannelAccessManager ())->SetTOnMinInMilliSec(tOnMinInMilliSecRight);
              NS_LOG_DEBUG ("assign LTE-U node " << counter
                            << " with tOnMinInMilliSec "
                            << DynamicCast<LteUCoexistenceManager>(ltePhy->GetChannelAccessManager ())->GetTOnMinInMilliSec()
                            << std::endl);
            }

        }

      //TYC: a quick workaround. When CSAT disable, set the default duty cycle in alternating order
      if (m_assignDutyCycle)
        {
          DoubleValue doubleValue;
          BooleanValue booleanValue;
          GlobalValue::GetValueByName ("csatDutyCycleMid", doubleValue);
          double csatDutyCycleMid = doubleValue.Get();
          GlobalValue::GetValueByName ("csatDutyCycleRight", doubleValue);
          double csatDutyCycleRight = doubleValue.Get();
          GlobalValue::GetValueByName ("csatEnabled", booleanValue);
          bool csatEnabled= booleanValue.Get();
          if (!csatEnabled)
            {
              if( counter % 2 == 0)
                {
                  DynamicCast<LteUCoexistenceManager>(ltePhy->GetChannelAccessManager ())->SetDutyCycle (csatDutyCycleMid);
                  NS_LOG_DEBUG ("Assigned LTE-U node " << counter
                                << " with duty cycle "
                                << DynamicCast<LteUCoexistenceManager>(ltePhy->GetChannelAccessManager ())->GetDutyCycle ()
                                << std::endl);
                }
              else
                {
                  DynamicCast<LteUCoexistenceManager>(ltePhy->GetChannelAccessManager ())->SetDutyCycle (csatDutyCycleRight);
                  NS_LOG_DEBUG ("Assigned LTE-U node " << counter
                                << " with duty cycle "
                                << DynamicCast<LteUCoexistenceManager>(ltePhy->GetChannelAccessManager ())->GetDutyCycle ()
                                << std::endl);
                }
            }
        }
      counter ++;
    }
}

void LteUWifiCoexistenceHelper::SetAssignTOnMinInMilliSec (bool assignTOnMinInMilliSec)
{
  m_assignTOnMinInMilliSec = assignTOnMinInMilliSec;
}

void LteUWifiCoexistenceHelper::SetAssignDutyCycle (bool assignDutyCycle)
{
  m_assignDutyCycle = assignDutyCycle;
}

void LteUWifiCoexistenceHelper::SetMonitorMode (MonitorMode monitorMode)
{
  m_monitorMode = monitorMode;
}

void LteUWifiCoexistenceHelper::SetEdThreshold (double edThreshold)
{
  m_edThreshold = edThreshold;
}

void LteUWifiCoexistenceHelper::SetSilent ()
{
  m_isSilent = true;
}

void
LteUWifiCoexistenceHelper::ConfigureLteUMultiWifiChannel (NetDeviceContainer enbDevices, std::vector< struct PhyParams > phyParamsList)
{
  NS_LOG_FUNCTION(this);
  NS_ASSERT_MSG (enbDevices.GetN () > 0, "empty enb device container");
  NS_ASSERT_MSG (phyParamsList.size() > 0, "empty phy params list");

  uint8_t carrierIndex = 1;
  for (std::vector< struct PhyParams >::iterator it = phyParamsList.begin(); it != phyParamsList.end(); it++)
    {
       struct PhyParams phyParams = *it;
       NS_LOG_INFO ("Configuring LTE-U secondary carrier "<<(uint32_t)carrierIndex<<" on channel :"<<phyParams.m_channelNumber);
       std::cout<<"\n Configuring LTE-U secondary carrier "<<(uint32_t)carrierIndex <<" on channel :"<<phyParams.m_channelNumber<<std::endl;
       ConfigureLteUCarrierOnWifiChannel (enbDevices, phyParams, carrierIndex);
       carrierIndex ++;
    }

}

} // namespace ns3
