/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2015 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 * Copyright (c) 2015 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Nicola Baldo <nbaldo@cttc.es> and Tom Henderson <tomh@tomh.org>
 *          Biljana Bojovic <bbojovic@cttc.es>
 */

#ifndef LAA_SCENARIO_HELPER_H
#define LAA_SCENARIO_HELPER_H

#include <ns3/core-module.h>
#include <ns3/lte-module.h>
#include <ns3/wifi-module.h>
#include <ns3/network-module.h>
#include <ns3/spectrum-module.h>
#include <ns3/flow-monitor-module.h>
#include <ns3/lte-u-wifi-coexistence-helper.h>
#include <ns3/coexistence-common.h>


using namespace ns3;

void
PrintFlowMonitorStats (Ptr<FlowMonitor> monitor, FlowMonitorHelper& flowmonHelper, double duration);

void
ConfigureLte (Config_e cellConfig, Ptr<LteHelper> lteHelper, Ptr<PointToPointEpcHelper> epcHelper,
              Ipv4AddressHelper& internetIpv4Helper, NodeContainer& bsNodes, NodeContainer ueNodes, NodeContainer clientNodes,
              NetDeviceContainer& bsDevices, NetDeviceContainer& ueDevices,
              struct PhyParams phyParams, std::vector<LteSpectrumValueCatcher>& lteDlSinrCatcherVector,
              std::bitset<40> absPattern, Transport_e transport);

void
ConfigureLte (Ptr<LteHelper> lteHelper, Ptr<PointToPointEpcHelper> epcHelper, Ipv4AddressHelper& internetIpv4Helper, NodeContainer bsNodes, NodeContainer ueNodes, NodeContainer clientNodes, NetDeviceContainer& bsDevices, NetDeviceContainer& ueDevices, struct PhyParams phyParams, std::vector<LteSpectrumValueCatcher>& lteDlSinrCatcherVector, std::bitset<40> absPattern, Transport_e transport);

void
ConfigureLteU (Ptr<LteUWifiCoexistenceHelper> lteUWifiCoexistenceHelper, NetDeviceContainer& lteEnbDevs, struct PhyParams phyParams);

NetDeviceContainer
ConfigureWifiAp (NodeContainer bsNodes, struct PhyParams phyParams, Ptr<SpectrumChannel> channel, Ssid ssid);

NetDeviceContainer
ConfigureWifiSta (NodeContainer ueNodes, struct PhyParams phyParams, Ptr<SpectrumChannel> channel, Ssid ssid);

void
ConfigureMonitor (NodeContainer bsNodesA, struct PhyParams phyParams, Ptr<SpectrumChannel> channel);

ApplicationContainer
ConfigureUdpServers (NodeContainer servers, Time startTime, Time stopTime);

ApplicationContainer
ConfigureUdpClients (NodeContainer client, Ipv4InterfaceContainer servers, Time startTime, Time stopTime, Time interval);

ApplicationContainer
ConfigureTcpServers (NodeContainer servers, Time startTime);

ApplicationContainer
ConfigureTcpClients (NodeContainer client, Ipv4InterfaceContainer servers, Time startTime);

ApplicationContainer
ConfigureFtpClients (NodeContainer client, Ipv4InterfaceContainer servers, Time startTime);

ApplicationContainer ConfigureFtpServers (NodeContainer servers, Time startTime);

void
StartFileTransfer (Ptr<ExponentialRandomVariable> ftpArrivals, ApplicationContainer clients, uint32_t nextClient, Time stopTime);

void
ConfigureAndRunScenario (Config_e cellConfigA,
                         Config_e cellConfigB,
                         NodeContainer bsNodesA,
                         NodeContainer bsNodesB,
                         NodeContainer ueNodesA,
                         NodeContainer ueNodesB,
                         struct PhyParams phyParams,
                         Time duration,
                         Transport_e transport,
                         std::string propagationLossModel,
                         bool disableApps,
                         double lteDutyCycle,
                         bool generateRem,
                         std::string outFileName,
                         std::string simulationParams,
                         bool saveResults = true);

void
StartFileTransferCbr (Ptr<ConstantRandomVariable> ftpArrivals,
                   ApplicationContainer clients, uint32_t nextClient,
                   Time stopTime);

void
StartFileTransfer (Ptr<ExponentialRandomVariable> ftpArrivals, 
                   ApplicationContainer clients, uint32_t nextClient, 
                   Time stopTime);


void
SingleFileTransfer (ApplicationContainer clients,
                    uint32_t nextClient, Time nextTime);

std::string
CellConfigToString (enum Config_e config);


void
SaveUdpFlowMonitorStats (std::string filename, std::string simulationParams, 
                         Ptr<FlowMonitor> monitor, 
                         FlowMonitorHelper& flowmonHelper, double duration);

void
SaveTcpFlowMonitorStats (std::string filename, std::string simulationParams, 
                         Ptr<FlowMonitor> monitor, 
                         FlowMonitorHelper& flowmonHelper, double duration);

void
PrintFlowMonitorStats (Ptr<FlowMonitor> monitor, 
                       FlowMonitorHelper& flowmonHelper, double duration);

void
SaveValidation (std::string filename, Ptr<FlowMonitor> monitorA, Ptr<FlowMonitor> monitorB);

// TYC, enable logging in a function call
void 
SetCsatDutyCycleLog (Time startTime, Time stopTime,
                     NetDeviceContainer &bsDevices);

void 
SetCsatDutyCycleStats (std::string fileName);
#endif

